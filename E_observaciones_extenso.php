<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';
require ('libreria/fpdf.php');
include ('Conexion.php');
$id_ponencia=$_POST["id_ponencia"];
$estado=$_POST["gender"];
$comentarios=$_POST["comentarios"];
///////////////////////////////////////////Se consulta el congreso Actual///////////////////////////////////////////
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
$row5=pg_fetch_row($consulta);
$consulta0=$row5[0];
$consulta1=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$consulta0'");
$row6=pg_fetch_row($consulta1);
$numeroCongreso=$row6[0];// se obtiene en id del congreso actual
///////////////////////////////////////////Se valida  si existen actualizacion /////////////////////////////////////
$consultaValidacion="SELECT * FROM actualizacion_p_oral WHERE id_ponencia_oral='$id_ponencia'";
$result=pg_query($conexion,$consultaValidacion);
$row=pg_fetch_row($result);
$validacion=@$row[0];


if(!empty($validacion)){
    ///////////////////////////////////////Si existe Actualizacion se guardan los cambios en dicha actualizacion////////////////////////////////////////////
$consulta1=pg_query($conexion,"SELECT MAX(numeracion) FROM actualizacion_p_oral where id_congreso='$numeroCongreso' and id_ponencia_oral ='$id_ponencia' ");
  $row7=pg_fetch_row($consulta1);
  $numeroid=$row7[0];
  ///////////////////////////////////////// Se inserta el comentario y el nuevo estatus de la ponencia//////////////////////////////////////////////////////
  $actualizacion="UPDATE actualizacion_p_oral SET observaciones ='$comentarios' WHERE numeracion='$numeroid' and id_ponencia_oral='$id_ponencia'";
  $actualizacion2="UPDATE actualizacion_p_oral SET estatus_actualizacion ='$estado' WHERE numeracion='$numeroid'and id_ponencia_oral='$id_ponencia'";
  pg_query($conexion,$actualizacion);
  pg_query($conexion,$actualizacion2);
  ///////////////////////////////////////// Se consulta la actualizacion del resumen aceptado///////////////////////////////////////////////////////////////
  $consultaR=pg_query($conexion,"SELECT MAX(numeracion) FROM actualizacion_resumen  where id_congreso='$numeroCongreso' and id_ponencia ='$id_ponencia' ");
    $rowR=pg_fetch_row($consultaR);
    $numeroR=$rowR[0];
  $consultaponencia=pg_query($conexion,"SELECT ar.titulo, ar.resumen, ar.referencias FROM actualizacion_resumen ar WHERE ar.id_ponencia= '$id_ponencia' and numeracion='$numeroR' and id_congreso='$numeroCongreso'");
  $datos=pg_fetch_row($consultaponencia);
  $Titulo=trim($datos[0]);
  $Resumen=trim($datos[1]);
  $Referencia=trim($datos[2]);
  

}else{
    ////////////////////////////////////// Si no existe Actualizacion se cambian los datos de la ponencia en la tabla ponencia_oral////////////////////////////
  $actualizacion="UPDATE ponencia_oral SET observaciones ='$comentarios' WHERE id_ponencia_oral='$id_ponencia'";
  $actualizacion2="UPDATE ponencia_oral SET estatus_extenso ='$estado' WHERE id_ponencia_oral='$id_ponencia'";
  pg_query($conexion,$actualizacion);
  pg_query($conexion,$actualizacion2); 
   ///////////////////////////////////////// Se consulta la actualizacion del resumen aceptado///////////////////////////////////////////////////////////////
 ECHO $id_ponencia."<br>".$numeroCongreso;
  $consultaponencia=pg_query($conexion,"SELECT p.titulo, p.resumen, p.referencia FROM ponencias p WHERE p.id_ponencia= '$id_ponencia' and id_congreso='$numeroCongreso'");
  $datos1=pg_fetch_row($consultaponencia);
  $Titulo=trim($datos1[0]);
  $Resumen=trim($datos1[1]);
  $Referencia=trim($datos1[2]); 
}
///////////////////////////////////////////////////////////////////////////////ENVIO CORREO DE PONENCIA ORAL CALIFICADA//////////////////////////////////////////////////
$infoAutor=pg_query($conexion,"SELECT u.nombres,u.primer_ap,u.segundo_ap FROM  usuario u, usuario_ponencias up WHERE up.id_ponencias ='$id_ponencia' and up.id_usuario=u.id_usuario  and up.tipo_autor='Autor';  ");
$row8=pg_fetch_row($infoAutor);
$nombre=trim($row8[0]);
$apPA=trim($row8[1]);
$apMa=trim($row8[2]);
$correoa=pg_query($conexion,"SELECT c.correo FROM correos_usuario c,usuario u, usuario_ponencias up WHERE up.id_ponencias='$id_ponencia' and up.id_usuario=u.id_usuario and u.id_usuario=c.id_usuario and up.tipo_autor='Autor'");
$i=0;
while($mostrarCR=pg_fetch_array($correoa)){
  $cor[$i]=trim($mostrarCR['correo']);
  $i=$i+1;
}
$infoCoau=pg_query($conexion,"SELECT u.nombres,u.primer_ap ,u.segundo_ap,c.correo FROM correos_usuario c, usuario u, usuario_ponencias up WHERE up.id_ponencias='$id_ponencia'and up.id_usuario=u.id_usuario and u.id_usuario= c.id_usuario and up.tipo_autor!='Autor';");
$j=0;


// Creacion de PDF Autor
class PDF extends FPDF
                  {
                  // Cabecera de página
                  function Header()
                  {
                      // Logo
                      $this->Image('logo.jpg',0,0,220);
                      // Arial bold 15
                      $this->SetFont('Arial','B',15);
                      // Movernos a la derecha
                      $this->Cell(80);
                      // Título
                      $this->Cell(50,80,utf8_decode('Congreso Matematicas'),30,0,'C');
                      // Salto de línea
                      $this->Ln(50);
                      
                  }
                  
                  // Pie de página
                  function Footer()
                  {
                      // Posición: a 1,5 cm del final
                      $this->SetY(-15);
                      // Arial italic 8
                      $this->SetFont('Arial','I',8);
                      // Número de página
                      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
                  }
                  }
                  
                  // Creación del objeto de la clase heredada
                  $pdf = new PDF();
                  $pdf->AliasNbPages();
                  $pdf->AddPage();
                  $pdf->SetFont('Times','',12);
                  
                  $pdf->Cell(40,10,utf8_decode('Registro de Ponencia Oral'),0,1);
                  $pdf->Cell(40,10,utf8_decode(''),0,1);
                  $pdf->Cell(40,10,utf8_decode($nombre.' '.$apPA.' '.$apMa.' se ha evaluado el siguiente resumen para ponencia oral'),0,1);
                  $pdf->Cell(40,10,utf8_decode('Estado de la ponencia: '.$estado),0,1);
                  $pdf->Cell(40,10,utf8_decode('Clave de la ponencia: '.$id_ponencia),0,1);
                  $pdf->Multicell(190,5,utf8_decode('Título de la ponencia: '.$Titulo),0,1);
                  $pdf->Multicell(190,5,utf8_decode('Resumen del trabajo: '.$Resumen),0,1);
                  $pdf->Multicell(190,5,utf8_decode('Referencias: '.$Referencia),0,1);
                  $pdf->Multicell(190,5,utf8_decode('Comentarios: '.$comentarios),0,1);
                  $pdf->Cell(40,10,utf8_decode('Coautores registrados en el trabajo:'),0,1);
                  while($infoCoa=pg_fetch_array($infoCoau)){
                      if(empty($infoCoa)){
                          $pdf->Cell(40,10,utf8_decode('No hay coautores registrados'),0,1); 
                      }
                      $nombreCoa[$j]=trim($infoCoa['nombres']);
                      $apellidoCoa[$j]=trim($infoCoa['primer_ap']);
                      $apellidoCoa2[$j]=trim($infoCoa['segundo_ap']);
                      $corA[$j]=trim($infoCoa['correo']);
                  $pdf->Cell(40,10,utf8_decode($nombreCoa[$j]." ".$apellidoCoa[$j]." ".$apellidoCoa2[$j]." (".$corA[$j].")"),0,1);
                      $j=$j+1;
                     }
                  $pdf->Cell(40,10,utf8_decode('Si desea agregar o modificar los coautores de este trabajo, lo podrá realizar'),0,1);
                  $pdf->Cell(40,10,utf8_decode('cuando edite su resumen en el apartado "trabajos registrados".'),0,1);
                  $pdf->Cell(40,10,utf8_decode('Le pedimos de la manera más atenta, estar al pendiente de su cuenta para conocer'),0,1);
                  $pdf->Cell(40,10,utf8_decode('el estatus de su trabajo.'),0,1);
                  $pdf->Cell(40,10,utf8_decode(''),0,1);
                  
                  $archivoAdjunto = $pdf->Output("", "S");

//Envio de correo Autor
$mail = new PHPMailer(true);
                  
                  try {
                      //Server settings
                      $mail->SMTPDebug = 0;                      // Enable verbose debug output
                      $mail->isSMTP();                                            // Send using SMTP
                      $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                      $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                      $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
                      $mail->Password   = 'CongresoMate2020';                               // SMTP password
                      $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                      $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                    
                      
                      //Recipients
                      $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
                      $mail->addAddress("$cor[0]", "$nombre $apPA");
                      if(!empty($cor[1])){
                          $mail->addAddress("$cor[1]", "$nombre $apPA");
                      }
                      if(!empty($cor[2])){
                          $mail->addAddress("$cor[2]", "$nombre $apPA");
                      }
                                      
                      // Content
                      $mail->isHTML(true);                                  // Set email format to HTML
                      $mail->Subject = 'Trabajo evaluado';
                      $mail->Body    = 'En el siguiente documento se adjuntan los datos de la ponencia oral evaluada';
                      $mail->addStringAttachment($archivoAdjunto, 'Evaluacion_Resumen_Ponencia_Oral.pdf');
                      $mail->send();
                      
                  } catch (Exception $e) {
                      echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
                  }



// Creacion de PDF Coautores 
if(!empty($corA[0]) || !empty($corA[1])|| !empty($corA[2])||!empty($corA[3])){
class PDF2 extends FPDF
                  {
                  // Cabecera de página
                  function Header()
                  {
                      // Logo
                      $this->Image('logo.jpg',0,0,220);
                      // Arial bold 15
                      $this->SetFont('Arial','B',15);
                      // Movernos a la derecha
                      $this->Cell(80);
                      // Título
                      $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
                      // Salto de línea
                      $this->Ln(50);
                      
                  }
                  
                  // Pie de página
                  function Footer()
                  {
                      // Posición: a 1,5 cm del final
                      $this->SetY(-15);
                      // Arial italic 8
                      $this->SetFont('Arial','I',8);
                      // Número de página
                      $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
                  }
                  }
                  
                  // Creación del objeto de la clase heredada
                  $pdf = new PDF();
                  $pdf->AliasNbPages();
                  $pdf->AddPage();
                  $pdf->SetFont('Times','',12);
                  
                  $pdf->Cell(40,10,utf8_decode('Evaluacion de resumen Ponencia Oral'),0,1);
                  $pdf->Cell(40,10,utf8_decode(''),0,1);
                  $pdf->Cell(40,10,utf8_decode('El siguiente trabajo ya fue evaluado:'),0,1);
                  $pdf->Cell(40,10,utf8_decode('Estado de la ponencia: '.$estado),0,1);
                  $pdf->Cell(40,10,utf8_decode('Clave de la ponencia: '.$id_ponencia),0,1);
                  $pdf->Multicell(190,5,utf8_decode('Título de la ponencia: '.$Titulo),0,1);
                  $pdf->Multicell(190,5,utf8_decode('Resumen del trabajo: '.$Resumen),0,1);
                  $pdf->Multicell(190,5,utf8_decode('Referencias: '.$Referencia),0,1);
                  $pdf->Multicell(190,5,utf8_decode('Comentarios: '.$comentarios),0,1);
                  
                  $pdf->Cell(40,10,utf8_decode(''),0,1);
                  $archivoAdjunto2 = $pdf->Output("", "S");

//Envio de correo coautores
$mail = new PHPMailer(true);
                  
                  try {
                      //Server settings
                      $mail->SMTPDebug = 0;                      // Enable verbose debug output
                      $mail->isSMTP();                                            // Send using SMTP
                      $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                      $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                      $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
                      $mail->Password   = 'CongresoMate2020';                               // SMTP password
                      $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                      $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                  
                      //Recipients
                      $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
                      while($infoCoa=pg_fetch_array($infoCoau)){
                         
                          $nombreCoa[$j]=trim($infoCoa['nombres']);
                          $apellidoCoa[$j]=trim($infoCoa['primer_ap']);
                          $apellidoCoa2[$j]=trim($infoCoa['segundo_ap']);
                          $corC[$j]=trim($infoCoa['correo']);
                         
                      
                          $j=$j+1;
                         }
                         if(!empty($corA[0])){
                          $mail->addAddress("$corA[0]",utf8_decode("$nombreCoa[0] $apellidoCoa[0] $apellidoCoa2[0]"));

                         }
                         if(!empty($corA[1])){
                          $mail->addAddress("$corA[1]",utf8_decode("$nombreCoa[1] $apellidoCoa[1] $apellidoCoa2[1]"));

                         }
                         if(!empty($corA[2])){
                          $mail->addAddress("$corA[2]",utf8_decode("$nombreCoa[2] $apellidoCoa[2] $apellidoCoa2[2]"));

                         }
                         if(!empty($corA[3])){
                          $mail->addAddress("$corA[3]",utf8_decode("$nombreCoa[3] $apellidoCoa[3] $apellidoCoa2[3]"));

                         }
                  
                      // Attachments
                      //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                      //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
                  
                      // Content
                      $mail->isHTML(true);                                  // Set email format to HTML
                      $mail->Subject = 'Trabajo evaluado ';
                      $mail->Body    = 'En el siguiente documento se adjuntan los datos de la ponencia oral Evaluada';
                      $mail->addStringAttachment($archivoAdjunto2, 'Evaluacion_Resumen_Ponencia_Oral.pdf');
                      $mail->send();
                      
                  } catch (Exception $e) {
                      echo "Error al enviar el mensaje a los coautores: {$mail->ErrorInfo}";
                  }
              }
              header("location:evaluartr.php");

?>