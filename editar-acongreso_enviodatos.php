<?php

session_start();
include ('Conexion.php');
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
$usuario=$_SESSION['Usuario'];

//Datos para tabla congreso
$nombre=$_POST["nombre"];
//$numero=$_POST["numero"];
$anio=$_POST["anio"];
$fecha_inicio=$_POST["fi"];
$fecha_fin=$_POST["ff"];

//Datos para tabla contacto
$correo=$_POST["correo"];
$telefono1=$_POST["numero"];
$telefono2=$_POST["numero2"];

//Datos para tabla tipo_pagos
$costoponente=$_POST['costopo'];
$costoasistente=$_POST['costoas'];
$costoestudiante=$_POST['costoes'];
$costoprototipo=$_POST['costopro'];

//Datos para tabla instrucciones
$instruccionesPO=$_POST['instruc_po'];
$instruccionesCA=$_POST['instruc_ca'];
$instruccionesTA=$_POST['instruc_ta'];
$instruccionesPRO=$_POST['instruc_pro'];


if(isset($_POST['submit'])){

    $numerocongreso=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
	$row0=pg_fetch_row($numerocongreso);
	$numero=$row0[0];
	$id_congreso_actual=pg_query($conexion,"SELECT id_congreso, id_contacto FROM congreso WHERE numero_congreso='$numero'"); //id del ultimo congreso
	$row=pg_fetch_row($id_congreso_actual);
    $id_congreso=$row[0];
    $id_contacto=$row[1];

    $update_congreso = "UPDATE congreso SET nombre_congreso = '$nombre', anio='$anio', fecha_inicio='$fecha_inicio', fecha_fin='$fecha_fin' WHERE id_congreso = '$id_congreso'";
    $actualizacion_congreso=pg_query($conexion, $update_congreso);

    $update_contacto = "UPDATE contacto SET email='$correo', telefono='$telefono1', telefono2='$telefono2' WHERE id_contacto = '$id_contacto'";
    $actualizacion_contacto=pg_query($conexion, $update_contacto);

    $update_pago_po = pg_query($conexion, "UPDATE tipo_pagos SET cantidad='$costoponente' WHERE tipo_pago='Ponente' and id_congreso='$id_congreso'");
    $update_pago_es = pg_query($conexion, "UPDATE tipo_pagos SET cantidad='$costoasistente' WHERE tipo_pago='Asistente' and id_congreso='$id_congreso'");
    $update_pago_as = pg_query($conexion, "UPDATE tipo_pagos SET cantidad='$costoestudiante' WHERE tipo_pago='Estudiante' and id_congreso='$id_congreso'");
    $update_pago_pro = pg_query($conexion, "UPDATE tipo_pagos SET cantidad='$costoprototipo' WHERE tipo_pago='Prototipo' and id_congreso='$id_congreso'");

    $update_instruc_po = pg_query($conexion, "UPDATE instrucciones_trabajos SET instrucciones='$instruccionesPO' WHERE tipo_trabajo='Ponencia Oral' and id_congreso='$id_congreso'");
    $update_instruc_ca = pg_query($conexion, "UPDATE instrucciones_trabajos SET instrucciones='$instruccionesCA' WHERE tipo_trabajo='Cartel' and id_congreso='$id_congreso'");
    $update_instruc_ta = pg_query($conexion, "UPDATE instrucciones_trabajos SET instrucciones='$instruccionesTA' WHERE tipo_trabajo='Taller' and id_congreso='$id_congreso'");
    $update_instruc_pro = pg_query($conexion, "UPDATE instrucciones_trabajos SET instrucciones='$instruccionesPRO' WHERE tipo_trabajo='Prototipo' and id_congreso='$id_congreso'");
    
    if($actualizacion_congreso || $actualizacion_contacto || $update_pago_po || $update_pago_es || $update_pago_as || $update_pago_pro || $update_instruc_po || $update_instruc_ca || $update_instruc_ta || $update_instruc_pro){
        echo"<script>alert('¡CONGRESO ACTUALIZADO!');window.location='editar-acongreso.php'</script>";
    } else{
        printf("Error message: %\n", pg_result_error($conexion));
    }
}

?>