<?php 
include ("Conexion.php");
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
					$row0=pg_fetch_row($consulta);
					$consulta0=$row0[0];
					$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
					$row=pg_fetch_row($consulta1);
					$congresoActual=$row[0];//es el id congreso
$contacto=pg_query($conexion,"SELECT cn.telefono,cn.telefono2,cn.email FROM contacto cn, congreso c
WHERE c.id_congreso='$congresoActual'
and  c.id_contacto =cn.id_contacto");
$contactos=pg_fetch_array($contacto);
$correoC=@$contactos[2];
$numeroC1=@$contactos[0];
$numeroC2=@$contactos[1];

session_start();
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
$usuario=$_SESSION['Usuario'];
include ('Conexion.php');
	//VALIDAR FECHA PARA EL PODER EDITAR 	
	date_default_timezone_set("America/Mexico_City");
	$date_nueva=date("d-m-Y");	
	// FUNCION PARA VALIDAR FECHA CONCURSO DE CARTELES
function verifica_rango_Editar($date_inicio, $date_fin, $date_nueva) {
    $date_inicio = strtotime($date_inicio);
    $date_fin = strtotime($date_fin);
    $date_nueva = strtotime($date_nueva);
    if (($date_nueva >= $date_inicio) && ($date_nueva <= $date_fin)){
        return true;//SI SE ENCUENTRA DENTRO DEL RANGO
    }
        
    return false;//SI NO SE ENCUENTRA DENTRO DEL RANGO
 }

//************************


	if(isset($_POST['id_ponencia_oralA'])){
		$id_ponencia_oralA=$_POST['id_ponencia_oralA'];
		$idPonencia=$id_ponencia_oralA;
	}elseif (isset($_POST['id_ponencia_oralN1'])) {
	
		$id_ponencia_oralN1=$_POST['id_ponencia_oralN1'];
		$idPonencia=$id_ponencia_oralN1;

	}elseif (!isset($_POST['id_ponencia_oralA'])&& !isset($_POST['id_ponencia_oralN1'])) {
		$idPonencia="";
	}
	
//******************************************************************************************************************************************************************************************
//TRAE EL NÚMERO DE LA CONGRESO
	$consulta_num_congreso=pg_query($conexion,"SELECT MAX(numero_congreso) FROM congreso");//numero de congreso el AS es un alias
	$row0=pg_fetch_row($consulta_num_congreso);//eL pg_fetch_row trae los datos de la consulta y los asigna a la variable $row0
	$num_congreso=$row0[0];//el número máximo que encontro del congreso lo asigna a la variable consulta0 y es el máximo por que es el [0]NUMERO MÁXIMO DEL CONGRESO

//TRAE EL ID DE LA CONGRESO CON AYUDA DE SU NUMERO
	$consulta_id_congreso=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$num_congreso'"); //Selecciona los datos con el resultado de la consulta anterior (NUMERO MAXIMO DEL CONGRESO)
	$row1=pg_fetch_row($consulta_id_congreso);//Los datos del ID_CONGRESO Y NOMBRE CONGRESO son asignados a la variable $row
	$id_congresoactual=$row1[0];//es el id congreso
//TRAE LAS FECHAS EN QUE SE PUEDE EDITAR ANTES DE SER EVALUADO
	$FechasI=pg_query($conexion,"SELECT f.fecha_inicio, f.fecha_fin FROM (SELECT (id_congreso) as id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso) C, fechas_importantes f
			WHERE f.id_congreso= C.id_congreso and f.descripcion= 'Recepción de Trabajos Extensos'");// trae las fechas de la tabla Fechas importantes
			$row=pg_fetch_row($FechasI);
			$date_inicio=$row[0];
			$date_fin=$row[1];	
//TRAE LAS FECHAS EN QUE SE PUEDE EDITAR DESPUES DE SER EVALUADO
	$FechasObs=pg_query($conexion,"SELECT f.fecha_inicio, f.fecha_fin FROM (SELECT (id_congreso) as id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso) C, fechas_importantes f
			WHERE f.id_congreso= C.id_congreso and f.descripcion= 'Notificación de observaciones de los trabajos extensos'");// trae las fechas de la tabla Fechas importantes
			$row=pg_fetch_row($FechasObs);
			$date_inicioObservacion=$row[0];
			$date_finObservacion=$row[1];	
//CUENTA EXTENSOS O ACTUALIZACIONES PARA DECIDIR SI USAR COMBO O CAMPO DE TEXTO
$consultaNumExtenso=pg_query($conexion,"SELECT  COUNT(*) id_ponencia_oral FROM ponencia_oral AS po, ponencias AS p, usuario_ponencias as up
	WHERE po.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'PO' and p.id_ponencia = po.id_ponencia_oral
	and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = po.id_congreso ");
	$rowNumExtenso=pg_fetch_assoc($consultaNumExtenso);
	//echo "Número de Extensos ".$rowNumExtenso['id_ponencia_oral']."<br>";


$consultaNumActualizaciones=pg_query($conexion, "SELECT  MAX (id_actualizacion_p_oral) as id_ultima_actualizacion, COUNT(id_actualizacion_p_oral) as Num_Actualizaciones_del_extenso, apo.id_ponencia_oral FROM actualizacion_p_oral AS apo, ponencia_oral AS po, ponencias AS p, usuario_ponencias as up
	WHERE po.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'PO' and p.id_ponencia = po.id_ponencia_oral
	and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = po.id_congreso and po.id_ponencia_oral = apo.id_ponencia_oral
	GROUP BY apo.id_ponencia_oral");
	$rowNumActualizaciones=pg_fetch_assoc($consultaNumActualizaciones);
	$numTotal=pg_num_rows($consultaNumActualizaciones);

$numeroAc=pg_query($conexion, "SELECT  COUNT(id_actualizacion_p_oral) FROM actualizacion_p_oral AS apo, ponencia_oral AS po, ponencias AS p, usuario_ponencias as up
	WHERE po.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'PO' and p.id_ponencia = po.id_ponencia_oral
	and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = po.id_congreso and po.id_ponencia_oral = apo.id_ponencia_oral");
	$rowNAc=pg_fetch_row($numeroAc);
	
$banderatipo="0";	
if ($rowNumExtenso['id_ponencia_oral']>1) {
	$banderatipo="1";
}

if($rowNumExtenso['id_ponencia_oral']==1 && empty($rowNumActualizaciones) || $numTotal==1 && empty($rowNumExtenso)){//SI HAY 1 EXTENO PERO 0 ACTUALIZACIONES O AL REVES
			
	//CONSULTA PARA EL CAMPO DE TEXTO (TRAE DATOS SABIENDO QUE NO HAY ACTUALIZACIONES)
	$consultaextenso=pg_query($conexion,"SELECT  id_ponencia_oral, p.titulo, estatus_extenso, extenso, up.tipo_autor, po.observaciones FROM ponencia_oral AS po, ponencias AS p, usuario_ponencias as up
			WHERE po.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'PO' and p.id_ponencia = po.id_ponencia_oral
			and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = po.id_congreso");
			$Extensos=pg_fetch_assoc($consultaextenso);
			
			$estatus=$Extensos['estatus_extenso'];
			$extenso=$Extensos['extenso'];
			$id_ponencia_oral=$Extensos['id_ponencia_oral'];
			$tipo_autor=$Extensos['tipo_autor'];
			$observacionesA=$Extensos['observaciones'];
							if(empty($observacionesA)){//SI  OBSERVACIONES ESTA VACIO
								$comentario='No hay comentarios hasta el momento';
							}else{
						
							$comentario=$observacionesA;
							}
			//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
			$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$idPonencia' AND id_congreso='$id_ponencia_oral' AND estatus_actualizacion='Aceptado'");
			$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);
							
			if(!empty($actualizacionTitulo)){
				$titulo=$actualizacionTitulo['titulo'];
			}else{
				$titulo=$Extensos['titulo'];
			}
		
	if($numTotal==1){

				$consulta_id_actualizacion_extenso=pg_query($conexion,"SELECT MAX(id_actualizacion_p_oral) FROM actualizacion_p_oral WHERE id_ponencia_oral = '$idPonencia' and id_congreso='$id_congresoactual'");
				$row2E=pg_fetch_row($consulta_id_actualizacion_extenso);
				$numeroidE=$row2E[0];
		$actualizacion_extenso2= pg_query($conexion,"SELECT apo.estatus_actualizacion, p.titulo, apo.extenso, apo.id_ponencia_oral, apo.observaciones, apo.id_actualizacion_p_oral, up.tipo_autor FROM actualizacion_p_oral as apo, ponencias as p, ponencia_oral as po, usuario_ponencias as up 
								WHERE  po.id_ponencia_oral = '$idPonencia' and p.id_ponencia = po.id_ponencia_oral  and apo.id_actualizacion_p_oral = '$numeroidE' and p.id_ponencia=up.id_ponencias and up.id_usuario='$usuario' AND apo.id_congreso='$id_congresoactual'");
							$rowActualizacionExtenso2=pg_fetch_assoc($actualizacion_extenso2);
							$id_ponencia_oral=$rowActualizacionExtenso2['id_ponencia_oral'];
							
							$estatus=$rowActualizacionExtenso2['estatus_actualizacion'];
							$extenso=$rowActualizacionExtenso2['extenso'];
							$observacionesA=$rowActualizacionExtenso2['observaciones'];
							if(empty($observacionesA)){//SI  OBSERVACIONES ESTA VACIO
								$comentario='No hay comentarios hasta el momento';
							}else{
						
							$comentario=$observacionesA;
							}
							$tipo_autor=$rowActualizacionExtenso2['tipo_autor'];

			//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
			$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$idPonencia' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
			$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);
							
			if(!empty($actualizacionTitulo)){
				$titulo=$actualizacionTitulo['titulo'];
			}else{
				$titulo=$rowActualizacionExtenso2['titulo'];
			}
	}
}
//OBTEN LO QUE SELECCIONA DEL COMBO Y LO PONE EN LOS CAMPOS DE TEXTOS CORRESPONDIENTES.

if(isset($_GET['id_extenso']) && empty($idPonencia) && $idPonencia ==""){
				
				$id_extensoSeleccionado=$_GET['id_extenso'];

				if(!empty($rowNumActualizaciones)){//CUANDO SI HAY ACTUALIZACIONES DENTRO DE LO QUE SELECCIONO EN EL COMBO
				$consulta_id_actualizacion_extenso=pg_query($conexion,"SELECT MAX(id_actualizacion_p_oral) FROM actualizacion_p_oral WHERE id_ponencia_oral = '$id_extensoSeleccionado' and id_congreso='$id_congresoactual'");
				$row2E=pg_fetch_row($consulta_id_actualizacion_extenso);

					if(@$row2E[0]!==NULL || !empty($row2E[0])){
						$numeroidE=$row2E[0];
							
							$actualizacion_extenso2= pg_query($conexion,"SELECT apo.estatus_actualizacion, p.titulo, apo.extenso, apo.id_ponencia_oral, apo.observaciones, apo.id_actualizacion_p_oral, up.tipo_autor FROM actualizacion_p_oral as apo, ponencias as p, ponencia_oral as po, usuario_ponencias as up  
								WHERE  po.id_ponencia_oral = '$id_extensoSeleccionado' and p.id_ponencia = po.id_ponencia_oral  and apo.id_actualizacion_p_oral = '$numeroidE' and p.id_ponencia=up.id_ponencias and up.id_usuario='$usuario'   AND apo.id_congreso='$id_congresoactual'");
							$rowActualizacionExtenso2=pg_fetch_assoc($actualizacion_extenso2);
							$id_ponencia_oral=$rowActualizacionExtenso2['id_ponencia_oral'];
							
							$estatus=$rowActualizacionExtenso2['estatus_actualizacion'];
							$extenso=$rowActualizacionExtenso2['extenso'];
							$observacionesA=$rowActualizacionExtenso2['observaciones'];
							if(empty($observacionesA)){//SI  OBSERVACIONES ESTA VACIO
								$comentario='No hay comentarios hasta el momento';
							}else{
							$comentario=$observacionesA;
							}
							$tipo_autor=$rowActualizacionExtenso2['tipo_autor'];

							//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
							$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_extensoSeleccionado' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
							$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);				
							if(!empty($actualizacionTitulo)){
								$titulo=$actualizacionTitulo['titulo'];
							}else{
								$titulo=$rowActualizacionExtenso2['titulo'];
							}
							
						}else if($id_extensoSeleccionado!= $row2E[0] && $id_extensoSeleccionado!=""){//SI ES DE LA TABLA EXTENSOS
							$extensos=pg_query($conexion,"SELECT  id_ponencia_oral, estatus_extenso, extenso, p.titulo, up.tipo_autor, po.observaciones FROM ponencia_oral AS po, ponencias AS p, usuario_ponencias as up
					WHERE po.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'PO' and p.id_ponencia = po.id_ponencia_oral
					and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = po.id_congreso and po.id_ponencia_oral='$id_extensoSeleccionado'");
							$rowExtensos2=pg_fetch_assoc($extensos);
							$id_ponencia_oral=$rowExtensos2['id_ponencia_oral'];
							
							$estatus=$rowExtensos2['estatus_extenso'];
							$extenso=$rowExtensos2['extenso'];
							$observacionesA=$rowExtensos2['observaciones'];
							if(empty($observacionesA)){//SI  OBSERVACIONES ESTA VACIO
								$comentario='No hay comentarios hasta el momento';
							}else{
						
							$comentario=$observacionesA;
							}
		
							$tipo_autor=$rowExtensos2['tipo_autor'];

							//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
							$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_extensoSeleccionado' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
							$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);				
							if(!empty($actualizacionTitulo)){
								$titulo=$actualizacionTitulo['titulo'];
							}else{
								$titulo=$rowExtensos2['titulo'];
							}
							//**********************************
						}else if($id_extensoSeleccionado==""){
							$id_ponencia_oral="";
							$titulo="";
							$estatus="";
							$extenso="";
							$comentario="";
							 echo"<script>alert('Selecciona el titulo del extenso que deseas visualizar.');window.location='trabr.php'</script>";

						}
				}else if(empty($rowNumActualizaciones)){//CUANDO NO HAY ACTUALIZACIONES PERO SI EXTENSOS DENTRO DEL COMBO
				$id_extensoSeleccionado=$_GET['id_extenso'];	

				$extensos=pg_query($conexion,"SELECT  id_ponencia_oral, estatus_extenso, extenso, p.titulo, up.tipo_autor, po.observaciones FROM ponencia_oral AS po, ponencias AS p, usuario_ponencias as up
			WHERE po.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'PO' and p.id_ponencia = po.id_ponencia_oral
			and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = po.id_congreso and po.id_ponencia_oral='$id_extensoSeleccionado'");
					$rowExtensos2=pg_fetch_assoc($extensos);
					

					$id_ponencia_oral=$rowExtensos2['id_ponencia_oral'];
					
					$estatus=$rowExtensos2['estatus_extenso'];
					$extenso=$rowExtensos2['extenso'];
					$observacionesA=$rowExtensos2['observaciones'];
							if(empty($observacionesA)){//SI  OBSERVACIONES ESTA VACIO
								$comentario='No hay comentarios hasta el momento';
							}else{
						
							$comentario=$observacionesA;
							}
					$tipo_autor=$rowExtensos2['tipo_autor'];

					//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
							$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_extensoSeleccionado' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
							$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);				
							if(!empty($actualizacionTitulo)){
								$titulo=$actualizacionTitulo['titulo'];
							}else{
								$titulo=$rowExtensos2['titulo'];
							}
							//**********************************
			}
				
	}else if($idPonencia !="" && empty($_GET['id_extenso']) && isset($idPonencia) ) {//REFERENTE A LO QUE TREA DE LA LISTA DE TRABAJOS PAG trabr.php
			
				if(!empty($rowNumActualizaciones)){ //SI SI HAY ACTUALIZACIONES
				$consulta_id_actualizacion_extenso=pg_query($conexion,"SELECT MAX(id_actualizacion_p_oral) FROM actualizacion_p_oral WHERE id_ponencia_oral = '$idPonencia' and id_congreso='$id_congresoactual'");
				$row2E=pg_fetch_row($consulta_id_actualizacion_extenso);

					if(@$row2E[0]!==NULL || !empty($row2E[0])){
						$numeroidE=$row2E[0];
							
							$actualizacion_extenso2= pg_query($conexion,"SELECT apo.estatus_actualizacion, p.titulo, apo.extenso, apo.id_ponencia_oral, apo.observaciones,  apo.id_actualizacion_p_oral, up.tipo_autor FROM actualizacion_p_oral as apo, ponencias as p, ponencia_oral as po, usuario_ponencias as up
								WHERE  po.id_ponencia_oral = '$idPonencia' and p.id_ponencia = po.id_ponencia_oral  and apo.id_actualizacion_p_oral = '$numeroidE' and p.id_ponencia=up.id_ponencias and up.id_usuario='$usuario'  AND apo.id_congreso='$id_congresoactual'");
							$rowActualizacionExtenso2=pg_fetch_assoc($actualizacion_extenso2);
							$id_ponencia_oral=$rowActualizacionExtenso2['id_ponencia_oral'];
							
							$estatus=$rowActualizacionExtenso2['estatus_actualizacion'];
							$extenso=$rowActualizacionExtenso2['extenso'];
							$observacionesA=$rowActualizacionExtenso2['observaciones'];
							if(empty($observacionesA)){//SI  OBSERVACIONES ESTA VACIO
								$comentario='No hay comentarios hasta el momento';
							}else{
							
							$comentario=$observacionesA;
							}
							$tipo_autor=$rowActualizacionExtenso2['tipo_autor'];

							//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
							$consultaTituloA=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$idPonencia' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
							$actualizacionTituloA=pg_fetch_assoc($consultaTituloA);	
								
							if(!empty($actualizacionTituloA)){
								$titulo=$actualizacionTituloA['titulo'];
								
							}else{
								$titulo=$rowActualizacionExtenso2['titulo'];
							}
							//**********************************
							
						}else{
							$extensos=pg_query($conexion,"SELECT  id_ponencia_oral, estatus_extenso, extenso, p.titulo, up.tipo_autor, po.observaciones FROM ponencia_oral AS po, ponencias AS p, usuario_ponencias as up
					WHERE po.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'PO' and p.id_ponencia = po.id_ponencia_oral
					and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = po.id_congreso and po.id_ponencia_oral='$idPonencia'");
							$rowExtensos2=pg_fetch_assoc($extensos);
							$id_ponencia_oral=$rowExtensos2['id_ponencia_oral'];
							
							$estatus=$rowExtensos2['estatus_extenso'];
							$extenso=$rowExtensos2['extenso'];
							$observacionesA=$rowExtensos2['observaciones'];
							if(empty($observacionesA)){//SI  OBSERVACIONES ESTA VACIO
								$comentario='No hay comentarios hasta el momento';
							}else{
						
							$comentario=$observacionesA;
							}
							$tipo_autor=$rowExtensos2['tipo_autor'];

							//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
							$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$idPonencia'  AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
							$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);				
							if(!empty($actualizacionTitulo)){
								$titulo=$actualizacionTitulo['titulo'];
							}else{
								$titulo=$rowExtensos2['titulo'];
							}
							//**********************************
							
						}
				}else{ 
					$extensos=pg_query($conexion,"SELECT  id_ponencia_oral, estatus_extenso, extenso, p.titulo, up.tipo_autor, po.observaciones FROM ponencia_oral AS po, ponencias AS p, usuario_ponencias as up
				WHERE po.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'PO' and p.id_ponencia = po.id_ponencia_oral
				and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = po.id_congreso and po.id_ponencia_oral='$idPonencia'");
						$rowExtensos2=pg_fetch_assoc($extensos);
						$id_ponencia_oral=$rowExtensos2['id_ponencia_oral'];
						
						$estatus=$rowExtensos2['estatus_extenso'];
						$extenso=$rowExtensos2['extenso'];
						$observacionesA=$rowExtensos2['observaciones'];
							if(empty($observacionesA)){//SI  OBSERVACIONES ESTA VACIO
								$comentario='No hay comentarios hasta el momento';
							}else{
						
							$comentario=$observacionesA;
							}
						$tipo_autor=$rowExtensos2['tipo_autor'];
						//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
							$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$idPonencia'  AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
							$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);				
							if(!empty($actualizacionTitulo)){
								$titulo=$actualizacionTitulo['titulo'];
							}else{
								$titulo=$rowExtensos2['titulo'];
							}
							//**********************************
			}
	}

if($idPonencia=="" && empty($_GET['id_extenso'])){
	echo"<script>alert('Selecciona un trabajo');window.location='trabr.php'</script>";
}
	$banderaFecha="";
//CONSULTA SI HAY EVALUADOR ASIGNADO
	$consultaValidacionEvaluador=pg_query($conexion, "SELECT * FROM usuario_evalua_ponencias WHERE id_congreso='$id_congresoactual' AND id_ponencias='$id_ponencia_oral'");
	$numValidacion=pg_num_rows($consultaValidacionEvaluador);
//VALIDACIÓN DE LAS FECHAS PARA EDITAR ANTES DE SER EVALUADO:
	if($estatus!='Aceptado' || $estatus!='Rechazado' || $estatus!='En Evaluacion'){
		if(verifica_rango_Editar($date_inicio, $date_fin, $date_nueva)==false){
			$banderaFecha="0";
		}else if(verifica_rango_Editar($date_inicio, $date_fin, $date_nueva)==true){ 
			if($numValidacion!=0){//SI NO TIENE ASIGNADO EVALUADOR
				$banderaFecha="1";//SI SE ENCUENTRA DENTRO DE LAS FECHAS
				}else{
				$banderaFecha="0";
			}
		}
	}
//VALIDACION DE FECHA PARA EDITAR DESPUES DE SER EVALUADO
	if($estatus!='Aceptado' || $estatus!='Rechazado' || $estatus!='En Evaluacion'){
		if(verifica_rango_Editar($date_inicioObservacion, $date_finObservacion, $date_nueva)==false){
			$banderaFecha="0";
		}else if(verifica_rango_Editar($date_inicioObservacion, $date_finObservacion, $date_nueva)==true){ 
			$banderaFecha="1";//SI SE ENCUENTRA DENTRO DE LAS FECHAS
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title style="background-image: url('images/logos.jpg');">CISEMATH</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/cismath.png">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" />

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98" onload="validarEstatus(); asignaridCombo();">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
      <?php
    ////Codigo para actualizar el banner
        include ('Conexion.php');
        $nmax=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
        $num=pg_fetch_row($nmax);
        $numcon=$num[0];
        $conact=pg_query($conexion,"SELECT id_congreso, banner FROM congreso WHERE numero_congreso='$numcon'");
        $ban=pg_fetch_row($conact);
        $banner=$ban[1];
        echo '<img src="'.$banner.'" style="width:100%; height:100%; background-size: cover; position: relative; display: flex;"/>';
    ?>

    <header class="top-header" >
        <div class="header_bottom">
          <div class="container">
		  <div class="row"> 
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 col-xl-12">
                <div class="menu_orange_section" style="background: #052450;">
                   <nav class="navbar header-nav navbar-expand-lg"> 
                     <div class="menu_section">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav" >
					    <li><a class="nav-link active" href="indexus.php"><img src="images/i4.png" alt="image"></a> </li>
                        <li><a class="nav-link active" href="acercadeus.php">Acerca de</a></li>
                        <li><a class="nav-link active" href="infus.php">Información</a></li>
                        <li><a class="nav-link active" href="instruccionesus.php">Instrucciones</a></li>
                        <li><a class="nav-link active" href="comitesus.php">Comités</a></li>
						<li><a class="nav-link active" href="memoriasus.php">Memorias</a></li>
						
						<li>
						<a style="position:relative; display:block;">
						<img src="images/user.png" alt="image"> 
						</a>
						</li>
						<li>
						<ul class="nav"><li class="dropdown langmenu" ><a href="#" style="display:block;"class="dropdown-toggle" data-toggle="dropdown" title="Idioma"><b class="caret"></b></a><ul class="dropdown-menu" style="color:black;"><li><a title="English ‎(en)‎" href="menu.php" style="color:black";>Menú‎‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="editarp.php"  style="color:black";>Perfil‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="index.php"  style="color:black";>Cerrar Sesión</a></li></ul></li></ul>
						</li>
						
                    </ul>
                </div>
                     </div>
                 </nav>
                 
                </div>
            </div>
          </div>
		  </div>
        </div>
        
    </header>
    <!-- End header -->
 
 <!-- Start CONTENIDO -->
 
 <form action="actualiza-extensos.php" method="post" enctype="multipart/form-data">	 
	<div class="container">
		
		<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"> 
						
			<h1 align="center" style="margin-top:8%; color:#052450;">EDITAR TRABAJOS EXTENSOS<h1>
				
		</div>
		
	<div class="row">
			
			<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"> 
			<div class="widget" style="margin-bottom: 2%;">
			
			<table cellpadding="10px" cellspacing="8px" style="text-align:left; font-size:18px;">
				<input type='hidden' name='banderaID' id='banderaID' value='<?php echo $id_ponencia_oral?>'>
				<input type='hidden' name='banderatipo' id='banderatipo' value='<?php echo $banderatipo?>'>
				<input type='hidden' name='banderaFecha' id='banderaFecha' value='<?php echo $banderaFecha?>'>
				<tr>
					<td>
						Status:
					</td>

					<td><?php	
					if (isset($rowActualizacionExtenso2)|| isset($rowExtensos2)|| isset($Extensos) || isset($idPonencia)){?>	
							<input type="text" class="form-control" name="status" id="status"  value="<?php echo $estatus ?>" readonly="readonly">
							<input  name="tipo_autor" id="tipo_autor" type="hidden" value="<?php echo $tipo_autor?>">
						<?php }else{?>
							<input type="text" class="form-control" name="status" id="status"  value="" readonly="readonly">
				<?php } ?>	
					</td>
		
				</tr>
				
				<tr>
					<td>
						Título del trabajo:
					</td>

					<td>
						<?php

	if ($rowNumExtenso['id_ponencia_oral']>1 && $numTotal>=1 || $rowNumExtenso['id_ponencia_oral']>1 && $numTotal==0) {//SI HAY MUCHO EXTENSOS Y HAY ACTUALIZACIONES DE EXTENSOS SE PONE COMBOBOX o cuando hay muchos extensos solamente

	
						?>
						<select class="form-control" id="idtituloTrabajo" name="tituloTrabajo" required="" title="SELECCIONE UN TRABAJO, POR FAVOR"  onchange="selecionaTitulo();">
							
							<option value="" >Selecciona el titulo de tu trabajo: </option>
							
							<?php
								
						$consulta_extensos=pg_query($conexion,"SELECT  id_ponencia_oral, p.titulo FROM ponencia_oral AS po, ponencias AS p, usuario_ponencias as up WHERE po.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'PO' and p.id_ponencia = po.id_ponencia_oral and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = po.id_congreso");
						
										
							
								while ($rowExtensos=pg_fetch_assoc($consulta_extensos)) {
									$id_ponencia_oralE=$rowExtensos['id_ponencia_oral'];
										//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
									$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_ponencia_oralE' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
									$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);	
									if(!empty($actualizacionTitulo)){
										$titulo=$actualizacionTitulo['titulo'];
									}else{
										$titulo=$rowExtensos['titulo'];
									}
									
								?>
							<option value="<?php echo $id_ponencia_oralE ?>"><?php echo $titulo?></option>
							<?php
							}  
							
							?>
						</select>
					</td>
					</tr>
		<?php  
			
			}else if($rowNumExtenso['id_ponencia_oral']==1 && $numTotal==1 || $rowNumExtenso['id_ponencia_oral']==1 && $numTotal==0){//SI HAY 1 EXTENO PERO 0 ACTUALIZACIONES O AL REVES
		
			if (isset($rowActualizacionExtenso2)|| isset($rowExtensos2)|| isset($Extensos) || isset($idPonencia)){?>
				<input type="text" class="form-control" name="titulo_text" id="titulo_text" value="<?php echo $titulo ?>" readonly="readonly">
		<?php }else{?>
					<input type="text" class="form-control" name="titulo_text" id="titulo_text" value="" readonly="readonly">
				<?php }
				} ?>
				
				
				<tr>
					<td>
						Clave:
					</td>

					<td><?php	if (isset($rowActualizacionExtenso2)|| isset($rowExtensos2)|| isset($Extensos) || isset($idPonencia)){?>

							<input type="text" class="form-control" name="Clave" id="Clave" value="<?php echo $id_ponencia_oral ?>" readonly="readonly">
					<?php }else{?>
							<input type="text" class="form-control" name="Clave" id="Clave" value="" readonly="readonly">
				<?php } ?>
						
                     </td>
		
				</tr>
				
				<tr>
				
					<td>
						
						Archivo actual:	
						
					</td>
					
					<td><?php	if (isset($rowActualizacionExtenso2)|| isset($rowExtensos2)|| isset($Extensos) || isset($idPonencia)){
						if($extenso!=""){
						$nombreArchivo= explode("/", $extenso);
						}else{
							$nombreArchivo[1]="";	
						}
						?> 

						<input type="text" class="form-control" name="archivoActual" id="archivoActual" value="<?php echo $nombreArchivo[1] ?>" readonly="readonly">
						<input  name="extenso" id= "extenso" type="hidden" value="<?php echo $extenso?>">
         				<?php }else{?>

							<input type="text" class="form-control" name="archivoActual" id="archivoActual" value="" readonly="readonly">
				<?php } ?>
					</td>
					<td>
						<input type="button" value="Editar" id="Editar" onclick="cambiarArchivo();borrarArchivoAnterior();" style=" margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;">
					</td>
				
				</tr>
				
				<tr> 
				
					<td>
						
						<label for="archivoN" id="archivoN"> Adjuntar archivo nuevo:</label>	
						
					</td>
					
					<td>
						
						<input type="file" name="archivoNuevo" id="archivoNuevo" required="required" disabled="" />
						
					</td>
				
				</tr>
				
				<tr>
				
					<td>
						
						Comentarios:	
						
					</td>
					
					<td><?php	if (isset($rowActualizacionExtenso2)|| isset($rowExtensos2)|| isset($Extensos) || isset($idPonencia)){?>
						<textarea name="textarea" id="comentarios" rows="5" cols="30" maxlength="5" readonly="readonly"><?php echo $comentario?></textarea>								 
					<?php }else{?>
							<textarea name="textarea" id="comentarios" rows="5" cols="30" maxlength="5" readonly="readonly"></textarea>	
				<?php } ?>	
					</td>
				
				</tr>
				
			</table>
			</div>
			
			<div class="btn" style="text-align:right; width:100%; margin-bottom:5%;">
				
				<input type="submit" value="Guardar"  id="submit" style=" margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px; display: none;">
			</form>

				<button type="reset" id="Regresar"  onclick="location.href='trabr.php'" style="background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;">Regresar</button>


				<input type="reset" value="Cancelar" id="Cancelar" onclick="cancelar()" style="background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px; display: none;">
			</div>
			
			</div>
		</div>
	</div>
	 
    <!-- End CONTENIDO -->
    
  <!-- Start Footer -->
        <footer class="footer-box">
        <div class="container">
            <div class="row">
               <div class="col-md-12 white_fonts" style="display: contents;">
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            
                            <div class="full">
							<h3>Dirección</h3>
                                <ul class="menu_footer">   
								     
								     <li><img src="images/i5.png"><span style="font-size: 85%;">Carr, Cuautitlán - Teoloyucan, San Sebastian Xhala, 54714 Cuautitlán Izcalli</span></li>

                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="full">
                                <div class="footer_blog full white_fonts">
                             <h3>Contacto</h3>
                             <ul class="full">
                               <li><img src="images/i6.png"><span style="font-size: 85%;"> <?php echo $correoC  ?></span></li>
                               <li><img src="images/i7.png"><span style="font-size: 85%;"><?php echo "$numeroC1    $numeroC2"; ?></span></li>
                             </ul>
                         </div>
                            </div>
                        </div>
                </div>
			 </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link active" href="derechosa.php" style="color:white">© Sitio desarrollado por CDD Solutions S.A. y administrado por el Departamento de Matemáticas. Hecho en México, (2020 - 2021)</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
	/* counter js */
	function asignaridCombo(){
		var id_ponenciaCombo=document.getElementById('banderaID').value;
		var banderatipo=document.getElementById('banderatipo').value;
		var combo = document.getElementById('idtituloTrabajo');
		
		if(banderatipo==="1"){
			combo.value=id_ponenciaCombo;
		}
	}

	function selecionaTitulo(){
		var id_extenso = document.getElementById('idtituloTrabajo').value;
		if(id_extenso ==""){
		document.getElementById('Clave').value = "";	
		}	
		window.location.href='?id_extenso='+id_extenso;

	}

	function cambiarArchivo(){ //habilitar boton de cambiar archivo
		var archivoNuevo = document.getElementById('archivoNuevo');
		var editarArchivo= document.getElementById('Editar');
		var status = document.getElementById("status").value
		var botonGuardar = document.getElementById("submit");
		var botonCancelar=document.getElementById("Cancelar");
		var botonRegresar= document.getElementById("Regresar");
		var banderaFecha=document.getElementById('banderaFecha').value;
		if (status !="Rechazado" || status!="Aceptado" || status =="En Evaluacion" && banderaFecha=="1") {
			archivoNuevo.disabled=false;
			editarArchivo.style.display='none';	
			botonGuardar.style.display='inline';
			botonCancelar.style.display='inline';
			botonRegresar.style.display='none';
		}
		else{
			archivoNuevo.style.display='none';
		}
		
	}
	function cancelar(){//FUNCION PARA EL BOTON CANCELAR
		var archivoNuevo = document.getElementById('archivoNuevo');
		var editarArchivo= document.getElementById("Editar");
		var botonGuardar = document.getElementById("submit");
		var botonRegresar= document.getElementById("Regresar");
		var botonCancelar=document.getElementById("Cancelar");


		archivoNuevo.disabled= true;
		editarArchivo.style.display='inline';
		botonGuardar.style.display='none';
		botonRegresar.style.display='inline';
		botonCancelar.style.display='none';


	}
function borrarArchivoAnterior(){
		var opcion = confirm("Recuerde que si continúa con la actualización, se eliminára el documento anteriormente registrado.");
		if (opcion!=true) {
			window.location='trabr.php';
		}
	}
function validarEstatus(){//SE VALIDA ESTATUS Y FECHA
		var estatusExtenso=document.getElementById("status").value;
		var tipo_autor=document.getElementById("tipo_autor").value;
		var archivoNuevo = document.getElementById("archivoNuevo");
		var editarArchivo= document.getElementById("Editar");
		var botonGuardar = document.getElementById("submit");
		var botonRegresar= document.getElementById("Regresar");
		var botonCancelar=document.getElementById("Cancelar");
		var banderaFecha=document.getElementById('banderaFecha').value;
		
		

		if (estatusExtenso =="Rechazado" || estatusExtenso =="Aceptado" || estatusExtenso =="En Evaluacion"  && tipo_autor=="Coautor" && banderaFecha=="0" || tipo_autor!="Coautor" && banderaFecha=="0" && estatusExtenso =="Rechazado" || estatusExtenso =="Aceptado" || estatusExtenso =="En Evaluacion"|| banderaFecha=="0" ){
			document.getElementById("archivoN").style.display='none';	
			archivoNuevo.style.display='none';
			editarArchivo.style.display='none';
			botonCancelar.style.display='none';
			botonGuardar.style.display='none';
			botonRegresar.style.display='inline';
			if(banderaFecha=="0"){
				alert('Han pasado las fechas para poder editar los Extensos.');
			}
			
			
		}
	}





(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
	</script>
</body>

</html>