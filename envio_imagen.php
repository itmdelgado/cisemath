<?php
	session_start();

	use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
   	require 'PHPMailer/Exception.php';
    require 'PHPMailer/PHPMailer.php';
    require 'PHPMailer/SMTP.php';
    require ('libreria/fpdf.php');
   
    $usuario=$_SESSION['Usuario'];
    $estatus_imagen="Enviado";
	date_default_timezone_set("America/Mexico_City");
	$fecha=date("Y-m-d");
    $hora=date("H:i:s");

    if (isset($_POST['clave2'])) {
    	 $clave2= $_POST["clave2"];
    	 $claveTrabajo=$clave2;
    	
    }else{
    	$Clave = $_POST['Clave'];
    	$claveTrabajo=$Clave;
    	
    }
    //echo "La clave es".$claveTrabajo;

    include ('Conexion.php');
    $consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
    $row5=pg_fetch_row($consulta);
    $consulta0=$row5[0];

    $consulta1=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$consulta0'");
    $row6=pg_fetch_row($consulta1);
    $numeroCongreso=$row6[0];

    $informacionTrabajo=pg_query($conexion, "SELECT titulo FROM ponencias WHERE id_ponencia='$claveTrabajo'");
    $rowinfo=pg_fetch_assoc($informacionTrabajo);
    
     //VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
    $consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$claveTrabajo' AND id_congreso='$numeroCongreso' AND estatus_actualizacion='Aceptado'");
    $actualizacionTitulo=pg_fetch_assoc($consultaTitulo);   

    if(!empty($actualizacionTitulo)){
    $titulo=$actualizacionTitulo['titulo'];

    }else{
    $titulo=$rowinfo['titulo'];
    }

    //VALIDACIÓN DE TERMINACIÓN DE ARCHIVOS
    $directorio="imagenesCarteles/";
    $archivo=basename($_FILES["seleccionArchivos"]["name"]);
    $tipoArchivo=strtolower(pathinfo($archivo, PATHINFO_EXTENSION));
    /////////////////////////////////TAMAÑO ARCHIVO 10 MB////////////////////
    $maximob=10000000;
    if ($tipoArchivo=="png" || $tipoArchivo=="jpg"|| $tipoArchivo=="jpge") {
        if($_FILES["seleccionArchivos"]["size"]<=$maximob){
            //echo "Ingresaste el tipo de imagen correcto";
        
        $nombre_final= "$claveTrabajo";
            $ruta=$directorio.$nombre_final.$numeroCongreso;
            $subirarchivos=move_uploaded_file($_FILES["seleccionArchivos"]["tmp_name"],$ruta.".".$tipoArchivo);
            $rutaF=$ruta.".".$tipoArchivo;
             if($subirarchivos){//SI SE SUBE EL ARCHIVO A LA CARPETA
              $insertarExtenso="INSERT INTO ponencia_cartel(id_ponencia_cartel,estatus_imagen,id_congreso,imagen,fecha,hora) VALUES ('$claveTrabajo','$estatus_imagen','$numeroCongreso','$rutaF','$fecha','$hora')";
                $resultado=pg_query($conexion,$insertarExtenso);  
                if($resultado){//Si se inserta en la tabla
                                    //CORREO AUTOR
                   
                    //******************************************************************************************
                 
                    
                    class PDF extends FPDF{
                    // Cabecera de página
                    function Header()
                    {
                        // Logo
                        $this->Image('logo.jpg',0,0,220);
                        // Arial bold 15
                        $this->SetFont('Arial','B',15);
                        // Movernos a la derecha
                        $this->Cell(80);
                        // Título
                        $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
                        // Salto de línea
                        $this->Ln(50);
                        
                    }
                    
                    // Pie de página
                    function Footer() {
                        // Posición: a 1,5 cm del final
                        $this->SetY(-15);
                        // Arial italic 8
                        $this->SetFont('Arial','I',8);
                        // Número de página
                        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
                        }
                    }
                    
                    // Creación del objeto de la clase heredada
                    $pdf = new PDF();
                    $pdf->AliasNbPages();
                    $pdf->AddPage();
                    $pdf->SetFont('Times','',12);
                    
                    $pdf->Cell(40,10,utf8_decode('Registro de Imagen Cartel'),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    $pdf->Cell(40,10,utf8_decode('Usted ha registrado la imagen del Cartel para la siguiente ponencia de Carteles'),0,1);
                    $pdf->Cell(40,10,utf8_decode('Clave: '.$claveTrabajo),0,1);
                    $pdf->Cell(40,10,utf8_decode('Título del Cartel: '.$titulo),0,1);
                    $pdf->Cell(40,10,utf8_decode('El nombre de la imagen registrada es: '.$archivo),0,1);
                    $pdf->Cell(40,10,utf8_decode('Fecha de registro de la imagen fue: '.$fecha),0,1);
                    $pdf->Cell(40,10,utf8_decode('Recuerde que si desea realizar alguna modificación a la imagen actual,'),0,1); 
                    $pdf->Cell(40,10,utf8_decode('se debe realizar durante las fechas correspondientes en'),0,1);
                    $pdf->Cell(40,10,utf8_decode('la página de "Trabajos Registrados" en el apartado de "Cartel / Imagen".'),0,1);
                    $pdf->Cell(40,10,utf8_decode('Le pedimos de la manera más atenta, estar al pendiente de su cuenta para conocer'),0,1);
                    $pdf->Cell(40,10,utf8_decode('el estatus de su trabajo.'),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    
                    $archivoAdjunto = $pdf->Output("", "S");
                    $correoa=pg_query($conexion,"SELECT c.correo,us.nombres,us.primer_ap,us.segundo_ap FROM correos_usuario c, usuario us WHERE c.id_usuario='$usuario' and us.id_usuario ='$usuario'");
                    $i=0;
                    while($mostrarCR=pg_fetch_array($correoa)){
                        $cor[$i]=trim($mostrarCR['correo']);
                        $nombre=trim($mostrarCR['nombres']);
                        $apPA=trim($mostrarCR['primer_ap']);
                        $apMa=trim($mostrarCR['segundo_ap']);
                       
                        $i=$i+1;
                    }
                    
                    
                    
                    $mail = new PHPMailer(true);
                    
                    try {
                        //Server settings
                        $mail->SMTPDebug = 0;                      // Enable verbose debug output
                        $mail->isSMTP();                                            // Send using SMTP
                        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                        $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
                        $mail->Password   = 'CongresoMate2020';                               // SMTP password
                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                    
                        //Recipients
                        $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
                        $mail->addAddress("$cor[0]", "$nombre $apPA");
                        if(!empty($cor[1])){
                            $mail->addAddress("$cor[1]", "$nombre $apPA");
                        }
                        if(!empty($cor[2])){
                            $mail->addAddress("$cor[2]", "$nombre $apPA");
                        }
                       // Content
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = 'Registro de Imagen para Concurso de Carteles';
                        $mail->Body    = 'En el siguiente documento se adjuntan los datos de la Imagen registrada.';
                        $mail->addStringAttachment($archivoAdjunto, 'Registro_Imagen_Carteles.pdf');
                        $mail->send();
                        
                        }catch (Exception $e) {
                            echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
                        }
                    
        /* }else{
        printf("errormessage: %s\n".pg_error($conexion));
        }*/


                    //*****************************************************************************************

                 $numCoautoresTrabajo=pg_query($conexion, "SELECT COUNT (*) id_usuario FROM usuario_ponencias 
                        WHERE id_ponencias ='$claveTrabajo' and tipo_autor='Coautor' and id_congreso = '$numeroCongreso'");
                        $numCoautores =pg_fetch_assoc($numCoautoresTrabajo);
                        
                        //VALIDA SI HAY COAUTORES
                            if($numCoautores['id_usuario']!=0){
                                  
//********************************************************************************************************************
                                 class PDF2 extends FPDF
                        {
                        // Cabecera de página
                        function Header()
                        {
                            // Logo
                            $this->Image('logo.jpg',0,0,220);
                            // Arial bold 15
                            $this->SetFont('Arial','B',15);
                            // Movernos a la derecha
                            $this->Cell(80);
                            // Título
                            $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
                            // Salto de línea
                            $this->Ln(50);
                            
                        }
                        
                        // Pie de página
                        function Footer()
                        {
                            // Posición: a 1,5 cm del final
                            $this->SetY(-15);
                            // Arial italic 8
                            $this->SetFont('Arial','I',8);
                            // Número de página
                            $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
                        }
                        }
                        
                        // Creación del objeto de la clase heredada
                         $pdf = new PDF();
                        $pdf->AliasNbPages();
                        $pdf->AddPage();
                        $pdf->SetFont('Times','',12);

                        $pdf->Cell(40,10,utf8_decode('Registro de Imagen del Cartel'),0,1);
                        $pdf->Cell(40,10,utf8_decode(''),0,1);
                        $pdf->Cell(40,10,utf8_decode('El autor '.$nombre.' '.$apPA.' '.$apMa.' ha registrado la Imagen para el siguiente Cartel'),0,1);
                        $pdf->Cell(40,10,utf8_decode('Clave: '.$claveTrabajo),0,1);
                        $pdf->Cell(40,10,utf8_decode('Título del Cartel: '.$titulo),0,1);
                        $pdf->Cell(40,10,utf8_decode('El nombre de la Imagen registrada es: '.$archivo),0,1);
                        $pdf->Cell(40,10,utf8_decode('Fecha de registro del documento fue: '.$fecha),0,1);
                        $pdf->Cell(40,10,utf8_decode('Si desea conocer el estatus de su trabajo, lo podrá visualizar'),0,1);
                        $pdf->Cell(40,10,utf8_decode('en el apartado "trabajos registrados".'),0,1);
                        $pdf->Cell(40,10,utf8_decode(''),0,1);

                        $archivoAdjunto2 = $pdf->Output("", "S");
                        
                        //Envio de correo coautores
                        $mail = new PHPMailer(true);
                                            
                        try {
                            //Server settings
                            $mail->SMTPDebug = 0;                      // Enable verbose debug output
                            $mail->isSMTP();                                            // Send using SMTP
                            $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                            $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
                            $mail->Password   = 'CongresoMate2020';                               // SMTP password
                            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                            $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                        
                            //Recipients
                            $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
                            // obtener nombre,apellidos y correo de coautores del trabajo
                       

                            $infoCoau=pg_query($conexion,"select u.nombres,u.primer_ap ,u.segundo_ap,c.correo FROM correos_usuario c, usuario u, usuario_ponencias up WHERE up.id_ponencias='$claveTrabajo'and up.id_usuario=u.id_usuario and u.id_usuario= c.id_usuario and up.tipo_autor!='Autor';");
                            $j=0;
                            while($infoCoa=pg_fetch_array($infoCoau)){
                               
                                $nombreCoa[$j]=trim($infoCoa['nombres']);
                                $apellidoCoa[$j]=trim($infoCoa['primer_ap']);
                                $apellidoCoa2[$j]=trim($infoCoa['segundo_ap']);
                                $corC[$j]=trim($infoCoa['correo']);
                               
                            
                                $j=$j+1;
                               }
                               if(!empty($corC[0])){
                                $mail->addAddress("$corC[0]",utf8_decode("$nombreCoa[0] $apellidoCoa[0] $apellidoCoa2[0]"));
    
                               }
                               if(!empty($corC[1])){
                                $mail->addAddress("$corC[1]",utf8_decode("$nombreCoa[1] $apellidoCoa[1] $apellidoCoa2[1]"));
    
                               }
                               if(!empty($corC[2])){
                                $mail->addAddress("$corC[2]",utf8_decode("$nombreCoa[2] $apellidoCoa[2] $apellidoCoa2[2]"));
    
                               }
                               if(!empty($corC[3])){
                                $mail->addAddress("$corC[3]",utf8_decode("$nombreCoa[3] $apellidoCoa[3] $apellidoCoa2[3]"));
    
                               }   
                        
                            // Attachments
                            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
                        
                            // Content
                            $mail->isHTML(true);                                  // Set email format to HTML
                            $mail->Subject = 'Registro de Imagen del Cartel';
                            $mail->Body    = 'En el siguiente documento se adjuntan los datos de la Imagen registrada para el concurso de Carteles.';
                            $mail->addStringAttachment($archivoAdjunto2, 'Registro_Imagen_Carteles.pdf');
                            $mail->send();
                            
                        } catch (Exception $e) {
                            echo "<script>alert('Error al enviar el mensaje para coautores: {$mail->ErrorInfo}');window.location='cartel.php'</script>";
                        }




//*********************************************************************************************************************
                               echo"<script>alert('¡La Imagen se registro correctamente!');window.location='cartel.php'</script>"; 

                            }else{//DE LOS COAUTOrES
                                echo"<script>alert('¡La Imagen se registro correctamente!');window.location='cartel.php'</script>"; 
                            }
         
                }else{//INSERTARLO EN LA TABLA
                  
                     echo"<script>alert('Error en el registro del la imagen.');window.location='cartel.php'</script>"; 
                }
             }else{//DEL SUBIR EL ARCHIVO
               
                echo"<script>alert('Error no se logro subir el archivo de manera correcta.');window.location='cartel.php'</script>"; 
             }
         }else{
                 echo"<script>alert('Solo se aceptan imagenes con un tamaño hasta de 10MB.');window.location='cartel.php'</script>"; 
         }
    }else{//DE LA TERMINACIÓN DE LA FOTO
        
          echo"<script>alert('Solo se aceptan archivos con terminación .png o .jpg. Por favor, seleccione el archivo  correspondiente.');window.location='cartel.php'</script>"; 
    }

?>