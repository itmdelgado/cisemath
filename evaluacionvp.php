<?php 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';
require ('libreria/fpdf.php');
include ("Conexion.php");
session_start();
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
if(isset($_POST['submit1'])){
    $estatusEv=$_POST['gender'];
    $comentarios=$_POST['comentarios'];
    $Clave=$_POST['id_ponencia'];
    $congresoActual=$_POST['id_congreso'];
    $titulo=$_POST['titulo'];
    //////////////////////////////////////////////////////SE INSERTA LA EVALUACION A LA BASE DE DATOS/////////////////////////////////////////////////////////////////////////////////////
    $insert=pg_query($conexion,"UPDATE ponencias SET estatus_video='$estatusEv', observaciones_video='$comentarios' WHERE id_ponencia='$Clave' and id_congreso='$congresoActual' ");
    /////////////////////////////////////////////////////////////////////////SE CONSULTA CORREO DEL AUTOR/////////////////////////////////////////////////////////////////////////////////
    $correoa=pg_query($conexion,"SELECT c.correo FROM correos_usuario c,usuario u, usuario_ponencias up WHERE up.id_ponencias='$Clave' and up.id_usuario=u.id_usuario and u.id_usuario=c.id_usuario and up.tipo_autor='Autor'");
    $i=0;
    while($mostrarCR=pg_fetch_array($correoa)){
        $cor[$i]=trim($mostrarCR['correo']);
        $i=$i+1;
    }
    //////////////////////////////////// SE CONSULTA LA INFORMACION DE LOS COAUTORES/////////////////////////////////////////////////////////////////////////////
    $infoCoau=pg_query($conexion,"SELECT u.nombres,u.primer_ap ,u.segundo_ap,c.correo FROM correos_usuario c, usuario u, usuario_ponencias up WHERE up.id_ponencias='$Clave'and up.id_usuario=u.id_usuario and u.id_usuario= c.id_usuario and up.tipo_autor!='Autor';");
    $j=0;
    
    
    // Creacion de PDF Autor
    class PDF extends FPDF
                        {
                        // Cabecera de página
                        function Header()
                        {
                            // Logo
                            $this->Image('logo.jpg',0,0,220);
                            // Arial bold 15
                            $this->SetFont('Arial','B',15);
                            // Movernos a la derecha
                            $this->Cell(80);
                            // Título
                            $this->Cell(50,80,utf8_decode('Congreso Matematicas'),30,0,'C');
                            // Salto de línea
                            $this->Ln(50);
                            
                        }
                        
                        // Pie de página
                        function Footer()
                        {
                            // Posición: a 1,5 cm del final
                            $this->SetY(-15);
                            // Arial italic 8
                            $this->SetFont('Arial','I',8);
                            // Número de página
                            $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
                        }
                        }
                        
                        // Creación del objeto de la clase heredada
                        $pdf = new PDF();
                        $pdf->AliasNbPages();
                        $pdf->AddPage();
                        $pdf->SetFont('Times','',12);
                        
                        $pdf->Cell(40,10,utf8_decode('Video de Ponencia Oral Evaluado'),0,1);
                        $pdf->Cell(40,10,utf8_decode(''),0,1);
                        $pdf->Cell(40,10,utf8_decode($Autor.' se ha evaluado el siguiente video para ponencia oral'),0,1);
                        $pdf->Cell(40,10,utf8_decode('Estado del video para ponencia oral: '.$estatusEv),0,1);
                        $pdf->Cell(40,10,utf8_decode('Clave de la ponencia oral: '.$Clave),0,1);
                        $pdf->Multicell(190,5,utf8_decode('Título de la ponencia oral: '.$titulo),0,1);
                        $pdf->Multicell(190,5,utf8_decode('Comentarios: '.$comentarios),0,1);
                        $pdf->Cell(40,10,utf8_decode('Coautores registrados en el trabajo:'),0,1);
                        while($infoCoa=pg_fetch_array($infoCoau)){
                            if(empty($infoCoa)){
                                $pdf->Cell(40,10,utf8_decode('No hay coautores registrados'),0,1); 
                            }
                            $nombreCoa[$j]=trim($infoCoa['nombres']);
                            $apellidoCoa[$j]=trim($infoCoa['primer_ap']);
                            $apellidoCoa2[$j]=trim($infoCoa['segundo_ap']);
                            $corA[$j]=trim($infoCoa['correo']);
                        $pdf->Cell(40,10,utf8_decode($nombreCoa[$j]." ".$apellidoCoa[$j]." ".$apellidoCoa2[$j]." (".$corA[$j].")"),0,1);
                            $j=$j+1;
                           }
                        $pdf->Cell(40,10,utf8_decode('Le pedimos de la manera más atenta, estar al pendiente de su cuenta para conocer'),0,1);
                        $pdf->Cell(40,10,utf8_decode('el estatus de su trabajo.'),0,1);
                        $pdf->Cell(40,10,utf8_decode(''),0,1);
                        
                        $archivoAdjunto = $pdf->Output("", "S");
    
    //Envio de correo Autor
    $mail = new PHPMailer(true);
                        
                        try {
                            //Server settings
                            $mail->SMTPDebug = 0;                      // Enable verbose debug output
                            $mail->isSMTP();                                            // Send using SMTP
                            $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                            $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
                            $mail->Password   = 'CongresoMate2020';                               // SMTP password
                            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                            $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                        
                            //Recipients
                            $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
                            $mail->addAddress("$cor[0]", "$Autor");
                            if(!empty($cor[1])){
                                $mail->addAddress("$cor[1]", "$Autor");
                            }
                            if(!empty($cor[2])){
                                $mail->addAddress("$cor[2]", "$Autor");
                            }
                                            
                            // Content
                            $mail->isHTML(true);                                  // Set email format to HTML
                            $mail->Subject = 'video evaluado';
                            $mail->Body    = 'En el siguiente documento se adjuntan los datos del video evaluado';
                            $mail->addStringAttachment($archivoAdjunto, 'Evaluacion_video_ponencia_oral.pdf');
                            $mail->send();
                            
                        } catch (Exception $e) {
                            echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
                        }
    
    
    
    // Creacion de PDF Coautores 
    if(!empty($corA[0]) || !empty($corA[1])|| !empty($corA[2])||!empty($corA[3])){
                        
                        // Creación del objeto de la clase heredada
                        $pdf = new PDF();
                        $pdf->AliasNbPages();
                        $pdf->AddPage();
                        $pdf->SetFont('Times','',12);
                        
                        $pdf->Cell(40,10,utf8_decode('Evaluacion de video para ponencia oral'),0,1);
                        $pdf->Cell(40,10,utf8_decode(''),0,1);
                        $pdf->Cell(40,10,utf8_decode('El video para el siguiente trabajo ya fue evaluado:'),0,1);
                        $pdf->Cell(40,10,utf8_decode('Estado del video: '.$estatusEv),0,1);
                        $pdf->Cell(40,10,utf8_decode('Clave de la ponencia oral: '.$Clave),0,1);
                        $pdf->Multicell(190,5,utf8_decode('Título de la ponencia oral: '.$titulo),0,1);
                        $pdf->Multicell(190,5,utf8_decode('Comentarios: '.$comentarios),0,1);
                        
                        $pdf->Cell(40,10,utf8_decode(''),0,1);
                        $archivoAdjunto2 = $pdf->Output("", "S");
    
    //Envio de correo coautores
    $mail = new PHPMailer(true);
                        
                        try {
                            //Server settings
                            $mail->SMTPDebug = 0;                      // Enable verbose debug output
                            $mail->isSMTP();                                            // Send using SMTP
                            $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                            $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
                            $mail->Password   = 'CongresoMate2020';                               // SMTP password
                            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                            $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                        
                            //Recipients
                            $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
                            while($infoCoa=pg_fetch_array($infoCoau)){
                               
                                $nombreCoa[$j]=trim($infoCoa['nombres']);
                                $apellidoCoa[$j]=trim($infoCoa['primer_ap']);
                                $apellidoCoa2[$j]=trim($infoCoa['segundo_ap']);
                                $corC[$j]=trim($infoCoa['correo']);
                               
                            
                                $j=$j+1;
                               }
                               if(!empty($corA[0])){
                                $mail->addAddress("$corA[0]",utf8_decode("$nombreCoa[0] $apellidoCoa[0] $apellidoCoa2[0]"));
    
                               }
                               if(!empty($corA[1])){
                                $mail->addAddress("$corA[1]",utf8_decode("$nombreCoa[1] $apellidoCoa[1] $apellidoCoa2[1]"));
    
                               }
                               if(!empty($corA[2])){
                                $mail->addAddress("$corA[2]",utf8_decode("$nombreCoa[2] $apellidoCoa[2] $apellidoCoa2[2]"));
    
                               }
                               if(!empty($corA[3])){
                                $mail->addAddress("$corA[3]",utf8_decode("$nombreCoa[3] $apellidoCoa[3] $apellidoCoa2[3]"));
    
                               }
                        
                            // Attachments
                            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
                        
                            // Content
                            $mail->isHTML(true);                                  // Set email format to HTML
                            $mail->Subject = 'Video evaluado ';
                            $mail->Body    = 'En el siguiente documento se adjuntan los datos del video Evaluado';
                            $mail->addStringAttachment($archivoAdjunto2, 'Evaluacion_Video_ponencia_oral.pdf');
                            $mail->send();
                            
                        } catch (Exception $e) {
                            echo "Error al enviar el mensaje a los coautores: {$mail->ErrorInfo}";
                        }
                    }
        echo"<script>alert('Video evaluado ');window.location='evaluartr.php'</script>";
    
    }

//////////////////////// SE RECIBE EL ID DE LA PONENCIA///////////////////////////
$id_ponencia=$_POST['id_ponencia'];

/////////////////////// SE CONSULTA EL CONGRESO ACTUAL////////////////////////////////
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
$row0=pg_fetch_row($consulta);
$consulta0=$row0[0];
$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
$row=pg_fetch_row($consulta1);
$congresoActual=$row[0];// id del congreso
/////////////////////// SE CONSULTA LA INFORMACION DE CONTACTO DEL CONGRESO ACTUAL/////////////////////////////////
$contacto=pg_query($conexion,"SELECT cn.telefono,cn.telefono2,cn.email FROM contacto cn, congreso c
WHERE c.id_congreso='$congresoActual'
and  c.id_contacto =cn.id_contacto");
$contactos=pg_fetch_array($contacto);
$correoC=@$contactos[2];
$numeroC1=@$contactos[0];
$numeroC2=@$contactos[1];
////////////////////// SE CONSULTA LA URL DEL VIDEO DE LA PONENCIA SELECCIONADA ////////////////////////////////////
$url=pg_query($conexion,"SELECT url_video, estatus_video,titulo FROM ponencias WHERE id_congreso='$congresoActual' and id_ponencia='$id_ponencia'");
$row1=pg_fetch_array($url);
if(!empty($row1['url_video'])){
	$urlp=$row1['url_video'];
	$estatus_v=$row1['estatus_video'];
	//////////////////// SE VALIDA SI EXISTE UNA ACTUALIZACION //////////////
	$ponenciaAc=pg_query($conexion,"SELECT titulo  FROM actualizacion_resumen ac, (SELECT MAX (numeracion) as maximo FROM actualizacion_resumen WHERE id_ponencia='$id_ponencia' and id_congreso='$congresoActual') m WHERE ac.id_ponencia='$id_ponencia' and ac.id_congreso='$congresoActual' and ac.numeracion = m.maximo ");
	$tituloAc=pg_fetch_array($ponenciaAc);
	//////SI LA CONSULTA DE ACTUALIZACIONES ESTA VACIA (QUE NO HAY ACTUALIZACIONES) EL TITULO DE LA PONENCIA SERÁ EL QUE SE HAYA REGISTRADO POR PRIMERA VEZ
	////// SI EXISTE UNA ACTUALIZACION SE ESCOJE LA ULTIMA ACTUALIZACION Y SE UTILIZA ESE TITULO, AL SER EL MAS RECIENTE.
	
	if(!empty($tituloAc['titulo'])){
		$titulo=$tituloAc['titulo'];
	}else{
		$titulo=$row1['titulo'];
	}
		///////////////////SE CONSUTLA AL AUTOR DEL TRABAJO//////////////////////////////////
		$datosAutor=pg_query($conexion,"SELECT u.nombres,u.primer_ap,u.segundo_ap FROM usuario u, usuario_ponencias up WHERE up.id_ponencias='$id_ponencia'
										 and up.tipo_autor='Autor' and up.id_congreso='$congresoActual' and up.id_usuario=u.id_usuario");
		$row2=pg_fetch_array($datosAutor);
		$n=trim($row2['nombres']);
		$p=trim($row2['primer_ap']);
		$m=trim($row2['segundo_ap']);
		$Autor="$n $p $m";
	
}else{
	//////////////////////SI NO HAY UN VIDEO REGISTRADO MANDA A LA PANTALLA DE TRABAJOS A EVALUAR//////////////////
	echo"<script>alert('No existe un video registrado para este trabajo');window.location='evaluartr.php'</script>";
	
	
}
//// SE RECIBE EL ESTATUS Y LOS COMENTARIOS DE LA EVALUACION


?>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title style="background-image: url('images/logos.jpg');">CISEMATH</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/cismath.png">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" />
	<script
	src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>


    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
      <?php
    ////Codigo para actualizar el banner
        include ('Conexion.php');
        $nmax=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
        $num=pg_fetch_row($nmax);
        $numcon=$num[0];
        $conact=pg_query($conexion,"SELECT id_congreso, banner FROM congreso WHERE numero_congreso='$numcon'");
        $ban=pg_fetch_row($conact);
        $banner=$ban[1];
        echo '<img src="'.$banner.'" style="width:100%; height:100%; background-size: cover; position: relative; display: flex;"/>';
    ?>

    <header class="top-header" >
        <div class="header_bottom">
          <div class="container">
		   <div class="row">
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 col-xl-12">
                <div class="menu_orange_section" style="background: #052450;">
                   <nav class="navbar header-nav navbar-expand-lg"> 
                     <div class="menu_section">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav" >
					    <li><a class="nav-link active" href="indexus.php"><img src="images/i4.png" alt="image"></a> </li>
                        <li><a class="nav-link active" href="acercadeus.php">Acerca de</a></li>
                        <li><a class="nav-link active" href="infus.php">Información</a></li>
                        <li><a class="nav-link active" href="instruccionesus.php">Instrucciones</a></li>
                        <li><a class="nav-link active" href="comitesus.php">Comités</a></li>
						<li><a class="nav-link active" href="memoriasus.php">Memorias</a></li>
						
						<li>
						<a style="position:relative; display:block;">
						<img src="images/user.png" alt="image"> 
						</a>
						</li>
						<li>
						<ul class="nav"><li class="dropdown langmenu" ><a href="#" style="display:block;"class="dropdown-toggle" data-toggle="dropdown" title="Idioma"><b class="caret"></b></a><ul class="dropdown-menu" style="color:black;"><li><a title="English ‎(en)‎" href="menu.php" style="color:black";>Menú‎‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="editarp.php"  style="color:black";>Perfil‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="index.php"  style="color:black";>Cerrar Sesión</a></li></ul></li></ul>
						</li>
						
                    </ul>
                </div>
                     </div>
                 </nav>
                 
                </div>
            </div>
          </div>
		  </div>
        </div>
        
    </header>
    <!-- End header -->
 
 <!-- Start CONTENIDO -->
	
	 <div class="container">
	    <div class="row">
	        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
	     
				<div class="titulo" style="margin-top:8%">
				<font style="font-size: 17px; font-weight: 300;">
	            <h1 align="center" style="color: #052450;">EVALUACIÓN DE VÍDEO: PONENCIAS ORALES</h1>
	            </div>
	        </div>
		</div>	 
	</div>	
    <div class="container" style="margin-top:3%; margin-bottom:3%;">	 
		<div class="row"  >
			<div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"  >
					<div class="field">
						<strong>
						Información del video<br></strong>
						Autor:<br>
						<input type="text" class="form-control" value="<?php echo $Autor ?>"  style="width:80%;" >
					</div>
					
					<div class="field">
						Título del trabajo:<br>
						<input type="" class="form-control" style="width:80%; " value="<?php echo $titulo ?>" name="titulot" id="titulot" maxlength="4" minlength="4" required=""  >
					</div>
					
					<div class="field">
						Clave:<br>
						<input type="text"  class="form-control" style="width:80%;"  value="<?php echo $id_ponencia ?>">
					</div>
		
					<div class="field">
						Vídeo:<br>
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" width="560" height="315"  src="<?php echo $urlp ?>" allowfullscreen></iframe>
						</div>
					</div>
			</div>	
			<div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6" > 
			<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="post">
            <input type="hidden" name="id_ponencia" value="<?php echo $id_ponencia?>">
            <input type="hidden" name="id_congreso" value="<?php echo $congresoActual?>">
            <input type="hidden" name="titulo" value="<?php echo $titulo?>">
					<div class="field">
						<strong>
						Elija la opción a su criterio:<br>
						</strong>
						<input type="radio" id="male" name="gender" value="Aceptado">
						<label for="male">Aceptar</label><br>
						<input type="radio" id="male" name="gender" value="Con Modificaciones">
						<label for="male">Modificar</label><br>
						<input type="radio" id="male" name="gender" value="Rechazado">
						<label for="male">Rechazar</label><br>
					</div>
					<div class="field">
						<strong>
						Escriba sus comentarios u observaciones: <br>
						</strong>
						<textarea name="comentarios" id="inst" rows="5" cols="30"></textarea>								    
					</div>	
			</div>
		</div>
		<div class="btn" style="text-align:right; width:100%; margin-bottom: 5%; margin-top: 2%;">
				<input type="submit" value="Evaluar" name="submit1" style="margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;"></form>
				<input type="submit" value="Cancelar" onclick='location.href=evaluartr.php' style="background: whitesmoke; color: black; padding: 6px 10px; border-color:whitesmoke ;border-radius: 20px;">
		</div>
	</div>
	
		 
		
	
	

    <!-- End CONTENIDO -->
    
     <!-- Start Footer -->
        <footer class="footer-box">
        <div class="container">
            <div class="row">
               <div class="col-md-12 white_fonts" style="display: contents;">
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            
                            <div class="full">
							<h3>Dirección</h3>
                                <ul class="menu_footer">   
								     
								     <li><img src="images/i5.png"><span style="font-size: 85%;">Carr, Cuautitlán - Teoloyucan, San Sebastian Xhala, 54714 Cuautitlán Izcalli</span></li>

                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="full">
                                <div class="footer_blog full white_fonts">
                             <h3>Contacto</h3>
                             <ul class="full">
                               <li><img src="images/i6.png"><span style="font-size: 85%;"> <?php echo $correoC  ?></span></li>
                               <li><img src="images/i7.png"><span style="font-size: 85%;"><?php echo "$numeroC1    $numeroC2"; ?></span></li>
                             </ul>
                         </div>
                            </div>
                        </div>
                </div>
			 </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link active" href="derechosa.php" style="color:white">© Sitio desarrollado por CDD Solutions S.A. y administrado por el Departamento de Matemáticas. Hecho en México, (2020 - 2021)</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
	/* counter js */

(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
	</script>
</body>

</html>