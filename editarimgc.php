<?php
session_start();
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
$usuario=$_SESSION['Usuario'];
include ('Conexion.php');
	//VALIDAR FECHA PARA EL PODER EDITAR 	
	date_default_timezone_set("America/Mexico_City");
	$date_nueva=date("d-m-Y");	
	// FUNCION PARA VALIDAR FECHA CONCURSO DE CARTELES
function verifica_rango_Editar($date_inicio, $date_fin, $date_nueva) {
    $date_inicio = strtotime($date_inicio);
    $date_fin = strtotime($date_fin);
    $date_nueva = strtotime($date_nueva);
    if (($date_nueva >= $date_inicio) && ($date_nueva <= $date_fin)){
        return true;//SI SE ENCUENTRA DENTRO DEL RANGO
    }
        
    return false;//SI NO SE ENCUENTRA DENTRO DEL RANGO
 }

//************************


if(isset($_POST['id_ponencia_cartelA'])){
		$id_ponencia_cartelA=$_POST['id_ponencia_cartelA'];
		$idPonencia=$id_ponencia_cartelA;
	}elseif (isset($_POST['id_ponencia_cartelN1'])) {
	
		$id_ponencia_cartelN1=$_POST['id_ponencia_cartelN1'];
		$idPonencia=$id_ponencia_cartelN1;

	}elseif (!isset($_POST['id_ponencia_cartelA'])&& !isset($_POST['id_ponencia_cartelN1'])) {
		$idPonencia="";
	}
	if($idPonencia=="" && empty($_GET['id_imagen'])){
	echo"<script>alert('Selecciona un trabajo');window.location='trabr.php'</script>";
}

//******************************************************************************************************************************************************************************************
//TRAE EL NÚMERO DE LA CONGRESO
	$consulta_num_congreso=pg_query($conexion,"SELECT MAX(numero_congreso) FROM congreso");
	$row0=pg_fetch_row($consulta_num_congreso);
	$num_congreso=$row0[0];

//TRAE EL ID DE LA CONGRESO CON AYUDA DE SU NUMERO
	$consulta_id_congreso=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$num_congreso'"); 
	$row1=pg_fetch_row($consulta_id_congreso);
	$id_congresoactual=$row1[0];
//FECHAS PARA EDITAR
$FechasI=pg_query($conexion,"SELECT f.fecha_inicio, f.fecha_fin FROM (SELECT (id_congreso) as id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso) C, fechas_importantes f
			WHERE f.id_congreso= C.id_congreso and f.descripcion= 'Recepción de Carteles' or f.descripcion= 'Recepción de Imagen para Carteles' or f.descripcion= 'Recepción de Imágenes'");// trae las fechas de la tabla Fechas importantes
			$row=pg_fetch_row($FechasI);
			$date_inicio=$row[0];
			$date_fin=$row[1];	
//TRAE LAS FECHAS EN QUE SE PUEDE EDITAR DESPUES DE SER EVALUADO
	$FechasObs=pg_query($conexion,"SELECT f.fecha_inicio, f.fecha_fin FROM (SELECT (id_congreso) as id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso) C, fechas_importantes f
			WHERE f.id_congreso= C.id_congreso and f.descripcion= 'Notificación de observaciones de las imágenes de los Carteles'");
			$row=pg_fetch_row($FechasObs);
			$date_inicioObservacion=$row[0];
			$date_finObservacion=$row[1];	
//CUENTA LAS IMAGENES O LAS  ACTUALIZACIONES DE LAS IMAGENES PARA DECIDIR SI USAR COMBO O CAMPO DE TEXTO
	$consultaNumImagenes=pg_query($conexion, "SELECT COUNT(*) id_ponencia_cartel FROM ponencia_cartel AS pc, ponencias AS p, usuario_ponencias as up
	WHERE pc.id_congreso='$id_congresoactual' and  p.id_tipo_ponencia = 'CA' and p.id_ponencia = pc.id_ponencia_cartel
	and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = pc.id_congreso");
	$numImgC=pg_fetch_assoc($consultaNumImagenes);
	
	$numeroAC=pg_query($conexion,"SELECT  COUNT(id_actualizacion)
	FROM actualizacion_cartel AS apc, ponencia_cartel AS pc, ponencias AS p, usuario_ponencias as up
	WHERE pc.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'CA' and p.id_ponencia = pc.id_ponencia_cartel
	and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = pc.id_congreso and pc.id_ponencia_cartel = apc.id_ponencia_cartel");
	$rowNAc=pg_fetch_row($numeroAC);

	$consultaNumCarteles=pg_query($conexion,"SELECT id_ponencia_cartel FROM ponencia_cartel AS pc, ponencias AS p, usuario_ponencias as up
	WHERE pc.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'CA' and p.id_ponencia = pc.id_ponencia_cartel
	and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = pc.id_congreso");

	$consultaNumActImg=pg_query($conexion,"SELECT  MAX ( id_actualizacion) as id_ultima_actualizacion, apc.id_ponencia_cartel 
	FROM actualizacion_cartel AS apc, ponencia_cartel AS pc, ponencias AS p, usuario_ponencias as up
	WHERE pc.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'CA' and p.id_ponencia = pc.id_ponencia_cartel
	and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = pc.id_congreso and pc.id_ponencia_cartel = apc.id_ponencia_cartel
	GROUP BY apc.id_ponencia_cartel");
	$numActImg=pg_fetch_assoc($consultaNumActImg);
	$banderatipo="0";	
	//PARA EL COMBOBOX
	if($numImgC['id_ponencia_cartel']>1){//SI ME TRAE ALGO DE LAS ACTUALIZACIONES
		$banderatipo="1";
	}
/***/if ($numImgC['id_ponencia_cartel']==1 && empty($numActImg) ||  $rowNAc[0]==1 && empty($numImgC)) {//SI  HAY UNA IMAGEN PERO HAY 0 ACTUALIZACIONES
		//CONSULTA PARA EL CAMPO DE TEXTO PARA (Traer datos sabiendo que no hay actualizaciones)
		$consulta_imagen2=pg_query($conexion,"SELECT pc.id_ponencia_cartel, p.titulo, pc.estatus_imagen, pc.imagen, up.tipo_autor, pc.observaciones FROM ponencia_cartel AS pc, ponencias AS p, usuario_ponencias as up WHERE up.id_usuario ='$usuario'and up.id_congreso ='$id_congresoactual' and pc.id_ponencia_cartel= p.id_ponencia and up.id_ponencias = p.id_ponencia");
			$infoImagen=pg_fetch_assoc($consulta_imagen2);
			$imagen=$infoImagen['imagen'];
			
			$estatus=$infoImagen['estatus_imagen'];
			$id_ponencia_cartel=$infoImagen['id_ponencia_cartel'];
			$tipo_autor=$infoImagen['tipo_autor'];
			$observacionesA=$infoImagen['observaciones'];
			if(empty($observacionesA)){

							$comentario='No hay comentario hasta el momento.';
						}else{
							$comentario=$observacionesA;
						}	
			//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
			$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_ponencia_cartel' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
			$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);
							
			if(!empty($actualizacionTitulo)){
				$titulo=$actualizacionTitulo['titulo'];
			}else{
				$titulo=$infoImagen['titulo'];
			}
			//*************************************************	
if($rowNAc[0]==1){
	$consulta_id_actualizacion_imagen=pg_query($conexion, "SELECT MAX(id_actualizacion), id_ponencia_cartel FROM actualizacion_cartel WHERE id_congreso='$id_congresoactual' AND id_ponencia_cartel = '$idPonencia' GROUP BY id_ponencia_cartel");//ULTIMA ACTUALIZACION DE LA IMAGEN
	$row2I=pg_fetch_row($consulta_id_actualizacion_imagen);
	$numeroidI=$row2I[0];
	$actualizacion_imagen2=pg_query($conexion,"SELECT apc.estatus_actualizacion, p.titulo, apc.imagen, apc.id_ponencia_cartel, apc.observaciones, apc.id_actualizacion, up.tipo_autor FROM actualizacion_cartel as apc, ponencias as p, ponencia_cartel as pc, usuario_ponencias as up 
						WHERE  pc.id_ponencia_cartel = '$idPonencia' and p.id_ponencia = pc.id_ponencia_cartel  and apc.id_actualizacion = '$numeroidI' and p.id_ponencia=up.id_ponencias and up.id_usuario='$usuario' AND apc.id_congreso='$id_congresoactual'");
						$rowActualizacionImagen2=pg_fetch_assoc($actualizacion_imagen2);
						$id_ponencia_cartel=$rowActualizacionImagen2['id_ponencia_cartel'];
						
						$estatus=$rowActualizacionImagen2['estatus_actualizacion'];
						$imagen=$rowActualizacionImagen2['imagen'];
						$observacionesA=$rowActualizacionImagen2['observaciones'];
						$tipo_autor=$rowActualizacionImagen2['tipo_autor'];

						
						if(empty($observacionesA)){
							$comentario='No hay comentario hasta el momento.';
						}else{
							
							$comentario=$observacionesA;
						}
				
				//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
			$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_ponencia_cartel' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
			$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);
							
			if(!empty($actualizacionTitulo)){
				$titulo=$actualizacionTitulo['titulo'];
			}else{
				$titulo=$rowActualizacionImagen2['titulo'];
			}
			//*************************************************	
	} 

}
//OBTENER LO QUE SELECCIONA DEL COMBO Y LO PONE EN EL CAMPO DE TEXTO CORRESPONDIENTE
if(isset($_GET['id_imagen']) && empty($idPonencia) && $idPonencia==""){
	
		$id_imagenSeleccionada=$_GET['id_imagen'];
		
	if(!empty($numActImg)){
			$consulta_id_actualizacion_imagen=pg_query($conexion, "SELECT MAX(id_actualizacion), id_ponencia_cartel FROM actualizacion_cartel WHERE id_congreso='$id_congresoactual' AND id_ponencia_cartel = '$id_imagenSeleccionada' GROUP BY id_ponencia_cartel");//ULTIMA ACTUALIZACION DE LA IMAGEN
													$row2I=pg_fetch_row($consulta_id_actualizacion_imagen);
													
			
			if(@$row2I[0]!==NULL || !empty($row2I[0])){
					$numeroidI=$row2I[0];
					$idponencia_cartelActualizadaI=$row2I[1];			
									
					$actualizacion_imagen2=pg_query($conexion,"SELECT apc.estatus_actualizacion, p.titulo, apc.imagen, apc.id_ponencia_cartel, apc.observaciones, apc.id_actualizacion, up.tipo_autor FROM actualizacion_cartel as apc, ponencias as p, ponencia_cartel as pc, usuario_ponencias as up 
						WHERE  pc.id_ponencia_cartel = '$id_imagenSeleccionada' and p.id_ponencia = pc.id_ponencia_cartel  and apc.id_actualizacion = '$numeroidI' and p.id_ponencia=up.id_ponencias and up.id_usuario='$usuario' AND apc.id_congreso='$id_congresoactual'");
				
						$rowActualizacionImagen2=pg_fetch_assoc($actualizacion_imagen2);
						$id_ponencia_cartel=$rowActualizacionImagen2['id_ponencia_cartel'];
						
						$estatus=$rowActualizacionImagen2['estatus_actualizacion'];
						$imagen=$rowActualizacionImagen2['imagen'];
						$observacionesA=$rowActualizacionImagen2['observaciones'];
						$tipo_autor=$rowActualizacionImagen2['tipo_autor'];
						if(empty($observacionesA)){
							$comentario='No hay comentario hasta el momento';
						}else{
							
							$comentario=$observacionesA;
						}


				//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
			$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_ponencia_cartel' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
			$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);
							
			if(!empty($actualizacionTitulo)){
				$titulo=$actualizacionTitulo['titulo'];
			}else{
				$titulo=$rowActualizacionImagen2['titulo'];
			}
			//*************************************************	
				
			
			}else{

			$imangen2=pg_query($conexion,"SELECT  id_ponencia_cartel, estatus_imagen, imagen, p.titulo, up.tipo_autor, pc.observaciones FROM ponencia_cartel AS pc, ponencias AS p, usuario_ponencias as up
					WHERE pc.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'CA' and p.id_ponencia = pc.id_ponencia_cartel
					and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = pc.id_congreso and pc.id_ponencia_cartel='$id_imagenSeleccionada'");
			
					$rowImagen2=pg_fetch_assoc($imangen2);
					$id_ponencia_cartel=$rowImagen2['id_ponencia_cartel'];
						
						$estatus=$rowImagen2['estatus_imagen'];
						$imagen=$rowImagen2['imagen'];
						$observacionesA=$rowImagen2['observaciones'];
						if(empty($observacionesA)){
							$comentario='No hay comentario hasta el momento.';
						}else{
							
							$comentario=$observacionesA;
						}
						$tipo_autor=$rowImagen2['tipo_autor'];
			

				//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
			$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_ponencia_cartel' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
			$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);
							
			if(!empty($actualizacionTitulo)){
				$titulo=$actualizacionTitulo['titulo'];
			}else{
				$titulo=$rowImagen2['titulo'];
			}
			//*************************************************	
				

			}
		
	}else{

			$imangen2=pg_query($conexion,"SELECT  id_ponencia_cartel, estatus_imagen, imagen, p.titulo, up.tipo_autor, pc.observaciones FROM ponencia_cartel AS pc, ponencias AS p, usuario_ponencias as up
					WHERE pc.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'CA' and p.id_ponencia = pc.id_ponencia_cartel
					and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = pc.id_congreso and pc.id_ponencia_cartel='$id_imagenSeleccionada'");
				

					$rowImagen2=pg_fetch_assoc($imangen2);
					$id_ponencia_cartel=$rowImagen2['id_ponencia_cartel'];
						
						$estatus=$rowImagen2['estatus_imagen'];
						$imagen=$rowImagen2['imagen'];
						$observacionesA=$rowImagen2['observaciones'];
						if(empty($observacionesA)){
							$comentario='No hay comentario hasta el momento.';
						}else{
							
							$comentario=$observacionesA;
						}
						$tipo_autor=$rowImagen2['tipo_autor'];
			
				//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
			$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_ponencia_cartel' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
			$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);
							
			if(!empty($actualizacionTitulo)){
				$titulo=$actualizacionTitulo['titulo'];
			}else{
				$titulo=$rowImagen2['titulo'];
			}
			//*************************************************	

			}
	if($id_imagenSeleccionada==""){
						$id_ponencia_cartel="";
						$titulo="";
						$estatus="";
						$imagen="";
						$comentario="";
						 echo"<script>alert('Selecciona el titulo del cartel que deseas visualizar.');window.location='trabr.php'</script>";
				
	}
}else if ($idPonencia !="" && empty($_GET['id_imagen']) && isset($idPonencia)) { //REFERENTE A LO QUE TREA DE LA LISTA DE TRABAJOS PAG trabr.php
	 
		if(!empty($numActImg)){
			$consulta_id_actualizacion_imagen=pg_query($conexion, "SELECT MAX(id_actualizacion), id_ponencia_cartel FROM actualizacion_cartel WHERE id_congreso='$id_congresoactual' AND id_ponencia_cartel = '$idPonencia' GROUP BY id_ponencia_cartel");//ULTIMA ACTUALIZACION DE LA IMAGEN
													$row2I=pg_fetch_row($consulta_id_actualizacion_imagen);
													
			
			if(@$row2I[0]!==NULL || !empty($row2I[0])){
							$numeroidI=$row2I[0];
													$idponencia_cartelActualizadaI=$row2I[1];		////////////////////////////////////////////////////SI NO EXISTE UNA ACTUALIZACION SE TRAEN LOS DATOS DE LA TABLA PONENCIAS//////////////////////////
									
					$actualizacion_imagen2=pg_query($conexion,"SELECT apc.estatus_actualizacion, p.titulo, apc.imagen, apc.id_ponencia_cartel, apc.observaciones, apc.id_actualizacion, up.tipo_autor FROM actualizacion_cartel as apc, ponencias as p, ponencia_cartel as pc, usuario_ponencias as up 
						WHERE  pc.id_ponencia_cartel = '$idPonencia' and p.id_ponencia = pc.id_ponencia_cartel  and apc.id_actualizacion = '$numeroidI' and p.id_ponencia=up.id_ponencias and up.id_usuario='$usuario' AND apc.id_congreso='$id_congresoactual'");
					
						$rowActualizacionImagen2=pg_fetch_assoc($actualizacion_imagen2);
						$id_ponencia_cartel=$rowActualizacionImagen2['id_ponencia_cartel'];
						
						$estatus=$rowActualizacionImagen2['estatus_actualizacion'];
						$imagen=$rowActualizacionImagen2['imagen'];
						$observacionesA=$rowActualizacionImagen2['observaciones'];
						$tipo_autor=$rowActualizacionImagen2['tipo_autor'];
						if(empty($observacionesA)){
							$comentario='No hay comentario hasta el momento';
						}else{
							
							$comentario=$observacionesA;
						}
					
						//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
					$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_ponencia_cartel' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
					$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);
									
					if(!empty($actualizacionTitulo)){
						$titulo=$actualizacionTitulo['titulo'];
					}else{
						$titulo=$rowActualizacionImagen2['titulo'];
					}
					//*************************************************	
			
			}else{

			$imangen2=pg_query($conexion,"SELECT  id_ponencia_cartel, estatus_imagen, imagen, p.titulo, up.tipo_autor, pc.observaciones FROM ponencia_cartel AS pc, ponencias AS p, usuario_ponencias as up
					WHERE pc.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'CA' and p.id_ponencia = pc.id_ponencia_cartel
					and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = pc.id_congreso and pc.id_ponencia_cartel='$idPonencia'");
					
					$rowImagen2=pg_fetch_assoc($imangen2);
					$id_ponencia_cartel=$rowImagen2['id_ponencia_cartel'];
						
						$estatus=$rowImagen2['estatus_imagen'];
						$imagen=$rowImagen2['imagen'];
						$observacionesA=$rowImagen2['observaciones'];
						if(empty($observacionesA)){
							$comentario='No hay comentario hasta el momento.';
						}else{
							
							$comentario=$observacionesA;
						}
						$tipo_autor=$rowImagen2['tipo_autor'];

						//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
					$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_ponencia_cartel' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
					$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);
									
					if(!empty($actualizacionTitulo)){
						$titulo=$actualizacionTitulo['titulo'];
					}else{
						$titulo=$rowImagen2['titulo'];
					}
					//*************************************************	
		}	

	}else{

			$imangen2=pg_query($conexion,"SELECT  id_ponencia_cartel, estatus_imagen, imagen, p.titulo, up.tipo_autor, pc.observaciones FROM ponencia_cartel AS pc, ponencias AS p, usuario_ponencias as up
					WHERE pc.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'CA' and p.id_ponencia = pc.id_ponencia_cartel
					and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = pc.id_congreso and pc.id_ponencia_cartel='$idPonencia'");
					
					$rowImagen2=pg_fetch_assoc($imangen2);
					$id_ponencia_cartel=$rowImagen2['id_ponencia_cartel'];
						
						$estatus=$rowImagen2['estatus_imagen'];
						$imagen=$rowImagen2['imagen'];
						$observacionesA=$rowImagen2['observaciones'];
						if(empty($observacionesA)){
							$comentario='No hay comentario hasta el momento.';
						}else{
							
							$comentario=$observacionesA;
						}
						$tipo_autor=$rowImagen2['tipo_autor'];


						//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
					$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_ponencia_cartel' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
					$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);
									
					if(!empty($actualizacionTitulo)){
						$titulo=$actualizacionTitulo['titulo'];
					}else{
						$titulo=$rowImagen2['titulo'];
					}
					//*************************************************	

		}	
}

$banderaFecha="";
//CONSULTA SI HAY EVALUADOR ASIGNADO
	$consultaValidacionEvaluador=pg_query($conexion, "SELECT * FROM usuario_evalua_cartel WHERE id_congreso='$id_congresoactual' AND id_ponencia_cartel='$id_ponencia_cartel'");
	$numValidacion=pg_num_rows($consultaValidacionEvaluador);
//VALIDACIÓN DE LAS FECHAS PARA EDITAR:
	if($estatus!='Aceptado' || $estatus!='Rechazado' || $estatus!='En Evaluacion'){
		if(verifica_rango_Editar($date_inicio, $date_fin, $date_nueva)==false){
			$banderaFecha="0";
		}else if(verifica_rango_Editar($date_inicio, $date_fin, $date_nueva)==true){ 
			if($numValidacion!=0){//SI NO TIENE ASIGNADO EVALUADOR
				$banderaFecha="1";//SI SE ENCUENTRA DENTRO DE LAS FECHAS
				}else{
				$banderaFecha="0";
			}
		}
	}	

//VALIDACION DE FECHA PARA EDITAR DESPUES DE SER EVALUADO
	if($estatus!='Aceptado' || $estatus!='Rechazado' || $estatus!='En Evaluacion'){
		if(verifica_rango_Editar($date_inicioObservacion, $date_finObservacion, $date_nueva)==false){
			$banderaFecha="0";
		}else if(verifica_rango_Editar($date_inicioObservacion, $date_finObservacion, $date_nueva)==true){ 
			$banderaFecha="1";//SI SE ENCUENTRA DENTRO DE LAS FECHAS
		}
	}
?>
<?php 
include ("Conexion.php");
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
					$row0=pg_fetch_row($consulta);
					$consulta0=$row0[0];
					$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
					$row=pg_fetch_row($consulta1);
					$congresoActual=$row[0];//es el id congreso
$contacto=pg_query($conexion,"SELECT cn.telefono,cn.telefono2,cn.email FROM contacto cn, congreso c
WHERE c.id_congreso='$congresoActual'
and  c.id_contacto =cn.id_contacto");
$contactos=pg_fetch_array($contacto);
$correoC=@$contactos[2];
$numeroC1=@$contactos[0];
$numeroC2=@$contactos[1];
?><!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title style="background-image: url('images/logos.jpg');">CISEMATH</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/cismath.png">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" />

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98" onload="validarEstatus(); asignaridCombo();">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
      <?php
    ////Codigo para actualizar el banner
        include ('Conexion.php');
        $nmax=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
        $num=pg_fetch_row($nmax);
        $numcon=$num[0];
        $conact=pg_query($conexion,"SELECT id_congreso, banner FROM congreso WHERE numero_congreso='$numcon'");
        $ban=pg_fetch_row($conact);
        $banner=$ban[1];
        echo '<img src="'.$banner.'" style="width:100%; height:100%; background-size: cover; position: relative; display: flex;"/>';
    ?>

    <header class="top-header" >
        <div class="header_bottom">
          <div class="container">
		  <div class="row"> 
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 col-xl-12">
                <div class="menu_orange_section" style="background: #052450;">
                   <nav class="navbar header-nav navbar-expand-lg"> 
                     <div class="menu_section">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav" >
					    <li><a class="nav-link active" href="indexus.php"><img src="images/i4.png" alt="image"></a> </li>
                        <li><a class="nav-link active" href="acercadeus.php">Acerca de</a></li>
                        <li><a class="nav-link active" href="infus.php">Información</a></li>
                        <li><a class="nav-link active" href="instruccionesus.php">Instrucciones</a></li>
                        <li><a class="nav-link active" href="comitesus.php">Comités</a></li>
						<li><a class="nav-link active" href="memoriasus.php">Memorias</a></li>
						
						<li>
						<a style="position:relative; display:block;">
						<img src="images/user.png" alt="image"> 
						</a>
						</li>
						<li>
						<ul class="nav"><li class="dropdown langmenu" ><a href="#" style="display:block;"class="dropdown-toggle" data-toggle="dropdown" title="Idioma"><b class="caret"></b></a><ul class="dropdown-menu" style="color:black;"><li><a title="English ‎(en)‎" href="menu.php" style="color:black";>Menú‎‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="editarp.php"  style="color:black";>Editar Perfil‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="ac.php"  style="color:black";>Agregar congreso‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="index.php"  style="color:black";>Cerrar Sesión</a></li></ul></li></ul>
						</li>
						
                    </ul>
                </div>
                     </div>
                 </nav>
                 
                </div>
            </div>
          </div>
		  </div>
        </div>
        
    </header>
    <!-- End header -->
 
 <!-- Start CONTENIDO -->
 
 <form action="actualizar-imgcartel.php" method="post" enctype="multipart/form-data">	 
	<div class="container">
		
		<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"> 
						
			<h1 align="center" style="margin-top:8%; color:#052450;">EDITAR IMAGEN CARTEL<h1>
				
		</div>
		
	<div class="row">
			
		<div class="col-12 col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8"> 
			
			<table cellpadding="10px" cellspacing="8px" align="center" style="text-align:left; font-size:18px;">
				<input type='hidden' name='banderaID' id='banderaID' value='<?php echo $id_ponencia_cartel?>'>
				<input type='hidden' name='banderatipo' id='banderatipo' value='<?php echo $banderatipo?>'>
				<input type='hidden' name='banderaFecha' id='banderaFecha' value='<?php echo $banderaFecha?>'>
				<tr>
					<td>
						Status:
					</td>

					<td>
						<?php	
						
					if (isset($rowActualizacionImagen2)|| isset($rowImagen2)|| isset($infoImagen) || isset($idPonencia)){ ?>	

							<input type="text" class="form-control" name="status" id="status"  value="<?php echo $estatus ?>" readonly="readonly">
							<input  name="tipo_autor" id="tipo_autor" type="hidden" value="<?php echo $tipo_autor?>">
						<?php }else{?>
							<input type="text" class="form-control" name="status" id="status"  value="" readonly="readonly">
				<?php } ?>	
					</td>
		
				</tr>
				
				<tr>
					<td>

						Título del trabajo:
					</td>
						
					<td>
						<?php 
			if($numImgC['id_ponencia_cartel']>1 && $rowNAc[0]>=1 || $numImgC['id_ponencia_cartel']>1 && $rowNAc[0]==0){//SI HAY MUCHOS CARTELES Y HAY ACTUALIZACIONES DE IMAGENES SE PONEN EL COMBO*************+
							?>
						<select class="form-control" id="idtituloTrabajo" name="tituloTrabajo" required="" title="SELECCIONE UN TRABAJO, POR FAVOR"  onchange="selecionaTitulo();" style="width:65%;">

							<option value="" >Selecciona el titulo de tu trabajo: </option>

							<?php
							$consulta_imagen=pg_query($conexion,"SELECT  pc.id_ponencia_cartel, p.titulo FROM ponencia_cartel AS pc, ponencias AS p, usuario_ponencias as up WHERE pc.id_congreso='$id_congresoactual' and p.id_tipo_ponencia = 'CA' and p.id_ponencia = pc.id_ponencia_cartel
								and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = pc.id_congreso");

							while ($rowImg=pg_fetch_assoc($consulta_imagen)) {
								$id_ponencia_cartelI=$rowImg['id_ponencia_cartel'];
								//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
								$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_ponencia_cartelI' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
								$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);
												
								if(!empty($actualizacionTitulo)){
									$titulo=$actualizacionTitulo['titulo'];
								}else{
									$titulo=$rowImg['titulo'];
								}
								//*************************************************	

								?>
								<option value="<?php echo $id_ponencia_cartelI ?>"><?php echo $titulo ?></option>
							<?php
							}
						
							?>
						</select>
					</td>
					</tr>
			<?php  
			
			}else if($numImgC['id_ponencia_cartel']==1 && $rowNAc[0]==0  && empty($_GET['id_imagen'])|| $rowNAc[0]==1 && empty($numImgC) && empty($_GET['id_imagen']) || $idPonencia !="" && $idPonencia==$numImgC['id_ponencia_cartel'] && empty($_GET['id_imagen']) || $numActImg["id_ponencia_cartel"]==$idPonencia && empty($_GET['id_imagen'])){//SI HAY 1 EXTENO PERO 0 ACTUALIZACIONES O AL REVES
			
			 //$titulo=$rowActualizacionExtenso2['titulo'];
		?>
				<input type="text" class="form-control" name="titulo_text" id="titulo_text" value="<?php echo $titulo ?>" readonly="readonly">
		<?php
			}
		?>		




				<tr>
					<td>
						Clave:
					</td>

					<td><?php
					if (isset($rowActualizacionImagen2)|| isset($rowImagen2)|| isset($infoImagen) || isset($idPonencia)){?>

							<input type="text" class="form-control" name="Clave" id="Clave" value="<?php echo $id_ponencia_cartel ?>" readonly="readonly">
					<?php }else{?>
							<input type="text" class="form-control" name="Clave" id="Clave" value="" readonly="readonly">
				<?php } ?>
				       </td>
		
				</tr>
				
				<tr>
				
					<td>
						
						Archivo actual:	
						
					</td>
					
					<td>         				
						<?php	if (isset($rowActualizacionImagen2)|| isset($rowImagen2)|| isset($infoImagen) || isset($idPonencia)){
						if($imagen!=""){
							
						$nombreArchivo= explode("/", $imagen);
						}else{
							$nombreArchivo[1]="";	
						}
						?> 

						<input type="text" class="form-control" name="archivoActual" id="archivoActual" value="<?php echo $nombreArchivo[1] ?>" readonly="readonly">
						<input  name="imagen" id= "imagen" type="hidden" value="<?php echo $imagen?>">
         				<?php }else{?>

							<input type="text" class="form-control" name="archivoActual" id="archivoActual" value="" readonly="readonly">
				<?php } ?>
					</td>
					<td>
						<input type="button" value="Editar" id="Editar" onclick="cambiarArchivo();borrarArchivoAnterior();" style=" margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;">
					</td>
				
				</tr>
				
				<tr> 
				
					<td>
						
						<label for="archivoN" id="archivoN"> Adjuntar archivo nuevo:</label>	
						
					</td>
					
					<td>
						
						<input type="file" id="seleccionArchivos" name="seleccionArchivos" required="required" accept="image/*" disabled="">
						
					</td>
				
				</tr>
				
				<tr>
				
					<td>
						
						Comentarios:	
						
					</td>
					
					<td><?php	if (isset($rowActualizacionImagen2)|| isset($rowImagen2)|| isset($infoImagen) || isset($idPonencia)){?>
						<textarea name="textarea" id="comentarios" rows="5" cols="30" maxlength="5" readonly="readonly"><?php echo $comentario?></textarea>								 
					<?php }else{?>
							<textarea name="textarea" id="comentarios" rows="5" cols="30" maxlength="5" readonly="readonly"></textarea>	
				<?php } ?>		
					</td>
				
				</tr>
				
			</table>
		</div>
		<div class="col-12 col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3"> 
			<img id="imagenPrevizualizacion" style="width:100%; height:100%;"/><script src="js/scriptImagenes.js"></script>
		</div>

			<div style="text-align:right; width:100%; margin-bottom:5%;">

				<input type="submit" value="Guardar"  id="submit" style=" margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px; display: none;">
		     
				</form>
			
				<button type="reset" id="Regresar" onclick="location.href='trabr.php'" style="background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;">Regresar</button>


				<input type="reset" value="Cancelar" id="Cancelar" onclick="cancelar()" style="background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px; display: none;">
			</div>
			
			
		</div>
	</div>
	 
    <!-- End CONTENIDO -->
    
  <!-- Start Footer -->
        <footer class="footer-box">
        <div class="container">
            <div class="row">
               <div class="col-md-12 white_fonts" style="display: contents;">
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            
                            <div class="full">
							<h3>Dirección</h3>
                                <ul class="menu_footer">   
								     
								     <li><img src="images/i5.png"><span style="font-size: 85%;">Carr, Cuautitlán - Teoloyucan, San Sebastian Xhala, 54714 Cuautitlán Izcalli</span></li>

                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="full">
                                <div class="footer_blog full white_fonts">
                             <h3>Contacto</h3>
                             <ul class="full">
                               <li><img src="images/i6.png"><span style="font-size: 85%;"> <?php echo $correoC  ?></span></li>
                               <li><img src="images/i7.png"><span style="font-size: 85%;"><?php echo "$numeroC1    $numeroC2"; ?></span></li>
                             </ul>
                         </div>
                            </div>
                        </div>
                </div>
			 </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link active" href="derechosa.php" style="color:white">© Sitio desarrollado por CDD Solutions S.A. y administrado por el Departamento de Matemáticas. Hecho en México, (2020 - 2021)</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
	/* counter js */
	function asignaridCombo(){
		var id_ponenciaCombo=document.getElementById('banderaID').value;
		var banderatipo=document.getElementById('banderatipo').value;
		var combo = document.getElementById('idtituloTrabajo');
		
		if(banderatipo==="1"){
			combo.value=id_ponenciaCombo;
		}
	}

	function selecionaTitulo(){
		var id_imagen = document.getElementById('idtituloTrabajo').value;
		if(id_imagen ==""){
		document.getElementById('Clave').value = "";	
		}	
		window.location.href='?id_imagen='+id_imagen;

	}

	function cambiarArchivo(){ //habilitar boton de cambiar archivo
		var archivoNuevo = document.getElementById('seleccionArchivos');
		var editarArchivo= document.getElementById('Editar');
		var status = document.getElementById("status").value
		var botonGuardar = document.getElementById("submit");
		var botonCancelar=document.getElementById("Cancelar");
		var botonRegresar= document.getElementById("Regresar");
		var banderaFecha=document.getElementById('banderaFecha').value;
		if (status !="Rechazado" || status!="Aceptado" || status =="En Evaluacion" && banderaFecha=="1") {
			archivoNuevo.disabled=false;
			editarArchivo.style.display='none';	
			botonGuardar.style.display='inline';
			botonCancelar.style.display='inline';
			botonRegresar.style.display='none';
		}
		else{
			archivoNuevo.style.display='none';
		}
		
	}
	function cancelar(){//FUNCION PARA EL BOTON CANCELAR
		var archivoNuevo = document.getElementById('seleccionArchivos');
		var editarArchivo= document.getElementById("Editar");
		var botonGuardar = document.getElementById("submit");
		var botonRegresar= document.getElementById("Regresar");
		var botonCancelar=document.getElementById("Cancelar");
		var imagenPrevizualizacion=document.getElementById("imagenPrevizualizacion");


		archivoNuevo.disabled= true;
		editarArchivo.style.display='inline';
		botonGuardar.style.display='none';
		botonRegresar.style.display='inline';
		botonCancelar.style.display='none';
			imagenPrevizualizacion.src="";


	}
function borrarArchivoAnterior(){
		var imagen=document.getElementById('imagen').value;
		
		var opcion = confirm("Recuerde que si continúa con la actualización, se eliminára la imagen anteriormente registrada.");
		if (opcion!=true) {
			window.location='trabr.php';
	
		}else{

		}
	}
	function validarEstatus(){
		var estatusImagen=document.getElementById("status").value;
		var tipo_autor=document.getElementById("tipo_autor").value;
		var archivoNuevo = document.getElementById("seleccionArchivos");
		var editarArchivo= document.getElementById("Editar");
		var botonGuardar = document.getElementById("submit");
		var botonRegresar= document.getElementById("Regresar");
		var botonCancelar=document.getElementById("Cancelar");
		var imagenPrevizualizacion=document.getElementById("imagenPrevizualizacion");
		var banderaFecha=document.getElementById('banderaFecha').value;
		
		//alert (tipo_autor);

		if (estatusImagen =="Rechazado" || estatusImagen =="Aceptado" || estatusImagen =="En Evaluacion"  && tipo_autor=="Coautor" && banderaFecha=="0" || tipo_autor!="Coautor" && banderaFecha=="0" && estatusImagen =="Rechazado" || estatusImagen =="Aceptado" || estatusImagen =="En Evaluacion"|| banderaFecha=="0" ){
			document.getElementById("archivoN").style.display='none';	
			archivoNuevo.style.display='none';
			editarArchivo.style.display='none';
			botonCancelar.style.display='none';
			botonGuardar.style.display='none';
			botonRegresar.style.display='inline';
			imagenPrevizualizacion.style.display='none';
			if(banderaFecha=="0"){
				alert('Han pasado las fechas para poder editar la imagen del Cartel.');
			}
		}
	}



(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
	</script>
</body>

</html>