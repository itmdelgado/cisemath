<?php
session_start();
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
$usuario=$_SESSION['Usuario'];
include ('Conexion.php');
//TRAE EL NÚMERO DE LA CONGRESO
	$consulta_num_congreso=pg_query($conexion,"SELECT MAX(numero_congreso) FROM congreso");//numero de congreso el AS es un alias
	$row0=pg_fetch_row($consulta_num_congreso);//eL pg_fetch_row trae los datos de la consulta y los asigna a la variable $row0
	$num_congreso=$row0[0];//el número máximo que encontro del congreso lo asigna a la variable consulta0 y es el máximo por que es el [0]NUMERO MÁXIMO DEL CONGRESO

//TRAE EL ID DE LA CONGRESO CON AYUDA DE SU NUMERO
	$consulta_id_congreso=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$num_congreso'"); //Selecciona los datos con el resultado de la consulta anterior (NUMERO MAXIMO DEL CONGRESO)
	$row1=pg_fetch_row($consulta_id_congreso);//Los datos del ID_CONGRESO Y NOMBRE CONGRESO son asignados a la variable $row
	$id_congresoactual=$row1[0];//es el id congreso
//VALIDAR SI HAY SALAS REGISTRADAS EN TABLA SALAS
	$consulta_id_sala=pg_query($conexion, "SELECT id_sala FROM salas");
	$numSalasTotales=pg_num_rows($consulta_id_sala);
	if ($numSalasTotales===0) {
		echo"<script>alert('No hay salas registradas, para poder registrar una sala es necesario que lo realice la Comisión de Salas en el apartado de Registro de Salas.');window.location='menu.php'</script>";
	}

//CHECA SI HAY PONENCIA REGISTRADA ANTERIORMENTE EN LA TABLA  SALAS _PONENCIAS
	$numero_salas_ponencia=pg_query($conexion,"SELECT COUNT(sp.id_ponencia) FROM salas_ponencias as sp, ponencias as p, usuario_ponencias as up, ponencia_taller as pt WHERE sp.id_ponencia=p.id_ponencia AND up.id_congreso='$id_congresoactual' AND up.id_ponencias=p.id_ponencia AND p.id_ponencia = pt.id_ponencia_taller and p.id_tipo_ponencia='TA'and p.estatus_video='Aceptado'");
	$rowNumSP=pg_fetch_row($numero_salas_ponencia);
	$numSP=$rowNumSP[0];

	$numtalleresP=pg_query($conexion, "SELECT COUNT(pt.id_ponencia_taller) FROM usuario u, usuario_ponencias up, ponencias p, ponencia_taller pt
									WHERE  u.id_usuario= up.id_usuario and up.id_ponencias=p.id_ponencia 
									and p.id_ponencia=pt.id_ponencia_taller and pt.id_congreso='$id_congresoactual' and up.tipo_autor='Autor' and p.id_tipo_ponencia='TA'and p.estatus_video='Aceptado'");
	$rowNumT=pg_fetch_row($numtalleresP);
	$numT=$rowNumT[0];
	 
	if($numSP!=0 && $numT!=0) {//SI HAY TRABAJOS REGISTRADOS EN LA SALAS_PONENCIAS PERO AUN NO SON TODAS LOS TRABAJOS DE PONENCIAS_TALLER
					$consultataller=pg_query($conexion, "SELECT pt.id_ponencia_taller, p.titulo
						FROM ponencias p, ponencia_taller pt,  usuario u, usuario_ponencias up
						WHERE  u.id_usuario= up.id_usuario and up.id_ponencias=p.id_ponencia 
						and p.id_ponencia=pt.id_ponencia_taller 
						and pt.id_congreso='$id_congresoactual' and up.tipo_autor='Autor' and p.id_tipo_ponencia='TA'
						and p.estatus_video='Aceptado' AND pt.id_ponencia_taller NOT IN(SELECT id_ponencia FROM salas_ponencias WHERE id_congreso='$id_congresoactual')");//PARA COMBO
				
				 $consultainfo=pg_query($conexion, "SELECT pt.id_ponencia_taller, p.titulo, u.nombres, u.primer_ap, u.segundo_ap, pt.materiales
						FROM usuario u, usuario_ponencias up, ponencias p, ponencia_taller pt
					    WHERE  u.id_usuario= up.id_usuario and up.id_ponencias=p.id_ponencia 
						and p.id_ponencia=pt.id_ponencia_taller 
						and pt.id_congreso='$id_congresoactual' and up.tipo_autor='Autor' and p.id_tipo_ponencia='TA'
						and p.estatus_video='Aceptado' AND pt.id_ponencia_taller NOT IN(SELECT id_ponencia FROM salas_ponencias WHERE id_congreso='$id_congresoactual')");
				
			


			}else if ($numSP==0 && $numT!=0){//CUANDO NO HAY NI UN TRABAJO REGISTRADO EN LA TABLA SALA_PONENCIAS PERO SI HAY TRABAJOS ACEPTADOS EN PONENCIAS_TALLERES
				$consultataller=pg_query($conexion, "SELECT pt.id_ponencia_taller, p.titulo
						FROM ponencias p, ponencia_taller pt,  usuario u, usuario_ponencias up
						WHERE  u.id_usuario= up.id_usuario and up.id_ponencias=p.id_ponencia 
						and p.id_ponencia=pt.id_ponencia_taller 
						and pt.id_congreso='$id_congresoactual' and up.tipo_autor='Autor' and p.id_tipo_ponencia='TA'
						and p.estatus_video='Aceptado' AND pt.id_ponencia_taller NOT IN(SELECT id_ponencia FROM salas_ponencias WHERE id_congreso='$id_congresoactual')");//PARA COMBO
				

				 $consultainfo=pg_query($conexion, "SELECT pt.id_ponencia_taller, p.titulo, u.nombres, u.primer_ap, u.segundo_ap, pt.materiales
							 	  FROM usuario u, usuario_ponencias up, ponencias p, ponencia_taller pt
								    WHERE  u.id_usuario= up.id_usuario and up.id_ponencias=p.id_ponencia 
									and p.id_ponencia=pt.id_ponencia_taller 
									and pt.id_congreso='$id_congresoactual' and up.tipo_autor='Autor' and p.id_tipo_ponencia='TA'
									and p.estatus_video='Aceptado' AND pt.id_ponencia_taller NOT IN(SELECT id_ponencia FROM salas_ponencias WHERE id_congreso='$id_congresoactual')");

			}if($numSP!=0 &&  $numT!=0 && $numSP == $numT){
				
				echo "<script>alert('Todos los talleres ya estan registrados'); window.location='menu.php';</script>";

			}if($numSP==0 &&  $numT==0){
				echo "<script>alert('No hay talleres registrados.'); window.location='menu.php';</script>";
			}

 
 		
 		if(isset($_GET['id_ponencia_taller'])){
 			$id_tallerSeleccionado=$_GET['id_ponencia_taller'];
 				$consultainfo=pg_query($conexion, "SELECT  pt.id_ponencia_taller, p.titulo, u.nombres, u.primer_ap, u.segundo_ap, pt.materiales
							 	  FROM usuario u, usuario_ponencias up, ponencias p, ponencia_taller pt
								    WHERE  u.id_usuario= up.id_usuario and up.id_ponencias=p.id_ponencia 
									and p.id_ponencia=pt.id_ponencia_taller 
									and pt.id_congreso='$id_congresoactual' and up.tipo_autor='Autor' and p.id_tipo_ponencia='TA'
									and p.estatus_video='Aceptado' AND pt.id_ponencia_taller ='$id_tallerSeleccionado'");
 				
 				 			$rowInfoTaller=pg_fetch_assoc($consultainfo);
 				 			$nombre=trim($rowInfoTaller['nombres']);
 				 			$apellido_p=trim($rowInfoTaller['primer_ap']);
 				 			$apellido_m=trim($rowInfoTaller['segundo_ap']);
 				 			
 				 			$nombreFinal="$nombre $apellido_p $apellido_m";
 				 			$materiales=$rowInfoTaller['materiales'];
 				 			//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
									$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_tallerSeleccionado' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
									$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);	
									if(!empty($actualizacionTitulo)){
										$tituloSeleccionado=$actualizacionTitulo['titulo'];
									}else{
										$tituloSeleccionado=$rowInfoTaller['titulo'];
									}
										

 		}else if(empty($_GET['id_ponencia_taller'])){
 			$nombreFinal="";
 			$materiales="";

 		}else{
 			$nombreFinal="";
 			$materiales="";
			  		
 		}
 		//PARA LLENAR EL CUPO CON LO QUE SELECCIONA EN EL COMBO
 		
 		
 		if(isset($_GET['id_sala']) && $_GET['id_sala']!=""){
 			$id_salaSeleccionada=$_GET['id_sala'];

		 		$consultaSalas=pg_query($conexion,"SELECT id_sala, nombre_sala, cupo FROM salas WHERE id_sala='$id_salaSeleccionada'");
		 		$rowSalaI=pg_fetch_assoc($consultaSalas);
		 		$cupo=$rowSalaI['cupo'];

		 		

 		}else if(empty($_GET['id_sala'])) {
 			$cupo="";
 		}else{
 			$cupo="";
 		}
 		//PARA EL COMBO DE SALAS
 		$consultaSalas=pg_query($conexion,"SELECT id_sala, nombre_sala FROM salas");	
 		//$rowSala=pg_fetch_assoc($consultaSalas);
?>

<?php 
include ("Conexion.php");
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
					$row0=pg_fetch_row($consulta);
					$consulta0=$row0[0];
					$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
					$row=pg_fetch_row($consulta1);
					$congresoActual=$row[0];//es el id congreso
$contacto=pg_query($conexion,"SELECT cn.telefono,cn.telefono2,cn.email FROM contacto cn, congreso c
WHERE c.id_congreso='$congresoActual'
and  c.id_contacto =cn.id_contacto");
$contactos=pg_fetch_array($contacto);
$correoC=@$contactos[2];
$numeroC1=@$contactos[0];
$numeroC2=@$contactos[1];
?><!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title style="background-image: url('images/logos.jpg');">CISEMATH</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/cismath.png">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" /> 

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98" onload="validacionCampoTexto();">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
      <?php
    ////Codigo para actualizar el banner
        include ('Conexion.php');
        $nmax=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
        $num=pg_fetch_row($nmax);
        $numcon=$num[0];
        $conact=pg_query($conexion,"SELECT id_congreso, banner FROM congreso WHERE numero_congreso='$numcon'");
        $ban=pg_fetch_row($conact);
        $banner=$ban[1];
        echo '<img src="'.$banner.'" style="width:100%; height:100%; background-size: cover; position: relative; display: flex;"/>';
    ?>

    <header class="top-header" >
        <div class="header_bottom">
          <div class="container">
		  <div class="row"> 
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 col-xl-12">
                <div class="menu_orange_section" style="background: #052450;">
                   <nav class="navbar header-nav navbar-expand-lg"> 
                     <div class="menu_section">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav" >
					    <li><a class="nav-link active" href="indexus.php"><img src="images/i4.png" alt="image"></a> </li>
                        <li><a class="nav-link active" href="acercadeus.php">Acerca de</a></li>
                        <li><a class="nav-link active" href="infus.php">Información</a></li>
                        <li><a class="nav-link active" href="instruccionesus.php">Instrucciones</a></li>
                        <li><a class="nav-link active" href="comitesus.php">Comités</a></li>
						<li><a class="nav-link active" href="memoriasus.php">Memorias</a></li>
						
						<li>
						<a style="position:relative; display:block;">
						<img src="images/user.png" alt="image"> 
						</a>
						</li>
						<li>
						<ul class="nav"><li class="dropdown langmenu" ><a href="#" style="display:block;"class="dropdown-toggle" data-toggle="dropdown" title="Idioma"><b class="caret"></b></a><ul class="dropdown-menu" style="color:black;"><li><a title="English ‎(en)‎" href="menu.php" style="color:black";>Menú‎‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="editarp.php"  style="color:black";>Perfil‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="index.php"  style="color:black";>Cerrar Sesión</a></li></ul></li></ul>
						</li>
						
                    </ul>
                </div>
                     </div>
                 </nav>
                 
                </div>
            </div>
          </div>
		  </div>
        </div>
        
    </header>
    <!-- End header -->
 
 <!-- Start CONTENIDO -->
 <form action="registrar-sala-taller.php" method="post" enctype="multipart/form-data">
	<div class="container">
		<div class="row">
			<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			 
				<div class="titulo" style="margin-top:8%">
					<h1 align="center" style="color:#052450;">REGISTRO DE TALLERES</h1>
				</div>
			</div>
		</div>	 
	</div>	
	<div class="container">
		<div class="row" style="display:flex; justify-content:center;  margin-bottom:2%;">
			<div class="table-responsive">
				<table cellpadding="6px" cellspacing="8px" align="center" style="text-align:left; margin-top:2%;">
					<tr>
						<td style="color:#052450;">               
							Nombre del taller:
						</td>
						<td>
							<select class="form-control" style="width:70%;"  id="tituloSelect" name="tituloSelect" title="SELECCIONE UN TRABAJO, POR FAVOR" required="" onchange="selecionaTitulo();">
								 <?php if (!empty($id_tallerSeleccionado)){ ?>
									<option value="<?php echo $rowInfoTaller['id_ponencia_taller'] ?>"><?php echo $tituloSeleccionado?></option>
								<?php }
								
								?>
								
								<option value=""> Selecciona un taller </option>

								<?php while( $rowTaller=pg_fetch_assoc($consultataller)){
											$id_ponencia_taller=$rowTaller['id_ponencia_taller'];
											//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
									$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_ponencia_taller' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
									$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);	
									if(!empty($actualizacionTitulo)){
										$titulo=$actualizacionTitulo['titulo'];
									}else{
										$titulo=$rowTaller['titulo'];
									}
											
									?>
								<option value="<?php echo $id_ponencia_taller ?>"><?php echo $titulo?></option>	

								<?php }?>
							</select>
						</td>
					</tr>
					<tr>
						<td style="color:#052450;">               
							Profesor:
						</td>
						<td> 
						<?php 
						if(!empty($id_tallerSeleccionado)){
						?>
						<input type="text" class="form-control" maxlength="100" style="width:80%;"  name="profesor" id="profesor" value="<?php echo $nombreFinal?>"  readonly="readonly">
						
						<?php }else{?>

						<input type="text" class="form-control" maxlength="100" style="width:80%;"  name="profesor" id="profesor" value=""  readonly="readonly">
	
						<?php }?>	

						</td>
					</tr>
					<tr>
						<td style="color:#052450;">               
							Fecha:
						</td>
						<td>
							<input type="date" class="form-control"  maxlength="100" style="width:80%;" name="fecha" id="fecha" required="" disabled="" min="2021-01-01" max="2030-12-31">
						</td>
					</tr>
					<tr>
						<td style="color:#052450;">               
							Hora:
						</td>
						<td>
							<input type="time" class="form-control"  maxlength="100" style="width:80%;" name="hora" id="hora" required="" disabled="">
						</td>
					</tr>

					<tr>
						<td style="color:#052450;">               
							Sala/Aula:
						</td>
						<td>
							<select class="form-control" style="width:70%;"  id="sala" name="sala" title="SELECCIONE UNA SALA, POR FAVOR"  required="" onchange="seleccionaSala();">
							<?php if(!empty($id_salaSeleccionada)){?>
								<option value="<?php echo $rowSalaI['id_sala'] ?>"><?php echo $rowSalaI['nombre_sala'] ?></option>
							<?php }
							?>
 
							<option value="" >Selecciona el nombre de la sala: </option>

							<?php 
							while($rowSala=pg_fetch_assoc($consultaSalas)){
									$id_sala=$rowSala["id_sala"];
									$nombreSala=$rowSala["nombre_sala"];
									
							?>
								<option value="<?php echo $id_sala ?>"><?php echo $nombreSala?></option>
							<?php
							}
							?>
						</td>
					</tr>
					<tr>
						<td style="color:#052450;">               
							Cupo:
						</td>
						<td>
							<?php 
							if(!empty($id_sala)){ ?>
							<input type="text" class="form-control"  maxlength="100" style="width:80%;" name="cupo" id="cupo" disabled="" value="<?php echo $cupo; ?>">
					<?php	}else{ ?>
							<input type="text" class="form-control"  maxlength="100" style="width:80%;" name="cupo" id="cupo" disabled="" value="">
					<?php }	?>	
						</td>
					</tr>
					<tr>
						<td style="color:#052450;">               
							Requisitos/Materiales:
						</td>
						<td>
						<?php 
						if(!empty($id_tallerSeleccionado) && $id_tallerSeleccionado!=""){
						?>	
							<textarea name="textar" class="form-control" rows="5" cols="30" style="width:80%;"  name="materiales" id="materiales" readonly="readonly" ><?php echo $materiales?></textarea>
						<?php }else{?>

						<textarea name="textar" class="form-control" rows="5" cols="30" style="width:80%;"  name="materiales" id="materiales" readonly="readonly" ></textarea>
	
						<?php }?>	
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
			<div class="btn" style="text-align:right; width:100%;  margin-bottom:5%;">
			<button type="B1" style="margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;">Guardar</button>
			<input type="reset" value="Cancelar" style="margin-right:5%; background: whitesmoke; color: black; padding: 6px 10px; border-color: whitesmoke;border-radius: 20px;" onclick="location.href='registro-taller.php'">
		</div>
	</div>
    <!-- End CONTENIDO -->
    
  <!-- Start Footer -->
        <footer class="footer-box">
        <div class="container">
            <div class="row">
               <div class="col-md-12 white_fonts" style="display: contents;">
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            
                            <div class="full">
							<h3>Dirección</h3>
                                <ul class="menu_footer">   
								     
								     <li><img src="images/i5.png"><span style="font-size: 85%;">Carr, Cuautitlán - Teoloyucan, San Sebastian Xhala, 54714 Cuautitlán Izcalli</span></li>

                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="full">
                                <div class="footer_blog full white_fonts">
                             <h3>Contacto</h3>
                             <ul class="full">
                               <li><img src="images/i6.png"><span style="font-size: 85%;"> <?php echo $correoC  ?></span></li>
                               <li><img src="images/i7.png"><span style="font-size: 85%;"><?php echo "$numeroC1    $numeroC2"; ?></span></li>
                             </ul>
                         </div>
                            </div>
                        </div>
                </div>
			 </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link active" href="derechosa.php" style="color:white">© Sitio desarrollado por CDD Solutions S.A. y administrado por el Departamento de Matemáticas. Hecho en México, (2020 - 2021)</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
	/* counter js */



function cancelar(){
	window.location='registro-taller.php';
}

function selecionaTitulo(){
		var id_ponencia_taller = document.getElementById('tituloSelect').value;	
		window.location.href='?id_ponencia_taller='+id_ponencia_taller;
		
		
		
	}	


function validacionCampoTexto(){
	var id_sala= document.getElementById('sala').value;
	if(id_sala!=""){
		
		document.getElementById('fecha').disabled=false;
		document.getElementById('hora').disabled=false;
		document.getElementById('grupo').disabled=false;
	}else{
		document.getElementById('fecha').disabled=true;
		document.getElementById('hora').disabled=true;
		document.getElementById('grupo').disabled=true;
	}
}

function seleccionaSala(){
var id_sala= document.getElementById('sala').value;
var id_ponencia_taller = document.getElementById('tituloSelect').value;	

if(id_ponencia_taller==""){
	alert("Primero selecciona el titulo del taller");
	var id_sala= document.getElementById('sala').value="";
}else{
	
	window.location.href='?id_ponencia_taller='+id_ponencia_taller+'&id_sala='+id_sala;

	}
}


(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
	</script>
</body>

</html>