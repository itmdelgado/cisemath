<?php
include ("Conexion.php");
$id_taller=$_POST['talleres'];
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
$row5=pg_fetch_row($consulta);
$consulta0=$row5[0];
$consulta1=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$consulta0'");
$row6=pg_fetch_row($consulta1);
$congresoActual=$row6[0];// se obtiene en id del congreso actual

$datos_autor=pg_query($conexion,"SELECT u.nombres,u.primer_ap, u.segundo_ap 
                                FROM usuario u, usuario_ponencias up
                                WHERE up.id_ponencias='$id_taller'
                                and   up.id_usuario=u.id_usuario
                                and   up.tipo_autor='Autor'");
$row1=@pg_fetch_row($datos_autor);
$n=@trim($row1[0]);
$p=@trim($row1[1]);
$m=@trim($row1[2]);

$nombre="$n $p $m";
if($nombre==NULL){
    $nombre="Seleccione un taller";
}
$datos_sala=pg_query($conexion,"SELECT s.nombre_sala, s.cupo, sp.fecha, sp.hora FROM salas s, salas_ponencias sp
WHERE sp.id_ponencia='$id_taller'
and s.id_sala=sp.id_sala");
$row2=pg_fetch_row($datos_sala);
if(@$row2[0]!=NULL){
$sala=@trim($row2[0]);
}else{
    $sala="No hay sala asignada";
}
if(@$row2[1]!=NULL){
$cupo=@trim($row2[1]);
}else{
    $cupo="No hay cupo definido";
}
if(@$row2[2]!=NULL){
$fecha=@trim($row2[2]);
}else{
    $fecha="No hay fecha asignada";
}
if(@$row2[3]!=NULL){
$hora=@trim($row2[3]);
}else{
    $hora="No hay hora asignada";
}
$consul=pg_query($conexion,"SELECT * FROM ponencia_taller WHERE id_ponencia_taller = '$id_taller' and id_congreso='$congresoActual'");
						$row8=@pg_fetch_row($consul);
						$material=@$row8[1];
if($material==NULL){
    $material="No hay materiales";
}

echo
"<div class='row'>
			<div class='col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>
			 
                    <table cellpadding='6px' cellspacing='8px' align='center' style='text-align:left; margin-top:2%;'>
						<tr>
							<td style='color:#052450;'>               
								Profesor:
							</td>
							<td>
								<input type='text' class='form-control' maxlength='100' readonly value='$nombre'style='width:80%;'>
							</td>
						</tr>
						<tr>
							<td style='color:#052450;'>               
								Fecha:
							</td>
							<td>
								<input type='text' class='form-control'  maxlength='100' readonly value='$fecha'  style='width:80%;'>
							</td>
						</tr>
						<tr>
							<td style='color:#052450;'>               
								Hora:
							</td>
							<td>
								<input type='text' class='form-control'  maxlength='100' readonly value='$hora'  style='width:80%;'>
							</td>
						</tr>
						<tr>
							<td style='color:#052450;'>               
								Sala/Aula:
							</td>
							<td>
								<input type='text' class='form-control'  maxlength='100' readonly value='$sala' style='width:80%;'>
							</td>
						</tr>
						<tr>
							<td style='color:#052450;'>               
								Cupo:
							</td>
							<td>
								<input type='text' class='form-control'  maxlength='100' readonly value='$cupo'  style='width:80%;'>
							</td>
						</tr>
						<tr>
							<td style='color:#052450;'>               
								Requisitos/Materiales:
							</td>
							<td>
								<textarea name='textar' class='form-control' rows='5' cols='30' style='width:80%;' readonly > $material</textarea>
							</td>
						</tr>
					</table>
            </div>
        </div>";


        echo "
                    <div class='col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>
	<div class='table-responsive'>
		<div class='titulo' style='margin-top:2%; display:flex; justify-content:center;'>
						<font style='font-size:18px; color:gray; text-align:center;'>Inscritos</font>
					</div>
					<table cellpadding='8px' border=1 class='table table-bordered' cellspacing='8px' align='center' style='text-align:left; margin-top:2%;'>
						<tr>
							<td style='color:#052450;'>               
								Nombre del asistente:
							</td>
							<td style='color:#052450;'>               
								Pago realizado:
							</td>
							<td style='color:#052450;'>               
								Asistió:
							</td>
                        </tr>";
                  $ConsultaTabla=pg_query($conexion,"SELECT u.nombres,u.primer_ap,u.segundo_ap,tp.tipo_pago,uca.tipo_asistencia
                  FROM usuario u, tipo_pagos_usuario tpu,
                              tipo_pagos tp,usuario_congreso_asistencia uca
                                      WHERE u.id_usuario=tpu.id_usuario
                                      and tpu.id_pago=tp.id_pago
                                      and uca.id_usuario=u.id_usuario
                                      and uca.tipo_asistencia='Asisitente Taller'
                                      and u.id_usuario IN (SELECT id_usuario FROM usuario_inscribe_taller 
                                                              WHERE id_ponencia_taller='$id_taller'
                                                              and id_congreso='$congresoActual')");
                    while($datostabla=@pg_fetch_array($ConsultaTabla)){
                       $nt=@trim($datostabla['nombres']);
                       $pt=@trim($datostabla['primer_ap']);
                       $mt=@trim($datostabla['segundo_ap']);
                       $nombreT="$nt $pt $mt";
                       $pago=@$datostabla['tipo_pago'];
                       if(@$datostabla['tipo_asistencia']==NULL){
                        $asistencia='asistencia no confirmada';
                       }else{
                           if(@$datostabla['tipo_asistencia']=='f'){
                            $asistencia="No asistió";
                           }
                           if(@$datostabla['tipo_asistencia']=='t'){
                            $asistencia="Asistió";
                           }
                       }
                    "<tr>

                        <td>               
                            $nombreT
                        </td>
                        <td>               
                            $pago
                        </td>
                        
                        <td>               
                            $asistencia
                        </td>
                    </tr>";

                    }
                    echo
                       "
					</table>
				</div>
			</div>
            ";

?>
		