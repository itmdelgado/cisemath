<?php
session_start();
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");	
}
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php'; 
require ('libreria/fpdf.php');
$usuario=$_SESSION['Usuario'];
include ('Conexion.php');

//TRAE EL NÚMERO DE LA CONGRESO
	$consulta_num_congreso=pg_query($conexion,"SELECT MAX(numero_congreso) FROM congreso");//numero de congreso el AS es un alias
	$row0=pg_fetch_row($consulta_num_congreso);//eL pg_fetch_row trae los datos de la consulta y los asigna a la variable $row0
	$num_congreso=$row0[0];//el número máximo que encontro del congreso lo asigna a la variable consulta0 y es el máximo por que es el [0]NUMERO MÁXIMO DEL CONGRESO

//TRAE EL ID DE LA CONGRESO CON AYUDA DE SU NUMERO
	$consulta_id_congreso=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$num_congreso'"); //Selecciona los datos con el resultado de la consulta anterior (NUMERO MAXIMO DEL CONGRESO)
	$row1=pg_fetch_row($consulta_id_congreso);//Los datos del ID_CONGRESO Y NOMBRE CONGRESO son asignados a la variable $row
	$id_congresoactual=$row1[0];//es el id congreso
//TRAE EL ID DEL TIPO DE PAGO
	$consultaIdPagos=pg_query($conexion, "SELECT id_pago FROM tipo_pagos WHERE tipo_pago='Ponente'and id_congreso='$id_congresoactual'");
	$rowPago=pg_fetch_row($consultaIdPagos);//Los datos del ID_CONGRESO Y NOMBRE CONGRESO son asignados a la variable $row
	
	if(empty($rowPago[0])){
	?>
	<script>alert("No se encuentra registrado el tipo de pago para Ponentes. Para continuar es necesario que el Administrador ingrese a el apartado de 'Congreso Actual / Cambiar Datos' e ingrese la cantidad que tiene que pagar los Ponentes.");
	window.location='menu.php';
	</script>
	<?php }else{
		$id_pago=$rowPago[0];
	}
	$banderaPonencias="0";
	$consultaNumPonencias=pg_query($conexion, "SELECT up.id_ponencias, up.id_usuario FROM  tipo_pagos_usuario as tpu, usuario AS u,
		usuario_ponencias AS up 
		WHERE tpu.id_usuario=u.id_usuario AND u.id_usuario=up.id_usuario and tpu.id_congreso='$id_congresoactual'
		and tpu.id_pago='$id_pago' AND tpu.estatus_pago IS NULL");
 	$numPonencia=pg_num_rows($consultaNumPonencias);
 		
	if($numPonencia===0 || $numPonencia===null){
	$banderaPonencias="1";	

	}else{//SI HAY PERSONAS QUE REGISTRAR
		$consultaPonencias=pg_query($conexion, "SELECT up.id_ponencias, up.id_usuario, p.titulo FROM  tipo_pagos_usuario as tpu, usuario AS u,
		usuario_ponencias AS up, ponencias AS p 
		WHERE p.id_ponencia=up.id_ponencias AND tpu.id_usuario=u.id_usuario AND u.id_usuario=up.id_usuario and tpu.id_congreso='$id_congresoactual'AND p.id_congreso='$id_congresoactual'
		and tpu.id_pago='$id_pago' AND tpu.estatus_pago IS NULL");

	}
	$nombreAutor="";
	if (isset($_GET['id_usuario'])) {
		$obtenido=$_GET['id_usuario'];
		$dato=explode("/", $obtenido);
		$id_ponenciaSel=$dato[1];

		$idUsuarioSeleccionado=$dato[0];
		
	
		if($_GET['id_usuario']!==""){
			
						$consultaNombreAutorPonencia=pg_query($conexion, "SELECT  u.nombres, u.primer_ap, u.segundo_ap
						FROM usuario u, usuario_ponencias up, ponencias p
					    WHERE  u.id_usuario= up.id_usuario and up.id_ponencias=p.id_ponencia
						and up.id_usuario='$idUsuarioSeleccionado'
						and up.id_ponencias='$id_ponenciaSel'
						and up.id_congreso='$id_congresoactual'");
						$resultadoAutor=pg_fetch_assoc($consultaNombreAutorPonencia);

						$nombre=trim($resultadoAutor['nombres']);
 				 		$apellido_p=trim($resultadoAutor['primer_ap']);
 				 		$apellido_m=trim($resultadoAutor['segundo_ap']);
 				 			
 				 		$nombreFinal="$nombre $apellido_p $apellido_m";
 				 		$id_usuarioP=$idUsuarioSeleccionado;
 				 			

		}
	}

	if(isset($_POST['agregar'])){
		$obtenido=@$_POST['idTrabajo'];
		$dato=explode("/", $obtenido);
		$id_ponencia=@$dato[1];
		$fechaPago=@$_POST['fechaPago'];
		$anioFecha=substr("$fechaPago",0,4);
		$anio=@$_POST['anio'];
		$id_usuario=@$_POST['us'];
		
		
		if($fechaPago===""){
			echo"<script> alert('Error, Selecciona una fecha.')</script>";
		}else if( $anio===""){
			echo"<script> alert('Error, Selecciona un año.')</script>";
		}else if($id_ponencia===""){
			echo"<script> alert('Error, Selecciona una ponencia para registrar.')</script>";
		}else if ($anio !=="" && $anioFecha!=="" && $anio!==$anioFecha) {
			echo"<script> alert('Error, Selecciona el año correspondiente a la Fecha indicada.')</script>";
		}else if($fechaPago!=="" && $anio!=="" && $id_ponencia!==""){
				$insertarDatos=pg_query($conexion,"UPDATE tipo_pagos_usuario set estatus_pago='TRUE', fecha='$fechaPago', anio='$anio' WHERE id_usuario='$id_usuario' AND id_congreso='$id_congresoactual'");
				if($insertarDatos){//SI SE INSERTA LOS DATOS

								 //CORREO PAGO
	                                    //**************************************************************************************
	                                    	class PDF extends FPDF
											{
											// Cabecera de página
											function Header()
											{
												// Logo
												$this->Image('logo.jpg',0,0,220);
												// Arial bold 15
												$this->SetFont('Arial','B',15);
												// Movernos a la derecha
												$this->Cell(80);
												// Título
												$this->Cell(50,80,utf8_decode('Congreso de Matematicas'),30,0,'C');
												// Salto de línea
												$this->Ln(50);
												
											}

											// Pie de página
											function Footer()
											{
												// Posición: a 1,5 cm del final
												$this->SetY(-15);
												// Arial italic 8
												$this->SetFont('Arial','I',8);
												// Número de página
												$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
											}
											}

											// Creación del objeto de la clase heredada
											$pdf = new PDF();
											$pdf->AliasNbPages();
											$pdf->AddPage();
											$pdf->SetFont('Times','',12);

	                                    $pdf->Cell(40,10,utf8_decode('Registro del pago'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('La información respecto a el pago realizado para presentarse como Ponente'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('ha sido registrado de manera correcta'),0,1);
	                        		   	$pdf->Cell(40,10,utf8_decode('le pedimos que continue atento a las indicaciones.'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    
	                                    $archivoAdjunto = $pdf->Output("", "S");
	                                    $correoa=pg_query($conexion,"SELECT c.correo,us.nombres,us.primer_ap,us.segundo_ap, us.rfc 
																FROM correos_usuario c, usuario us, usuario_ponencias as up
																WHERE up.id_ponencias='$id_ponencia' and up.id_usuario='$id_usuario'
																and up.id_usuario = c.id_usuario and
																c.id_usuario= us.id_usuario");
	                                    $i=0;
	                                    while($mostrarCR=pg_fetch_array($correoa)){
	                                        $cor[$i]=trim($mostrarCR['correo']);
	                                        $nombre=trim($mostrarCR['nombres']);
	                                        $apPA=trim($mostrarCR['primer_ap']);
	                                        $apMa=trim($mostrarCR['segundo_ap']);
	                                        $rfcA=trim($mostrarCR['rfc']);
	                                        $i=$i+1;
	                                    }

	                                      $mail = new PHPMailer(true);
	                                    
	                                    try {
	                                        //Server settings
	                                        $mail->SMTPDebug = 0;                      // Enable verbose debug output
	                                        $mail->isSMTP();                                            // Send using SMTP
	                                        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
	                                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
	                                        $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
	                                        $mail->Password   = 'CongresoMate2020';                               // SMTP password
	                                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
	                                        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
	                                        $mail->CharSet  = 'UTF-8';//PARA EL ACENTO
	                                    
	                                        //Recipients
	                                        $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
	                                        $mail->addAddress("$cor[0]", "$nombre $apPA");
	                                        if(!empty($cor[1])){
	                                            $mail->addAddress("$cor[1]", "$nombre $apPA");
	                                        }
	                                        if(!empty($cor[2])){
	                                            $mail->addAddress("$cor[2]", "$nombre $apPA");
	                                        }
	                                       // Content
	                                        $mail->isHTML(true);                                  // Set email format to HTML
	                                        $mail->Subject = 'Información del registro de pago';
	                                        $mail->Body    = 'En el siguiente documento se adjuntan la información del estatus del registro de pago..';
	                                        $mail->addStringAttachment($archivoAdjunto, 'Registro_Pago.pdf');
	                                        $mail->send();
	                                        
	                                        }catch (Exception $e) {
	                                            echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
	                                        }
					echo"<script> alert('Se registro el pago de manera correcta.'); window.location='registrop-comite.php';</script>";  
				}else{
					echo"<script> alert('Error al registrar el pago.'); window.location='registrop-comite.php';</script>";
				}
			
			}
	}

?>
<?php 
include ("Conexion.php");
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
					$row0=pg_fetch_row($consulta);
					$consulta0=$row0[0];
					$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
					$row=pg_fetch_row($consulta1);
					$congresoActual=$row[0];//es el id congreso
$contacto=pg_query($conexion,"SELECT cn.telefono,cn.telefono2,cn.email FROM contacto cn, congreso c
WHERE c.id_congreso='$congresoActual'
and  c.id_contacto =cn.id_contacto");
$contactos=pg_fetch_array($contacto);
$correoC=@$contactos[2];
$numeroC1=@$contactos[0];
$numeroC2=@$contactos[1];
?><!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title style="background-image: url('images/logos.jpg');">CISEMATH</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/cismath.png">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" /> 

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98" onload="listasDesplegables(); validarPonencias();">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
     <?php
    ////Codigo para actualizar el banner
        include ('Conexion.php');
        $nmax=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
        $num=pg_fetch_row($nmax);
        $numcon=$num[0];
        $conact=pg_query($conexion,"SELECT id_congreso, banner FROM congreso WHERE numero_congreso='$numcon'");
        $ban=pg_fetch_row($conact);
        $banner=$ban[1];
        echo '<img src="'.$banner.'" style="width:100%; height:100%; background-size: cover; position: relative; display: flex;"/>';
    ?>

    <header class="top-header" >
        <div class="header_bottom">
          <div class="container">
		  <div class="row"> 
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 col-xl-12">
                <div class="menu_orange_section" style="background: #052450;">
                   <nav class="navbar header-nav navbar-expand-lg"> 
                     <div class="menu_section">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav" >
					    <li><a class="nav-link active" href="indexus.php"><img src="images/i4.png" alt="image"></a> </li>
                        <li><a class="nav-link active" href="acercadeus.php">Acerca de</a></li>
                        <li><a class="nav-link active" href="infus.php">Información</a></li>
                        <li><a class="nav-link active" href="instruccionesus.php">Instrucciones</a></li>
                        <li><a class="nav-link active" href="comitesus.php">Comités</a></li>
						<li><a class="nav-link active" href="memoriasus.php">Memorias</a></li>
						
						<li>
						<a style="position:relative; display:block;">
						<img src="images/user.png" alt="image"> 
						</a>
						</li>
						<li>
						<ul class="nav"><li class="dropdown langmenu" ><a href="#" style="display:block;"class="dropdown-toggle" data-toggle="dropdown" title="Idioma"><b class="caret"></b></a><ul class="dropdown-menu" style="color:black;"><li><a title="English ‎(en)‎" href="menu.php" style="color:black";>Menú‎‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="editarp.php"  style="color:black";>Perfil‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="index.php"  style="color:black";>Cerrar Sesión</a></li></ul></li></ul>
						</li>
						
                    </ul>
                </div>
                     </div>
                 </nav>
                 
                </div>
            </div>
          </div>
		  </div>
        </div>
        
    </header>
    <!-- End header -->
 
 <!-- Start CONTENIDO -->
 <div class="container">
		<div class="row">
			<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			 
				<div class="titulo" style="margin-top:8%">
					<h1 align="center" style="color:#052450;">REGISTRO DE PAGO</h1>
				</div>
			</div>
		</div>	 
	</div>	
 <div class="container">
		<div class="row" style="display:flex; justify-content:center;  margin-top:2%; margin-bottom:2%;">
			<input type="hidden" name="banderaPonencias" id="banderaPonencias" value="<?php echo $banderaPonencias;?>">
			<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="table-responsive">
					<table align="center" cellpadding="8px" cellspacing="8px" style="text-align:center; margin-top:2%;">
						<tr>
							<td>
								<font style="font-size:18px; color:#052450;; text-align:left;">NOMBRE</font>
							</td>
							<td>
								<font style="font-size:18px; color:#052450;; text-align:left;">TÍTULO DEL TRABAJO</font>
							</td>	
							<td>
								<font style="font-size:18px; color:#052450;; text-align:left;">FECHA</font>
							</td>
						</tr>
						<?php 
						$consultaPagos=pg_query($conexion, "
								SELECT u.nombres||' '|| u.primer_ap||' '|| u.segundo_ap as autor, p.titulo, tpu.fecha
								FROM usuario AS u, ponencias AS p, tipo_pagos_usuario as tpu, usuario_ponencias AS up
								WHERE tpu.id_usuario=u.id_usuario and up.id_usuario=u.id_usuario AND p.id_ponencia=up.id_ponencias and tpu.id_congreso='$id_congresoactual'
								and tpu.id_pago='$id_pago'AND tpu.estatus_pago='TRUE' AND p.id_ponencia NOT IN (SELECT ar.id_ponencia FROM actualizacion_resumen AS ar, ponencias as p,  usuario AS u, tipo_pagos_usuario as tpu, usuario_ponencias AS up WHERE ar.id_ponencia=p.id_ponencia AND ar.id_congreso='$id_congresoactual' AND ar.estatus_actualizacion='Aceptado' AND tpu.id_usuario=u.id_usuario and up.id_usuario=u.id_usuario AND p.id_ponencia=up.id_ponencias and tpu.id_congreso='$id_congresoactual'
								and tpu.id_pago='$id_pago'AND tpu.estatus_pago='TRUE')");
						//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
									$consultaTituloA=pg_query($conexion, "SELECT  u.nombres||' '|| u.primer_ap||' '|| u.segundo_ap as autor, tpu.fecha, ar.titulo FROM actualizacion_resumen AS ar, ponencias as p,  usuario AS u, tipo_pagos_usuario as tpu, usuario_ponencias AS up WHERE ar.id_ponencia=p.id_ponencia AND ar.id_congreso='$id_congresoactual' AND ar.estatus_actualizacion='Aceptado' AND tpu.id_usuario=u.id_usuario and up.id_usuario=u.id_usuario AND p.id_ponencia=up.id_ponencias and tpu.id_congreso='$id_congresoactual'
								and tpu.id_pago='$id_pago'AND tpu.estatus_pago='TRUE'");
									$numP=@pg_num_rows($consultaPagos);
									$numA=@pg_num_rows($consultaTituloA);
									
									if($numP!=0  &&  $numA!=0 || $numP!=0  &&  $numA===0){
											while ($datosPago=@pg_fetch_array($consultaPagos)) {
												$autor=$datosPago['autor'];
												$titulo=$datosPago['titulo'];
												$fecha=$datosPago['fecha'];
												
													
												
												echo"
										<tr>
											<td>
												$autor 
											</td>
											<td>
												$titulo 
											</td>
											<td>
												$fecha 
											</td>
											
										</tr>";
							}
						} if($numA!=0 && $numP!=0 || $numA!=0 && $numP===0){
							
							while ($datosPA=@pg_fetch_array($consultaTituloA)) {
								
								$autorA=$datosPA['autor'];
								$tituloA=$datosPA['titulo'];
								$fechaA=$datosPA['fecha'];
								
								echo"
						
						<tr>
							<td>
								$autorA 
							</td>
							<td>
								$tituloA 
							</td>
							<td>
								$fechaA 
							</td>
							
						</tr>";
						}
					}
						?>
					</table>
				</div>
			</div>
			<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="table-responsive">
					<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="POST">
					<table cellpadding="2px" cellspacing="2px" align="center" style="text-align:left; margin-top:2%;">
						
						<tr>
							<td>
								Título del trabajo 
							</td>
							<td>
								<select  class="form-control" id="idTrabajo" name="idTrabajo" required="" onchange="idUsuarioSelec();" style="width:100%;">
								  <option value="">Selecciona la clave del trabajo</option>
								 <?php
										while ($id_ponenciasResultado=pg_fetch_assoc($consultaPonencias)) {
											$id_ponenciasPS=trim($id_ponenciasResultado['id_ponencias']);
											$id_usuariosPS=trim($id_ponenciasResultado['id_usuario']);

											//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
												$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_ponenciasPS' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
												$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);	
												if(!empty($actualizacionTitulo)){
													$tituloPS=$actualizacionTitulo['titulo'];
												}else{
													$tituloPS=trim($id_ponenciasResultado['titulo']);
												}
											?>
											<option value="<?php echo $id_usuariosPS."/".$id_ponenciasPS ?>"><?php echo $tituloPS ?></option>
									<?php	}
									?>

								</select>
							</td>
						</tr>
						<tr>
							<td>
								Nombre 
							</td>
							<td><?php if(isset($idUsuarioSeleccionado)){?>
								<input type="text" class="form-control" required="" name="nombre" id="nombre" maxlength="100"  value="<?php echo $nombreFinal; ?>" readonly="readonly" style="width: 100%;">
								<input id="usuarioSelec" name="usuarioSelec" type="hidden" value="<?php echo $obtenido; ?>">
								<input id="bandera" name="bandera" type="hidden" value="1">
								<input type="hidden" name="us" id="us" value="<?php echo$id_usuarioP?>">
								<?php }else{?>
									<input type="text" class="form-control" required="" name="nombre" id="nombre" maxlength="100"  value="" readonly="readonly" style="width: 100%;">
									<input id="usuarioSelec" name="usuarioSelec" type="hidden" value="">
									<input id="bandera" name="bandera" type="hidden" value="0">
									<input type="hidden" name="us" id="us" value="">
								<?php }?>
							</td>
						</tr>
						<tr>
							<td>
								Fecha de pago
							</td>
							<td>
								<input type="date" class="form-control" name="fechaPago" placeholder="dd/mm/aaaa" id="fechaPago" value="" maxlength="100"  min="2010-01-01" max="2070-12-31" disabled="" required="">
							</td>
						</tr>
						<tr>
							<td>
								Año
							</td>
							<td>
								<?php
								$cont = date('Y');
								?>
								<select  class="form-control" id="anio" name="anio" disabled="" required="">
								<option value="" >Selecciona un año </option>	
								<?php while ($cont >= 2010) { ?>
								  <option value="<?php echo($cont); ?>"><?php echo($cont); ?></option>
								<?php $cont = ($cont-1); } ?>
								</select>
							</td>
						</tr>
						</form>
						<tr>
							<td colspan="2" align="center">
								
								<button type="agregar" id="agregar" name="agregar" style="background: #052450; color: white; margin-top:2%; margin-bottom:2%; padding: 6px 10px; border-color: #052450;border-radius: 20px;">Agregar</button>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
			
	</div>
	
	<div class="btn" style="text-align:right; width:100%;  margin-top:2%; margin-bottom:6%;">
		
		<button type="B1" onclick="cancelar();" style="margin-right:5%; background: whitesmoke; color: black; padding: 6px 10px; border-color: whitesmoke;border-radius: 20px;">Cancelar</button>
	</div>
	<?php pg_close();?>
    <!-- End CONTENIDO -->
    
  <!-- Start Footer -->
        <footer class="footer-box">
        <div class="container">
            <div class="row">
               <div class="col-md-12 white_fonts" style="display: contents;">
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            
                            <div class="full">
							<h3>Dirección</h3>
                                <ul class="menu_footer">   
								     
								     <li><img src="images/i5.png"><span style="font-size: 85%;">Carr, Cuautitlán - Teoloyucan, San Sebastian Xhala, 54714 Cuautitlán Izcalli</span></li>

                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="full">
                                <div class="footer_blog full white_fonts">
                             <h3>Contacto</h3>
                             <ul class="full">
                               <li><img src="images/i6.png"><span style="font-size: 85%;"> <?php echo $correoC  ?></span></li>
                               <li><img src="images/i7.png"><span style="font-size: 85%;"><?php echo "$numeroC1    $numeroC2"; ?></span></li>
                             </ul>
                         </div>
                            </div>
                        </div>
                </div>
			 </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link active" href="derechosa.php" style="color:white">© Sitio desarrollado por CDD Solutions S.A. y administrado por el Departamento de Matemáticas. Hecho en México, (2020 - 2021)</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
	/* counter js */
	function idUsuarioSelec(){
		var id_usuarioSS=document.getElementById('idTrabajo').value;
		if(id_usuarioSS!==""){
			window.location.href='?id_usuario='+id_usuarioSS;	
		}
	}
	function listasDesplegables(){
		var bandera=document.getElementById('bandera').value;
		var id_usuarioSS=document.getElementById('idTrabajo');
		
		
		
	if(bandera==="1" && id_usuarioSS!==null){
		var usuarioSelec=document.getElementById('usuarioSelec').value;
		var fechaPago=document.getElementById('fechaPago');
		var anio=document.getElementById('anio');
		id_usuarioSS.value=usuarioSelec;
		fechaPago.disabled=false;
		anio.disabled=false;
		}
	}
	function validarPonencias(){
		var banderaPonencias=document.getElementById('banderaPonencias').value;
		if(banderaPonencias==="1"){
			alert('No hay ponentes para registrar.'); 
			window.location='menucomite.php';
		}
		
	}

	function cancelar(){
		//RESTABLECER 
		window.location='registrop-comite.php';	
	}

(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
	</script>
</body>

</html>