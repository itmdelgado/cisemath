<html>
<?php
session_start();
include ("Conexion.php"); 
use PHPMailer\PHPMailer\PHPMailer;
    	use PHPMailer\PHPMailer\Exception;
      	require 'PHPMailer/Exception.php';
    	require 'PHPMailer/PHPMailer.php';
    	require 'PHPMailer/SMTP.php'; 
	    require('libreria/fpdf.php');
$Titulo=$_POST["Titulo"];
$Resumen=$_POST["textarea"];
$Referencia=$_POST["textarea2"];
$TipoP= "TA";
$constanciaA=$_POST['constanciaA'];
$constanciaC1=@$_POST['constanciaC1'];
$Materiales=$_POST["textarea3"];
$Coa1=strtolower($_POST["ca1"]);
$Coa2=strtolower($_POST["ca2"]);
$constanciaC2=@$_POST['constanciaC2'];
////////////VALIDAR CORREOS DE LOS COAUTORES//////////////////////
$numeroVal=0;
if(!empty($Coa1)){
    $validacion_correo1=pg_query($conexion,"SELECT correo FROM correos_usuario WHERE '$Coa1'=correo and correo in (SELECT correo FROM correos_usuario)");
    $validacion1=pg_fetch_row($validacion_correo1);
    if(empty($validacion1[0])){
        echo"<script>alert('¡Coautor $Coa1 no encontrado!');window.location='resument.php'</script>";
        $numeroVal=1;
    }
}
if(!empty($Coa2)){
    $validacion_correo1=pg_query($conexion,"SELECT correo FROM correos_usuario WHERE '$Coa2'=correo and correo in (SELECT correo FROM correos_usuario)");
    $validacion1=pg_fetch_row($validacion_correo1);
    if(empty($validacion1[0])){
        echo"<script>alert('¡Coautor $Coa2 no encontrado!');window.location='resument.php'</script>";
        $numeroVal=1;
    }
}


date_default_timezone_set("America/Mexico_City");
$fecha=date("d-m-Y h:i a");// obtinene la hora del registro

if($numeroVal==0){

include ('Conexion.php');
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero de congreso
$row0=pg_fetch_row($consulta);
$consulta0=$row0[0];
$consulta1=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$consulta0'");
$row=pg_fetch_row($consulta1);
$numeroCongreso=$row[0];//es el id congreso
$consulta2=pg_query($conexion,"SELECT MAX(numeracion) FROM ponencias  where id_congreso='$numeroCongreso'");
$row2=pg_fetch_row($consulta2);
if(empty($row2)){
$numeroid =1;
$id_ponencia=$TipoP.$numeroid;// se crea id ponencia en 1 si no existe ponencia;

}else{
    $numeroid=$row2[0]+1;
    $id_ponencia=$TipoP.$numeroid;// se crea id ponencia sumando el muero ponencia anterior mas 1;
    
}

$estatus="Enviado";
$insert1="INSERT INTO ponencias (id_ponencia,numeracion,id_tipo_ponencia,titulo,resumen,fecha_registro,estatus_resumen,referencia,id_congreso) 
                     VALUES ('$id_ponencia','$numeroid', '$TipoP','$Titulo','$Resumen','$fecha','$estatus','$Referencia','$numeroCongreso')";//,'$modalidad'//id_modalidad
pg_query($conexion,$insert1);// registra la ponencia tabla ponencias
$llenado="INSERT INTO ponencia_taller (id_ponencia_taller,materiales,id_congreso) VALUES ('$id_ponencia','$Materiales','$numeroCongreso')";
pg_query($conexion,$llenado);//registra los materiales de la ponencia en la tabla ponencia_taller


$usuario=$_SESSION['Usuario'];
$autor="Autor"; 
$coautor="Coautor";
$insert2="INSERT INTO usuario_ponencias (id_usuario, id_ponencias,tipo_autor,id_congreso) VALUES ('$usuario','$id_ponencia','$autor','$numeroCongreso')";
pg_query($conexion,$insert2);// registra al autor con la ponencia tabla usuario_ponencias
$validacion_rol=pg_query($conexion,"SELECT * FROM usuario_roles WHERE id_usuario='$usuario' and id_rol='ATA004'");
$rowvalidacion=pg_fetch_row($validacion_rol);

if(@$rowvalidacion[0]==NULL){
pg_query($conexion,"INSERT INTO usuario_roles(id_usuario,id_rol,estatus_rol,fecha_inicio) VALUES ('$usuario','ATA004','TRUE','$fecha')");
}

// Solicitud constancia 
if($constanciaA=="SI"){
    $numeroCo=pg_query($conexion,"SELECT MAX(numeracion) FROM usuario_constancia WHERE id_congreso='$numeroCongreso'");
    $rowConstancia=pg_fetch_row($numeroCo);
    $ultimaactualizacion=@$rowConstancia[0];
    $numeroConstancias=0;
    if($ultimaactualizacion==NULL){
        $numeroCostancias=1;
        $id_generado="CONS".$numeroConstancias;
        
    }else{
        $numeroConstancias=$ultimaactualizacion+1;
        $id_generado="CONS".$numeroConstancias;
    }
    $anio=date("Y");
    pg_query($conexion,"INSERT INTO usuario_constancia(id_generado, id_constancia,id_usuario,estatus_constancia,fecha,anio,id_congreso,numeracion,id_ponencia) 
                                            VALUES    ('$id_generado','PAR002','$usuario','Solicitada','$fecha','$anio','$numeroCongreso','$numeroConstancias','$id_ponencia')");
}

//Obtener nombre, primer apellido y los correos del Autor de la ponencia
$correoa=pg_query($conexion,"SELECT c.correo,us.nombres,us.primer_ap,us.segundo_ap FROM correos_usuario c, usuario us WHERE c.id_usuario='$usuario' and us.id_usuario ='$usuario'");
$i=0;
while($mostrarCR=pg_fetch_array($correoa)){
    $cor[$i]=trim($mostrarCR['correo']);
    $nombre=trim($mostrarCR['nombres']);
    $apPA=trim($mostrarCR['primer_ap']);
    $apMa=trim($mostrarCR['segundo_ap']);
    $i=$i+1;
}
if($Coa1 != NULL){
    $consult=pg_query($conexion,"SELECT usuario.id_usuario FROM usuario, correos_usuario WHERE '$Coa1' = correos_usuario.correo and correos_usuario.id_usuario=usuario.id_usuario");
    $row3=pg_fetch_row($consult);
    $idCoautor=$row3[0];
$insert3="INSERT INTO usuario_ponencias (id_usuario, id_ponencias,tipo_autor,id_congreso) VALUES ('$idCoautor','$id_ponencia','$coautor','$numeroCongreso')";
pg_query($conexion,$insert3);// registra al autor con la ponencia tabla usuario_ponencias
if($constanciaC1=="SI"){
    $numeroCostanciasC=pg_query($conexion,"SELECT MAX(numeracion) FROM usuario_constancia WHERE id_congreso='$numeroCongreso'");
    $rowConstancia1=pg_fetch_row($numeroCostanciasC);
    $numeracion=@$rowConstancia1[0];
    if($numeracion==NULL){
        $numeracion=1;
        $id_generado="CONS".$numeracion;
        
    }else{
        $numeracion=$numeracion+1;
        $id_generado="CONS".$numeracion;
    }
    $anio=date("Y");
    pg_query($conexion,"INSERT INTO usuario_constancia(id_generado, id_constancia,id_usuario,estatus_constancia,fecha,anio,id_congreso,numeracion) 
                                            VALUES    ('$id_generado','PAR002','$idCoautor','Solicitada','$fecha','$anio','$numeroCongreso','$numeracion')");
}
}
if($Coa2 != NULL){
    $consult=pg_query($conexion,"SELECT usuario.id_usuario FROM usuario, correos_usuario WHERE '$Coa2' = correos_usuario.correo and correos_usuario.id_usuario=usuario.id_usuario");
    $row3=pg_fetch_row($consult);
    $idCoautor=$row3[0];
$insert3="INSERT INTO usuario_ponencias (id_usuario, id_ponencias,tipo_autor,id_congreso) VALUES ('$idCoautor','$id_ponencia','$coautor','$numeroCongreso')";
pg_query($conexion,$insert3);// registra al autor con la ponencia tabla usuario_ponencias
if($constanciaC2=="SI"){
    $numeroCostanciasC=pg_query($conexion,"SELECT MAX(numeracion) FROM usuario_constancia WHERE id_congreso='$numeroCongreso'");
    $rowConstancia1=pg_fetch_row($numeroCostanciasC);
    $numeracion=@$rowConstancia1[0];
    if($numeracion==NULL){
        $numeracion=1;
        $id_generado="CONS".$numeracion;
        
    }else{
        $numeracion=$numeracion+1;
        $id_generado="CONS".$numeracion;
    }
    $anio=date("Y");
    pg_query($conexion,"INSERT INTO usuario_constancia(id_generado, id_constancia,id_usuario,estatus_constancia,fecha,anio,id_congreso,numeracion) 
                                            VALUES    ('$id_generado','PAR002','$idCoautor','Solicitada','$fecha','$anio','$numeroCongreso','$numeracion')");
}
}

// obtener nombre,apellidos y correo de coautores del trabajo

$infoCoau=pg_query($conexion,"select u.nombres,u.primer_ap ,u.segundo_ap,c.correo FROM correos_usuario c, usuario u, usuario_ponencias up WHERE up.id_ponencias='$id_ponencia'and up.id_usuario=u.id_usuario and u.id_usuario= c.id_usuario and up.tipo_autor!='Autor';");
$j=0;


// Creacion de PDF Autor
class PDF extends FPDF
                    {
                    // Cabecera de página
                    function Header()
                    {
                        // Logo
                        $this->Image('logo.jpg',0,0,220);
                        // Arial bold 15
                        $this->SetFont('Arial','B',15);
                        // Movernos a la derecha
                        $this->Cell(80);
                        // Título
                        $this->Cell(50,80,utf8_decode('Congreso Matematicas'),30,0,'C');
                        // Salto de línea
                        $this->Ln(50);
                        
                    }
                    
                    // Pie de página
                    function Footer()
                    {
                        // Posición: a 1,5 cm del final
                        $this->SetY(-15);
                        // Arial italic 8
                        $this->SetFont('Arial','I',8);
                        // Número de página
                        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
                    }
                    }
                     
                    // Creación del objeto de la clase heredada
                    $pdf = new PDF();
                    $pdf->AliasNbPages();
                    $pdf->AddPage();
                    $pdf->SetFont('Times','',12);
                    
                    $pdf->Cell(40,10,utf8_decode('Registro de Taller'),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    $pdf->Cell(40,10,utf8_decode($nombre.' '.$apPA.' '.$apMa.' usted ha registrado el siguiente resumen para un taller'),0,1);
                    $pdf->Cell(40,10,utf8_decode('Clave del Taller: '.$id_ponencia),0,1);
                    $pdf->Multicell(190,5,utf8_decode('Título del Taller: '.$Titulo),0,1);
                    $pdf->Multicell(190,5,utf8_decode('Resumen del trabajo: '.$Resumen),0,1);
                    $pdf->Multicell(190,5,utf8_decode('Referencias: '.$Referencia),0,1);
                    $pdf->Cell(40,10,utf8_decode('Coautor registrado en el trabajo:'),0,1);
                    while($infoCoa=pg_fetch_array($infoCoau)){
                        if(empty($infoCoa)){
                            $pdf->Cell(40,10,utf8_decode('No hay ningun coautor registrado'),0,1); 
                        }
                        $nombreCoa[$j]=trim($infoCoa['nombres']);
                        $apellidoCoa[$j]=trim($infoCoa['primer_ap']);
                        $apellidoCoa2[$j]=trim($infoCoa['segundo_ap']);
                        $corA[$j]=trim($infoCoa['correo']);
                    $pdf->Cell(40,10,utf8_decode($nombreCoa[$j]." ".$apellidoCoa[$j]." ".$apellidoCoa2[$j]." (".$corA[$j].")"),0,1);
                        $j=$j+1;
                       }
                    $pdf->Cell(40,10,utf8_decode('Si desea agregar o modificar los coautores de este trabajo, lo podrá realizar'),0,1);
                    $pdf->Cell(40,10,utf8_decode('cuando edite su resumen en el apartado "trabajos registrados".'),0,1);
                    $pdf->Cell(40,10,utf8_decode('Le pedimos de la manera más atenta, estar al pendiente de su cuenta para conocer'),0,1);
                    $pdf->Cell(40,10,utf8_decode('el estatus de su trabajo.'),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    
                    $archivoAdjunto = $pdf->Output("", "S");

//Envio de correo Autor
$mail = new PHPMailer(true);
                    
                    try {
                        //Server settings
                        $mail->SMTPDebug = 0;                      // Enable verbose debug output
                        $mail->isSMTP();                                            // Send using SMTP
                        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                        $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
                        $mail->Password   = 'CongresoMate2020';                               // SMTP password
                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                    
                        //Recipients
                        $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
                        $mail->addAddress("$cor[0]", "$nombre $apPA");
                        if(!empty($cor[1])){
                            $mail->addAddress("$cor[1]", "$nombre $apPA");
                        }
                        if(!empty($cor[2])){
                            $mail->addAddress("$cor[2]", "$nombre $apPA");
                        }
                                        
                        // Content
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = 'Registro de trabajo';
                        $mail->Body    = 'En el siguiente documento se adjuntan los datos del taller registrado';
                        $mail->addStringAttachment($archivoAdjunto, 'Registro_Resumen_taller.pdf');
                        $mail->send();
                        
                    } catch (Exception $e) {
                        echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
                    }



// Creacion de PDF Coautores 
if(!empty($Coa1)){
class PDF2 extends FPDF
                    {
                    // Cabecera de página
                    function Header()
                    {
                        // Logo
                        $this->Image('logo.jpg',0,0,220);
                        // Arial bold 15
                        $this->SetFont('Arial','B',15);
                        // Movernos a la derecha
                        $this->Cell(80);
                        // Título
                        $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
                        // Salto de línea
                        $this->Ln(50);
                        
                    }
                    
                    // Pie de página
                    function Footer()
                    {
                        // Posición: a 1,5 cm del final
                        $this->SetY(-15);
                        // Arial italic 8
                        $this->SetFont('Arial','I',8);
                        // Número de página
                        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
                    }
                    }
                    
                    // Creación del objeto de la clase heredada
                    $pdf = new PDF();
                    $pdf->AliasNbPages();
                    $pdf->AddPage();
                    $pdf->SetFont('Times','',12);
                    
                    $pdf->Cell(40,10,utf8_decode('Registro de Taller'),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    $pdf->Cell(40,10,utf8_decode('El Autor '.$nombre.' '.$apPA.' '.$apMa.' lo ha registrado como coautor en el siguiente trabajo:'),0,1);
                    $pdf->Cell(40,10,utf8_decode('Clave de Taller: '.$id_ponencia),0,1);
                    $pdf->Multicell(190,5,utf8_decode('Título de Taller: '.$Titulo),0,1);
                    $pdf->Multicell(190,5,utf8_decode('Resumen del trabajo: '.$Resumen),0,1);
                    $pdf->Multicell(190,5,utf8_decode('Referencias: '.$Referencia),0,1);
                    $pdf->Cell(40,10,utf8_decode('Si desea conocer el estatus de su trabajo, lo podrá visualizar'),0,1);
                    $pdf->Cell(40,10,utf8_decode('en el apartado "trabajos registrados".'),0,1);
                    
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    $archivoAdjunto2 = $pdf->Output("", "S");

//Envio de correo coautores
$mail = new PHPMailer(true);
                    
                    try {
                        //Server settings
                        $mail->SMTPDebug = 0;                      // Enable verbose debug output
                        $mail->isSMTP();                                            // Send using SMTP
                        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                        $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
                        $mail->Password   = 'CongresoMate2020';                               // SMTP password
                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                    
                        //Recipients
                        $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
                        while($infoCoa=pg_fetch_array($infoCoau)){
                           
                            $nombreCoa[$j]=trim($infoCoa['nombres']);
                            $apellidoCoa[$j]=trim($infoCoa['primer_ap']);
                            $apellidoCoa2[$j]=trim($infoCoa['segundo_ap']);
                            $corC[$j]=trim($infoCoa['correo']);
                           
                        
                            $j=$j+1;
                           }
                           if(!empty($Coa1)){
                            $mail->addAddress("$Coa1",utf8_decode("$nombreCoa[0] $apellidoCoa[0] $apellidoCoa2[0]"));

                           }
                           
                    
                        // Attachments
                        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
                    
                        // Content
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = 'Registro de trabajo';
                        $mail->Body    = 'En el siguiente documento se adjuntan los datos del Taller registrado';
                        $mail->addStringAttachment($archivoAdjunto2, 'Registro_Resumen_Taller.pdf');
                        $mail->send();
                        
                    } catch (Exception $e) {
                        echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
                    }
                }


echo"<script>alert('¡Taller Registrado!');window.location='menu.php'</script>";
            }

?>

</html>