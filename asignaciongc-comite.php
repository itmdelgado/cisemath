<?php
session_start();
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
$usuario=$_SESSION['Usuario'];

include ("Conexion.php");
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';
require ('libreria/fpdf.php');
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
$row5=pg_fetch_row($consulta);
$consulta0=$row5[0];
$consulta1=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$consulta0'");
$row6=pg_fetch_row($consulta1);
$congresoActual=$row6[0];// se obtiene en id del congreso actual

if(isset($_POST['submit1'])){

	$Clave1=$_POST['select1'];
	$Clave2=$_POST['select2'];
	$Clave3=$_POST['select3'];
	$Clave4=@$_POST['select4'];
	$Clave5=@$_POST['select5'];
	$lugar1=$_POST['lugar1'];
	$lugar2=$_POST['lugar2'];
	$lugar3=$_POST['lugar3'];
	$lugar4=@$_POST['lugar4'];
	$lugar5=@$_POST['lugar5'];
///////////////////////////////////////////////////// SE CREA CLASE PARA PDF/////////////////////////////////////////////////////////////////////////7
class PDF extends FPDF
											{
											// Cabecera de página
											function Header()
											{
												// Logo
												$this->Image('logo.jpg',0,0,220);
												// Arial bold 15
												$this->SetFont('Arial','B',15);
												// Movernos a la derecha
												$this->Cell(80);
												// Título
												$this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
												// Salto de línea
												$this->Ln(50);
												
											}

											// Pie de página
											function Footer()
											{
												// Posición: a 1,5 cm del final
												$this->SetY(-15);
												// Arial italic 8
												$this->SetFont('Arial','I',8);
												// Número de página
												$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
											}
											}

	///////////////////////////////////////////SE INSERTA EL LUGAR PARA LA PRIMERA CLAVE///////////////////////////////////////////////////////////
	$ponencias=pg_query($conexion,"SELECT id_ponencia,titulo FROM ponencias WHERE id_tipo_ponencia='CA' and id_ponencia='$Clave1' and id_congreso='$congresoActual' and id_ponencia IN (SELECT id_ponencia FROM usuario_evalua_cartel WHERE id_congreso='$congresoActual' and calificacion is not null)");
								  while($row2=pg_fetch_array($ponencias)){
									  $id_ponenciaS=$row2['id_ponencia'];
									  
									$datosActu=pg_query($conexion,"SELECT id_ponencia, titulo FROM actualizacion_resumen WHERE id_ponencia='$id_ponenciaS' and id_congreso='$congresoActual'");
									$validacion=pg_fetch_row($datosActu);
									if($validacion[0]==NULL){
										pg_query($conexion, "UPDATE ponencia_cartel set lugar_ganador='$lugar1'
										where id_ponencia_cartel='$Clave1'");
									}else{
										$numeroC=pg_query($conexion,"SELECT MAX(numeracion) FROM actualizacion_cartel WHERE id_congreso='$congresoActual'  and id_ponencia_cartel='$Clave1'");
										$rowCa=pg_fetch_row($numeroC);
										$numeracionAC=@$rowCa[0];

										pg_query($conexion, "UPDATE actualizacion_cartel set lugar_ganador='$lugar1'
										where id_ponencia_cartel='$Clave1' and numeracion='$numeracionAC'");
									}

								}
////////////////////////////////////////////// SE OBTIENE EL NOMBRE DEL AUTOR DEL CARTEL/////////////////////////////////////////////////////////////////
			$datosAutor=pg_query($conexion,"SELECT u.id_usuario, u.nombres,u.primer_ap FROM usuario u, usuario_ponencias up WHERE up.id_ponencias='$Clave1' and u.id_usuario=up.id_usuario");
			while($datosC1=pg_fetch_array($datosAutor)){
			$id_usuario=$datosC1['id_usuario'];
			$nombre=trim($datosC1['nombres']);
			$apPA=trim($datosC1['primer_ap']);
///////////////////////////////////////////// SE OBTIENE LOS CORREOS/////////////////////////////////////////////////////////////////////////////////////
			$correos1=pg_query($conexion,"SELECT correo FROM correos_usuario WHERE id_usuario='$id_usuario'");
			$i=0;
			$correosA[0]=NULL;
			$correosA[1]=NULL;
			$correosA[2]=NULL;
			while($correos=pg_fetch_array($correos1)){
				$correosA[$i]=$correos['correo'];
				$i=$i+1;
			}
////////////////////////////////////////////// SE ENVIA CORREO AL LUGAR GANADOR//////////////////////////////////////////////////////////////////////////
											
											// Creación del objeto de la clase heredada
											$pdf = new PDF();
											$pdf->AliasNbPages();
											$pdf->AddPage();
											$pdf->SetFont('Times','',12);

											$pdf->Cell(40,10,utf8_decode('¡FELICIDADES!'),0,1);
											$pdf->Cell(40,10,utf8_decode('Su Cartel ha quedado entre los primeros lugares ganadores'),0,1);
											$pdf->Cell(40,10,utf8_decode('Clave del Cartel:'.$Clave1),0,1);
											$pdf->Cell(40,10,utf8_decode('Lugar Ganador:'.$lugar1),0,1);
											$pdf->Cell(40,10,utf8_decode('Le recordamos estar atento a las fechas importantes'),0,1);
											$pdf->Cell(40,10,utf8_decode('publicadas en la pagína del congreso.'),0,1);
											$pdf->Cell(40,10,utf8_decode(''),0,1);
											$pdf->Cell(40,10,utf8_decode('Telefonos: 56-23-18-86              56-23-18-90'),0,1);

											$archivoAdjunto = $pdf->Output("", "S");




											$mail = new PHPMailer(true);

											try {
												//Server settings
												$mail->SMTPDebug = 0;                      // Enable verbose debug output
												$mail->isSMTP();                                            // Send using SMTP
												$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
												$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
												$mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
												$mail->Password   = 'CongresoMate2020';                               // SMTP password
												$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
												$mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

												//Recipients
												$mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
												$mail->addAddress("$correosA[0]", "$nombre $apPA");
												if(!empty($correosA[2])){
													$mail->addAddress("$correosA[2]", "$nombre $apPA");
												}
												if(!empty($correosA[3])){
													$mail->addAddress("$correosA[3]", "$nombre $apPA");
												}


												/* Attachments
												$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
												$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
											*/
												// Content
												$mail->isHTML(true);                                  // Set email format to HTML
												$mail->Subject = 'Lugar Ganador';
												$mail->Body    = 'Lugar Ganador del concurso de carteles';
												$mail->addStringAttachment($archivoAdjunto, 'Lugar_Ganador.pdf');
												$mail->send();
												
											} catch (Exception $e) {
												echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
											}

			}

///////////////////////////////////////////SE INSERTA EL LUGAR PARA LA SEGUNDA CLAVE///////////////////////////////////////////////////////////
	$ponencias=pg_query($conexion,"SELECT id_ponencia,titulo FROM ponencias WHERE id_tipo_ponencia='CA' and id_ponencia='$Clave2' and id_congreso='$congresoActual' and id_ponencia IN (SELECT id_ponencia FROM usuario_evalua_cartel WHERE id_congreso='$congresoActual' and calificacion is not null)");
								  while($row2=pg_fetch_array($ponencias)){
									  $id_ponenciaS=$row2['id_ponencia'];
									  $tituloS=$row2['titulo'];
									$datosActu=pg_query($conexion,"SELECT id_ponencia, titulo FROM actualizacion_resumen WHERE id_ponencia='$id_ponenciaS' and id_congreso='$congresoActual'");
									$validacion=pg_fetch_row($datosActu);
									if($validacion[0]==NULL){
										pg_query($conexion, "UPDATE ponencia_cartel set lugar_ganador='$lugar2'
										where id_ponencia_cartel='$Clave2'");
									}else{
										$numeroC=pg_query($conexion,"SELECT MAX(numeracion) FROM actualizacion_cartel WHERE id_congreso='$congresoActual'  and id_ponencia_cartel='$Clave2'");
										$rowCa=pg_fetch_row($numeroC);
										$numeracionAC=@$rowCa[0];

										pg_query($conexion, "UPDATE ponencia_cartel set lugar_ganador='$lugar2'
										where id_ponencia_cartel='$Clave2' and numeracion='$numeracionAC'");
									}
								}
////////////////////////////////////////////// SE OBTIENE EL NOMBRE DEL AUTOR DEL CARTEL/////////////////////////////////////////////////////////////////
			$datosAutor=pg_query($conexion,"SELECT u.id_usuario, u.nombres,u.primer_ap FROM usuario u, usuario_ponencias up WHERE up.id_ponencias='$Clave2' and u.id_usuario=up.id_usuario");
			while($datosC1=pg_fetch_array($datosAutor)){
			$id_usuario=$datosC1['id_usuario'];
			$nombre=trim($datosC1['nombres']);
			$apPA=trim($datosC1['primer_ap']);
///////////////////////////////////////////// SE OBTIENE LOS CORREOS/////////////////////////////////////////////////////////////////////////////////////
			$correos1=pg_query($conexion,"SELECT correo FROM correos_usuario WHERE id_usuario='$id_usuario'");
			$i=0;
			$correosA[0]=NULL;
			$correosA[1]=NULL;
			$correosA[2]=NULL;
			while($correos=pg_fetch_array($correos1)){
				$correosA[$i]=$correos['correo'];
				$i=$i+1;
			}
////////////////////////////////////////////// SE ENVIA CORREO AL LUGAR GANADOR//////////////////////////////////////////////////////////////////////////
											
											// Creación del objeto de la clase heredada
											$pdf = new PDF();
											$pdf->AliasNbPages();
											$pdf->AddPage();
											$pdf->SetFont('Times','',12);

											$pdf->Cell(40,10,utf8_decode('¡FELICIDADES!'),0,1);
											$pdf->Cell(40,10,utf8_decode('Su Cartel ha quedado entre los primeros lugares ganadores'),0,1);
											$pdf->Cell(40,10,utf8_decode('Clave del Cartel:'.$Clave2),0,1);
											$pdf->Cell(40,10,utf8_decode('Lugar Ganador:'.$lugar2),0,1);
											$pdf->Cell(40,10,utf8_decode('Le recordamos estar atento a las fechas importantes'),0,1);
											$pdf->Cell(40,10,utf8_decode('publicadas en la pagína del congreso.'),0,1);
											$pdf->Cell(40,10,utf8_decode(''),0,1);
											$pdf->Cell(40,10,utf8_decode('Telefonos: 56-23-18-86              56-23-18-90'),0,1);

											$archivoAdjunto = $pdf->Output("", "S");




											$mail = new PHPMailer(true);

											try {
												//Server settings
												$mail->SMTPDebug = 0;                      // Enable verbose debug output
												$mail->isSMTP();                                            // Send using SMTP
												$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
												$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
												$mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
												$mail->Password   = 'CongresoMate2020';                               // SMTP password
												$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
												$mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

												//Recipients
												$mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
												$mail->addAddress("$correosA[0]", "$nombre $apPA");
												if(!empty($correosA[2])){
													$mail->addAddress("$correosA[2]", "$nombre $apPA");
												}
												if(!empty($correosA[3])){
													$mail->addAddress("$correosA[3]", "$nombre $apPA");
												}


												/* Attachments
												$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
												$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
											*/
												// Content
												$mail->isHTML(true);                                  // Set email format to HTML
												$mail->Subject = 'Lugar Ganador';
												$mail->Body    = 'Lugar Ganador del concurso de carteles';
												$mail->addStringAttachment($archivoAdjunto, 'Lugar_Ganador.pdf');
												$mail->send();
												
											} catch (Exception $e) {
												echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
											}

			}
///////////////////////////////////////////SE INSERTA EL LUGAR PARA LA TERCERA CLAVE///////////////////////////////////////////////////////////
$ponencias=pg_query($conexion,"SELECT id_ponencia,titulo FROM ponencias WHERE id_tipo_ponencia='CA' and id_ponencia='$Clave3' and id_congreso='$congresoActual' and id_ponencia IN (SELECT id_ponencia FROM usuario_evalua_cartel WHERE id_congreso='$congresoActual' and calificacion is not null)");
									while($row2=pg_fetch_array($ponencias)){
										$id_ponenciaS=$row2['id_ponencia'];
										$tituloS=$row2['titulo'];
									$datosActu=pg_query($conexion,"SELECT id_ponencia, titulo FROM actualizacion_resumen WHERE id_ponencia='$id_ponenciaS' and id_congreso='$congresoActual'");
									$validacion=pg_fetch_row($datosActu);
									if($validacion[0]==NULL){
										pg_query($conexion, "UPDATE ponencia_cartel set lugar_ganador='$lugar3'
										where id_ponencia_cartel='$Clave3'");
									}else{
										$numeroC=pg_query($conexion,"SELECT MAX(numeracion) FROM actualizacion_cartel WHERE id_congreso='$congresoActual'  and id_ponencia_cartel='$Clave3'");
										$rowCa=pg_fetch_row($numeroC);
										$numeracionAC=@$rowCa[0];

										pg_query($conexion, "UPDATE ponencia_cartel set lugar_ganador='$lugar3'
										where id_ponencia_cartel='$Clave3' and numeracion='$numeracionAC'");
									}
								}
////////////////////////////////////////////// SE OBTIENE EL NOMBRE DEL AUTOR DEL CARTEL/////////////////////////////////////////////////////////////////
$datosAutor=pg_query($conexion,"SELECT u.id_usuario, u.nombres,u.primer_ap FROM usuario u, usuario_ponencias up WHERE up.id_ponencias='$Clave3' and u.id_usuario=up.id_usuario");
while($datosC1=pg_fetch_array($datosAutor)){
$id_usuario=$datosC1['id_usuario'];
$nombre=trim($datosC1['nombres']);
$apPA=trim($datosC1['primer_ap']);
///////////////////////////////////////////// SE OBTIENE LOS CORREOS/////////////////////////////////////////////////////////////////////////////////////
$correos1=pg_query($conexion,"SELECT correo FROM correos_usuario WHERE id_usuario='$id_usuario'");
$i=0;
$correosA[0]=NULL;
$correosA[1]=NULL;
$correosA[2]=NULL;
while($correos=pg_fetch_array($correos1)){
	$correosA[$i]=$correos['correo'];
	$i=$i+1;
}
////////////////////////////////////////////// SE ENVIA CORREO AL LUGAR GANADOR//////////////////////////////////////////////////////////////////////////
								
								// Creación del objeto de la clase heredada
								$pdf = new PDF();
								$pdf->AliasNbPages();
								$pdf->AddPage();
								$pdf->SetFont('Times','',12);

								$pdf->Cell(40,10,utf8_decode('¡FELICIDADES!'),0,1);
								$pdf->Cell(40,10,utf8_decode('Su Cartel ha quedado entre los primeros lugares ganadores'),0,1);
								$pdf->Cell(40,10,utf8_decode('Clave del Cartel:'.$Clave3),0,1);
								$pdf->Cell(40,10,utf8_decode('Lugar Ganador:'.$lugar3),0,1);
								$pdf->Cell(40,10,utf8_decode('Le recordamos estar atento a las fechas importantes'),0,1);
								$pdf->Cell(40,10,utf8_decode('publicadas en la pagína del congreso.'),0,1);
								$pdf->Cell(40,10,utf8_decode(''),0,1);
								$pdf->Cell(40,10,utf8_decode('Telefonos: 56-23-18-86              56-23-18-90'),0,1);

								$archivoAdjunto = $pdf->Output("", "S");




								$mail = new PHPMailer(true);

								try {
									//Server settings
									$mail->SMTPDebug = 0;                      // Enable verbose debug output
									$mail->isSMTP();                                            // Send using SMTP
									$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
									$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
									$mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
									$mail->Password   = 'CongresoMate2020';                               // SMTP password
									$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
									$mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

									//Recipients
									$mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
									$mail->addAddress("$correosA[0]", "$nombre $apPA");
									if(!empty($correosA[2])){
										$mail->addAddress("$correosA[2]", "$nombre $apPA");
									}
									if(!empty($correosA[3])){
										$mail->addAddress("$correosA[3]", "$nombre $apPA");
									}


									/* Attachments
									$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
									$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
								*/
									// Content
									$mail->isHTML(true);                                  // Set email format to HTML
									$mail->Subject = 'Lugar Ganador';
									$mail->Body    = 'Lugar Ganador del concurso de carteles';
									$mail->addStringAttachment($archivoAdjunto, 'Lugar_Ganador.pdf');
									$mail->send();
									
								} catch (Exception $e) {
									echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
								}

}



								if($Clave4!=NULL){
///////////////////////////////////////////SE INSERTA EL LUGAR PARA LA CUARTA CLAVE///////////////////////////////////////////////////////////
$ponencias=pg_query($conexion,"SELECT id_ponencia,titulo FROM ponencias WHERE id_tipo_ponencia='CA' and id_ponencia='$Clave4' and id_congreso='$congresoActual' and id_ponencia IN (SELECT id_ponencia FROM usuario_evalua_cartel WHERE id_congreso='$congresoActual' and calificacion is not null)");
									while($row2=pg_fetch_array($ponencias)){
										$id_ponenciaS=$row2['id_ponencia'];
										$tituloS=$row2['titulo'];
									$datosActu=pg_query($conexion,"SELECT id_ponencia, titulo FROM actualizacion_resumen WHERE id_ponencia='$id_ponenciaS' and id_congreso='$congresoActual'");
									$validacion=pg_fetch_row($datosActu);
									if($validacion[0]==NULL){
										pg_query($conexion, "UPDATE ponencia_cartel set lugar_ganador='$lugar4'
										where id_ponencia_cartel='$Clave4'");
									}else{
										$numeroC=pg_query($conexion,"SELECT MAX(numeracion) FROM actualizacion_cartel WHERE id_congreso='$congresoActual'  and id_ponencia_cartel='$Clave4'");
										$rowCa=pg_fetch_row($numeroC);
										$numeracionAC=@$rowCa[0];

										pg_query($conexion, "UPDATE ponencia_cartel set lugar_ganador='$lugar4'
										where id_ponencia_cartel='$Clave4' and numeracion='$numeracionAC'");
									}
								}
								////////////////////////////////////////////// SE OBTIENE EL NOMBRE DEL AUTOR DEL CARTEL/////////////////////////////////////////////////////////////////
			$datosAutor=pg_query($conexion,"SELECT u.id_usuario, u.nombres,u.primer_ap FROM usuario u, usuario_ponencias up WHERE up.id_ponencias='$Clave4' and u.id_usuario=up.id_usuario");
			while($datosC1=pg_fetch_array($datosAutor)){
			$id_usuario=$datosC1['id_usuario'];
			$nombre=trim($datosC1['nombres']);
			$apPA=trim($datosC1['primer_ap']);
///////////////////////////////////////////// SE OBTIENE LOS CORREOS/////////////////////////////////////////////////////////////////////////////////////
			$correos1=pg_query($conexion,"SELECT correo FROM correos_usuario WHERE id_usuario='$id_usuario'");
			$i=0;
			$correosA[0]=NULL;
			$correosA[1]=NULL;
			$correosA[2]=NULL;
			while($correos=pg_fetch_array($correos1)){
				$correosA[$i]=$correos['correo'];
				$i=$i+1;
			}
////////////////////////////////////////////// SE ENVIA CORREO AL LUGAR GANADOR//////////////////////////////////////////////////////////////////////////
											
											// Creación del objeto de la clase heredada
											$pdf = new PDF();
											$pdf->AliasNbPages();
											$pdf->AddPage();
											$pdf->SetFont('Times','',12);

											$pdf->Cell(40,10,utf8_decode('¡FELICIDADES!'),0,1);
											$pdf->Cell(40,10,utf8_decode('Su Cartel ha quedado entre los primeros lugares ganadores'),0,1);
											$pdf->Cell(40,10,utf8_decode('Clave del Cartel:'.$Clave4),0,1);
											$pdf->Cell(40,10,utf8_decode('Lugar Ganador:'.$lugar4),0,1);
											$pdf->Cell(40,10,utf8_decode('Le recordamos estar atento a las fechas importantes'),0,1);
											$pdf->Cell(40,10,utf8_decode('publicadas en la pagína del congreso.'),0,1);
											$pdf->Cell(40,10,utf8_decode(''),0,1);
											$pdf->Cell(40,10,utf8_decode('Telefonos: 56-23-18-86              56-23-18-90'),0,1);

											$archivoAdjunto = $pdf->Output("", "S");




											$mail = new PHPMailer(true);

											try {
												//Server settings
												$mail->SMTPDebug = 0;                      // Enable verbose debug output
												$mail->isSMTP();                                            // Send using SMTP
												$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
												$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
												$mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
												$mail->Password   = 'CongresoMate2020';                               // SMTP password
												$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
												$mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

												//Recipients
												$mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
												$mail->addAddress("$correosA[0]", "$nombre $apPA");
												if(!empty($correosA[2])){
													$mail->addAddress("$correosA[2]", "$nombre $apPA");
												}
												if(!empty($correosA[3])){
													$mail->addAddress("$correosA[3]", "$nombre $apPA");
												}


												/* Attachments
												$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
												$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
											*/
												// Content
												$mail->isHTML(true);                                  // Set email format to HTML
												$mail->Subject = 'Lugar Ganador';
												$mail->Body    = 'Lugar Ganador del concurso de carteles';
												$mail->addStringAttachment($archivoAdjunto, 'Lugar_Ganador.pdf');
												$mail->send();
												
											} catch (Exception $e) {
												echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
											}

			}
							}

							if($Clave5!=NULL){
///////////////////////////////////////////SE INSERTA EL LUGAR PARA LA QUINTA CLAVE///////////////////////////////////////////////////////////
$ponencias=pg_query($conexion,"SELECT id_ponencia,titulo FROM ponencias WHERE id_tipo_ponencia='CA' and id_ponencia='$Clave5' and id_congreso='$congresoActual' and id_ponencia IN (SELECT id_ponencia FROM usuario_evalua_cartel WHERE id_congreso='$congresoActual' and calificacion is not null)");
									while($row2=pg_fetch_array($ponencias)){
										$id_ponenciaS=$row2['id_ponencia'];
										$tituloS=$row2['titulo'];
									$datosActu=pg_query($conexion,"SELECT id_ponencia, titulo FROM actualizacion_resumen WHERE id_ponencia='$id_ponenciaS' and id_congreso='$congresoActual'");
									$validacion=pg_fetch_row($datosActu);
									if($validacion[0]==NULL){
										pg_query($conexion, "UPDATE ponencia_cartel set lugar_ganador='$lugar5'
										where id_ponencia_cartel='$Clave5'");
									}else{
										$numeroC=pg_query($conexion,"SELECT MAX(numeracion) FROM actualizacion_cartel WHERE id_congreso='$congresoActual'  and id_ponencia_cartel='$Clave5'");
										$rowCa=pg_fetch_row($numeroC);
										$numeracionAC=@$rowCa[0];

										pg_query($conexion, "UPDATE ponencia_cartel set lugar_ganador='$lugar5'
										where id_ponencia_cartel='$Clave5' and numeracion='$numeracionAC'");
									}
								}
								////////////////////////////////////////////// SE OBTIENE EL NOMBRE DEL AUTOR DEL CARTEL/////////////////////////////////////////////////////////////////
			$datosAutor=pg_query($conexion,"SELECT u.id_usuario, u.nombres,u.primer_ap FROM usuario u, usuario_ponencias up WHERE up.id_ponencias='$Clave5' and u.id_usuario=up.id_usuario");
			while($datosC1=pg_fetch_array($datosAutor)){
			$id_usuario=$datosC1['id_usuario'];
			$nombre=trim($datosC1['nombres']);
			$apPA=trim($datosC1['primer_ap']);
///////////////////////////////////////////// SE OBTIENE LOS CORREOS/////////////////////////////////////////////////////////////////////////////////////
			$correos1=pg_query($conexion,"SELECT correo FROM correos_usuario WHERE id_usuario='$id_usuario'");
			$i=0;
			$correosA[0]=NULL;
			$correosA[1]=NULL;
			$correosA[2]=NULL;
			while($correos=pg_fetch_array($correos1)){
				$correosA[$i]=$correos['correo'];
				$i=$i+1;
			}
////////////////////////////////////////////// SE ENVIA CORREO AL LUGAR GANADOR//////////////////////////////////////////////////////////////////////////
											
											// Creación del objeto de la clase heredada
											$pdf = new PDF();
											$pdf->AliasNbPages();
											$pdf->AddPage();
											$pdf->SetFont('Times','',12);

											$pdf->Cell(40,10,utf8_decode('¡FELICIDADES!'),0,1);
											$pdf->Cell(40,10,utf8_decode('Su Cartel ha quedado entre los primeros lugares ganadores'),0,1);
											$pdf->Cell(40,10,utf8_decode('Clave del Cartel:'.$Clave5),0,1);
											$pdf->Cell(40,10,utf8_decode('Lugar Ganador:'.$lugar5),0,1);
											$pdf->Cell(40,10,utf8_decode('Le recordamos estar atento a las fechas importantes'),0,1);
											$pdf->Cell(40,10,utf8_decode('publicadas en la pagína del congreso.'),0,1);
											$pdf->Cell(40,10,utf8_decode(''),0,1);
											$pdf->Cell(40,10,utf8_decode('Telefonos: 56-23-18-86              56-23-18-90'),0,1);

											$archivoAdjunto = $pdf->Output("", "S");




											$mail = new PHPMailer(true);

											try {
												//Server settings
												$mail->SMTPDebug = 0;                      // Enable verbose debug output
												$mail->isSMTP();                                            // Send using SMTP
												$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
												$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
												$mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
												$mail->Password   = 'CongresoMate2020';                               // SMTP password
												$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
												$mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

												//Recipients
												$mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
												$mail->addAddress("$correosA[0]", "$nombre $apPA");
												if(!empty($correosA[2])){
													$mail->addAddress("$correosA[2]", "$nombre $apPA");
												}
												if(!empty($correosA[3])){
													$mail->addAddress("$correosA[3]", "$nombre $apPA");
												}


												/* Attachments
												$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
												$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
											*/
												// Content
												$mail->isHTML(true);                                  // Set email format to HTML
												$mail->Subject = 'Lugar Ganador';
												$mail->Body    = 'Lugar Ganador del concurso de carteles';
												$mail->addStringAttachment($archivoAdjunto, 'Lugar_Ganador.pdf');
												$mail->send();
												
											} catch (Exception $e) {
												echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
											}

			}
							}


}
?>
<?php 
include ("Conexion.php");
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
					$row0=pg_fetch_row($consulta);
					$consulta0=$row0[0];
					$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
					$row=pg_fetch_row($consulta1);
					$congresoActual=$row[0];//es el id congreso
$contacto=pg_query($conexion,"SELECT cn.telefono,cn.telefono2,cn.email FROM contacto cn, congreso c
WHERE c.id_congreso='$congresoActual'
and  c.id_contacto =cn.id_contacto");
$contactos=pg_fetch_array($contacto);
$correoC=@$contactos[2];
$numeroC1=@$contactos[0];
$numeroC2=@$contactos[1];
?><!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title style="background-image: url('images/logos.jpg');">CISEMATH</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/cismath.png">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" /> 

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
      <?php
    ////Codigo para actualizar el banner
        include ('Conexion.php');
        $nmax=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
        $num=pg_fetch_row($nmax);
        $numcon=$num[0];
        $conact=pg_query($conexion,"SELECT id_congreso, banner FROM congreso WHERE numero_congreso='$numcon'");
        $ban=pg_fetch_row($conact);
        $banner=$ban[1];
        echo '<img src="'.$banner.'" style="width:100%; height:100%; background-size: cover; position: relative; display: flex;"/>';
    ?>

    <header class="top-header" >
        <div class="header_bottom">
          <div class="container">
		  <div class="row"> 
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 col-xl-12">
                <div class="menu_orange_section" style="background: #052450;">
                   <nav class="navbar header-nav navbar-expand-lg"> 
                     <div class="menu_section">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav" >
					    <li><a class="nav-link active" href="indexus.php"><img src="images/i4.png" alt="image"></a> </li>
                        <li><a class="nav-link active" href="acercadeus.php">Acerca de</a></li>
                        <li><a class="nav-link active" href="infus.php">Información</a></li>
                        <li><a class="nav-link active" href="instruccionesus.php">Instrucciones</a></li>
                        <li><a class="nav-link active" href="comitesus.php">Comités</a></li>
						<li><a class="nav-link active" href="memoriasus.php">Memorias</a></li>
						
						<li>
						<a style="position:relative; display:block;">
						<img src="images/user.png" alt="image"> 
						</a>
						</li>
						<li>
						<ul class="nav"><li class="dropdown langmenu" ><a href="#" style="display:block;"class="dropdown-toggle" data-toggle="dropdown" title="Idioma"><b class="caret"></b></a><ul class="dropdown-menu" style="color:black;"><li><a title="English ‎(en)‎" href="menu.php" style="color:black";>Menú‎‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="editarp.php"  style="color:black";>Perfil‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="index.php"  style="color:black";>Cerrar Sesión</a></li></ul></li></ul>
						</li>
						
                    </ul>
                </div>
                     </div>
                 </nav>
                 
                </div>
            </div>
          </div>
		  </div>
        </div>
        
    </header>
    <!-- End header -->
 
 <!-- Start CONTENIDO -->
 <div class="container">
		<div class="row">
			<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			 
				<div class="titulo" style="margin-top:8%">
					<h1 align="center" style="color:#052450;">ASIGNACIÓN DE GANADORES DE CARTELES</h1>
				</div>
			</div>
		</div>	 
	</div>	
 <div class="container">
		<div class="row" style="display:flex; justify-content:center;  margin-top:6%; margin-bottom:2%;">
			<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="table-responsive">
					<table  border=1 class="table table-bordered" align="center" style="text-align:left; margin-top:2%;">
						<tr>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">CLAVE</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">TÍTULO</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">AUTOR</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">IMAGEN</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">RÚBRICA</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">CALIFICACIÓN </font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">EVALUADOR</font>
							</td>
						</tr>
						<?php
						$numeroC=@pg_query($conexion,"SELECT MAX(numeracion) FROM actualizacion_cartel WHERE id_congreso='$congresoActual'");
						$rowCa1=@pg_fetch_row($numeroC);
						$numeracion=$rowCa1[0];
						if($numeracion!=NULL){
						$numeroR=pg_query($conexion,"SELECT MAX(numeracion) FROM actualizacion_resumen WHERE id_congreso='$congresoActual'");
						$rowResumenes=pg_fetch_row($numeroR);
						$numeracionR=@$rowResumenes[0];
						$datost=pg_query($conexion,"SELECT p.id_ponencia,ap.titulo,u.nombres,u.primer_ap, u.segundo_ap, ac.imagen, uec.rubrica,  uec.id_usuario
						FROM ponencias p, actualizacion_resumen ap, usuario u, actualizacion_cartel ac, usuario_evalua_cartel uec, usuario_ponencias up
						WHERE p.id_tipo_ponencia='CA'
						and p.id_ponencia=ap.id_ponencia
						and up.id_ponencias=ap.id_ponencia
						and up.id_usuario=u.id_usuario
						and ac.id_ponencia_cartel=p.id_ponencia
						and ac.numeracion='$numeracion'
						and uec.id_ponencia_cartel=p.id_ponencia
						and p.id_congreso='$congresoActual'
						and ap.id_congreso=p.id_congreso
						and uec.id_congreso=p.id_congreso
						and uec.calificacion IS NOT NULL
						and up.tipo_autor='Autor'");
						while($datostabla=pg_fetch_array($datost)){
						$n=@trim($datostabla['nombres']);
						$p=@trim($datostabla['primer_ap']);
						$m=@trim($datostabla['segundo_ap']);
						$Autor="$n $p $m";
						$id_ponencia=trim($datostabla['id_ponencia']);
						$titulo=trim($datostabla['titulo']);
						$imagen=trim($datostabla['imagen']);
						$id_evaluador=trim($datostabla['id_usuario']);
						$rubrica=trim($datostabla['rubrica']);
							$consultaCalif=pg_query($conexion,"SELECT calificacion FROM usuario_evalua_cartel WHERE id_ponencia_cartel='$id_ponencia'");
							$suma=0;
							$i=0;
							while($calif=pg_fetch_array($consultaCalif)){
								$suma=$calif['calificacion'];
								$i=$i+1;
							}
							$calificacion=$suma/$i;

					
						////////////////////////////SE CONSULTA EVALUADOR/////////////
						$datosE=pg_query($conexion,"SELECT u.nombres, u.primer_ap, u.segundo_ap FROM usuario u, usuario_evalua_cartel uec
						WHERE uec.id_usuario='$id_evaluador' and uec.id_congreso='$congresoActual'
							  and uec.id_usuario=u.id_usuario  ");
						$row1=pg_fetch_row($datosE);
						$nE=@trim($row1[0]);
						$pE=@trim($row1[1]);
						$mE=@trim($row1[2]);
						$evaluador="$nE $pE $mE";
						
						echo"	
						<tr>
							<td>
								$id_ponencia
							</td>
							<td>
								$titulo
							</td>
							<td>
								$Autor
							</td>
							<td>
							<a title='Descargar Archivo' href=' $imagen ' download=' $imagen ?>' style='color: blue; font-size:18px;'> Descargar Imagen</span> </a>
								
							</td>
							<td>
							<a title='Descargar Archivo' href=' $rubrica ' download='$rubrica' style='color: blue; font-size:18px;'> Descargar Rúbrica</span> </a>
							
							</td>
							<td>
								$calificacion 
							</td>
							<td>
								$evaluador
							</td>
						</tr>";
						}
						}
						?>

						
					</table>
				</div>
			</div>
			<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="table-responsive">
				<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="post">
					<table cellpadding="2px" cellspacing="2px" align="center" style="text-align:left; margin-top:2%;">
						<tr>
							<td colspan="2">
								<font style="font-size:18px; color:#052450; text-align:left;">Seleccione un trabajo e ingrese su lugar correspondiente</font>
								<br>
							</td>
						</tr>
						<tr style="font-size:18px; color:#052450;">
							<td>
								1.-
							</td>
						</tr>
						<tr>
							<td>
								Clave del trabajo
							</td>
							<td>
								<select  class="form-control" name="select1" required="" style="width:100%;">
								  <option disabled="" >Clave de trabajo</option>
								  <?php
								  ////////////////////////////////////////////// Se consultan todos los carteles ///////////////////////////////////////
								  $ponencias=pg_query($conexion,"SELECT id_ponencia,titulo, estatus_resumen FROM ponencias WHERE id_tipo_ponencia='CA' and id_congreso='$congresoActual'");

									while($row2=pg_fetch_array($ponencias)){
										$id_ponenciaS=$row2['id_ponencia'];
										$estatus_p=trim($row2['estatus_resumen']);
										$tituloS=$row2['titulo'];
										/////////////////////////////////////////////////////////////////// Se consulta si existe actualizaciones //////////////////////////////////////////////////////////////////////////////////////////
										$datosActu=pg_query($conexion,"SELECT id_ponencia, titulo FROM actualizacion_resumen WHERE id_ponencia='$id_ponenciaS' and id_congreso='$congresoActual' and estatus_actualizacion='Aceptado'  ");
										$validacion=pg_fetch_row($datosActu);
										if($validacion[0]==NULL  ){
											///////////////////////////////////////////// si no hay actualizaciones se condiciona que el resumen este aceptado, para poder ser mostrado/////////////////////////////////////////////////////
											if($estatus_p=='Aceptado'){
											echo "<option value='$id_ponenciaS' >$tituloS </option>";
											}
										}else{
											///////////////////////////////////////////// Si, sí existen actualizaciones se muestran la actualizacion que fue aceptada//////////////////////////////////////////////////////////////////////
											$id_ponenciaA=$validacion[0];
											$tituloA=$validacion[1];
											echo "<option value='$id_ponenciaA' >$tituloA </option>";
										}
										

									}
								?>
								 </select>
								
							</td>
						</tr>
						<tr>
							<td>
								Lugar Ganador
							</td>
							<td>
							<select  class="form-control" name="lugar1" style="width:100%;">
								  <option disabled="" >Seleccione un Lugar</option>
								  <option value="Primer Lugar" >Primer Lugar</option>
								  <option value="Segundo Lugar" >Segundo Lugar</option>
								  <option value="Tercer Lugar" >Tercer Lugar</option>
							</select>
							</td>
						</tr>
						<tr style="font-size:18px; color:#052450;">
							<td>
								2.-
							</td>
						</tr>
						<tr>
							<td>
								Clave del trabajo
							</td>
							<td>
							<select  class="form-control" name="select2" required="" style="width:100%;">
								  <option disabled="" >Clave de trabajo</option>
								  <?php
								  ////////////////////////////////////////////// Se consultan todos los carteles ///////////////////////////////////////
								  $ponencias=pg_query($conexion,"SELECT id_ponencia,titulo, estatus_resumen FROM ponencias WHERE id_tipo_ponencia='CA' and id_congreso='$congresoActual'");

									while($row2=pg_fetch_array($ponencias)){
										$id_ponenciaS=$row2['id_ponencia'];
										$estatus_p=trim($row2['estatus_resumen']);
										$tituloS=$row2['titulo'];
										/////////////////////////////////////////////////////////////////// Se consulta si existe actualizaciones //////////////////////////////////////////////////////////////////////////////////////////
										$datosActu=pg_query($conexion,"SELECT id_ponencia, titulo FROM actualizacion_resumen WHERE id_ponencia='$id_ponenciaS' and id_congreso='$congresoActual' and estatus_actualizacion='Aceptado'  ");
										$validacion=pg_fetch_row($datosActu);
										if($validacion[0]==NULL  ){
											///////////////////////////////////////////// si no hay actualizaciones se condiciona que el resumen este aceptado, para poder ser mostrado/////////////////////////////////////////////////////
											if($estatus_p=='Aceptado'){
											echo "<option value='$id_ponenciaS' >$tituloS </option>";
											}
										}else{
											///////////////////////////////////////////// Si, sí existen actualizaciones se muestran la actualizacion que fue aceptada//////////////////////////////////////////////////////////////////////
											$id_ponenciaA=$validacion[0];
											$tituloA=$validacion[1];
											echo "<option value='$id_ponenciaA' >$tituloA </option>";
										}
										

									}
								?>
								 </select>
							</td>
						</tr>
						<tr>
							<td>
								Lugar Ganador
							</td>
							<td>
							<select  class="form-control" name="lugar2" style="width:100%;">
								  <option disabled="" >Seleccione un Lugar</option>
								  <option value="Primer Lugar" >Primer Lugar</option>
								  <option value="Segundo Lugar" >Segundo Lugar</option>
								  <option value="Tercer Lugar" >Tercer Lugar</option>
							</select>
								</td>
						</tr>
						<tr style="font-size:18px; color:#052450;">
							<td>
								3.-
							</td>
						</tr>
						<tr>
							<td>
								Clave del trabajo
							</td>
							<td>
							<select  class="form-control" name="select3" required="" style="width:100%;">
								  <option disabled="" >Clave de trabajo</option>
								  <?php
								  ////////////////////////////////////////////// Se consultan todos los carteles ///////////////////////////////////////
								  $ponencias=pg_query($conexion,"SELECT id_ponencia,titulo, estatus_resumen FROM ponencias WHERE id_tipo_ponencia='CA' and id_congreso='$congresoActual'");

									while($row2=pg_fetch_array($ponencias)){
										$id_ponenciaS=$row2['id_ponencia'];
										$estatus_p=trim($row2['estatus_resumen']);
										$tituloS=$row2['titulo'];
										/////////////////////////////////////////////////////////////////// Se consulta si existe actualizaciones //////////////////////////////////////////////////////////////////////////////////////////
										$datosActu=pg_query($conexion,"SELECT id_ponencia, titulo FROM actualizacion_resumen WHERE id_ponencia='$id_ponenciaS' and id_congreso='$congresoActual' and estatus_actualizacion='Aceptado'  ");
										$validacion=pg_fetch_row($datosActu);
										if($validacion[0]==NULL  ){
											///////////////////////////////////////////// si no hay actualizaciones se condiciona que el resumen este aceptado, para poder ser mostrado/////////////////////////////////////////////////////
											if($estatus_p=='Aceptado'){
											echo "<option value='$id_ponenciaS' >$tituloS </option>";
											}
										}else{
											///////////////////////////////////////////// Si, sí existen actualizaciones se muestran la actualizacion que fue aceptada//////////////////////////////////////////////////////////////////////
											$id_ponenciaA=$validacion[0];
											$tituloA=$validacion[1];
											echo "<option value='$id_ponenciaA' >$tituloA </option>";
										}
										

									}
								?>
								 </select>
							</td>
						</tr>
						<tr>
							<td>
								Lugar Ganador
							</td>
							<td>
							<select  class="form-control" name="lugar3" style="width:100%;">
								  <option disabled="" >Seleccione un Lugar</option>
								  <option value="Primer Lugar" >Primer Lugar</option>
								  <option value="Segundo Lugar" >Segundo Lugar</option>
								  <option value="Tercer Lugar" >Tercer Lugar</option>
							</select>
							</td>
						</tr>
						<tr>
							<td style="font-size:18px; color:#052450;">
								Si no hay mas lugares puede dejar estos campos vacios
							</td>
						</tr>
						<tr style="font-size:18px; color:#052450;">
							<td>
								4.-
							</td>
						</tr>
						<tr>
							<td>
								Clave del trabajo
							</td>
							<td>
							<select  class="form-control" name="select4" style="width:100%;">
								  <option disabled="" >Clave de trabajo</option>
								  <?php
								  ////////////////////////////////////////////// Se consultan todos los carteles ///////////////////////////////////////
								  $ponencias=pg_query($conexion,"SELECT id_ponencia,titulo, estatus_resumen FROM ponencias WHERE id_tipo_ponencia='CA' and id_congreso='$congresoActual'");

									while($row2=pg_fetch_array($ponencias)){
										$id_ponenciaS=$row2['id_ponencia'];
										$estatus_p=trim($row2['estatus_resumen']);
										$tituloS=$row2['titulo'];
										/////////////////////////////////////////////////////////////////// Se consulta si existe actualizaciones //////////////////////////////////////////////////////////////////////////////////////////
										$datosActu=pg_query($conexion,"SELECT id_ponencia, titulo FROM actualizacion_resumen WHERE id_ponencia='$id_ponenciaS' and id_congreso='$congresoActual' and estatus_actualizacion='Aceptado'  ");
										$validacion=pg_fetch_row($datosActu);
										if($validacion[0]==NULL  ){
											///////////////////////////////////////////// si no hay actualizaciones se condiciona que el resumen este aceptado, para poder ser mostrado/////////////////////////////////////////////////////
											if($estatus_p=='Aceptado'){
											echo "<option value='$id_ponenciaS' >$tituloS </option>";
											}
										}else{
											///////////////////////////////////////////// Si, sí existen actualizaciones se muestran la actualizacion que fue aceptada//////////////////////////////////////////////////////////////////////
											$id_ponenciaA=$validacion[0];
											$tituloA=$validacion[1];
											echo "<option value='$id_ponenciaA' >$tituloA </option>";
										}
										

									}
								?>
								 </select>
							</td>
						</tr>
						<tr>
							<td>
								Lugar Ganador
							</td>
							<td>
							<select  class="form-control" name='lugar4' style="width:100%;">
								  <option disabled="" >Seleccione un Lugar</option>
								  <option value="Primer Lugar" >Primer Lugar</option>
								  <option value="Segundo Lugar" >Segundo Lugar</option>
								  <option value="Tercer Lugar" >Tercer Lugar</option>
							</select>
							</td>
						</tr>
						<tr style="font-size:18px; color:#052450;">
							<td>
								5.-
							</td>
						</tr>
						<tr>
							<td>
								Clave del trabajo
							</td>
							<td>
							<select  class="form-control" name="select5" style="width:100%;">
								  <option disabled="" >Clave de trabajo</option>
								  <?php
								  ////////////////////////////////////////////// Se consultan todos los carteles ///////////////////////////////////////
								  $ponencias=pg_query($conexion,"SELECT id_ponencia,titulo, estatus_resumen FROM ponencias WHERE id_tipo_ponencia='CA' and id_congreso='$congresoActual'");

									while($row2=pg_fetch_array($ponencias)){
										$id_ponenciaS=$row2['id_ponencia'];
										$estatus_p=trim($row2['estatus_resumen']);
										$tituloS=$row2['titulo'];
										/////////////////////////////////////////////////////////////////// Se consulta si existe actualizaciones //////////////////////////////////////////////////////////////////////////////////////////
										$datosActu=pg_query($conexion,"SELECT id_ponencia, titulo FROM actualizacion_resumen WHERE id_ponencia='$id_ponenciaS' and id_congreso='$congresoActual' and estatus_actualizacion='Aceptado'  ");
										$validacion=pg_fetch_row($datosActu);
										if($validacion[0]==NULL  ){
											///////////////////////////////////////////// si no hay actualizaciones se condiciona que el resumen este aceptado, para poder ser mostrado/////////////////////////////////////////////////////
											if($estatus_p=='Aceptado'){
											echo "<option value='$id_ponenciaS' >$tituloS </option>";
											}
										}else{
											///////////////////////////////////////////// Si, sí existen actualizaciones se muestran la actualizacion que fue aceptada//////////////////////////////////////////////////////////////////////
											$id_ponenciaA=$validacion[0];
											$tituloA=$validacion[1];
											echo "<option value='$id_ponenciaA' >$tituloA </option>";
										}
										

									}
								?>
								 </select>
							</td>
						</tr>
						<tr>
							<td>
								Lugar Ganador
							</td>
							<td>
							<select  class="form-control" name="lugar5" style="width:100%;">
								  <option disabled="" >Seleccione un Lugar</option>
								  <option value="Primer Lugar" >Primer Lugar</option>
								  <option value="Segundo Lugar" >Segundo Lugar</option>
								  <option value="Tercer Lugar" >Tercer Lugar</option>
							</select>
								</td>
						</tr>
						
					</table>
				
				</div>
			</div>
		</div>
			
	</div>
	
	<div class="btn" style="text-align:right; width:100%;  margin-top:2%; margin-bottom:6%;">
		<button name="submit1" style="margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;">Guardar</button>
		</form>
		<button type="B1" onclick="location.href='menucomite.php'" style="margin-right:5%; background: whitesmoke; color: black; padding: 6px 10px; border-color: whitesmoke;border-radius: 20px;">Regresar</button>
	</div>
	
    <!-- End CONTENIDO -->
    
  <!-- Start Footer -->
        <footer class="footer-box">
        <div class="container">
            <div class="row">
               <div class="col-md-12 white_fonts" style="display: contents;">
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            
                            <div class="full">
							<h3>Dirección</h3>
                                <ul class="menu_footer">   
								     
								     <li><img src="images/i5.png"><span style="font-size: 85%;">Carr, Cuautitlán - Teoloyucan, San Sebastian Xhala, 54714 Cuautitlán Izcalli</span></li>

                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="full">
                                <div class="footer_blog full white_fonts">
                             <h3>Contacto</h3>
                             <ul class="full">
                               <li><img src="images/i6.png"><span style="font-size: 85%;"> <?php echo $correoC  ?></span></li>
                               <li><img src="images/i7.png"><span style="font-size: 85%;"><?php echo "$numeroC1    $numeroC2"; ?></span></li>
                             </ul>
                         </div>
                            </div>
                        </div>
                </div>
			 </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link active" href="derechosa.php" style="color:white">© Sitio desarrollado por CDD Solutions S.A. y administrado por el Departamento de Matemáticas. Hecho en México, (2020 - 2021)</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
	/* counter js */

(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
	</script>
</body>

</html>