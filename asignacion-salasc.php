<?php
session_start();
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");	
}
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php'; 
require ('libreria/fpdf.php');
$usuario=$_SESSION['Usuario'];
include ('Conexion.php');
//TRAE EL NÚMERO DE LA CONGRESO
	$consulta_num_congreso=pg_query($conexion,"SELECT MAX(numero_congreso) FROM congreso");
	$row0=pg_fetch_row($consulta_num_congreso);
	$num_congreso=$row0[0];

//TRAE EL ID DE LA CONGRESO CON AYUDA DE SU NUMERO
	$consulta_id_congreso=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$num_congreso'"); 
	$row1=pg_fetch_row($consulta_id_congreso);
	$id_congresoactual=$row1[0];

	$banderaPonencias="0";
	$banderaSalas="0";
	//NUMERO DE TRABAJOS ACEPTADOS DE LAS PONENCIAS ORALES Y QUE NO ESTEN YA REGISTRADAS EN LA SALAS
	$consultaNumPonenciasOrales=pg_query($conexion, "SELECT id_ponencia_oral FROM ponencia_oral  WHERE estatus_extenso='Aceptado' AND id_congreso='$id_congresoactual' and id_ponencia_oral NOT IN(SELECT id_ponencia FROM salas_ponencias WHERE id_congreso='$id_congresoactual')
		UNION
		SELECT id_ponencia_oral  FROM actualizacion_p_oral WHERE estatus_actualizacion='Aceptado'AND id_congreso='$id_congresoactual' and id_ponencia_oral NOT IN(SELECT id_ponencia FROM salas_ponencias WHERE id_congreso='$id_congresoactual')");
 	$numPonenciaOrales=pg_num_rows($consultaNumPonenciasOrales);
	//NUMERO DE LAS IMAGENES ACEPTADAS Y QUE NO ESTEN EN LAS SALAS YA REGISTRADAS
 	$consultaNumCarteles=pg_query($conexion, "SELECT id_ponencia_cartel FROM ponencia_cartel WHERE estatus_imagen='Aceptado' AND id_congreso='$id_congresoactual' and id_ponencia_cartel NOT IN(SELECT id_ponencia FROM salas_ponencias WHERE id_congreso='$id_congresoactual')
	UNION
	SELECT id_ponencia_cartel FROM actualizacion_cartel WHERE estatus_actualizacion='Aceptado' AND id_congreso='$id_congresoactual' and id_ponencia_cartel NOT IN(SELECT id_ponencia FROM salas_ponencias WHERE id_congreso='$id_congresoactual')");
	$numCarteles=pg_num_rows($consultaNumCarteles);

	

	
	if($numPonenciaOrales===0 && $numCarteles===0){		
		$banderaPonencias="1";
	}else{
		$consultaPonenciasOrales=pg_query($conexion, "SELECT id_ponencia_oral FROM ponencia_oral  WHERE estatus_extenso='Aceptado' AND id_congreso='$id_congresoactual' and id_ponencia_oral NOT IN(SELECT id_ponencia FROM salas_ponencias WHERE id_congreso='$id_congresoactual')
		UNION
		SELECT id_ponencia_oral  FROM actualizacion_p_oral WHERE estatus_actualizacion='Aceptado'AND id_congreso='$id_congresoactual' and id_ponencia_oral NOT IN(SELECT id_ponencia FROM salas_ponencias WHERE id_congreso='$id_congresoactual')");
		//NUMERO DE LAS IMAGENES ACEPTADAS Y QUE NO ESTEN EN LAS SALAS YA REGISTRADAS
	 	$consultaCarteles=pg_query($conexion, "SELECT id_ponencia_cartel FROM ponencia_cartel WHERE estatus_imagen='Aceptado' AND id_congreso='$id_congresoactual' and id_ponencia_cartel NOT IN(SELECT id_ponencia FROM salas_ponencias WHERE id_congreso='$id_congresoactual')
		UNION
		SELECT id_ponencia_cartel FROM actualizacion_cartel WHERE estatus_actualizacion='Aceptado' AND id_congreso='$id_congresoactual' and id_ponencia_cartel NOT IN(SELECT id_ponencia FROM salas_ponencias WHERE id_congreso='$id_congresoactual')");

		
	}





	$consultaNumSalas=pg_query($conexion, "SELECT id_sala, nombre_sala FROM salas");
	$numSalas=pg_num_rows($consultaNumSalas);
	
	if($numSalas===0){
		
		$banderaSalas="1";
	}else{
		$consultaSalas=pg_query($conexion, "SELECT id_sala, nombre_sala FROM salas");
	}

	if(isset($_GET['id_ponencia'])){
		$idp=$_GET['id_ponencia'];
		if($idp!==""){
			$idponenciaSeleccionada=$idp;
		}	
	}

	if(isset($_GET['id_sala'])){
		$idSeleccionado=$_GET['id_sala'];
		if($idSeleccionado!==""){
		$consultaCupo=pg_query($conexion, "SELECT cupo FROM salas WHERE id_sala='$idSeleccionado'");
		$resultadoCupo=pg_fetch_assoc($consultaCupo);
		$cupo=$resultadoCupo['cupo'];
		}
	}
	if(isset($_POST['agregar'])){
		$id_ponencia=@$_POST['idTrabajo'];
		$id_sala=@$_POST['nombreSala'];
		$hora=@$_POST['hora'];
		$fecha=@$_POST['fecha'];
		$anioFecha=substr("$fecha",0,4);
		$anio=@$_POST['anio'];
		
		if($fecha===""){
			echo"<script> alert('Error, Selecciona una fecha.'')</script>";
		}else if( $anio===""){
			echo"<script> alert('Error, Selecciona un año.')</script>";
		}else if($hora===""){
				echo"<script> alert('Error, Selecciona una hora.'')</script>";
		}else if($id_ponencia===""){
			echo"<script> alert('Error, Selecciona una ponencia para registrar.')</script>";
		}else if($id_sala===""){
			echo"<script> alert('Error, Selecciona una sala.')</script>";
		}else if ($anio !=="" && $anioFecha!=="" && $anio!==$anioFecha) {
			echo"<script> alert('Error, Selecciona el año correspondiente a la Fecha indicada.'); window.location='asignacion-salasc.php';</script>";
		}else if($fecha!=="" && $anio!=="" && $hora!=="" && $id_ponencia!=="" && $id_sala!==""){//VALIDACION DE QUE NO EXITA UNA PONENCIA EN ESA HORA, FECHA DE ESA SALA:
		
		$consultaPonenciasAnterioes=pg_query($conexion, "SELECT sp.id_ponencia FROM salas_ponencias AS sp, salas AS s, ponencias AS p
						WHERE sp.id_congreso ='$id_congresoactual' AND sp.id_sala=s.id_sala AND
						p.id_ponencia=sp.id_ponencia AND p.id_congreso = sp.id_congreso
						AND sp.hora='$hora' AND fecha='$fecha' AND sp.id_sala='$id_sala'");
						$resultadosAnteriores=pg_fetch_assoc($consultaPonenciasAnterioes);
						
					
						if($resultadosAnteriores!==NULL && !empty($resultadosAnteriores)){
							echo"<script> alert('Error, Ya hay una ponencia registrada en esa sala a la misma fecha y hora.'); window.location='asignacion-salasc.php';</script>";
						}else{//SI NO HAY PONENCIAS ´REGISTRADAS EN LA MISMA SALA, FECHA Y HORA
							$insertarDatos=pg_query($conexion,"INSERT INTO salas_ponencias(id_sala,id_ponencia,hora,fecha, anio,id_congreso) VALUES('$id_sala','$id_ponencia','$hora','$fecha','$anio','$id_congresoactual')");
							if ($insertarDatos) {//SI REGISTRA EN LA TABLA SALAS_PONENCIAS
							$consultaInfoSala=pg_query($conexion,"SELECT s.nombre_sala, s.cupo, s.referencia, p.titulo FROM salas as s, ponencias as p WHERE S.id_sala='$id_sala' and p.id_ponencia='$id_ponencia'");
								$infoSala=pg_fetch_assoc($consultaInfoSala);
								
								
								 //CORREO AUTOR
	                                    //**************************************************************************************
	                                    class PDF extends FPDF{
	                                    // Cabecera de página
	                                    function Header()
	                                    {
	                                        // Logo
	                                        $this->Image('logo.jpg',0,0,220);
	                                        // Arial bold 15
	                                        $this->SetFont('Arial','B',15);
	                                        // Movernos a la derecha
	                                        $this->Cell(80);
	                                        // Título
	                                        $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
	                                        // Salto de línea
	                                        $this->Ln(50);
	                                        
	                                    }
	                                    
	                                    // Pie de página
	                                    function Footer() {
	                                        // Posición: a 1,5 cm del final
	                                        $this->SetY(-15);
	                                        // Arial italic 8
	                                        $this->SetFont('Arial','I',8);
	                                        // Número de página
	                                        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	                                        }
	                                    }
	                                     // Creación del objeto de la clase heredada
	                                    $pdf = new PDF();
	                                    $pdf->AliasNbPages();
	                                    $pdf->AddPage();
	                                    $pdf->SetFont('Times','',12);
	                                    
	                                    $pdf->Cell(40,10,utf8_decode('Información del trabajo a presentar'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('La información respecto a el trabajo que desea presentar es el siguiente'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Clave: '.$id_ponencia),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Nombre: '.$infoSala['titulo']),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Con respecto a la sala donde presentará su trabajo:'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('El nombre de la sala es: '.$infoSala['nombre_sala']),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('el cupo designado para esta taller es de: '.$infoSala['cupo']),0,1); 
	                                    $pdf->Cell(40,10,utf8_decode('las referencias donde se impartira el taller son las siguientes: '.$infoSala['referencia']),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('la fecha y hora en que se impartira dentro del congreso son: '.$fecha." ".$hora),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    
	                                    $archivoAdjunto = $pdf->Output("", "S");
	                                    $correoa=pg_query($conexion,"SELECT c.correo,us.nombres,us.primer_ap,us.segundo_ap, us.rfc 
																FROM correos_usuario c, usuario us, usuario_ponencias as up
																WHERE up.id_ponencias='$id_ponencia'
																and up.tipo_autor='Autor'and up.id_usuario = c.id_usuario and
																c.id_usuario= us.id_usuario");
	                                    $i=0;
	                                    while($mostrarCR=pg_fetch_array($correoa)){
	                                        $cor[$i]=trim($mostrarCR['correo']);
	                                        $nombre=trim($mostrarCR['nombres']);
	                                        $apPA=trim($mostrarCR['primer_ap']);
	                                        $apMa=trim($mostrarCR['segundo_ap']);
	                                        
	                                        $i=$i+1;
	                                    }

	                                      $mail = new PHPMailer(true);
	                                    
	                                    try {
	                                        //Server settings
	                                        $mail->SMTPDebug = 0;                      // Enable verbose debug output
	                                        $mail->isSMTP();                                            // Send using SMTP
	                                        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
	                                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
	                                        $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
	                                        $mail->Password   = 'CongresoMate2020';                               // SMTP password
	                                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
	                                        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
	                                        $mail->CharSet  = 'UTF-8';//PARA EL ACENTO
	                                    
	                                        //Recipients
	                                        $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
	                                        $mail->addAddress("$cor[0]", "$nombre $apPA");
	                                        if(!empty($cor[1])){
	                                            $mail->addAddress("$cor[1]", "$nombre $apPA");
	                                        }
	                                        if(!empty($cor[2])){
	                                            $mail->addAddress("$cor[2]", "$nombre $apPA");
	                                        }
	                                       // Content
	                                        $mail->isHTML(true);                                  // Set email format to HTML
	                                        $mail->Subject = 'Información del trabajo a presentar';
	                                        $mail->Body    = 'En el siguiente documento se adjuntan la información de la sala designada para presentar.';
	                                        $mail->addStringAttachment($archivoAdjunto, 'Información_Sala.pdf');
	                                        $mail->send();
	                                        
	                                        }catch (Exception $e) {
	                                            echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
	                                        }

	                                    //*****************************************************************************************
	                                        $numCoautoresTrabajo=pg_query($conexion, "SELECT COUNT (*) id_usuario FROM usuario_ponencias 
					                        WHERE id_ponencias ='$id_ponencia' and tipo_autor='Coautor' and id_congreso = '$id_congresoactual'");
					                        $numCoautores =pg_fetch_assoc($numCoautoresTrabajo);
					                        

					                        //VALIDA SI HAY COAUTORES
					                            if($numCoautores['id_usuario']!=0){
					                                  
					//********************************************************************************************************************
					                                class PDF2 extends FPDF
					                        {
					                        // Cabecera de página
					                        function Header()
					                        {
					                            // Logo
					                            $this->Image('logo.jpg',0,0,220);
					                            // Arial bold 15
					                            $this->SetFont('Arial','B',15);
					                            // Movernos a la derecha
					                            $this->Cell(80);
					                            // Título
					                            $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
					                            // Salto de línea
					                            $this->Ln(50);
					                            
					                        }
					                        
					                        // Pie de página
					                        function Footer()
					                        {
					                            // Posición: a 1,5 cm del final
					                            $this->SetY(-15);
					                            // Arial italic 8
					                            $this->SetFont('Arial','I',8);
					                            // Número de página
					                            $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
					                        	}
					                        }
					                        
					                        // Creación del objeto de la clase heredada
					                         $pdf = new PDF();
					                        $pdf->AliasNbPages();
					                        $pdf->AddPage();
					                        $pdf->SetFont('Times','',12);

					                    $pdf->Cell(40,10,utf8_decode('Información del trabajo a presentar'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('La información respecto a la asignación de sala'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Clave: '.$id_ponencia),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Nombre: '.$infoSala['titulo']),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Con respecto a la sala donde se encuentra registrado su trabajo:'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('El nombre de la sala es: '.$infoSala['nombre_sala']),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('el cupo designado para esta taller es de: '.$infoSala['cupo']),0,1); 
	                                    $pdf->Cell(40,10,utf8_decode('las referencias donde se impartira el taller son las siguientes: '.$infoSala['referencia']),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('la fecha y hora en que se impartira dentro del congreso son: '.$fecha." ".$hora),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);

					                        $archivoAdjunto2 = $pdf->Output("", "S");
					    
					                        //Envio de correo coautores
					                        $mail = new PHPMailer(true);
					                        
					                        try {
					                            //Server settings
					                            $mail->SMTPDebug = 0;                      // Enable verbose debug output
					                            $mail->isSMTP();                                            // Send using SMTP
					                            $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
					                            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
					                            $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
					                            $mail->Password   = 'CongresoMate2020';                               // SMTP password
					                            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
					                            $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
					                            $mail->CharSet  = 'UTF-8';//PARA EL ACENTO
					                        
					                            //Recipients
					                            $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
					                            // obtener nombre,apellidos y correo de coautores del trabajo
					                          

					                            $infoCoau=pg_query($conexion,"select u.nombres,u.primer_ap ,u.segundo_ap,c.correo FROM correos_usuario c, usuario u, usuario_ponencias up WHERE up.id_ponencias='$id_ponencia'and up.id_usuario=u.id_usuario and u.id_usuario= c.id_usuario and up.tipo_autor!='Autor'");
					                            $j=0;
					                            while($infoCoa=pg_fetch_array($infoCoau)){
					                               
					                                $nombreCoa[$j]=trim($infoCoa['nombres']);
					                                $apellidoCoa[$j]=trim($infoCoa['primer_ap']);
					                                $apellidoCoa2[$j]=trim($infoCoa['segundo_ap']);
					                                $corC[$j]=trim($infoCoa['correo']);
					                               
					                            
					                                $j=$j+1;
					                               }
					                               if(!empty($corC[0])){
					                                $mail->addAddress("$corC[0]",utf8_decode("$nombreCoa[0] $apellidoCoa[0] $apellidoCoa2[0]"));
					    
					                               }
					                               if(!empty($corC[1])){
					                                $mail->addAddress("$corC[1]",utf8_decode("$nombreCoa[1] $apellidoCoa[1] $apellidoCoa2[1]"));
					    
					                               }
					                               if(!empty($corC[2])){
					                                $mail->addAddress("$corC[2]",utf8_decode("$nombreCoa[2] $apellidoCoa[2] $apellidoCoa2[2]"));
					    
					                               }
					                               if(!empty($corC[3])){
					                                $mail->addAddress("$corC[3]",utf8_decode("$nombreCoa[3] $apellidoCoa[3] $apellidoCoa2[3]"));
					    
					                               }   
					                        
					                            // Attachments
					                            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
					                            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
					                        
					                            // Content
					                            $mail->isHTML(true);                                  // Set email format to HTML
					                            $mail->Subject = 'Registro de Sala para presentar Trabajo';
					                            $mail->Body    = 'En el siguiente documento se adjuntan los datos de la sala donde se presentara su trabajo.';
					                            $mail->addStringAttachment($archivoAdjunto2, 'Registro_sala.pdf');
					                            $mail->send();
					                            
					                        } catch (Exception $e) {
					                            echo "Error al enviar el mensaje para coautores: {$mail->ErrorInfo}";
					                        }
					                    }

					//*********************************************************************************************************************
					           echo"<script> alert('Se insertaron de manera correcta.'); window.location='asignacion-salasc.php';</script>";         

							}else{
								echo"<script> alert('Error al Asignar la sala.'); window.location='asignacion-salasc.php';</script>";
							}
						}
			}


	}

	

?>
<?php 
include ("Conexion.php");
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
					$row0=pg_fetch_row($consulta);
					$consulta0=$row0[0];
					$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
					$row=pg_fetch_row($consulta1);
					$congresoActual=$row[0];//es el id congreso
$contacto=pg_query($conexion,"SELECT cn.telefono,cn.telefono2,cn.email FROM contacto cn, congreso c
WHERE c.id_congreso='$congresoActual'
and  c.id_contacto =cn.id_contacto");
$contactos=pg_fetch_array($contacto);
$correoC=@$contactos[2];
$numeroC1=@$contactos[0];
$numeroC2=@$contactos[1];
?><!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title style="background-image: url('images/logos.jpg');">CISEMATH</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/cismath.png">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" /> 

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98" onload="listasDesplegables(); validacionDatos();">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
      <?php
    ////Codigo para actualizar el banner
        include ('Conexion.php');
        $nmax=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
        $num=pg_fetch_row($nmax);
        $numcon=$num[0];
        $conact=pg_query($conexion,"SELECT id_congreso, banner FROM congreso WHERE numero_congreso='$numcon'");
        $ban=pg_fetch_row($conact);
        $banner=$ban[1];
        echo '<img src="'.$banner.'" style="width:100%; height:100%; background-size: cover; position: relative; display: flex;"/>';
    ?>

    <header class="top-header" >
        <div class="header_bottom">
          <div class="container">
		  <div class="row"> 
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 col-xl-12">
                <div class="menu_orange_section" style="background: #052450;">
                   <nav class="navbar header-nav navbar-expand-lg"> 
                     <div class="menu_section">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav" >
					    <li><a class="nav-link active" href="indexus.php"><img src="images/i4.png" alt="image"></a> </li>
                        <li><a class="nav-link active" href="acercadeus.php">Acerca de</a></li>
                        <li><a class="nav-link active" href="infus.php">Información</a></li>
                        <li><a class="nav-link active" href="instruccionesus.php">Instrucciones</a></li>
                        <li><a class="nav-link active" href="comitesus.php">Comités</a></li>
						<li><a class="nav-link active" href="memoriasus.php">Memorias</a></li>
						
						<li>
						<a style="position:relative; display:block;">
						<img src="images/user.png" alt="image"> 
						</a>
						</li>
						<li>
						<ul class="nav"><li class="dropdown langmenu" ><a href="#" style="display:block;"class="dropdown-toggle" data-toggle="dropdown" title="Idioma"><b class="caret"></b></a><ul class="dropdown-menu" style="color:black;"><li><a title="English ‎(en)‎" href="menu.php" style="color:black";>Menú‎‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="editarp.php"  style="color:black";>Perfil‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="index.php"  style="color:black";>Cerrar Sesión</a></li></ul></li></ul>
						</li>
						
                    </ul>
                </div>
                     </div>
                 </nav>
                 
                </div>
            </div>
          </div>
		  </div>
        </div>
        
    </header>
    <!-- End header -->
 
 <!-- Start CONTENIDO -->
 <div class="container">
		<div class="row">
			<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			 
				<div class="titulo" style="margin-top:8%; margin-bottom:4%;">
					<h1 align="center" style="color:#052450;">ASIGNACIÓN DE SALAS</h1>
				</div>
			</div>
		</div>	 
	</div>	
 <div class="container">
		<div class="row" style="display:flex; justify-content:center;   margin-bottom:2%;">
			<div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
				<div class="table-responsive">
					<input id="banderaPonencias" name="banderaPonencias" type="hidden" value="<?php echo $banderaPonencias; ?>">
					<input id="banderaSalas" name="banderaSalas" type="hidden" value="<?php echo $banderaSalas; ?>">
					<table  border=1 class="table table-bordered" align="center" style="text-align:center; ">
						<tr>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">CLAVE PONENCIA</font>
							</td>
							
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">SALA </font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">CUPO</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">HORA</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">FECHA</font>
						</tr>
						<?php 
						$consultaSalasPonencias=pg_query($conexion, "SELECT sp.id_ponencia, s.nombre_sala, s.cupo, sp.hora, sp.fecha
						FROM salas_ponencias AS sp, salas AS s, ponencias AS p
						WHERE sp.id_congreso ='$id_congresoactual' AND sp.id_sala=s.id_sala AND
						p.id_ponencia=sp.id_ponencia AND p.id_congreso = sp.id_congreso ");
							while ($datosSalasP=@pg_fetch_array($consultaSalasPonencias)) {
								$idPonenciaSP=$datosSalasP['id_ponencia'];
								$nombreSalaSP=$datosSalasP['nombre_sala'];
								$cupo=$datosSalasP['cupo'];
								$fecha=$datosSalasP['fecha'];
								$hora=$datosSalasP['hora'];
								echo"
						<tr>
							<td>
								$idPonenciaSP 
							</td>
							<td>
								$nombreSalaSP 
							</td>
							<td>
								$cupo 
							</td>
							<td>
								$hora 

							</td>
							<td>
								$fecha 
							</td>
							
						</tr>";
							}
						?>
					</table>
				</div>
			</div>
			<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="table-responsive">
					<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="POST">
					<table cellpadding="2px" cellspacing="2px" align="center" style="text-align:left; margin-top:2%;">
						
						<tr>
							<td>
								Clave de la Ponencia:
							</td>
							<td>

								<select class="form-control" id="idTrabajo" name="idTrabajo" required="" title="SELECCIONE UN TRABAJO, POR FAVOR"  onchange="idponencia();" style="width:100%;">
									<option value="" >Selecciona la clave de la ponencia: </option>
									<?php
										while ($id_ponenciasOralesResultado=pg_fetch_assoc($consultaPonenciasOrales)) {
											$id_ponencias=trim($id_ponenciasOralesResultado['id_ponencia_oral']);

											?>
											<option value="<?php echo $id_ponencias ?>"><?php echo $id_ponencias ?></option>
									<?php	}
									?>
									<?php
										while ($id_CartelesResultado=pg_fetch_assoc($consultaCarteles)) {
											$id_ponencias=trim($id_CartelesResultado['id_ponencia_cartel']);

											?>
											<option value="<?php echo $id_ponencias ?>"><?php echo $id_ponencias ?></option>
									<?php	}
									?>
									

								</select>	
							</td>
						</tr>
						<tr>
							<td>
								Nombre de la Sala:
							</td>
							<td>
									<select class="form-control" id="nombreSala" name="nombreSala" required="" title="SELECCIONA UNA SALA, POR FAVOR"  onchange="cupof();" disabled="" style="width:100%;">
									<option value="" >Selecciona el nombre de la Sala </option>
									<?php
										while ($salas=pg_fetch_assoc($consultaSalas)) {
											$id_salas=trim($salas['id_sala']);
											$nombre_sala=trim($salas['nombre_sala']);
											
											?>

											
											
											<option value="<?php echo $id_salas ?>"><?php echo $nombre_sala ?></option>
									<?php	}
									?>
									
								</select>	
								
							</td>
						</tr>
						<?php 
									if(isset($idponenciaSeleccionada)){?>
										<input id="ponenciaSelec" name="ponenciaSelec" type="hidden" value="<?php echo $idponenciaSeleccionada; ?>">
										<input id="bandera2" name="bandera2" type="hidden" value="2">
									<?php }else{?>
									<input id="ponenciaSelec" name="ponenciaSelec" type="hidden" value="">
									<input id="bandera2" name="bandera2" type="hidden" value="0">
								<?php }?>
						<tr>
							<td>
								Cupo:
							</td>
							<td>
								<?php if(isset($idSeleccionado)){?>
								<input type="text" class="form-control" name="cupo" id="cupo" maxlength="100"  value="<?php echo $cupo; ?>" readonly="readonly" style="width: 100%;">
								<input id="bandera" name="bandera" type="hidden" value="1">
								<input id="salaselc" name="salaselc" type="hidden" value="<?php echo $idSeleccionado; ?>">
							<?php }else{?>
								<input type="text" class="form-control" name="cupo" id="cupo" maxlength="100"  value="" readonly="readonly" style="width: 100%;">
								<input id="bandera" name="bandera" type="hidden" value="0">
							<?php }?>
							</td>
						</tr>
						<tr>
							<td>
								Hora:
							</td>
							<td>
								<input type="time"  class="form-control" name="hora" id="hora" value="" maxlength="100"  style="width: 100%;" disabled="" required="">
							</td>
						</tr>
						<tr>
							<td>
								Fecha:
							</td>
							<td>
								<input type="date" class="form-control" id="fecha" name="fecha" placeholder="dd/mm/aaaa"  value="" style="width: 100%;" maxlength="100" min="2010-01-01" max="2070-12-31" disabled="" required="">
							</td>
						</tr>
						
						<tr>
							<td>
								Año
							</td>
							<td>
								<?php
								$cont = date('Y');
								?>
								<select  class="form-control" id="anio" name="anio" disabled="" required="">
									<option value="" >Selecciona un año </option>
								<?php while ($cont >= 2010) { ?>
								  <option value="<?php echo($cont); ?>"><?php echo($cont); ?></option>
								<?php $cont = ($cont-1); } ?>
								</select>
							</td>
						</form>	
						</tr>
						<tr>
							<td colspan="2" align="center">
								<button type="agregar" id="agregar" name="agregar" style="margin-right:5%; margin-top:6%; background: #052450; color: white; padding: 6px 10px; border-color:#052450; border-radius: 20px;">Agregar</button>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
			
	</div>
	
	<div class="btn" style="text-align:right; width:100%;  margin-top:2%; margin-bottom:6%;">
		
		<button type="B2"  onclick="cancelar();" style="background: whitesmoke; color: black; padding: 6px 10px; border-color: whitesmoke;border-radius: 20px;">Cancelar</button>
	</div>
	<?php pg_close();?>
    <!-- End CONTENIDO -->
    
  <!-- Start Footer -->
        <footer class="footer-box">
        <div class="container">
            <div class="row">
               <div class="col-md-12 white_fonts" style="display: contents;">
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            
                            <div class="full">
							<h3>Dirección</h3>
                                <ul class="menu_footer">   
								     
								     <li><img src="images/i5.png"><span style="font-size: 85%;">Carr, Cuautitlán - Teoloyucan, San Sebastian Xhala, 54714 Cuautitlán Izcalli</span></li>

                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="full">
                                <div class="footer_blog full white_fonts">
                             <h3>Contacto</h3>
                             <ul class="full">
                               <li><img src="images/i6.png"><span style="font-size: 85%;"> <?php echo $correoC  ?></span></li>
                               <li><img src="images/i7.png"><span style="font-size: 85%;"><?php echo "$numeroC1    $numeroC2"; ?></span></li>
                             </ul>
                         </div>
                            </div>
                        </div>
                </div>
			 </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link active" href="derechosa.php" style="color:white">© Sitio desarrollado por CDD Solutions S.A. y administrado por el Departamento de Matemáticas. Hecho en México, (2020 - 2021)</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
	/* counter js */
	function validacionDatos(){
		var banderaPonencias=document.getElementById('banderaPonencias').value;
		var banderaSalas=document.getElementById('banderaSalas').value;
		
		if(banderaSalas==="0" && banderaPonencias==="1"){
			alert("No hay trabajos para registrar.");
			window.location='menucomite.php';
		}else if(banderaSalas==="1" && banderaPonencias==="0"){
			alert("No hay salas para registrar.");
			window.location='menucomite.php';
		}else if(banderaSalas==="1" && banderaPonencias==="1"){
			alert("No hay salas ni trabajos para registrar.");
			window.location='menucomite.php';
		}

	}
	
	function cancelar(){
		//RESTABLECER 
		window.location='asignacion-salasc.php';	
	}
	function idponencia(){
		var id_ponencia=document.getElementById('idTrabajo').value;
		if(id_ponencia===""){
			alert('Por favor selecciona un trabajo.');
		}else{
			window.location.href='?id_ponencia='+id_ponencia;
			
			
		}
	}

	function listasDesplegables(){
		
		var bandera=document.getElementById('bandera').value;
		var bandera2=document.getElementById('bandera2').value;

	if(bandera==="1"){
		var id_sala=document.getElementById('salaselc').value;
		var select_id_sala=document.getElementById('nombreSala');
		var fecha=document.getElementById('fecha');
		var hora=document.getElementById('hora');
		var anio=document.getElementById('anio');
		select_id_sala.value=id_sala;
		fecha.disabled=false;
		hora.disabled=false;
		anio.disabled=false;


		}if(bandera2==="2"){
			var id_ponencia=document.getElementById('idTrabajo');
			var select_id_sala=document.getElementById('nombreSala');
			var ponenciaSelect=document.getElementById('ponenciaSelec').value;
			id_ponencia.value=ponenciaSelect;
			select_id_sala.disabled=false;
		}


	}

	function cupof(){
		var id_ponencia=document.getElementById('idTrabajo').value;
		var id_sala=document.getElementById('nombreSala').value;
		var ponenciaSelect=document.getElementById('ponenciaSelec').value;
		
		if(id_sala===""){
			alert('Por favor selecciona una sala.');
		}else{
			window.location.href='?id_ponencia='+ponenciaSelect+'&id_sala='+id_sala;
			
			
		}
	}

(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
	</script>
</body>

</html>