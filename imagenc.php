<?php //DETECTA A EL USUARIO QUE INICIO SESIÓN
session_start();
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
$usuario=$_SESSION['Usuario'];
include ('Conexion.php');

//******************************************************************************************************************************************************************************************
//TRAE EL NÚMERO DE LA CONGRESO
	$consulta_num_congreso=pg_query($conexion,"SELECT MAX(numero_congreso) FROM congreso");//numero de congreso el AS es un alias
	$row0=pg_fetch_row($consulta_num_congreso);//eL pg_fetch_row trae los datos de la consulta y los asigna a la variable $row0
	$num_congreso=$row0[0];//el número máximo que encontro del congreso lo asigna a la variable consulta0 y es el máximo por que es el [0]NUMERO MÁXIMO DEL CONGRESO

//TRAE EL ID DE LA CONGRESO CON AYUDA DE SU NUMERO
	$consulta_id_congreso=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$num_congreso'"); //Selecciona los datos con el resultado de la consulta anterior (NUMERO MAXIMO DEL CONGRESO)
	$row1=pg_fetch_row($consulta_id_congreso);//Los datos del ID_CONGRESO Y NOMBRE CONGRESO son asignados a la variable $row
	$id_congresoactual=$row1[0];//es el id congreso

//NUMERO DE IMAGENES ACEPTADAS
	$consultaNumImagenes=pg_query($conexion, "SELECT COUNT(*) id_ponencia_cartel 
	FROM ponencia_cartel AS pc, ponencias AS p, usuario_ponencias as up
	WHERE pc.id_congreso='$id_congresoactual' and p.id_ponencia = pc.id_ponencia_cartel
	and p.id_ponencia= up.id_ponencias and up.id_usuario ='$usuario'and up.id_congreso = pc.id_congreso AND up.tipo_autor='Autor'");
	$numImgC=pg_fetch_row($consultaNumImagenes);

	$numeroImagenes=@$numImgC[0];

$consultaPonenciaAT=pg_query($conexion, "SELECT p.id_ponencia FROM usuario_ponencias up, ponencias p WHERE p.id_tipo_ponencia='CA' AND p.estatus_resumen='Aceptado' and p.id_ponencia = up.id_ponencias AND up.id_usuario='$usuario' and up.id_congreso ='$id_congresoactual' AND p.id_congreso=up.id_congreso and up.tipo_autor='Autor' AND p.id_ponencia 
	NOT IN (SELECT id_ponencia_cartel FROM ponencia_cartel as pc, usuario_ponencias AS up, ponencias AS p
				WHERE pc.id_ponencia_cartel=p.id_ponencia AND p.id_ponencia=up.id_ponencias AND up.id_usuario ='$usuario'and pc.id_congreso ='$id_congresoactual' AND up.tipo_autor='Autor')");	
$pat=pg_num_rows($consultaPonenciaAT);

$consultaActualizadasAT=pg_query($conexion,"SELECT ar.id_ponencia FROM  actualizacion_resumen ar, usuario_ponencias up, ponencias AS p WHERE up.id_usuario='$usuario' and up.id_congreso ='$id_congresoactual' and up.tipo_autor='Autor'
	AND ar.estatus_actualizacion='Aceptado' AND p.id_ponencia = up.id_ponencias and ar.id_ponencia = up.id_ponencias AND p.id_tipo_ponencia='CA' AND ar.id_ponencia
 NOT IN (SELECT id_ponencia_cartel FROM ponencia_cartel as pc, usuario_ponencias AS up, ponencias AS p
 WHERE pc.id_ponencia_cartel=p.id_ponencia AND p.id_ponencia=up.id_ponencias AND up.id_usuario ='$usuario'and pc.id_congreso ='$id_congresoactual' AND up.tipo_autor='Autor')");
$arat=pg_num_rows($consultaActualizadasAT);	

//VALIDACION DE PONENCIAS ACEPTADAS
	$consultaPonenciasCAceptadas=pg_query($conexion, "SELECT p.id_ponencia FROM usuario_ponencias up, ponencias p WHERE p.id_tipo_ponencia='CA' AND p.estatus_resumen='Aceptado' and p.id_ponencia = up.id_ponencias AND up.id_usuario='$usuario' and up.id_congreso ='$id_congresoactual' AND p.id_congreso=up.id_congreso and up.tipo_autor='Autor' AND p.id_ponencia 
		NOT IN (SELECT id_ponencia_cartel FROM ponencia_cartel as pc, usuario_ponencias AS up, ponencias AS p
																	WHERE pc.id_ponencia_cartel=p.id_ponencia AND p.id_ponencia=up.id_ponencias AND up.id_usuario ='$usuario'and pc.id_congreso ='$id_congresoactual' AND up.tipo_autor='Autor')
		UNION
		SELECT ar.id_ponencia FROM  actualizacion_resumen ar, usuario_ponencias up, ponencias AS p WHERE up.id_usuario='$usuario' and up.id_congreso ='$id_congresoactual' and up.tipo_autor='Autor'
	AND ar.estatus_actualizacion='Aceptado' AND p.id_ponencia = up.id_ponencias and ar.id_ponencia = up.id_ponencias AND p.id_tipo_ponencia='CA' AND ar.id_ponencia
 NOT IN (SELECT id_ponencia_cartel FROM ponencia_cartel as pc, usuario_ponencias AS up, ponencias AS p
																	WHERE pc.id_ponencia_cartel=p.id_ponencia AND p.id_ponencia=up.id_ponencias AND up.id_usuario ='$usuario'and pc.id_congreso ='$id_congresoactual' AND up.tipo_autor='Autor')");
$numPCAceptadas=pg_num_rows($consultaPonenciasCAceptadas);
@$totalPonenciasA=@$numPCAceptadas;	

//$totalImgPonen=$totalPonenciasA-$numeroImagenes;

//VALIDACION GENERAL 
$consultageneral=pg_query($conexion,"SELECT p.id_ponencia FROM usuario_ponencias up, ponencias p WHERE p.id_tipo_ponencia='CA' AND p.estatus_resumen!='Aceptado' and p.id_ponencia = up.id_ponencias AND up.id_usuario='$usuario' and up.id_congreso ='$id_congresoactual' AND p.id_congreso=up.id_congreso and up.tipo_autor='Autor' AND p.id_ponencia 
		NOT IN (SELECT id_ponencia_cartel FROM ponencia_cartel as pc, usuario_ponencias AS up, ponencias AS p
																	WHERE pc.id_ponencia_cartel=p.id_ponencia AND p.id_ponencia=up.id_ponencias AND up.id_usuario ='$usuario'and pc.id_congreso ='$id_congresoactual' AND up.tipo_autor='Autor')
		UNION
		SELECT ar.id_ponencia FROM  actualizacion_resumen ar, usuario_ponencias up, ponencias AS p WHERE up.id_usuario='$usuario' and up.id_congreso ='$id_congresoactual' and up.tipo_autor='Autor'
	AND ar.estatus_actualizacion!='Aceptado' AND p.id_ponencia = up.id_ponencias and ar.id_ponencia = up.id_ponencias AND p.id_tipo_ponencia='CA' AND ar.id_ponencia
 NOT IN (SELECT id_ponencia_cartel FROM ponencia_cartel as pc, usuario_ponencias AS up, ponencias AS p
																	WHERE pc.id_ponencia_cartel=p.id_ponencia AND p.id_ponencia=up.id_ponencias AND up.id_usuario ='$usuario'and pc.id_congreso ='$id_congresoactual' AND up.tipo_autor='Autor')");
$consultag=pg_num_rows($consultageneral);
@$numPonenciasNoA=@$consultag;


//TRAE LA INFORMACIÓN DESCARTANDO LAS QUE ESTAN EN LA TABLA PONENCIAS CARTEL
$consultainfoCartel=pg_query($conexion, "SELECT p.id_ponencia, p.titulo FROM ponencias AS p, usuario_ponencias AS up WHERE  up.id_usuario='$usuario' AND up.id_ponencias=p.id_ponencia AND p.id_congreso='$id_congresoactual' AND up.tipo_autor='Autor' AND 
p.id_ponencia IN (SELECT p.id_ponencia FROM ponencias AS p, usuario_ponencias as up WHERE p.estatus_resumen='Aceptado' and p.id_tipo_ponencia='CA' and up.tipo_autor='Autor' and up.id_usuario='$usuario' and p.id_ponencia=up.id_ponencias  AND p.id_congreso='$id_congresoactual' AND p.id_ponencia 
NOT IN (SELECT id_ponencia_cartel FROM ponencia_cartel as pc, usuario_ponencias AS up, ponencias AS p
																WHERE pc.id_ponencia_cartel=p.id_ponencia AND p.id_ponencia=up.id_ponencias AND up.id_usuario ='$usuario'and pc.id_congreso ='$id_congresoactual' AND up.tipo_autor='Autor'))");

$consultainfoCartelA=pg_query($conexion, "SELECT ar.id_ponencia, ar.titulo FROM actualizacion_resumen AS ar, usuario_ponencias as up, ponencias AS p WHERE ar.estatus_actualizacion='Aceptado' AND ar.id_ponencia=p.id_ponencia  and p.id_tipo_ponencia='CA' and up.id_usuario = '$usuario' AND p.id_congreso='$id_congresoactual' AND up.id_ponencias=p.id_ponencia AND up.tipo_autor='Autor' AND ar.id_ponencia
 NOT IN (SELECT id_ponencia_cartel FROM ponencia_cartel as pc, usuario_ponencias AS up, ponencias AS p
																	WHERE pc.id_ponencia_cartel=p.id_ponencia AND p.id_ponencia=up.id_ponencias AND up.id_usuario ='$usuario'and pc.id_congreso ='$id_congresoactual'  AND up.tipo_autor='Autor')");

?>

<?php 
include ("Conexion.php");
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
					$row0=pg_fetch_row($consulta);
					$consulta0=$row0[0];
					$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
					$row=pg_fetch_row($consulta1);
					$congresoActual=$row[0];//es el id congreso
$contacto=pg_query($conexion,"SELECT cn.telefono,cn.telefono2,cn.email FROM contacto cn, congreso c
WHERE c.id_congreso='$congresoActual'
and  c.id_contacto =cn.id_contacto");
$contactos=pg_fetch_array($contacto);
$correoC=@$contactos[2];
$numeroC1=@$contactos[0];
$numeroC2=@$contactos[1];
?><!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title style="background-image: url('images/logos.jpg');">CISEMATH</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/cismath.png">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" />

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98" onload="habilitarBotones();">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
      <?php
    ////Codigo para actualizar el banner
        include ('Conexion.php');
        $nmax=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
        $num=pg_fetch_row($nmax);
        $numcon=$num[0];
        $conact=pg_query($conexion,"SELECT id_congreso, banner FROM congreso WHERE numero_congreso='$numcon'");
        $ban=pg_fetch_row($conact);
        $banner=$ban[1];
        echo '<img src="'.$banner.'" style="width:100%; height:100%; background-size: cover; position: relative; display: flex;"/>';
    ?>

    <header class="top-header" >
        <div class="header_bottom">
          <div class="container">
		  <div class="row"> 
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 col-xl-12">
                <div class="menu_orange_section" style="background: #052450;">
                   <nav class="navbar header-nav navbar-expand-lg"> 
                     <div class="menu_section">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav" >
					    <li><a class="nav-link active" href="indexus.php"><img src="images/i4.png" alt="image"></a> </li>
                        <li><a class="nav-link active" href="acercadeus.php">Acerca de</a></li>
                        <li><a class="nav-link active" href="infus.php">Información</a></li>
                        <li><a class="nav-link active" href="instruccionesus.php">Instrucciones</a></li>
                        <li><a class="nav-link active" href="comitesus.php">Comités</a></li>
						<li><a class="nav-link active" href="memoriasus.php">Memorias</a></li>
						
						<li>
						<a style="position:relative; display:block;">
						<img src="images/user.png" alt="image"> 
						</a>
						</li>
						<li>
						<ul class="nav"><li class="dropdown langmenu" ><a href="#" style="display:block;"class="dropdown-toggle" data-toggle="dropdown" title="Idioma"><b class="caret"></b></a><ul class="dropdown-menu" style="color:black;"><li><a title="English ‎(en)‎" href="menu.php" style="color:black";>Menú‎‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="editarp.php"  style="color:black";>Perfil‎</a></li><li><li><a title="Español - Internacional ‎(es)‎" href="index.php"  style="color:black";>Cerrar Sesión</a></li></ul></li></ul>
						</li>
						
                    </ul>
                </div>
                     </div>
                 </nav>
                 
                </div>
            </div>
          </div>
		  </div>
        </div>
        
    </header>
    <!-- End header -->
 
 <!-- Start CONTENIDO -->
 
<form action="envio_imagen.php" method="post" enctype="multipart/form-data">	
	<div class="container">
	    <div class="row">
	        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
	     
				<div class="titulo" style="margin-top:8%">
				<font style="font-size: 17px; font-weight: 300;">
	            <h1 align="center" style="margin-bottom:5%; color: #052450;">ENVÍO DE IMAGEN DE CARTEL</h1>
	            </div>
	        </div>
		</div>	 
	</div>	
	
		<div class="container">
				
			<div class="row">
				
				<div class="col-12 col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8"> 
				
					<table cellpadding="10px" cellspacing="8px" style="text-align:left; font-size:18px;">
						
						<tr>
							<td>
								Título del trabajo:
							</td>

							<td>

								<?php 
								if($numImgC[0] < $totalPonenciasA && !empty($totalPonenciasA)|| $numImgC[0]  = $totalPonenciasA && !empty($totalPonenciasA)){
										if($totalPonenciasA>1){

											?>
											<input id="bandera" name="bandera" type="hidden" value="1">

											 <select class="form-control" id="idtituloTrabajo" name="tituloTrabajo" required="" title="SELECCIONE UN TRABAJO, POR FAVOR" onchange="selecionaTitulo();">
											<option value="" >Selecciona el titulo de tu trabajo: </option>
									<?php 		while($infoCarteles=pg_fetch_assoc($consultainfoCartel)){
												$id_ponencia_cartel=$infoCarteles[id_ponencia];
												$titulo = $infoCarteles[titulo]; 
												?>
												<option value="<?php echo $id_ponencia_cartel ?>"><?php echo $titulo ?></option>
												<?php							

													}
												?>
												<?php 		while($infoCarteles=pg_fetch_assoc($consultainfoCartelA)){
												$id_ponencia_cartel=$infoCarteles[id_ponencia];
												$titulo = $infoCarteles[titulo]; 
												?>
												<option value="<?php echo $id_ponencia_cartel ?>"><?php echo $titulo ?></option>
												<?php							

													}
												?>

										</select>

									</td>
						
								</tr>
								
								<tr>
								
									<td>
										
										Clave:	
										
									</td>

									<td>
										<input type="text" class="form-control" name="Clave" id="Clave"  value="" readonly="readonly">
									</td>
								
								</tr>

								<?php
							}else if($totalPonenciasA=1){
								if(empty($arat) && $pat===1){//PONENCIAS
										$row2= pg_fetch_row($consultainfoCartel);
							
							?>
								<input id="bandera" name="bandera" type="hidden" value="2">

									<input type="text" class="form-control" name="titulo_text" id="titulo_text"  value="<?php echo "$row2[1]" ?>" readonly="readonly" >
									
									</td>
						
								</tr>
							<tr>
								<td>
									Clave:
								</td>
								<td>
									<input type="text" class="form-control" name="clave2" id="clave2"  value="<?php echo "$row2[0]" ?>" readonly="readonly">
								</td>
							</tr>
								
								<?php 
								$idtituloTrabajo="";
							
								}else if(empty($pat) && $arat===1){//ACTUALIZACION RESUMEN
									$row2= pg_fetch_row($consultainfoCartelA);
							
							?>
								<input id="bandera" name="bandera" type="hidden" value="2">

									<input type="text" class="form-control" name="titulo_text" id="titulo_text"  value="<?php echo "$row2[1]" ?>" readonly="readonly" >
									
									</td>
						
								</tr>
							<tr>
								<td>
									Clave:
								</td>
								<td>
									<input type="text" class="form-control" name="clave2" id="clave2"  value="<?php echo "$row2[0]" ?>" readonly="readonly">
								</td>
							</tr>
								
								<?php 
								$idtituloTrabajo="";
								}
							}
								 
									?>
						
								<?php  pg_close($conexion); 

					}else if(empty($totalPonenciasA)  && empty($numPonenciasNoA) && !empty($numeroImagenes)) {
						
				echo"<script>alert('Ya se tienen registradas todas las imágenes de carteles para este congreso, si desea actualizarlas por favor ingresa a la opción de Trabajos Registrados, en el apartado de Imágenes.');window.location='cartel.php'</script>";

					}else if(empty($totalPonenciasA) && empty($numPonenciasNoA) && empty($numeroImagenes)) {//CUANDO HAY UN RESUMEN EN ENVIADO PERO NO HAY IMAGENES REGISTRADAS
						

				?><script>alert('Primero registre el resumen de su cartel para este congreso. Hasta que se encuentre aceptado podrá subir la imagen, o en caso de ya haber registrado un resumen verifique el estatus de su resumen, hasta que se encuentre "Aceptado" podrá subir la imagen de su cartel.');
					window.location='cartel.php';</script>
					--><?php 
				}else if(empty($totalPonenciasA) && !empty($numPonenciasNoA)  && !empty($numeroImagenes) || empty($totalPonenciasA) && !empty($numPonenciasNoA)  && empty($numeroImagenes)){//CUANDO NO HAY IMAGENES NI RESUMENES EN ENVIADO
					

						echo"<script>alert('No tienes resumenes aceptados por favor verifica el estatus en Trabajos Registrados.');window.location='cartel.php'</script>";

					}
					?>
						
						<tr>
						
							<td>
								
								Seleccionar archivo:	
								
							</td>
							
							<td>
								
								<input type="file" id="seleccionArchivos" name="seleccionArchivos" required="required" accept="image/*" disabled="" onchange="habilitarCancelar();">
								
							</td>
						
						</tr>
						
							
						
						
					</table>
				</div>
				<div class="col-12 col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3"> 
				<img id="imagenPrevizualizacion" style="width:100%; height:100%;"/><script src="js/scriptImagenes.js"></script>
				</div>

				
				<div class="btn" style="text-align:right; width:100%; margin-bottom:5%; margin-top:5%;">
					
					<input type="submit" value="Enviar"  style=" margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;">
				 
					<input type="reset" value="Cancelar"  id="Cancelar" onclick="cancelar();" style="background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px; display: none;">

					<input type="reset" value="Cancelar"  id="Cancelar2" onclick="cancelarCampoClave();" style="background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px; display: none;">
				
				</div>
				</form>
			</div>
		</div>
	</div>
	 
    <!-- End CONTENIDO -->
    
  <!-- Start Footer -->
        <footer class="footer-box">
        <div class="container">
            <div class="row">
               <div class="col-md-12 white_fonts" style="display: contents;">
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            
                            <div class="full">
							<h3>Dirección</h3>
                                <ul class="menu_footer">   
								     
								     <li><img src="images/i5.png"><span style="font-size: 85%;">Carr, Cuautitlán - Teoloyucan, San Sebastian Xhala, 54714 Cuautitlán Izcalli</span></li>

                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="full">
                                <div class="footer_blog full white_fonts">
                             <h3>Contacto</h3>
                             <ul class="full">
                               <li><img src="images/i6.png"><span style="font-size: 85%;"> <?php echo $correoC  ?></span></li>
                               <li><img src="images/i7.png"><span style="font-size: 85%;"><?php echo "$numeroC1    $numeroC2"; ?></span></li>
                             </ul>
                         </div>
                            </div>
                        </div>
                </div>
			 </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link active" href="derechosa.php" style="color:white">© Sitio desarrollado por CDD Solutions S.A. y administrado por el Departamento de Matemáticas. Hecho en México, (2020 - 2021)</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
	/* counter js */


		//FUNCION PARA INSERTAR EN EL CAMPO DE TEXTO LA CLAVE DEL TRABAJO
function selecionaTitulo(){//SI ES COMBOBOX
					var id_trabajo = document.getElementById("idtituloTrabajo").value;
					document.getElementById("Clave").value = id_trabajo;
					var archivo= document.getElementById("seleccionArchivos");
					
					archivo.disabled=false;
					
							
}
function habilitarBotones(){
	var bandera=document.getElementById("bandera").value;
	if(bandera==2){
		var archivo= document.getElementById("seleccionArchivos");
					archivo.disabled=false;
	}

}
function habilitarCancelar(){//HABILITA LOS BOTONES DE CANCELAR CUANDO SE SELECCIONE EL ARCHIVO
	var archivo= document.getElementById("seleccionArchivos").value;
	var cancelar2= document.getElementById("Cancelar2");
	var imagenPrevizualizacion=document.getElementById("imagenPrevizualizacion").value;
	var cancelar= document.getElementById("Cancelar");
	var bandera=document.getElementById("bandera").value;
				if(archivo!= ""){
					if(bandera==2){
					cancelar2.style.display='inline';
					}else{
					cancelar.style.display='inline';
					}	
				}
}


function cancelar(){//CANCELAR PARA COMBOBOX
	var cancelar= document.getElementById("Cancelar");
	var titulo = document.getElementById("idtituloTrabajo").value;
	var clave= document.getElementById("Clave");
	var seleccionArchivos=document.getElementById("seleccionArchivos");
	var imagenPrevizualizacion=document.getElementById("imagenPrevizualizacion");					
	
	
	if(titulo!=="" && typeof titulo!=='undefined' && typeof titulo!==null){
					seleccionArchivos.disabled=true;
					cancelar.style.display='none';
					titulo.value="";
					clave.value="";
					seleccionArchivos.value="";
					imagenPrevizualizacion.src="";
					cancelar.style.display='none';

	}

}
function cancelarCampoClave(){//CANCELAR CUANDO ES CAMPO DE TEXTO
	var titulo=document.getElementById("titulo_text").value;
	var seleccionArchivos=document.getElementById("seleccionArchivos");
	var imagenPrevizualizacion=document.getElementById("imagenPrevizualizacion");
	var cancelar2= document.getElementById("Cancelar2");
	if( typeof titulo!==null && typeof titulo!=='undefined'){
		var archivo= document.getElementById("seleccionArchivos");
					
					archivo.disabled=false;
					
					archivo.value="";
					imagenPrevizualizacion.src="";
					cancelar2.style.display='none';
				
	}
}

(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
	</script>
</body>

</html>