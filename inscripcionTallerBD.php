<?php
session_start();

	use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
   	require 'PHPMailer/Exception.php';
    require 'PHPMailer/PHPMailer.php';
    require 'PHPMailer/SMTP.php';
    require ('libreria/fpdf.php');
   
    $usuario=$_SESSION['Usuario'];

     include ('Conexion.php');
     $id_ponencia_taller= $_POST['inscribirse'];
     
     //TRAE EL NÚMERO DE LA CONGRESO
	$consulta_num_congreso=pg_query($conexion,"SELECT MAX(numero_congreso) FROM congreso");
	$row0=pg_fetch_row($consulta_num_congreso);
	$num_congreso=$row0[0];

//TRAE EL ID DE LA CONGRESO CON AYUDA DE SU NUMERO
	$consulta_id_congreso=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$num_congreso'"); 
	$row1=pg_fetch_row($consulta_id_congreso);
	$id_congresoactual=$row1[0];
    //NUMERACION POR TALLER
    $consultaNumActualizaciones=pg_query($conexion, "SELECT MAX(numeracion_por_taller) FROM usuario_inscribe_taller where id_congreso='$id_congresoactual' AND id_ponencia_taller='$id_ponencia_taller'");
$rowNumActualizaciones=pg_fetch_row($consultaNumActualizaciones);

if(empty($rowNumActualizaciones)){
    $numeroid=1;
    $numeracionT=$numeroid;// se crea id actualizacion en 1 si no existe ninguna actualizacion;
}else{
     $numeroid=$rowNumActualizaciones[0]+1;
    $numeracionT=$numeroid;// se crea id actualizacion sumando el numero actualizacion anterior mas 1;
}

	$inforTaller=pg_query($conexion, "SELECT  p.titulo,  u.nombres||' '|| u.primer_ap||' '|| u.segundo_ap as profesor ,sp.fecha, sp.hora,
				 s.nombre_sala,  pt.materiales
				FROM ponencias as p, usuario as u, usuario_ponencias as up, salas_ponencias as sp, salas as s, ponencia_taller as pt
				WHERE sp.id_ponencia=p.id_ponencia AND p.id_ponencia=pt.id_ponencia_taller AND up.id_ponencias=p.id_ponencia
				AND up.id_usuario=u.id_usuario and sp.id_sala=s.id_sala AND sp.id_congreso='$id_congresoactual' AND pt.id_ponencia_taller='$id_ponencia_taller'");
	$infoT=pg_fetch_assoc($inforTaller);
    //VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
                                    $consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_ponencia_taller' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
                                    $actualizacionTitulo=pg_fetch_assoc($consultaTitulo);   
                                    if(!empty($actualizacionTitulo)){
                                        $titulo=$actualizacionTitulo['titulo'];
                                    }else{
                                        $titulo=$infoT['titulo'];
                                    }

	$insertarInscripción="INSERT INTO usuario_inscribe_taller(id_usuario, id_congreso, id_ponencia_taller, numeracion_por_taller) VALUES ('$usuario', '$id_congresoactual', '$id_ponencia_taller', '$numeracionT')";
	$insert=pg_query($conexion,$insertarInscripción);
			if($insert && $infoT){
				 //CORREO DE REGISTRO
                   
                    //******************************************************************************************
                 
                    
                    class PDF extends FPDF{
                    // Cabecera de página
	                    function Header()
	                    {

	                        // Logo
	                        $this->Image('logo.jpg',0,0,220);
	                        // Arial bold 15
	                        $this->SetFont('Arial','B',15);
	                        // Movernos a la derecha
	                        $this->Cell(80);
	                        // Título
	                        $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
	                        // Salto de línea
	                        $this->Ln(50);
	                        
	                    }
	                    
	                    // Pie de página
	                    function Footer() {
	                        // Posición: a 1,5 cm del final
	                        $this->SetY(-15);
	                        // Arial italic 8
	                        $this->SetFont('Arial','I',8);
	                        // Número de página
	                        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	                        }
	                     }
	                   // Creación del objeto de la clase heredada
                    $pdf = new PDF();
                    $pdf->AliasNbPages();
                    $pdf->AddPage();
                    $pdf->SetFont('Times','',12);
                    
                    $pdf->Cell(40,10,utf8_decode('Registro de asistencia a Taller'),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    $pdf->Cell(40,10,utf8_decode('Usted ha se ha registrado para el siguiente Taller:'),0,1);
                    $pdf->Cell(40,10,utf8_decode('Nombre del taller: '.$titulo),0,1);
                    $pdf->Cell(40,10,utf8_decode('El nombre del profesor que lo impartirá es: '.$infoT['profesor']),0,1);
                    $pdf->Cell(40,10,utf8_decode('Fecha del Taller: '.$infoT['fecha']),0,1);
                    $pdf->Cell(40,10,utf8_decode('Hora en que iniciara el Taller: '.$infoT['hora']),0,1);
                    $pdf->Cell(40,10,utf8_decode('Le pedimos por favor estar de manera puntal en el aula '.$infoT['nombre_sala']),0,1);
                    $pdf->Cell(40,10,utf8_decode('recordandole que los materiales para este taller son los siguientes: '),0,1);
                    $pdf->Cell(40,10,utf8_decode($infoT['materiales']),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    
                    $archivoAdjunto = $pdf->Output("", "S");
                    $correoa=pg_query($conexion,"SELECT c.correo,us.nombres,us.primer_ap,us.segundo_ap FROM correos_usuario c, usuario us WHERE c.id_usuario='$usuario' and us.id_usuario ='$usuario'");
                    $i=0;
                    while($mostrarCR=pg_fetch_array($correoa)){
                        $cor[$i]=trim($mostrarCR['correo']);
                        $nombre=trim($mostrarCR['nombres']);
                        $apPA=trim($mostrarCR['primer_ap']);
                        $apMa=trim($mostrarCR['segundo_ap']);
                        
                        $i=$i+1;
                    }
                    
                    
                    
                    $mail = new PHPMailer(true);
                    
                    try {
                        //Server settings
                        $mail->SMTPDebug = 0;                      // Enable verbose debug output
                        $mail->isSMTP();                                            // Send using SMTP
                        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                        $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
                        $mail->Password   = 'CongresoMate2020';                               // SMTP password
                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                    	$mail->CharSet  = 'UTF-8';//PARA EL ACENTO

                        //Recipients
                        $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
                        $mail->addAddress("$cor[0]", "$nombre $apPA");
	                        if(!empty($cor[1])){
	                            $mail->addAddress("$cor[1]", "$nombre $apPA");
	                        }
	                        if(!empty($cor[2])){
	                            $mail->addAddress("$cor[2]", "$nombre $apPA");
	                        }
                       // Content
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = 'Inscripción al Taller';
                        $mail->Body    = 'En el siguiente documento se adjuntan la información del Taller en el cual se encuentra registrado.';
                        $mail->addStringAttachment($archivoAdjunto, 'Inscripcion_a_Taller.pdf');
                        $mail->send();
                        
                        }catch (Exception $e){
                            echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
                        }
                    

                    //*****************************************************************************************

                    

			echo "<script>alert('Te registraste de manera correcta, por favor revisa tu correo principal para encontrar mayor información.');window.location='menu.php';</script>";
			}else{
				echo "<script>alert('Error al registrar el Taller.');window.location='menu.php';</script>";
			}
?>