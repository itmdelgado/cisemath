<?php 
include ("Conexion.php");
session_start();
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
					$row0=pg_fetch_row($consulta);
					$consulta0=$row0[0];
					$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
					$row=pg_fetch_row($consulta1);
					$congresoActual=$row[0];//es el id congreso
$contacto=pg_query($conexion,"SELECT cn.telefono,cn.telefono2,cn.email FROM contacto cn, congreso c
WHERE c.id_congreso='$congresoActual'
and  c.id_contacto =cn.id_contacto");
$contactos=pg_fetch_array($contacto);
$correoC=@$contactos[2];
$numeroC1=@$contactos[0];
$numeroC2=@$contactos[1];
?>
<!DOCTYPE html>
<html lang="en">
<?php
	$numerocongreso=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
	$row0=pg_fetch_row($numerocongreso);
	$numero=$row0[0];
	$id_congreso_actual=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$numero'"); //id del ultimo congreso
	$row=pg_fetch_row($id_congreso_actual);
	$id_congreso=$row[0];

	$datos_congreso=pg_query($conexion, "SELECT 
	co.id_congreso, co.nombre_congreso, co.anio, co.numero_congreso, co.fecha_inicio, co.fecha_fin, co.id_contacto,
	ct.id_contacto, ct.telefono, ct.telefono2, ct.email, ct.numeracion
	FROM 
	congreso co, 
	contacto ct
	WHERE co.id_congreso='$id_congreso'
	and co.id_contacto=ct.id_contacto");

	while($congreso_actual=pg_fetch_array($datos_congreso)){
		$id=$congreso_actual['id_congreso'];
		$nombre_congreso=$congreso_actual['nombre_congreso'];
		$numero_congreso=$congreso_actual['numero_congreso'];
		$anio=$congreso_actual['anio'];
		$fecha_inicio=$congreso_actual['fecha_inicio'];
		$fecha_fin=$congreso_actual['fecha_fin'];
		$correo=$congreso_actual['email'];
		$telefono=$congreso_actual['telefono'];
		$telefono2=$congreso_actual['telefono2'];
		
		$costos=pg_query($conexion,"SELECT cantidad, tipo_pago FROM tipo_pagos WHERE id_congreso='$id'");
		
		while($costos_congreso=pg_fetch_array($costos)){
			if($costos_congreso['tipo_pago']=='Asistente'){
				$costoasistente=$costos_congreso['cantidad'];
			}
			if($costos_congreso['tipo_pago']=='Estudiante'){
				$costoestudiante=$costos_congreso['cantidad'];
			}
			if($costos_congreso['tipo_pago']=='Prototipo'){
				$costoprototipo=$costos_congreso['cantidad'];
			}
			if($costos_congreso['tipo_pago']=='Ponente'){
				$costoponente=$costos_congreso['cantidad'];
			}
		}

		$instrucciones=pg_query($conexion, "SELECT instrucciones, tipo_trabajo FROM instrucciones_trabajos WHERE id_congreso='$id'");

		while($instrucciones_congreso=pg_fetch_array($instrucciones)){
			if($instrucciones_congreso['tipo_trabajo']=='Ponencia Oral'){
				$instruccionesponencia=$instrucciones_congreso['instrucciones'];
			}
			if($instrucciones_congreso['tipo_trabajo']=='Cartel'){
				$instruccionescartel=$instrucciones_congreso['instrucciones'];
			}
			if($instrucciones_congreso['tipo_trabajo']=='Taller'){
				$instruccionestaller=$instrucciones_congreso['instrucciones'];
			}
			if($instrucciones_congreso['tipo_trabajo']=='Prototipo'){
				$instruccionesprototipo=$instrucciones_congreso['instrucciones'];
			}
		}
	} 
?>

<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title style="background-image: url('images/logos.jpg');">CISEMATH</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/cismath.png">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" />
	<script
	src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>


    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
      <?php
    ////Codigo para actualizar el banner
        include ('Conexion.php');
        $nmax=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
        $num=pg_fetch_row($nmax);
        $numcon=$num[0];
        $conact=pg_query($conexion,"SELECT id_congreso, banner FROM congreso WHERE numero_congreso='$numcon'");
        $ban=pg_fetch_row($conact);
        $banner=$ban[1];
        echo '<img src="'.$banner.'" style="width:100%; height:100%; background-size: cover; position: relative; display: flex;"/>';
    ?>

    <header class="top-header" >
        <div class="header_bottom">
          <div class="container">
		   <div class="row">
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 col-xl-12">
                <div class="menu_orange_section" style="background: #052450;">
                   <nav class="navbar header-nav navbar-expand-lg"> 
                     <div class="menu_section">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav" >
					    <li><a class="nav-link active" href="indexus.php"><img src="images/i4.png" alt="image"></a> </li>
                        <li><a class="nav-link active" href="acercadeus.php">Acerca de</a></li>
                        <li><a class="nav-link active" href="infus.php">Información</a></li>
                        <li><a class="nav-link active" href="instruccionesus.php">Instrucciones</a></li>
                        <li><a class="nav-link active" href="comitesus.php">Comités</a></li>
						<li><a class="nav-link active" href="memoriasus.php">Memorias</a></li>
						
						<li>
						<a style="position:relative; display:block;">
						<img src="images/user.png" alt="image"> 
						</a>
						</li>
						<li>
						<ul class="nav"><li class="dropdown langmenu" ><a href="#" style="display:block;"class="dropdown-toggle" data-toggle="dropdown" title="Idioma"><b class="caret"></b></a><ul class="dropdown-menu" style="color:black;"><li><a title="English ‎(en)‎" href="menu.php" style="color:black";>Menú‎‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="editarp.php"  style="color:black";>Perfil‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="index.php"  style="color:black";>Cerrar Sesión</a></li></ul></li></ul>
						</li>
						
                    </ul>
                </div>
                     </div>
                 </nav>
                 
                </div>
            </div>
          </div>
		  </div>
        </div>
        
    </header>
    <!-- End header -->
 
 <!-- Start CONTENIDO -->
<div id="vercongreso">	
	<div class="container">
	    <div class="row">
	        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
	     
				<div class="titulo" style="margin-top:8%">
				<font style="font-size: 17px; font-weight: 300;">
	            <h1 align="center" style="color: #052450;">CONGRESO - DATOS</h1>
	            </div>

	        </div>
		</div>	 
	</div>

    
    <div class="container" style="margin-top:3% ">	 
		<div class="row">
			<div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
				<div class="table-responsive" style="display:flex; justify-content:center;">
					<form>
						<div class="field">
							<strong>
							INFORMACIÓN DEL CONGRESO ACTUAL:<br><br>
							</strong>
						</div>
						<div class="field">
							Nombre:<br>
							<input type="text" class="form-control" name="nombre" id="nombre" value="<?php echo $nombre_congreso ?>" readonly >
						</div>
							
						<div class="field">
							Número:<br>
							<input type="text" class="form-control" name="num" id="num" value="<?php echo $numero_congreso ?>" readonly >
						</div>
						
						<div class="field">
							Año:<br>
							<input type="" class="form-control" name="año" id="año" value="<?php echo $anio ?>" readonly >
						</div>
						
						<div class="field">
							Fecha Inicio:<br>
							<input type="date"  class="form-control" name="fi" id="fi" value="<?php echo $fecha_inicio ?>" readonly >
						</div>
			
						<div class="field">
							Fecha Término:<br>
							<input type="date" class="form-control" name="ff" id="ff" value="<?php echo $fecha_fin ?>" readonly>
						</div>
			
						<div class="field">
							Correo Electrónico: <br>
							<input type="email" class="form-control" size="20" name="ce1" value="<?php echo $correo ?>" readonly> 								    
						</div>
								
						<div class="field">
				
							Teléfono: <br>
							<input type="text" class="form-control" size="20" name="numero" id="numero" value="<?php echo $telefono ?>" readonly> 
						</div>

						<div class="field">
				
							Teléfono secundario: <br>
							<input type="text" class="form-control" size="20" name="numero" id="numero" value="<?php echo $telefono2 ?>" readonly> 
						</div>

						<table cellpadding="2px" cellspacing="2px" style="text-align:left;">
							<tr>
								<td>
									<strong>
										COSTOS:<br><br>
									</strong>
								</td>
							</tr>
							<tr>
								<td>
									Ponentes:
								</td>
								<td>
									<input type="text" class="form-control" size="13" name="costo" id="costo" value="<?php echo $costoponente ?>" readonly> 
								</td>
							</tr>
							<tr>
								<td>
									Asistentes:
								</td>
								<td>							
									<input type="text" class="form-control" size="13" name="costo2" id="costo2" value="<?php echo $costoasistente ?>" readonly> 
								</td>
							</tr>
							<tr>
								<td>
									Estudiantes:
								</td>
								<td>
									<input type="text" class="form-control" size="13" name="costo3" id="costo3" value="<?php echo $costoestudiante ?>" readonly> 
								</td>
							</tr>
							<tr>
								<td>
									Prototipos:
								</td>
								<td>
									<input type="text" class="form-control" size="13" name="costo4" id="costo4" value="<?php echo $costoprototipo ?>" readonly> 
								</td>
							</tr>
						</table>
				</div>
			</div>
		
			<div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"> 
				<div class="table-responsive" ">
						
							
							<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
							<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
							<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
													
							<script>

								$(document).ready(function(){
									var i = 1;

									$('#add').click(function () {
										i++;
										$('#dynamic_field').append('<tr id="row'+i+'">' +
																	'<td><input type="text" name="name[]" placeholder="Descripción" class="form-control name_list" /></td>' +
																	'<td><input type="date" name="name[]" placeholder="dd/mm/aaaa" class="form-control name_list" /></td>'+
																	'<td><input type="date" name="name[]" placeholder="dd/mm/aaaa" class="form-control name_list" /></td>'+
																	'<td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td>' +
																	'</tr>');
									});
									
									$(document).on('click', '.btn_remove', function () {
										var id = $(this).attr('id');
									   $('#row'+ id).remove();
									});
								})
							</script>
												
			<div class="field">
				<strong>
		            INSTRUCCIONES DE TRABAJOS:<br><br>
				</strong>
		    </div>
		    <div class="field">
		        Ponencias Orales:<br>
				<textarea disabled name="PO"  id="inst" rows="5" cols="30"  value="<?php echo $instruccionesponencia ?>"></textarea>
			</div>
						
			<div class="field">
				Carteles:<br>
				<textarea disabled name="CA" id="inst" rows="5" cols="30"  value="<?php echo $instruccionescartel ?>"></textarea>
			</div>
					
			<div class="field">
				Talleres:<br>
				<textarea disabled name="TA" id="inst" rows="5" cols="30"  value="<?php echo $instruccionestaller ?>"></textarea>
			</div>
			
			<div class="field">
				Prototipos:<br>
				<textarea disabled name="PRO" id="inst" rows="5" cols="30"  value="<?php echo $instruccionesprototipo ?>"></textarea>
			</div>
			</div>
		    </div>
			</form>
		    <div class="btn" style="text-align:right; width:100%; margin-bottom: 5%; margin-top: 2%;">
				<button type="B1" onclick="myFunctionB2()" style="margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;">Editar</button>
				<button onclick="location.href='editar-fechas.php'" style="margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;">Editar fechas</button>
				<button onclick="location.href='menuadmin.php'" style="background: whitesmoke; color: black; padding: 6px 10px; border-color: whitesmoke;border-radius: 20px;">Regresar</button>
		    </div>
		 	
		</div>
	</div>
</div>

<div id="editarcongreso">	
	<div class="container">
	    <div class="row">
	        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
	     
				<div class="titulo" style="margin-top:8%">
				<font style="font-size: 17px; font-weight: 300;">
	            <h1 align="center" style="color: #052450;">EDITAR CONGRESO</h1>
	            </div>

	        </div>
		</div>	 
	</div>

    
    <div class="container" style="margin-top:3% ">	 
		<div class="row">
			<div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
				<div class="table-responsive" style="display:flex; justify-content:center;">
					<form action="editar-acongreso_enviodatos.php" method="post">
						<div class="field">
							<strong>
							INGRESE LOS SIGUIENTES DATOS:<br><br>
							</strong>
						</div>
						<div class="field">
							Nombre:<br>
							<input type="text" class="form-control" name="nombre" id="nombre" value="<?php echo $nombre_congreso ?>" required="">
						</div>
						
						<div class="field">
							Año:<br>
							<input type="" class="form-control" name="anio" id="anio" placeholder="Año"  maxlength="4" minlength="4" value="<?php echo $anio ?>" onkeypress='return validaNumericos(event)' required="">
							<script type="text/javascript">
								document.getElementById("anio").addEventListener("input", (e) => {
								let value = e.target.value;
								e.target.value = value.replace(/[^A-Z\d-]/g, "");
								});
							</script>
						</div>
						
						<div class="field">
							Fecha Inicio:<br>
							<input type="date"  class="form-control" name="fi" id="fi" placeholder="dd/mm/aaaa"  maxlength="6" minlength="6" value="<?php echo $fecha_inicio ?>" size= 36 required="">
						</div>
			
						<div class="field">
							Fecha Término:<br>
							<input type="date" class="form-control" name="ff" id="ff" placeholder="dd/mm/aaaa"  maxlength="6" minlength="6" value="<?php echo $fecha_fin ?>" size= 36 required="">
						</div>
			
						<div class="field">
							Correo Electrónico: <br>
							<input type="email" class="form-control" size="20" name="correo" placeholder="nombre@dominio.com" value="<?php echo $correo ?>" required=""> 								    
						</div>
								
						<div class="field">
				
							Telefono: <br>
							<input type="text" class="form-control" size="20" name="numero" id="numero" required="" maxlength="15" minlength="10" value="<?php echo $telefono ?>" title="Ingresa Telefono fijo, por favor" onkeypress='return validaNumericos(event)' required=""> 
						</div>
						
										<div class="field">
											Teléfono Secundario:  <br>						
											<input type="text"  class="form-control" size="20" name="numero2" id="numero"  maxlength="15" minlength="10" value="<?php echo $telefono2 ?>" placeholder="Campo no obligatorio" title="Ingresa Telefono fijo, por favor" onkeypress='return validaNumericos(event)'> 

											<script type="text/javascript">
													document.getElementById("numero").addEventListener("input", (e) => {
													let value = e.target.value;
													e.target.value = value.replace(/[^A-Z\d-]/g, "");
													});
											</script>
										</div>
								

						<table cellpadding="2px" cellspacing="2px" style="text-align:left;">
							<tr>
								<td>
									<strong>
										COSTOS:<br><br>
									</strong>
								</td>
							</tr>
							<tr>
								<td>
									Ponentes:
								</td>
								<td>
									<input type="text" class="form-control" size="13" name="costopo" id="costo" value="<?php echo $costoponente ?>" maxlength="10" placeholder="0.00" onkeypress='return validaNumericos(event)' required=""> 
									<script type="text/javascript">
									document.getElementById("costo").addEventListener("input", (e) => {
									let value = e.target.value;
									e.target.value = value.replace(/[^A-Z\d-]/g, "");
									});
									</script>
								</td>
							</tr>
							<tr>
								<td>
									Asistentes:
								</td>
								<td>							
									<input type="text" class="form-control" size="13" name="costoas" id="costo2" value="<?php echo $costoasistente ?>" maxlength="10" placeholder="0.00" onkeypress='return validaNumericos(event)'required=""> 
									<script type="text/javascript">
									document.getElementById("costo2").addEventListener("input", (e) => {
									let value = e.target.value;
									e.target.value = value.replace(/[^A-Z\d-]/g, "");
									});
									</script>
								</td>
							</tr>
							<tr>
								<td>
									Estudiantes:
								</td>
								<td>
									<input type="text" class="form-control" size="13" name="costoes" id="costo3" value="<?php echo $costoestudiante ?>" maxlength="10" placeholder="0.00" onkeypress='return validaNumericos(event)' required=""> 
									<script type="text/javascript">
									document.getElementById("costo3").addEventListener("input", (e) => {
									let value = e.target.value;
									e.target.value = value.replace(/[^A-Z\d-]/g, "");
									});
									</script>
								</td>
							</tr>
							<tr>
								<td>
									Prototipos:
								</td>
								<td>
									<input type="text" class="form-control" size="13" name="costopro" id="costo4" value="<?php echo $costoprototipo ?>" maxlength="10" placeholder="0.00" onkeypress='return validaNumericos(event)'> 
									<script type="text/javascript">
									document.getElementById("costo4").addEventListener("input", (e) => {
									let value = e.target.value;
									e.target.value = value.replace(/[^A-Z\d-]/g, "");
									});
									</script>
								</td>
							</tr>
						</table>
						<script type="text/javascript">
								document.getElementById("numero").addEventListener("input", (e) => {
								let value = e.target.value;
								e.target.value = value.replace(/[^A-Z\d-]/g, "");
								});
						</script>
				</div>
			</div>
		
			<div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"> 
				<div class="table-responsive" ">
						
							
							<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
							<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
							<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
													
							<script>

								$(document).ready(function(){
									var i = 1;

									$('#add').click(function () {
										i++;
										$('#dynamic_field').append('<tr id="row'+i+'">' +
																	'<td><input type="text" name="name[]" placeholder="Descripción" class="form-control name_list" /></td>' +
																	'<td><input type="date" name="name[]" placeholder="dd/mm/aaaa" class="form-control name_list" /></td>'+
																	'<td><input type="date" name="name[]" placeholder="dd/mm/aaaa" class="form-control name_list" /></td>'+
																	'<td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td>' +
																	'</tr>');
									});
									
									$(document).on('click', '.btn_remove', function () {
										var id = $(this).attr('id');
									   $('#row'+ id).remove();
									});
								})
							</script>
												
			<div class="field">
				<strong>
		            INSTRUCCIONES DE TRABAJOS:<br><br>
				</strong>
				<table>
						<tr>
							<td>
								<h3>
								<a class="nav-link active">Nota: Recuerda que las instrucciones se deben ingresar con estructura HTML, para ello puedes guiarte del siguiente documento:</a>
								</h3>
							</td>
						</tr>
						
						<tr>
							<td>
								<h3>
								<a class="nav-link active" href="images/Instrucciones_HTML.pdf"><u><font style="font-size: 22px;color: #052450;">Instrucciones para trabajos con estructura HTML</font></u></a>
								</h3>
							</td>
						</tr>
				</table>
		    </div>
		    <div class="field">
		        Ponencias Orales:<br>
				<textarea placeholder="Escriba aquí las instrucciones correspondientes al congreso" required="" name="instruc_po"  id="inst1" rows="" cols="" value="<?php echo $instruccionesponencia ?>"></textarea>
			</div>
						
			<div class="field">
				Carteles:<br>
				<textarea placeholder="Escriba aquí las instrucciones correspondientes al congreso" required="" name="instruc_ca" id="inst2" rows="5" cols="30" value="<?php echo $instruccionescartel ?>"></textarea>
			</div>
					
			<div class="field">
				Talleres:<br>
				<textarea placeholder="Escriba aquí las instrucciones correspondientes al congreso" required="" name="instruc_ta" id="inst3" rows="5" cols="30" value="<?php echo $instruccionestaller ?>"></textarea>
			</div>
			
			<div class="field">
				Prototipos:<br>
				<textarea placeholder="Escriba aquí las instrucciones correspondientes al congreso" required="" name="instruc_pro" id="inst3" rows="5" cols="30" value="<?php echo $instruccionesprototipo ?>"></textarea>
			</div>
			</div>
		    </div>

			<div class="btn" style="text-align:right; width:100%; margin-bottom: 5%; margin-top: 2%;">
				<button type="submit" name="submit" id="submit" style="margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;">Guardar</button>
			</form>
				<button type="B1" onclick="myFunctionB2()" style="background: whitesmoke; color: black; padding: 6px 10px; border-color: whitesmoke;border-radius: 20px;">Cancelar</button>
		    </div>
		 	
		</div>
	</div>
</div>



    <!-- End CONTENIDO -->
    
 
    <!-- Start Footer -->
     <!-- Start Footer -->
        <footer class="footer-box">
        <div class="container">
            <div class="row">
               <div class="col-md-12 white_fonts">
                    <div class="row">
                        
                        <div class="col-sm-6 col-md-6">
                            
                            <div class="full">
							<h3>Dirección</h3>
                                <ul class="menu_footer">
									<li><img src="images/i5.png"><span style="font-size: 85%;">Carr, Cuautitlán - Teoloyucan, San Sebastian Xhala, 54714 Cuautitlán Izcalli</span></li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-md-6">
                            <div class="full">
                                <div class="footer_blog full white_fonts">
                             <h3>Contacto</h3>
                             <ul class="full">
                               <li><img src="images/i6.png"><span style="font-size: 85%;"> <?php echo $correoC  ?></span></li>
                               <li><img src="images/i7.png"><span style="font-size: 85%;"><?php echo "$numeroC1    $numeroC2"; ?></span></li>
                             </ul>
                         </div>
                            </div>
                        </div>
					</div>
                </div>
			 </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link active" href="derechosa.php" style="color:white">© Sitio desarrollado por CDD Solutions S.A. y administrado por el Departamento de Matemáticas. Hecho en México, (2020 - 2021)</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
	/* counter js */

(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
function validaNumericos(event) {
    if(event.charCode >= 48 && event.charCode <= 57){
      return true;
     }
     return false;        
}


//FUNCION PARA EDITAR O CANCELAR EDICION
function init(){
	var x = document.getElementById("vercongreso");
    var y = document.getElementById("editarcongreso");
    
    x.style.display = "block";
    y.style.display = "none";
     

}

function myFunctionB1() {
    var x = document.getElementById("vercongreso");
    var y = document.getElementById("editarcongreso");
    
    if (x.style.display === "none") {
        x.style.display = "block";
        y.style.display = "none";
         
    } else {
        x.style.display = "none";
        y.style.display = "block";
              
    }
    
}

function myFunctionB2() {
    var x = document.getElementById("vercongreso");
    var y = document.getElementById("editarcongreso");
    if (y.style.display === "none") {
        y.style.display = "block";
        x.style.display = "none";
        
    } else {
        x.style.display = "block";
        y.style.display = "none";
              
    }
}
init();
	</script>
</body>

</html>