<?php
session_start();
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
$usuario=$_SESSION['Usuario'];

use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
   	require 'PHPMailer/Exception.php';
    require 'PHPMailer/PHPMailer.php';
    require 'PHPMailer/SMTP.php';
    require ('libreria/fpdf.php');

$estatus=$_POST["estatusIMG"];
$clave=$_POST["id_ponencia"];
$titulo=$_POST["titulo"];
date_default_timezone_set("America/Mexico_City");
$fecha=date("d-m-y");
$hora=date("H-i-s");
if(isset($_POST['calificacion'])){
	$calificacion=$_POST['calificacion'];
	}else{
		$calificacion="";
	}
if (isset($_POST["comentario"])) {
	$comentario=$_POST["comentario"];
	}else{
		$comentario="";	
	}
if(isset($_POST['rcriterio'])){
	$rcriterio=$_POST['rcriterio'];
	}else{
	$rcriterio="";	
	}
if (isset($_POST['idActualizacion'])) {
	$idActualizacion=$_POST['idActualizacion'];
}else{
	$idActualizacion="";
}
include ('Conexion.php');
		//TRAE EL NÚMERO DE LA CONGRESO
	$consulta_num_congreso=pg_query($conexion,"SELECT MAX(numero_congreso) FROM congreso");//numero de congreso el AS es un alias
	$row0=pg_fetch_row($consulta_num_congreso);//eL pg_fetch_row trae los datos de la consulta y los asigna a la variable $row0
	$num_congreso=$row0[0];//el número máximo que encontro del congreso lo asigna a la variable consulta0 y es el máximo por que es el [0]NUMERO MÁXIMO DEL CONGRESO

//TRAE EL ID DE LA CONGRESO CON AYUDA DE SU NUMERO
	$consulta_id_congreso=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$num_congreso'"); //Selecciona los datos con el resultado de la consulta anterior (NUMERO MAXIMO DEL CONGRESO)
	$row1=pg_fetch_row($consulta_id_congreso);//Los datos del ID_CONGRESO Y NOMBRE CONGRESO son asignados a la variable $row
	$id_congresoactual=$row1[0];//es el id congreso
	
	if($estatus==="Enviado" || $estatus==='En Evaluacion'){ //SI EL ESTATUS ES ENVIADO (EVALUACION DE IMAGEN)
		
		if($idActualizacion!==""){
			$insertarActualizacionImagen=pg_query($conexion, "UPDATE actualizacion_cartel SET observaciones='$comentario' ,estatus_actualizacion ='$rcriterio' WHERE id_actualizacion='$idActualizacion' and id_ponencia_cartel='$clave'");
				if ($idActualizacion) {
					$estatusRegistro="HECHO";
				}else{
					$estatusRegistro="ERROR";
					
				}
		}else{//SI NO HAY ACTUALIZACIONES DE ESA IMAGEN
			$imagenCartel=pg_query($conexion, "UPDATE ponencia_cartel SET observaciones='$comentario' ,estatus_imagen ='$rcriterio' WHERE id_ponencia_cartel='$clave'");
			if($imagenCartel){
				$estatusRegistro="HECHO";
				$estatusCImg="";
				}else{
				$estatusRegistro="ERROR";
				$estatusCImg="";
				}
		}
	}else if($estatus==="Aceptado"){
		$directorio="evaluacionesConcursoCarteles/";
			if(isset($_POST['archivo'])){
			$archivox=$_POST['archivo'];
			}else{
			$archivox="";	
			}
		
		$archivo=basename($_FILES["archivo"]["name"]);
		
		$tipoArchivo=strtolower(pathinfo($archivo, PATHINFO_EXTENSION));
		
   		 if ($tipoArchivo=="xlsx" || $tipoArchivo=="xls") {
   		 	$nombre_final= "$clave-$usuario-$fecha-$hora-_$archivo";
   		 	$ruta=$directorio.$nombre_final;
   		 	$subirarchivos=move_uploaded_file($_FILES["archivo"]["tmp_name"],$ruta);
   		 	
             if($subirarchivos){
             	$concursoImg="INSERT INTO usuario_evalua_cartel(id_usuario, id_ponencia_cartel, calificacion, id_congreso, comentarios, rubrica) VALUES ('$usuario','$clave','$calificacion', '$id_congresoactual', '$comentario', '$ruta')";
				$registroCalificacion=pg_query($conexion, $concursoImg);
				if ($registroCalificacion) {
					$estatusCImg="HECHO";
					$estatusRegistro="";
				}else{
					$estatusCImg="ERROR";
					$estatusRegistro="";
				}
             }else{//AL SUBIR EL ARCHIVO
             	echo"<script>alert('Error no se logro subir el archivo de manera correcta.');window.location='evaluartr.php'</script>"; 
             }
   		 }else{//TIPO DE ARCHIVO
   		 	 echo"<script>alert('Error ingresaste el tipo de archivo incorrecto, recuerda que solo se aceptan archivos terminación .xlsx y .xls');window.location='evaluartr.php'</script>"; 
   		 }

	}
		//**********************************ENVIO DEL PDF***************************
		if(!empty($estatusRegistro) && $estatusRegistro==="HECHO" && empty($estatusCImg) && $estatusCImg===""){
						if($estatusRegistro==="HECHO"){
							 //CORREO AUTOR
	                                    //**************************************************************************************
	                                    class PDF extends FPDF{
	                                    // Cabecera de página
	                                    function Header()
	                                    {
	                                        // Logo
	                                        $this->Image('logo.jpg',0,0,220);
	                                        // Arial bold 15
	                                        $this->SetFont('Arial','B',15);
	                                        // Movernos a la derecha
	                                        $this->Cell(80);
	                                        // Título
	                                        $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
	                                        // Salto de línea
	                                        $this->Ln(50);
	                                        
	                                    }
	                                    
	                                    // Pie de página
	                                    function Footer() {
	                                        // Posición: a 1,5 cm del final
	                                        $this->SetY(-15);
	                                        // Arial italic 8
	                                        $this->SetFont('Arial','I',8);
	                                        // Número de página
	                                        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	                                        }
	                                    }
	                                     // Creación del objeto de la clase heredada
	                                    $pdf = new PDF();
	                                    $pdf->AliasNbPages();
	                                    $pdf->AddPage();
	                                    $pdf->SetFont('Times','',12);
	                                    
	                                    $pdf->Cell(40,10,utf8_decode('Información de la Evaluacion correspondiente a la Imagen del Cartel registrado'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Clave del Cartel: '.$clave),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Nombre del Cartel: '.$titulo),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('El cual fue considerado con el estatus de: '.$rcriterio),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('con la siguiente observacion: '.$comentario),0,1);
	                                    if($rcriterio==="Aceptado"){
		                                    $pdf->Cell(40,10,utf8_decode('Le pedimos que este atento a su correo y las fechas correspondiente al Concurso de Carteles,'),0,1); 
		                                    $pdf->Cell(40,10,utf8_decode('ya que se le notificará la calificación otorgada.'),0,1); 
	                                	}else{
	                                		 $pdf->Cell(40,10,utf8_decode('Por lo cual le pedimos que verifique las fechas para la recepción de Imagenes y'),0,1); 
	                                		 $pdf->Cell(40,10,utf8_decode('realice los cambios correspondientes considerando la observación otorgada, posteriormente suba la'),0,1);
	                                		 $pdf->Cell(40,10,utf8_decode('actualización en el apartado de Trabajos Registrados, Carteles/Imagen.'),0,1);

	                                	}
	                                	$pdf->Cell(40,10,utf8_decode(''),0,1);
	                                	$pdf->Cell(40,10,utf8_decode(''),0,1);
	                                	$pdf->Cell(40,10,utf8_decode(''),0,1);
	                                	$pdf->Cell(40,10,utf8_decode(''),0,1);
	                                	$pdf->Cell(40,10,utf8_decode(''),0,1);
	                                	$pdf->Cell(40,10,utf8_decode('                                             Atentamente: Comité Organizador.'),0,1);
	                                	
	                        			$pdf->Cell(40,10,utf8_decode('                                              Por mi raza hablará el espíritu.'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    
	                                    $archivoAdjunto = $pdf->Output("", "S");
	                                    $correoa=pg_query($conexion,"SELECT c.correo,us.nombres,us.primer_ap,us.segundo_ap, us.rfc 
																FROM correos_usuario c, usuario us, usuario_ponencias as up
																WHERE up.id_ponencias='$clave'
																and up.tipo_autor='Autor'and up.id_usuario = c.id_usuario and
																c.id_usuario= us.id_usuario");
	                                    $i=0;
	                                    while($mostrarCR=pg_fetch_array($correoa)){
	                                         $cor[$i]=trim($mostrarCR['correo']);
	                                         $nombre=trim($mostrarCR['nombres']);
	                                         $apPA=trim($mostrarCR['primer_ap']);
	                                         $apMa=trim($mostrarCR['segundo_ap']);
	                                         $rfcA=trim($mostrarCR['rfc']);
	                                        $i=$i+1;
	                                    }

	                                     $mail = new PHPMailer(true);
	                                    
	                                    try {
	                                        //Server settings
	                                        $mail->SMTPDebug = 0;                      // Enable verbose debug output
	                                        $mail->isSMTP();                                            // Send using SMTP
	                                        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
	                                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
	                                        $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
	                                        $mail->Password   = 'CongresoMate2020';                               // SMTP password
	                                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
	                                        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
	                                        $mail->CharSet  = 'UTF-8';//PARA EL ACENTO
	                                    
	                                        //Recipients
	                                        $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
	                                        $mail->addAddress("$cor[0]", "$nombre $apPA");
	                                        if(!empty($cor[1])){
	                                            $mail->addAddress("$cor[1]", "$nombre $apPA");
	                                        }
	                                        if(!empty($cor[2])){
	                                            $mail->addAddress("$cor[2]", "$nombre $apPA");
	                                        }
	                                       // Content
	                                        $mail->isHTML(true);                                  // Set email format to HTML
	                                        $mail->Subject = 'Información Evaluacion  de la Imagen del Cartel registrado';
	                                        $mail->Body    = 'En el siguiente documento se adjuntan la información de la evaluación otorgada a su Cartel registrado.';
	                                        $mail->addStringAttachment($archivoAdjunto, 'Evaluación_Imagen_cartel.pdf');
	                                        $mail->send();
	                                        
	                                        }catch (Exception $e) {
	                                            echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
	                                        }

	                                    //*****************************************************************************************
	                                          $numCoautoresTrabajo=pg_query($conexion, "SELECT COUNT (*) id_usuario FROM usuario_ponencias 
					                        WHERE id_ponencias ='$clave' and tipo_autor='Coautor' and id_congreso = '$id_congresoactual'");
					                        $numCoautores =pg_fetch_assoc($numCoautoresTrabajo);
					                        

					                        //VALIDA SI HAY COAUTORES
					                            if($numCoautores['id_usuario']!=0){
					                            	
					                                  
					//********************************************************************************************************************
					                                class PDF2 extends FPDF
					                        {
					                        // Cabecera de página
					                        function Header()
					                        {
					                            // Logo
					                            $this->Image('logo.jpg',0,0,220);
					                            // Arial bold 15
					                            $this->SetFont('Arial','B',15);
					                            // Movernos a la derecha
					                            $this->Cell(80);
					                            // Título
					                            $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
					                            // Salto de línea
					                            $this->Ln(50);
					                            
					                        }
					                        
					                        // Pie de página
					                        function Footer()
					                        {
					                            // Posición: a 1,5 cm del final
					                            $this->SetY(-15);
					                            // Arial italic 8
					                            $this->SetFont('Arial','I',8);
					                            // Número de página
					                            $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
					                        	}
					                        }
					                        
					                        // Creación del objeto de la clase heredada
					                         $pdf = new PDF();
					                        $pdf->AliasNbPages();
					                        $pdf->AddPage();
					                        $pdf->SetFont('Times','',12);


	                                    $pdf->Cell(40,10,utf8_decode('Información de la Evaluacion correspondiente a la Imagen del Cartel registrado'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('A continuación se le indicará la evaluación del cartel al que usted pertenece:'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Clave del Cartel: '.$clave),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Nombre del cartel: '.$titulo),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('El cual fue considerado con el estatus de: '.$rcriterio),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('con la siguiente observacion: '.$comentario),0,1);
	                                    if($rcriterio==="Aceptado"){
		                                    $pdf->Cell(40,10,utf8_decode('Le pedimos que este atento a su correo y las fechas correspondiente al Concurso de Carteles,'),0,1); 
		                                    $pdf->Cell(40,10,utf8_decode('ya que se le notificará la calificación otorgada.'),0,1); 
	                                	}else{
	                                		 $pdf->Cell(40,10,utf8_decode('Por lo cual le pedimos que verifique las fechas para la recepción de Imagenes y'),0,1); 
	                                		 $pdf->Cell(40,10,utf8_decode('recordandole que para realizar los cambios correspondientes es necesario que el autor los modifique.'),0,1);
	                                		

	                                	}
	                                	$pdf->Cell(40,10,utf8_decode(''),0,1);
	                                	$pdf->Cell(40,10,utf8_decode(''),0,1);
	                                	$pdf->Cell(40,10,utf8_decode(''),0,1);
	                                	$pdf->Cell(40,10,utf8_decode(''),0,1);
	                                	$pdf->Cell(40,10,utf8_decode(''),0,1);
	                                	$pdf->Cell(40,10,utf8_decode('                                             Atentamente: Comité Organizador.'),0,1);
	                                	
	                        			$pdf->Cell(40,10,utf8_decode('                                              Por mi raza hablará el espíritu.'),0,1);

					                        $archivoAdjunto2 = $pdf->Output("", "S");
					    
					                        //Envio de correo coautores
					                        $mail = new PHPMailer(true);
					                        
					                        try {
					                            //Server settings
					                            $mail->SMTPDebug = 0;                      // Enable verbose debug output
					                            $mail->isSMTP();                                            // Send using SMTP
					                            $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
					                            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
					                            $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
					                            $mail->Password   = 'CongresoMate2020';                               // SMTP password
					                            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
					                            $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
					                            $mail->CharSet  = 'UTF-8';//PARA EL ACENTO
					                        
					                            //Recipients
					                            $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
					                            // obtener nombre,apellidos y correo de coautores del trabajo
					                          

					                            $infoCoau=pg_query($conexion,"select u.nombres,u.primer_ap ,u.segundo_ap,c.correo FROM correos_usuario c, usuario u, usuario_ponencias up WHERE up.id_ponencias='$clave'and up.id_usuario=u.id_usuario and u.id_usuario= c.id_usuario and up.tipo_autor!='Autor'");
					                            $j=0;
					                            while($infoCoa=pg_fetch_array($infoCoau)){
					                               
					                                $nombreCoa[$j]=trim($infoCoa['nombres']);
					                                $apellidoCoa[$j]=trim($infoCoa['primer_ap']);
					                                $apellidoCoa2[$j]=trim($infoCoa['segundo_ap']);
					                                $corC[$j]=trim($infoCoa['correo']);
					                               
					                            
					                                $j=$j+1;
					                               }
					                               if(!empty($corC[0])){
					                                $mail->addAddress("$corC[0]",utf8_decode("$nombreCoa[0] $apellidoCoa[0] $apellidoCoa2[0]"));
					    
					                               }
					                               if(!empty($corC[1])){
					                                $mail->addAddress("$corC[1]",utf8_decode("$nombreCoa[1] $apellidoCoa[1] $apellidoCoa2[1]"));
					    
					                               }
					                               if(!empty($corC[2])){
					                                $mail->addAddress("$corC[2]",utf8_decode("$nombreCoa[2] $apellidoCoa[2] $apellidoCoa2[2]"));
					    
					                               }
					                               if(!empty($corC[3])){
					                                $mail->addAddress("$corC[3]",utf8_decode("$nombreCoa[3] $apellidoCoa[3] $apellidoCoa2[3]"));
					    
					                               }   
					                        
					                            // Attachments
					                            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
					                            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
					                        
					                            // Content
					                            $mail->isHTML(true);                                  // Set email format to HTML
					                            $mail->Subject = 'Información Evaluacion  de la Imagen del Cartel registrado';
					                            $mail->Body    = 'En el siguiente documento se adjuntan la información de la evaluación otorgada a su Cartel registrado.';
					                            $mail->addStringAttachment($archivoAdjunto2, 'Evaluación_Imagen_cartel.pdf');
					                            $mail->send();

					                        } catch (Exception $e) {
					                            echo "Error al enviar el mensaje para coautores: {$mail->ErrorInfo}";
					                        }
					                    }

					//*********************************************************************************************************************
					                     //CORREO Evaulador
	                                    //**************************************************************************************
	                                  
	                                     // Creación del objeto de la clase heredada
	                                    $pdf = new PDF();
	                                    $pdf->AliasNbPages();
	                                    $pdf->AddPage();
	                                    $pdf->SetFont('Times','',12);
	                                    
	                                    $pdf->Cell(40,10,utf8_decode('Información de la evaluacion Realizada sobre la imagen de un Cartel'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('La evaluacion que realizó del siguiente cartel:'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Clave del Cartel: '.$clave),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Nombre del Cartel: '.$titulo),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('El cual evaluo con el siguiente criterio '.$rcriterio),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('y con la siguiente observacion: '.$comentario),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Fue registrada con Éxito.'),0,1); 
		                                $pdf->Cell(40,10,utf8_decode('Agradecemos su apoyo.'),0,1); 
	                                	$pdf->Cell(40,10,utf8_decode(''),0,1);
	                                	
	                                	
	                        			$pdf->Cell(40,10,utf8_decode('                                              Por mi raza hablará el espíritu.'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    
	                                    $archivoAdjunto3 = $pdf->Output("", "S");
	                                    $correoe=pg_query($conexion,"SELECT c.correo,us.nombres,us.primer_ap,us.segundo_ap
																FROM correos_usuario c, usuario us
																WHERE us.id_usuario='$usuario'
																and c.id_usuario= us.id_usuario");
	                                    $i=0;
	                                    while($mostrarE=pg_fetch_array($correoe)){
	                                         $corE[$i]=trim($mostrarE['correo']);
	                                         $nombreE=trim($mostrarE['nombres']);
	                                         $apPAE=trim($mostrarE['primer_ap']);
	                                         $apMaE=trim($mostrarE['segundo_ap']);
	                                         
	                                        $i=$i+1;
	                                    }

	                                     $mail = new PHPMailer(true);
	                                    
	                                    try {
	                                        //Server settings
	                                        $mail->SMTPDebug = 0;                      // Enable verbose debug output
	                                        $mail->isSMTP();                                            // Send using SMTP
	                                        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
	                                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
	                                        $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
	                                        $mail->Password   = 'CongresoMate2020';                               // SMTP password
	                                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
	                                        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
	                                        $mail->CharSet  = 'UTF-8';//PARA EL ACENTO
	                                    
	                                        //Recipients
	                                        $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
	                                        $mail->addAddress("$corE[0]", "$nombreE $apPAE");
	                                        if(!empty($corE[1])){
	                                            $mail->addAddress("$corE[1]", "$nombreE $apPAE");
	                                        }
	                                        if(!empty($corE[2])){
	                                            $mail->addAddress("$corE[2]", "$nombreE $apPAE");
	                                        }
	                                       // Content
	                                        $mail->isHTML(true);                                  // Set email format to HTML
	                                        $mail->Subject = 'Información del Cartel Evaluado';
	                                        $mail->Body    = 'En el siguiente documento se adjuntan la información de la evaluación que realizó.';
	                                        $mail->addStringAttachment($archivoAdjunto3, 'Evaluación_Imagen_cartel.pdf');
	                                        $mail->send();
	                                        echo "<script>alert('Se registro la evaluacion correctamente'); window.location=' evaluartr.php';</script>";
	                                        }catch (Exception $e) {
	                                            echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
	                                        }

	             
	              		echo "<script>alert('La evaluacion se registro correctamente'); window.location='evaluartr.php';</script>";

						}else{
					
							echo "<script>alert('Error, No se registro la evaluacion correctamente'); window.location='evaluartr.php';</script>";
						}
		}else if(!empty($estatusCImg) && $estatusCImg==="HECHO" && empty($estatusRegistro) && $estatusRegistro===""){
					if($estatusCImg==="HECHO"){
					 //CORREO AUTOR
	                                    //**************************************************************************************
	                                    class PDF extends FPDF{
	                                    // Cabecera de página
	                                    function Header()
	                                    {
	                                        // Logo
	                                        $this->Image('logo.jpg',0,0,220);
	                                        // Arial bold 15
	                                        $this->SetFont('Arial','B',15);
	                                        // Movernos a la derecha
	                                        $this->Cell(80);
	                                        // Título
	                                        $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
	                                        // Salto de línea
	                                        $this->Ln(50);
	                                        
	                                    }
	                                    
	                                    // Pie de página
	                                    function Footer() {
	                                        // Posición: a 1,5 cm del final
	                                        $this->SetY(-15);
	                                        // Arial italic 8
	                                        $this->SetFont('Arial','I',8);
	                                        // Número de página
	                                        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	                                        }
	                                    }
	                                     // Creación del objeto de la clase heredada
	                                    $pdf = new PDF();
	                                    $pdf->AliasNbPages();
	                                    $pdf->AddPage();
	                                    $pdf->SetFont('Times','',12);
	                                    
	                                    $pdf->Cell(40,10,utf8_decode('Información de la evaluacion Realizada para el concurso de Carteles'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('La evaluacion que realizó, para el concurso de carteles del siguiente cartel:'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Clave del Cartel: '.$clave),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Nombre del Cartel: '.$titulo),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('El cual evaluo con la calificación de: '.$calificacion),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('con la siguiente observacion: '.$comentario),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Fue registrada con Éxito.'),0,1); 
		                                $pdf->Cell(40,10,utf8_decode('Agradecemos su apoyo.'),0,1); 
	                                	$pdf->Cell(40,10,utf8_decode(''),0,1);
	                                	
	                                	
	                        			$pdf->Cell(40,10,utf8_decode('                                              Por mi raza hablará el espíritu.'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    
	                                    $archivoAdjunto3 = $pdf->Output("", "S");
	                                    $correoe=pg_query($conexion,"SELECT c.correo,us.nombres,us.primer_ap,us.segundo_ap
																FROM correos_usuario c, usuario us
																WHERE us.id_usuario='$usuario'
																and c.id_usuario= us.id_usuario");
	                                    $i=0;
	                                    while($mostrarE=pg_fetch_array($correoe)){
	                                         $corE[$i]=trim($mostrarE['correo']);
	                                         $nombreE=trim($mostrarE['nombres']);
	                                         $apPAE=trim($mostrarE['primer_ap']);
	                                         $apMaE=trim($mostrarE['segundo_ap']);
	                                         
	                                        $i=$i+1;
	                                    }

	                                     $mail = new PHPMailer(true);
	                                    
	                                    try {
	                                        //Server settings
	                                        $mail->SMTPDebug = 0;                      // Enable verbose debug output
	                                        $mail->isSMTP();                                            // Send using SMTP
	                                        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
	                                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
	                                        $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
	                                        $mail->Password   = 'CongresoMate2020';                               // SMTP password
	                                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
	                                        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
	                                        $mail->CharSet  = 'UTF-8';//PARA EL ACENTO
	                                    
	                                        //Recipients
	                                        $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
	                                        $mail->addAddress("$corE[0]", "$nombreE $apPAE");
	                                        if(!empty($corE[1])){
	                                            $mail->addAddress("$corE[1]", "$nombreE $apPAE");
	                                        }
	                                        if(!empty($corE[2])){
	                                            $mail->addAddress("$corE[2]", "$nombreE $apPAE");
	                                        }
	                                       // Content
	                                        $mail->isHTML(true);                                  // Set email format to HTML
	                                        $mail->Subject = 'Información del Cartel Evaluado';
	                                        $mail->Body    = 'En el siguiente documento se adjuntan la información de la evaluación que realizó.';
	                                        $mail->addStringAttachment($archivoAdjunto3, 'Evaluación_Imagen_cartel.pdf');
	                                        $mail->send();
	                                        
	                                        }catch (Exception $e) {
	                                            echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
	                                        }
	                                        echo "<script>alert('Se registro la evaluacion correctamente'); window.location='evaluartr.php';</script>";
				}else{
					echo "<script>alert('Error, No se registro la evaluacion correctamente'); window.location='evaluartr.php';</script>";
				}
		}



?>