<?php
session_start();
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php'; 
require ('libreria/fpdf.php');
include ('Conexion.php');
date_default_timezone_set("America/Mexico_City");

$fecha=date("d-m-Y ");// obtinene la hora del registro
$hora=date("h:i a");

$Estatus="Enviado";
$Clave=$_POST['Clave'];
$Titulo=$_POST['Titulo'];
$funcionalidad=$_POST['textarea'];

$Referencias=$_POST['textarea2'];
$usuario=$_SESSION['Usuario'];

$coa1=@strtolower($_POST['Coa0']);
$coa2=@strtolower($_POST['Coa1']);
$coa3=@strtolower($_POST['Coa2']);
$coa4=@strtolower($_POST['Coa3']);

$coau1=@strtolower($_POST['Coautor0']);//Si no hay cambios de coautores es el id del coautor original
$coau2=@strtolower($_POST['Coautor1']);//Si no hay cambios de coautores
$coau3=@strtolower($_POST['Coautor2']);//Si no hay cambios de coautores
$coau4=@strtolower($_POST['Coautor3']);//Si no hay cambios de coautores



$TipoAutor="Coautor";
//CONSULTAS 
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
$row5=pg_fetch_row($consulta);
$consulta0=$row5[0];
$consulta1=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$consulta0'");
$row6=pg_fetch_row($consulta1);
$numeroCongreso=$row6[0];// se obtiene en id del congreso actual

$consulta1=pg_query($conexion,"SELECT MAX(numeracion) FROM actualizacion_resumen_pro  where id_congreso='$numeroCongreso'");
$row7=pg_fetch_row($consulta1);

if(empty($row7)){
$numeroid =1;
$id_actualizacion="ACT".$Clave.$numeroid;// se crea id actualizacion en 1 si no existe ninguna actualizacion;

}else{
    $numeroid=$row7[0]+1;
    $id_actualizacion="ACT".$Clave.$numeroid;// se crea id actualizacion sumando el numero actualizacion anterior mas 1;
    
}

$insert1=("INSERT INTO actualizacion_resumen_pro(id_actualiza_r_p,id_prototipo,titulo,resumen,referencias,fecha,hora,id_congreso,numeracion,estatus_r_p) 
                                        VALUES('$id_actualizacion','$Clave','$Titulo','$funcionalidad','$Referencias','$fecha','$hora','$numeroCongreso','$numeroid','$Estatus')");
       pg_query($conexion,$insert1); //Se inserta la actualizacion

// se selecciona el coautor si es que se agrego o se modifico alguno
if($coau1!= NULL && $coa1==NULL){
    $coautor1=$coau1;
 }
if( $coa1!=NULL&& $coau1!=NULL || $coa1!=NULL && $coau1==NULL){
        $id=@pg_query($conexion,"SELECT u.id_usuario FROM usuario u, correos_usuario cu WHERE cu.correo='$coa1' and cu.id_usuario = u.id_usuario");
        $row=@pg_fetch_row($id);
        $coautor1=$row[0];
        
        if($coau1==NULL){
            pg_query($conexion,"INSERT INTO  prototipos_usuarios VALUES ('$coautor1','$Clave','$TipoAutor','$numeroCongreso')");
        }else{
        pg_query($conexion,"UPDATE prototipos_usuarios SET id_usuario= '$coautor1' WHERE id_prototipo='$Clave' and id_usuario='$coau1' and id_congreso='$numeroCongreso'");
        }   
}
if($coau2!= NULL && $coa2==NULL){
       $coautor2=$coau2;
      }
if( $coa2!=NULL&& $coau2!=NULL || $coa2!=NULL && $coau2==NULL){
        $id2=@pg_query($conexion,"SELECT u.id_usuario FROM usuario u, correos_usuario cu WHERE cu.correo='$coa2' and cu.id_usuario = u.id_usuario");
        $row2=@pg_fetch_row($id2);
        $coautor2=$row2[0];
        if($coau2==NULL){
            pg_query($conexion,"INSERT INTO  prototipos_usuarios VALUES ('$coautor2','$Clave','$TipoAutor','$numeroCongreso')");
        }else{
        pg_query($conexion,"UPDATE prototipos_usuarios SET id_usuario= '$coautor2' WHERE id_prototipo='$Clave' and id_usuario='$coau2' and id_congreso='$numeroCongreso'");
        }
    }
if($coau3!= NULL && $coa3==NULL){
        $coautor3=$coau3;
    }
if( $coa3!=NULL&& $coau3!=NULL || $coa3!=NULL && $coau3==NULL){
        $id3=@pg_query($conexion,"SELECT u.id_usuario FROM usuario u, correos_usuario cu WHERE cu.correo='$coa3' and cu.id_usuario = u.id_usuario");
        $row3=@pg_fetch_row($id3);
        $coautor3=$row3[0];
        if($coau3==NULL){
            pg_query($conexion,"INSERT INTO  prototipos_usuario VALUES ('$coautor3','$Clave','$TipoAutor','$numeroCongreso')");
        }else{
        pg_query($conexion,"UPDATE prototipos_usuario SET id_usuario= '$coautor3' WHERE id_prototipo='$Clave' and id_usuario='$coua3' and id_congreso='$numeroCongreso'");                
     }
    }
if($coau4!= NULL && $coa4==NULL){
        $coautor4=$coau4;
        }
if( $coa4!=NULL&& $coau4!=NULL || $coa4!=NULL && $coau4==NULL){
        $id4=@pg_query($conexion,"SELECT u.id_usuario FROM usuario u, correos_usuario cu WHERE cu.correo='$coa4' and cu.id_usuario = u.id_usuario");
        $row4=@pg_fetch_row($id4);
        $coautor4=$row4[0];
        if($coau4==NULL){
            pg_query($conexion,"INSERT INTO  prototipos_usuario VALUES ('$coautor4','$Clave','$TipoAutor','$numeroCongreso')");
        }else{
        pg_query($conexion,"UPDATE prototipos_usuario SET id_usuario= '$coautor4' WHERE id_prototipo='$Clave' and id_usuario='$coau4' and id_congreso='$numeroCongreso'");
    }
}
$infoCoau=pg_query($conexion,"select u.nombres,u.primer_ap ,u.segundo_ap,c.correo FROM correos_usuario c, usuario u, prototipos_usuario up WHERE up.id_prototipo='$Clave'and up.id_usuario=u.id_usuario and u.id_usuario= c.id_usuario and up.tipo_autor!='Autor';");
$j=0;                       
// Creacion de PDF Autor
class PDF extends FPDF
                    {
                    // Cabecera de página
                    function Header()
                    {
                        // Logo
                        $this->Image('logo.jpg',0,0,220);
                        // Arial bold 15
                        $this->SetFont('Arial','B',15);
                        // Movernos a la derecha
                        $this->Cell(80);
                        // Título
                        $this->Cell(50,80,utf8_decode('Congreso Matematicas'),30,0,'C');
                        // Salto de línea
                        $this->Ln(50);
                        
                    }
                    
                    // Pie de página
                    function Footer()
                    {
                        // Posición: a 1,5 cm del final
                        $this->SetY(-15);
                        // Arial italic 8
                        $this->SetFont('Arial','I',8);
                        // Número de página
                        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
                    }
                    }
                    
                    // Creación del objeto de la clase heredada
                    $pdf = new PDF();
                    $pdf->AliasNbPages();
                    $pdf->AddPage();
                    $pdf->SetFont('Times','',12);
                    
                    $pdf->Cell(40,10,utf8_decode('Actualizacion Prototipo'),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    $pdf->Cell(40,10,utf8_decode('Usted ha Actualizado el siguiente resumen para presentar un Prototipo'),0,1);
                    $pdf->Multicell(190,5,utf8_decode('Clave del Prototipo: '.$Clave),0,1);
                    $pdf->Multicell(190,5,utf8_decode('Título del Prototipo: '.$Titulo),0,1);
                    $pdf->Multicell(190,5,utf8_decode('Resumen del trabajo: '.$funcionalidad),0,1);
                    $pdf->Multicell(190,5,utf8_decode('Referencias: '.$Referencias),0,1);
                    //$pdf->Cell(40,10,utf8_decode('Referencias: '.$Materiales),0,1);

                    $pdf->Cell(40,10,utf8_decode('Coautores registrados en el trabajo:'),0,1);

                    while($infoCoa=pg_fetch_array($infoCoau)){
                        if(empty($infoCoa)){
                            $pdf->Cell(40,10,utf8_decode('No hay coautores registrados'),0,1); 
                        }
                        $nombreCoa[$j]=trim($infoCoa['nombres']);
                        $apellidoCoa[$j]=trim($infoCoa['primer_ap']);
                        $apellidoCoa2[$j]=trim($infoCoa['segundo_ap']);
                        $corA[$j]=trim($infoCoa['correo']);
                    $pdf->Cell(40,10,utf8_decode($nombreCoa[$j]." ".$apellidoCoa[$j]." ".$apellidoCoa2[$j]." (".$corA[$j].")"),0,1);
                        $j=$j+1;
                       }
                    $pdf->Cell(40,10,utf8_decode('Si desea agregar o modificar los coautores de este trabajo, lo podrá realizar'),0,1);
                    $pdf->Cell(40,10,utf8_decode('cuando edite su resumen en el apartado "trabajos registrados".'),0,1);
                    $pdf->Cell(40,10,utf8_decode('Le pedimos de la manera más atenta, estar al pendiente de su cuenta para conocer'),0,1);
                    $pdf->Cell(40,10,utf8_decode('el estatus de su trabajo.'),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    
                    $archivoAdjunto = $pdf->Output("", "S");
//Envio de correo Autor
$correoa=pg_query($conexion,"SELECT c.correo,us.nombres,us.primer_ap,us.segundo_ap FROM correos_usuario c, usuario us WHERE c.id_usuario='$usuario' and us.id_usuario ='$usuario'");
$i=0;
while($mostrarCR=pg_fetch_array($correoa)){
    $cor[$i]=trim($mostrarCR['correo']);
    $nombre=trim($mostrarCR['nombres']);
    $apPA=trim($mostrarCR['primer_ap']);
    $apMa=trim($mostrarCR['segundo_ap']);
    $i=$i+1;
}

$mail = new PHPMailer(true);
                    
                    try {
                        //Server settings
                        $mail->SMTPDebug = 0;                      // Enable verbose debug output
                        $mail->isSMTP();                                            // Send using SMTP
                        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                        $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
                        $mail->Password   = 'CongresoMate2020';                               // SMTP password
                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                    
                        //Recipients
                        $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
                        $mail->addAddress("$cor[0]", "$nombre $apPA");
                        if(!empty($cor[1])){
                            $mail->addAddress("$cor[1]", "$nombre $apPA");
                        }
                        if(!empty($cor[2])){
                            $mail->addAddress("$cor[2]", "$nombre $apPA");
                        }
                                        
                        // Content
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = 'Actualizacion de trabajo';
                        $mail->Body    = 'En el siguiente documento se adjuntan los datos del Prototipo actualizado';
                        $mail->addStringAttachment($archivoAdjunto, 'Actualizacion_Prototipo.pdf');
                        $mail->send();
                        
                    } catch (Exception $e) {
                        echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
                    }

                    // Creacion de PDF Coautores 
if(!empty($coautor1) || !empty($coautor2)|| !empty($coautor3)||!empty($coautor4)){
    class PDF2 extends FPDF
                        {
                        // Cabecera de página
                        function Header()
                        {
                            // Logo
                            $this->Image('logo.jpg',0,0,220);
                            // Arial bold 15
                            $this->SetFont('Arial','B',15);
                            // Movernos a la derecha
                            $this->Cell(80);
                            // Título
                            $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
                            // Salto de línea
                            $this->Ln(50);
                            
                        }
                        
                        // Pie de página
                        function Footer()
                        {
                            // Posición: a 1,5 cm del final
                            $this->SetY(-15);
                            // Arial italic 8
                            $this->SetFont('Arial','I',8);
                            // Número de página
                            $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
                        }
                        }
                        
                        // Creación del objeto de la clase heredada
                        $pdf = new PDF();
                        $pdf->AliasNbPages();
                        $pdf->AddPage();
                        $pdf->SetFont('Times','',12);
                        
                        $pdf->Cell(40,10,utf8_decode('Actualizacion de Prototipo'),0,1);
                        $pdf->Cell(40,10,utf8_decode(''),0,1);
                        $pdf->Cell(40,10,utf8_decode('El Autor '.$nombre.' '.$apPA.' '.$apMa.' ha actualizado el siguiente trabajo:'),0,1);
                        $pdf->cell(40,10,utf8_decode('Clave del Prototipo: '.$Clave),0,1);
                        $pdf->Multicell(190,5,utf8_decode('Nombre del Prototipo: '.$Titulo),0,1);
                        $pdf->Multicell(190,5,utf8_decode('Resumen del trabajo: '.$Resumen),0,1);
                        $pdf->Multicell(190,5,10,utf8_decode('Referencias: '.$Referencias),0,1);
                        //$pdf->Cell(40,10,utf8_decode('Materiales: '.$Materiales),0,1);
                        $pdf->Cell(40,10,utf8_decode('Si desea conocer el estatus de su trabajo, lo podrá visualizar'),0,1);
                        $pdf->Cell(40,10,utf8_decode('en el apartado "trabajos registrados".'),0,1);
                        
                        $pdf->Cell(40,10,utf8_decode(''),0,1);
                        $archivoAdjunto2 = $pdf->Output("", "S");
    
    //Envio de correo coautores
    $mail = new PHPMailer(true);
                        
                        try {
                            //Server settings
                            $mail->SMTPDebug = 0;                      // Enable verbose debug output
                            $mail->isSMTP();                                            // Send using SMTP
                            $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                            $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
                            $mail->Password   = 'CongresoMate1';                               // SMTP password
                            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                            $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                        
                            //Recipients
                            $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
                            // obtener nombre,apellidos y correo de coautores del trabajo

                            $infoCoau=pg_query($conexion,"select u.nombres,u.primer_ap ,u.segundo_ap,c.correo FROM correos_usuario c, usuario u, prototipos_usuario up WHERE up.id_prototipo='$Clave'and up.id_usuario=u.id_usuario and u.id_usuario= c.id_usuario and up.tipo_autor!='Autor';");
                            
                            while($infoCoa=pg_fetch_array($infoCoau)){
                               
                                $nombreCoa[$j]=trim($infoCoa['nombres']);
                                $apellidoCoa[$j]=trim($infoCoa['primer_ap']);
                                $apellidoCoa2[$j]=trim($infoCoa['segundo_ap']);
                                $corC[$j]=trim($infoCoa['correo']);
                               
                            
                                $j=$j+1;
                               }
                               if(!empty($corC[0])){
                                $mail->addAddress("$corC[0]",utf8_decode("$nombreCoa[0] $apellidoCoa[0] $apellidoCoa2[0]"));
    
                               }
                               if(!empty($corC[1])){
                                $mail->addAddress("$corC[1]",utf8_decode("$nombreCoa[1] $apellidoCoa[1] $apellidoCoa2[1]"));
    
                               }
                               if(!empty($corC[2])){
                                $mail->addAddress("$corC[2]",utf8_decode("$nombreCoa[2] $apellidoCoa[2] $apellidoCoa2[2]"));
    
                               }
                               if(!empty($corC[3])){
                                $mail->addAddress("$corC[3]",utf8_decode("$nombreCoa[3] $apellidoCoa[3] $apellidoCoa2[3]"));
    
                               }
                        
                            // Attachments
                            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
                        
                            // Content
                            $mail->isHTML(true);                                  // Set email format to HTML
                            $mail->Subject = 'Actualizacion de trabajo';
                            $mail->Body    = 'En el siguiente documento se adjuntan los datos del Prototipo actualizado';
                            $mail->addStringAttachment($archivoAdjunto2, 'Actualizacion_Prototipo.pdf');
                            $mail->send();
                            
                        } catch (Exception $e) {
                            echo "Error al enviar el mensaje para coautores: {$mail->ErrorInfo}";
                        }
                    }
    
    
     echo"<script>alert('¡Prototipo Actualizado!');window.location='trabr.php'</script>";
    
    
    

?>