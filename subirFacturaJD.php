<?php
session_start();
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
$usuario=$_SESSION['Usuario'];
		include ('Conexion.php');
		use PHPMailer\PHPMailer\PHPMailer;
		use PHPMailer\PHPMailer\Exception;
		require 'PHPMailer/Exception.php';
		require 'PHPMailer/PHPMailer.php';
		require 'PHPMailer/SMTP.php'; 
		require ('libreria/fpdf.php');
	
	if(isset($_POST['nombre']) && !empty($_POST['nombre'])){
		$id_usuario=$_POST['nombre'];
	//TRAE EL NÚMERO DE LA CONGRESO
		$consulta_num_congreso=pg_query($conexion,"SELECT MAX(numero_congreso) FROM congreso");
		$row0=pg_fetch_row($consulta_num_congreso);
		$num_congreso=$row0[0];

	//TRAE EL ID DE LA CONGRESO CON AYUDA DE SU NUMERO
		$consulta_id_congreso=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$num_congreso'"); 
		$row1=pg_fetch_row($consulta_id_congreso);
		$id_congresoactual=$row1[0];
		  //VALIDACIÓN DE TERMINACIÓN DE ARCHIVOS
    $directorio="facturasJd/";
    $archivo=basename($_FILES["archivo"]["name"]);
    $tipoArchivo=strtolower(pathinfo($archivo, PATHINFO_EXTENSION));
    $maximob=10000000;
     if ($tipoArchivo=="pdf") {
     	
     		$nombre_final= "$id_usuario";
		    $ruta=$directorio.$nombre_final.$id_congresoactual;
		    $subirarchivos=move_uploaded_file($_FILES["archivo"]["tmp_name"],$ruta.".".$tipoArchivo);
             $rutaF=$ruta.".".$tipoArchivo;
		    if($subirarchivos){
		    		$insertarRutaFactura="UPDATE tipo_pagos_usuario set factura='$rutaF' WHERE id_usuario='$id_usuario' AND id_congreso='$id_congresoactual'";
		    		$resultado=pg_query($conexion, $insertarRutaFactura);
		    		if($resultado){
		  
		    			    //CORREO AUTOR
                   
                    //******************************************************************************************
                 
                    
                    class PDF extends FPDF{
                    // Cabecera de página
                    function Header()
                    {
                        // Logo
                        $this->Image('logo.jpg',0,0,220);
                        // Arial bold 15
                        $this->SetFont('Arial','B',15);
                        // Movernos a la derecha
                        $this->Cell(80);
                        // Título
                        $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
                        // Salto de línea
                        $this->Ln(50);
                        
                    }
                    
                    // Pie de página
                    function Footer() {
                        // Posición: a 1,5 cm del final
                        $this->SetY(-15);
                        // Arial italic 8
                        $this->SetFont('Arial','I',8);
                        // Número de página
                        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
                        }
                    }
                    
                    // Creación del objeto de la clase heredada
                    $pdf = new PDF();
                    $pdf->AliasNbPages();
                    $pdf->AddPage();
                    $pdf->SetFont('Times','',12);
                    
                    $pdf->Cell(40,10,utf8_decode('Registro de Factura'),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    $pdf->Cell(40,10,utf8_decode('Le informamos que hemos registrado la factura'),0,1);
                    $pdf->Cell(40,10,utf8_decode('de su pago de manera correcta.'),0,1);
                    $pdf->Cell(40,10,utf8_decode('Si desea visualizarla por favor ingrese al apartado de Facturas'),0,1);
                    $pdf->Cell(40,10,utf8_decode('por favor le pedimos que continue atento a su correo para más indicaciones.'),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
      		        $pdf->Cell(40,10,utf8_decode(''),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);
                    $pdf->Cell(40,10,utf8_decode(''),0,1);

                    $archivoAdjunto = $pdf->Output("", "S");
                    $correoa=pg_query($conexion,"SELECT c.correo,us.nombres,us.primer_ap,us.segundo_ap FROM correos_usuario c, usuario us WHERE c.id_usuario='$id_usuario' and us.id_usuario ='$id_usuario'");
                    $i=0;
                    while($mostrarCR=pg_fetch_array($correoa)){
                        $cor[$i]=trim($mostrarCR['correo']);
                        $nombre=trim($mostrarCR['nombres']);
                        $apPA=trim($mostrarCR['primer_ap']);
                        $apMa=trim($mostrarCR['segundo_ap']);
                        
                        $i=$i+1;
                    }
                    
                    
                    
                    $mail = new PHPMailer(true);
                    
                    try {
                        //Server settings
                        $mail->SMTPDebug = 0;                      // Enable verbose debug output
                        $mail->isSMTP();                                            // Send using SMTP
                        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                        $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
                        $mail->Password   = 'CongresoMate2020';                               // SMTP password
                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                    
                        //Recipients
                        $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
                        $mail->addAddress("$cor[0]", "$nombre $apPA");
                        if(!empty($cor[1])){
                            $mail->addAddress("$cor[1]", "$nombre $apPA");
                        }
                        if(!empty($cor[2])){
                            $mail->addAddress("$cor[2]", "$nombre $apPA");
                        }
                       // Content
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = 'Factura';
                        $mail->Body    = 'En el siguiente documento se adjuntan los datos de la factura.';
                        $mail->addStringAttachment($archivoAdjunto, 'Factura.pdf');
                        $mail->send();
                        
                        }catch (Exception $e) {
                            echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
                        }
                    
        /* }else{
        printf("errormessage: %s\n".pg_error($conexion));
        }*/


                    //*****************************************************************************************
        				echo"<script>alert('Se registro correctamente.');window.location='facturas-jd.php'</script>";


		    		}else{
		    		echo"<script>alert('Error al subir la insertar ruta.');window.location='facturas-jd.php'</script>";	
		    		}
		    }else{//subir archivo
		    	echo"<script>alert('Error al subir la factura.');window.location='facturas-jd.php'</script>";
		    }

     }else{
     	echo"<script>alert('Solo se aceptan archivos con terminación .pdf, Por favor selecciona el que corresponde.');window.location='facturas-jd.php'</script>";
     }


}//VALIDAR 
?>