<?php
	session_start();
	use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
   	require 'PHPMailer/Exception.php';
    require 'PHPMailer/PHPMailer.php';
    require 'PHPMailer/SMTP.php';
    require ('libreria/fpdf.php');
   
    $usuario=$_SESSION['Usuario'];
     include ('Conexion.php');
	$id_ponencia_taller= $_POST['tituloSelect'];
	
	$profesor=$_POST['profesor'];
	$fecha=$_POST['fecha'];
	$hora=$_POST['hora'];
	//$grupo=$_POST['grupo'];
	$id_sala=$_POST['sala'];
	
	$fechaEntera = strtotime($fecha);
	$anio = date("Y", $fechaEntera);

	//CONSULTA PARA VER QUE NO INGRESE LA MISMA HORA Y FECHA

		 		$consultaFechaHora=pg_query($conexion, "SELECT sp.fecha, sp.hora,sp.id_sala
									FROM salas_ponencias AS sp, ponencias AS p,
									ponencia_taller AS pt
									WHERE sp.id_ponencia=p.id_ponencia 
									AND p.id_ponencia=pt.id_ponencia_taller
									AND sp.id_sala='$id_sala'");
		 		$numfyh=pg_num_rows($consultaFechaHora);

		 		
		 		if($numfyh!=0){
 
		 			while($fechayHoraSala=pg_fetch_assoc($consultaFechaHora)){
		 				
		 				 //echo $fechayHoraSala['fecha'].$fechayHoraSala['hora'];
		 				 
		 				 //echo $fechaEntera." Y ".$hora;
		 				
		 				if(!empty($fechayHoraSala['fecha']) && !empty($fechayHoraSala['hora'])){
							 $fechaAnterior = strtotime($fechayHoraSala['fecha']);
				 			$horaAnterior=strtotime($fechayHoraSala['hora']);
				 			$horaIngresada=strtotime($hora);
				 				if($fechaAnterior==$fechaEntera && $horaAnterior == $horaIngresada){
				 					$bandera="IGUALES";
				 					
				 					
				 				}else{
				 					$bandera="DIFERENTES";
				 				}
				 			}				
		 			}
		 		}
		 		if(!empty($bandera) && $bandera=="IGUALES"){
		 			echo"<script>alert('Error, estas ingresando para la sala fecha y hora ya registrada en otro traller, por favor ingresa una diferente.'); window.location= 'menu.php';</script>";
		 		}else{
	
	//TRAE EL NÚMERO DE LA CONGRESO
	$consulta_num_congreso=pg_query($conexion,"SELECT MAX(numero_congreso) FROM congreso");//numero de congreso el AS es un alias
	$row0=pg_fetch_row($consulta_num_congreso);//eL pg_fetch_row trae los datos de la consulta y los asigna a la variable $row0
	$num_congreso=$row0[0];//el número máximo que encontro del congreso lo asigna a la variable consulta0 y es el máximo por que es el [0]NUMERO MÁXIMO DEL CONGRESO

//TRAE EL ID DE LA CONGRESO CON AYUDA DE SU NUMERO
	$consulta_id_congreso=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$num_congreso'"); //Selecciona los datos con el resultado de la consulta anterior (NUMERO MAXIMO DEL CONGRESO)
	$row1=pg_fetch_row($consulta_id_congreso);//Los datos del ID_CONGRESO Y NOMBRE CONGRESO son asignados a la variable $row
	$id_congresoactual=$row1[0];//es el id congreso

//INSERTAR EN TABLA SALAS_PONENCIAS	
	$insertarSalasPonencias="INSERT INTO salas_ponencias(id_sala, id_ponencia, hora, fecha, anio, id_congreso) VALUES ('$id_sala','$id_ponencia_taller','$hora', '$fecha', '$anio', '$id_congresoactual')";
	$insert=pg_query($conexion,$insertarSalasPonencias);
			if($insert){
				$consultaInfoSala=pg_query($conexion,"SELECT s.nombre_sala, s.cupo, s.referencia, p.titulo FROM salas as s, ponencias as p WHERE S.id_sala='$id_sala' and p.id_ponencia='$id_ponencia_taller'");
				$infoSala=pg_fetch_assoc($consultaInfoSala);
				 //VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
									$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_ponencia_taller' AND id_congreso='$id_congresoactual' AND estatus_actualizacion='Aceptado'");
									$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);	
									if(!empty($actualizacionTitulo)){
										$titulo=$actualizacionTitulo['titulo'];
									}else{
										$titulo=$infoSala['titulo'];
									}
				 //CORREO AUTOR
	                                    //**************************************************************************************
	                                    class PDF extends FPDF{
	                                    // Cabecera de página
	                                    function Header()
	                                    {
	                                        // Logo
	                                        $this->Image('logo.jpg',0,0,220);
	                                        // Arial bold 15
	                                        $this->SetFont('Arial','B',15);
	                                        // Movernos a la derecha
	                                        $this->Cell(80);
	                                        // Título
	                                        $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
	                                        // Salto de línea
	                                        $this->Ln(50);
	                                        
	                                    }
	                                    
	                                    // Pie de página
	                                    function Footer() {
	                                        // Posición: a 1,5 cm del final
	                                        $this->SetY(-15);
	                                        // Arial italic 8
	                                        $this->SetFont('Arial','I',8);
	                                        // Número de página
	                                        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	                                        }
	                                    }
	                                     // Creación del objeto de la clase heredada
	                                    $pdf = new PDF();
	                                    $pdf->AliasNbPages();
	                                    $pdf->AddPage();
	                                    $pdf->SetFont('Times','',12);
	                                    
	                                    $pdf->Cell(40,10,utf8_decode('Información de taller a impartartir'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('La información respecto a el taller que desea impartir es el siguiente'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Clave del taller: '.$id_ponencia_taller),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Nombre del taller: '.$titulo),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Con respecto a la sala donde impartira su taller:'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('El nombre de la sala es: '.$infoSala['nombre_sala']),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('el cupo designado para esta taller es de: '.$infoSala['cupo']),0,1); 
	                                    $pdf->Cell(40,10,utf8_decode('las referencias donde se impartira el taller son las siguientes: '.$infoSala['referencia']),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('la fecha  y hora en que se impartira dentro del congreso son: '.$fecha.$hora),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Le pedimos de la manera más atenta, estar al pendiente a su correo principal ya que nos contactaremos'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('para brindarle mayor información.'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    
	                                    $archivoAdjunto = $pdf->Output("", "S");
	                                    $correoa=pg_query($conexion,"SELECT c.correo,us.nombres,us.primer_ap,us.segundo_ap, us.rfc 
																FROM correos_usuario c, usuario us, usuario_ponencias as up
																WHERE up.id_ponencias='$id_ponencia_taller'
																and up.tipo_autor='Autor'and up.id_usuario = c.id_usuario and
																c.id_usuario= us.id_usuario");
	                                    $i=0;
	                                    while($mostrarCR=pg_fetch_array($correoa)){
	                                        $cor[$i]=trim($mostrarCR['correo']);
	                                        $nombre=trim($mostrarCR['nombres']);
	                                        $apPA=trim($mostrarCR['primer_ap']);
	                                        $apMa=trim($mostrarCR['segundo_ap']);
	                                        $rfcA=trim($mostrarCR['rfc']);
	                                        $i=$i+1;
	                                    }

	                                      $mail = new PHPMailer(true);
	                                    
	                                    try {
	                                        //Server settings
	                                        $mail->SMTPDebug = 0;                      // Enable verbose debug output
	                                        $mail->isSMTP();                                            // Send using SMTP
	                                        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
	                                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
	                                        $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
	                                        $mail->Password   = 'CongresoMate2020';                               // SMTP password
	                                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
	                                        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
	                                        $mail->CharSet  = 'UTF-8';//PARA EL ACENTO
	                                    
	                                        //Recipients
	                                        $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
	                                        $mail->addAddress("$cor[0]", "$nombre $apPA");
	                                        if(!empty($cor[1])){
	                                            $mail->addAddress("$cor[1]", "$nombre $apPA");
	                                        }
	                                        if(!empty($cor[2])){
	                                            $mail->addAddress("$cor[2]", "$nombre $apPA");
	                                        }
	                                       // Content
	                                        $mail->isHTML(true);                                  // Set email format to HTML
	                                        $mail->Subject = 'Información de impartición Taller';
	                                        $mail->Body    = 'En el siguiente documento se adjuntan la información de la sala designada para el taller a presentar.';
	                                        $mail->addStringAttachment($archivoAdjunto, 'Información_Impartición_de_taller.pdf');
	                                        $mail->send();
	                                        
	                                        }catch (Exception $e) {
	                                            echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
	                                        }

	                                    //*****************************************************************************************
	                                        $numCoautoresTrabajo=pg_query($conexion, "SELECT COUNT (*) id_usuario FROM usuario_ponencias 
					                        WHERE id_ponencias ='$id_ponencia_taller' and tipo_autor='Coautor' and id_congreso = '$id_congresoactual'");
					                        $numCoautores =pg_fetch_assoc($numCoautoresTrabajo);
					                        

					                        //VALIDA SI HAY COAUTORES
					                            if($numCoautores['id_usuario']!=0){
					                                  
					//********************************************************************************************************************
					                                class PDF2 extends FPDF
					                        {
					                        // Cabecera de página
					                        function Header()
					                        {
					                            // Logo
					                            $this->Image('logo.jpg',0,0,220);
					                            // Arial bold 15
					                            $this->SetFont('Arial','B',15);
					                            // Movernos a la derecha
					                            $this->Cell(80);
					                            // Título
					                            $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
					                            // Salto de línea
					                            $this->Ln(50);
					                            
					                        }
					                        
					                        // Pie de página
					                        function Footer()
					                        {
					                            // Posición: a 1,5 cm del final
					                            $this->SetY(-15);
					                            // Arial italic 8
					                            $this->SetFont('Arial','I',8);
					                            // Número de página
					                            $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
					                        	}
					                        }
					                        
					                        // Creación del objeto de la clase heredada
					                         $pdf = new PDF();
					                        $pdf->AliasNbPages();
					                        $pdf->AddPage();
					                        $pdf->SetFont('Times','',12);

					                       $pdf->Cell(40,10,utf8_decode('Información de taller a impartartir'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('La información respecto a el taller que desea impartir es el siguiente'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Clave del taller: '.$id_ponencia_taller),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Título del taller: '.$titulo),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Con respecto a la sala donde impartira su taller:'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('El nombre de la sala es: '.$infoSala['nombre_sala'].','),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('el cupo designado para esta taller es de: '.$infoSala['cupo']),0,1); 
	                                    $pdf->Cell(40,10,utf8_decode('las referencias donde se impartira el taller son las siguientes: '.$infoSala['referencia']),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('la fecha  y hora en que se impartira dentro del congreso son: '.$fecha.$hora),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Le pedimos de la manera más atenta, estar al pendiente a su correo principal ya que nos contactaremos'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('para brindarle mayor información.'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);

					                        $archivoAdjunto2 = $pdf->Output("", "S");
					    
					                        //Envio de correo coautores
					                        $mail = new PHPMailer(true);
					                        
					                        try {
					                            //Server settings
					                            $mail->SMTPDebug = 0;                      // Enable verbose debug output
					                            $mail->isSMTP();                                            // Send using SMTP
					                            $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
					                            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
					                            $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
					                            $mail->Password   = 'CongresoMate2020';                               // SMTP password
					                            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
					                            $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
					                            $mail->CharSet  = 'UTF-8';//PARA EL ACENTO
					                        
					                            //Recipients
					                            $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
					                            // obtener nombre,apellidos y correo de coautores del trabajo
					                          

					                            $infoCoau=pg_query($conexion,"select u.nombres,u.primer_ap ,u.segundo_ap,c.correo FROM correos_usuario c, usuario u, usuario_ponencias up WHERE up.id_ponencias='$id_trabajo'and up.id_usuario=u.id_usuario and u.id_usuario= c.id_usuario and up.tipo_autor!='Autor'");
					                            $j=0;
					                            while($infoCoa=pg_fetch_array($infoCoau)){
					                               
					                                $nombreCoa[$j]=trim($infoCoa['nombres']);
					                                $apellidoCoa[$j]=trim($infoCoa['primer_ap']);
					                                $apellidoCoa2[$j]=trim($infoCoa['segundo_ap']);
					                                $corC[$j]=trim($infoCoa['correo']);
					                               
					                            
					                                $j=$j+1;
					                               }
					                               if(!empty($corC[0])){
					                                $mail->addAddress("$corC[0]",utf8_decode("$nombreCoa[0] $apellidoCoa[0] $apellidoCoa2[0]"));
					    
					                               }
					                               if(!empty($corC[1])){
					                                $mail->addAddress("$corC[1]",utf8_decode("$nombreCoa[1] $apellidoCoa[1] $apellidoCoa2[1]"));
					    
					                               }
					                               if(!empty($corC[2])){
					                                $mail->addAddress("$corC[2]",utf8_decode("$nombreCoa[2] $apellidoCoa[2] $apellidoCoa2[2]"));
					    
					                               }
					                               if(!empty($corC[3])){
					                                $mail->addAddress("$corC[3]",utf8_decode("$nombreCoa[3] $apellidoCoa[3] $apellidoCoa2[3]"));
					    
					                               }   
					                        
					                            // Attachments
					                            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
					                            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
					                        
					                            // Content
					                            $mail->isHTML(true);                                  // Set email format to HTML
					                            $mail->Subject = 'Registro de Sala para Taller';
					                            $mail->Body    = 'En el siguiente documento se adjuntan los datos donde se impartira el Taller.';
					                            $mail->addStringAttachment($archivoAdjunto2, 'Registro_sala_taller.pdf');
					                            $mail->send();
					                            
					                        } catch (Exception $e) {
					                            echo "Error al enviar el mensaje para coautores: {$mail->ErrorInfo}";
					                        }
					                    }

					//*********************************************************************************************************************

					                            
				
				echo "<script>alert('Se registro el taller en la sala de manera correcta'); window.location=' menu.php';</script>";
			}else{
				echo "<script>alert('Error en el registro de la sala para el taller.'); window.location='menu.php';</script>";
			}
		}
?>