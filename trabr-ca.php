<?php 
include ("Conexion.php");
session_start();
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
					$row0=pg_fetch_row($consulta);
					$consulta0=$row0[0];
					$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
					$row=pg_fetch_row($consulta1);
					$congresoActual=$row[0];//es el id congreso
$contacto=pg_query($conexion,"SELECT cn.telefono,cn.telefono2,cn.email FROM contacto cn, congreso c
WHERE c.id_congreso='$congresoActual'
and  c.id_contacto =cn.id_contacto");
$contactos=pg_fetch_array($contacto);
$correoC=@$contactos[2];
$numeroC1=@$contactos[0];
$numeroC2=@$contactos[1];
?><!DOCTYPE html>
<html lang="en">
<!-- Basic -->



<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title style="background-image: url('images/logos.jpg');">CISEMATH</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/cismath.png">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" /> 

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
      <?php
    ////Codigo para actualizar el banner
        include ('Conexion.php');
        $nmax=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
        $num=pg_fetch_row($nmax);
        $numcon=$num[0];
        $conact=pg_query($conexion,"SELECT id_congreso, banner FROM congreso WHERE numero_congreso='$numcon'");
        $ban=pg_fetch_row($conact);
        $banner=$ban[1];
        echo '<img src="'.$banner.'" style="width:100%; height:100%; background-size: cover; position: relative; display: flex;"/>';
    ?>

    <header class="top-header" >
        <div class="header_bottom">
          <div class="container">
		  <div class="row"> 
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 col-xl-12">
                <div class="menu_orange_section" style="background: #052450;">
                   <nav class="navbar header-nav navbar-expand-lg"> 
                     <div class="menu_section">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav" >
					    <li><a class="nav-link active" href="indexus.php"><img src="images/i4.png" alt="image"></a> </li>
                        <li><a class="nav-link active" href="acercadeus.php">Acerca de</a></li>
                        <li><a class="nav-link active" href="infus.php">Información</a></li>
                        <li><a class="nav-link active" href="instruccionesus.php">Instrucciones</a></li>
                        <li><a class="nav-link active" href="comitesus.php">Comités</a></li>
						<li><a class="nav-link active" href="memoriasus.php">Memorias</a></li>
						
						<li>
						<a style="position:relative; display:block;">
						<img src="images/user.png" alt="image"> 
						</a>
						</li>
						<li>
						<ul class="nav"><li class="dropdown langmenu" ><a href="#" style="display:block;"class="dropdown-toggle" data-toggle="dropdown" title="Idioma"><b class="caret"></b></a><ul class="dropdown-menu" style="color:black;"><li><a title="English ‎(en)‎" href="menu.php" style="color:black";>Menú‎‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="editarp.php"  style="color:black";>Perfil‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="index.php"  style="color:black";>Cerrar Sesión</a></li></ul></li></ul>
						</li>
						
                    </ul>
                </div>
                     </div>
                 </nav>
                 
                </div>
            </div>
          </div>
		  </div>
        </div>
        
    </header>
    <!-- End header -->
 
 <!-- Start CONTENIDO -->
 <div class="container">
		<div class="row">
			<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			 
				<div class="titulo" style="margin-top:8%">
					<h1 align="center" style="color:#052450;">TRABAJOS REGISTRADOS EN CONGRESOS ANTERIORES</h1>
				</div>
			</div>
		</div>	 
	</div>	
 <div class="container">
		<div class="row" style="display:flex; justify-content:center;  margin-bottom:2%;">
		<div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
			<div class="table-responsive">
				<!------------------------------------------------TABLA DE LOS TIPOS DE TRABAJO ----------------------------------------------------------------------------------->
				<table cellpadding="6px" cellspacing="8px" align="center" style="text-align:left; margin-top:2%;">
					<tr>
						<td colspan="4" align="center" style="color:gray;">
							<strong>Elija la(s) opción(es) que desea consultar:</label></strong>
						</td>
					</tr>
					
					<tr>
						<td style="color:#052450;">
							<label><input type="checkbox" id="boton1"  value="first_checkbox" onchange="javascript:showContent()">Ponencias Orales</label>
						</td>
						<td style="color:#052450;">
							<label><input type="checkbox" id="boton2" value="first_checkbox" onchange="javascript:showContent2()">Carteles</label>
						</td>
						<td style="color:#052450;">
							<label><input type="checkbox" id="boton3" value="first_checkbox" onchange="javascript:showContent3()">Talleres</label>
						</td>
						<td style="color:#052450;">
							<label><input type="checkbox" id="boton4" value="first_checkbox" onchange="javascript:showContent4()">Prototipos</label>
						</td>
					</tr>
					<tr>
						<td>
							<div id="cbox1"  style="display: none;" >
								<label><input type="checkbox" id="RPO" value="first_checkbox" onchange="javascript:showContentRPO()">Resumenes</label>
							</div>
						</td>
						<td>
							<div id="cbox2"  style="display: none;" >
								<label><input type="checkbox" id="RCA" value="second_checkbox" onchange="javascript:showContentRCA()">Resumenes</label>
							</div>
						</td>
						<td>
							<div id="cbox3"  style="display: none;" >
								<label><input type="checkbox" id="RTA" value="third_checkbox" onchange="javascript:showContentRTA()">Resumenes</label>
							</div>
						</td>
						<td>
							<div id="cbox4"  style="display: none;" >
								<label><input type="checkbox"id="RPRO" value="four_checkbox" onchange="javascript:showContentRPRO()">Resumenes</label>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div id="cbox12"  style="display: none;" >
								<label><input type="checkbox" id="TPO" value="first_checkbox" onchange="javascript:showContentTPO()">Trabajos extensos</label>
							</div>
						</td>
						<td>
							<div id="cbox21"  style="display: none;" >
								<label><input type="checkbox" id="ICA" value="second_checkbox" onchange="javascript:showContentICA()">Imágenes</label>
							</div>
						</td>
						<td>
							<div id="cbox31"  style="display: none;" >
								<label><input type="checkbox" id="VTA" value="third_checkbox" onchange="javascript:showContentVTA()">Vídeos</label>
							</div>
						</td>
						<td>
						
						</td>
					</tr>
					<tr>
						<td>
							<div id="cbox13"  style="display: none;" >
								<label><input type="checkbox" id="VPO" value="second_checkbox" onchange="javascript:showContentVPO()">Vídeos</label>
							</div>
						</td>
						<td>
							<div id="cbox22"  style="display: none;" >
								<label><input type="checkbox" id="VCA" value="third_checkbox" onchange="javascript:showContentVCA()">Vídeos</label>
							</div>
						</td>
						<td>
						
						</td>
						<td>
						
						</td>
					</tr>
				</table>
				<!--------------------------------------------------------FIN TABLA DE LOS TIPOS DE TRABAJOS--------------------------------------------------------------------------->
			</div>
		</div>
		<!------------------------SELECT PARA BUSCAR POR AÑO-------------------------------------------------------------->
		<!--<div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
			<table cellpadding="6px" cellspacing="8px" align="center" style="text-align:left; margin-top:2%;">
				<tr>
					<td colspan="2" align="center" style="color:gray;">
							<strong>Ingrese el año:</label></strong>
					</td>
				</tr>
				
				<tr>
					<td align="right">
						<form method="POST">	
							<select  class="form-control" id="aniocon" name="aniocon">
							<option value=""> Año... </option>
							<?php
								include ('Conexion.php');
								//$consultaidc=pg_query($conexion,"SELECT id_congreso FROM congreso");
								//$row1=pg_fetch_row($consultaidc);
								//$id_congreso=$row1[0];//consultamos el id del congreso "CON13"
								//$consultanc=pg_query($conexion,"SELECT nombre_congreso FROM congreso");
								//$row2=pg_fetch_row($consultanc);
								//$nombre_congreso=$row2[0];//consultamos el nombre del congreso
								//$consultaanio=pg_query($conexion,"SELECT anio FROM congreso");
								//$row3=pg_fetch_row($consultaanio);
								//$anio=$row3[0];//consultamos el año del congreso
								$consultaanio="SELECT id_congreso,anio FROM congreso ORDER BY anio DESC";
								$consultaglobal=pg_query($conexion,$consultaanio);
								while ($row=pg_fetch_row($consultaglobal)){
							?>
								<option> <?php echo "$row[1]" ?> - <?php echo "$row[0]"?> </option>
							<?php 
								}
							?>
						</form>
						</select>
					</td>
					<td align="left">
						<a class="nav-link active" href="indexus.php"><img src="images/buscar.png" alt="image"></a> <!--BOTON-->
					<td>
				</tr>
			</table>
		</div>
		<!------------------------FIN DEL SELECT PARA BUSCAR POR AÑO-------------------------------------------------------------->
	</div>


<!-----------------------------------------------------INICIO DE TODAS LAS TABLAS QUE SE MUESTRAN------------------------------------------------------------------------------>
	<div class="container">
		<div class="row" style="display:flex; justify-content:center;  margin-bottom:2%;">
			<div class="table-responsive" id="PO">

			<!-----------------------------------------------------TABLA DE RESUMENES DE PONENCIAS ORALES------------------------------------------------------------------------------>
			<div id="resumenPO" style="display: none;">
				<table cellpadding="8px" border=1 class="table table-bordered" cellspacing="8px" align="center" style="text-align:left; margin-top:2%;">
					<tr>
						<td style="color:#052450;" colspan=8 align=center>
							<strong>RESUMEN - PONENCIAS ORALES</strong>
						</td>
					</tr>
					<tr>
						<td style="color:#052450;">
							<strong>Clave</strong>
						</td>
						<td style="color:#052450;">
							<strong>Categoría</strong>
						</td>
						<td style="color:#052450;">
							<strong>Modalidad</strong>
						</td>
						<td style="color:#052450;">
							<strong>Título</strong>
						</td>
						<td style="color:#052450;">
							<strong>Resumen</strong>
						</td>
						<td style="color:#052450;">
							<strong>Fecha</strong>
						</td>
						<td style="color:#052450;">
							<strong>Autor</strong>
						</td>
						<td style="color:#052450;">
							<strong>Evaluado por</strong>
						</td>
						<td style="color:#052450;">
							<strong>Año</strong>
						</td>
					</tr>
					
					<?php
                        include ("Conexion.php");
                        $consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//consulta del numero actual de congreso
                        $row0=pg_fetch_row($consulta); //$consulta (numero_congreso) es igual a $row0 ya lo hicimos un array
                        $consulta0=$row0[0];//$consulta0 es igual al index 0 de $row0 (es el numero 14...)
						$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso,anio FROM congreso WHERE numero_congreso='$consulta0'"); //estamos usando $consulta0 para comparar y solo traer el numero maximo del congreso (14...)
						//$consulta1 es para traer el id_congreso, nombre_congreso y anio del congreso para hacerlo un array
                        $row=pg_fetch_row($consulta1);//transformamos a $consulta1 en $row el cual se hizo un index: $row[0]=id_congreso, $row[1]nombre_congreso y $row[2]anio
                        $congresoActual=$row[0];//es el id congreso, estamos convirtiendo el indice 0 ($row[0]) en la variable $congresoActual para usar como comparación en la siguiente consulta.
							
							$sql="SELECT 
							p.id_ponencia,
							p.titulo,p.resumen,p.referencias,p.fecha,
							u.nombres, u.primer_ap, u.segundo_ap, 
							ca.nombre_categoria, 
							mo.nombre_modalidad,
							co.id_congreso, co.anio
                            FROM 
							 actualizacion_resumen p,
                             ponencias po,
                             usuario_ponencias up,
                             usuario u,
                             categoria ca,
                             modalidad mo,
							 congreso co
                            WHERE  
                                po.id_tipo_ponencia='PO' 
                                and po.id_ponencia=p.id_ponencia
								and co.id_congreso=up.id_congreso
								and up.id_congreso=po.id_congreso
								and po.id_congreso=p.id_congreso
								and co.id_congreso!='$congresoActual'
                                and up.id_ponencias=p.id_ponencia 
                                and up.tipo_autor='Autor' 
                                and up.id_usuario=u.id_usuario
                                and po.id_categoria=ca.id_categoria
                                and po.id_modalidad=mo.id_modalidad
								and p.estatus_actualizacion='Aceptado' ORDER BY co.anio DESC";
						
						// se consultan los resumenes registrados exclusivamente ponencia oral en el ultimo congreso

						$result=pg_query($conexion, $sql);
							
						while($mostrar=pg_fetch_array($result)){
                            $id_ponencia2=$mostrar['id_ponencia'];
                            $consulta2="SELECT u.nombres, u.primer_ap, u.segundo_ap FROM usuario u, usuario_evalua_ponencias uep WHERE uep.id_ponencias='$id_ponencia2' and uep.id_usuario=u.id_usuario";
                            $consul2=pg_query($conexion,$consulta2);
                            $row2=pg_fetch_row($consul2);
					?>

					<tr>
						<td>
							<?php echo $mostrar['id_ponencia'] ?>
						</td>
						<td>
							<?php echo $mostrar['nombre_categoria']?>
						</td>
						<td>
							<?php echo $mostrar['nombre_modalidad']?>
						</td>
						<td>
							<?php echo $mostrar['titulo']?>
						</td>
						<td>
							<?php echo $mostrar['resumen']?>
						</td>
						<td>
							<?php echo $mostrar['fecha']?>
						</td>
						<td>
							<?php echo $mostrar['nombres']?>
							<?php echo $mostrar['primer_ap']?>
							<?php echo $mostrar['segundo_ap']?> 
						</td>
						<td>
							<?php echo @$row2[0]?>
							<?php echo @$row2[1]?>
							<?php echo @$row2[2]?> 
						</td>
						<td>
							<?php echo $mostrar['anio']?>
						</td>
					</tr>
					<?php
						}
                    ?>
				</table>
			</div>
			<!----------------------------------------------------------------FIN TABLA DE PONENCIAS ORALES RESUMENES------------------------------------------------------------------->
			
			<!-----------------------------------------------------TABLA DE EXTENSOS PONENCIAS ORALES------------------------------------------------------------------------------>
			<div id="trabajoPO" style="display: none;">
			<table cellpadding="8px" border=1 class="table table-bordered" cellspacing="8px" align="center" style="text-align:left; margin-top:2%;">
					<tr>
						<td style="color:#052450;" colspan=8 align=center>
							<strong>TRABAJOS EXTENSOS - PONENCIAS ORALES</strong>
						</td>
					</tr>
					<tr>
						<td style="color:#052450;">
							<strong>Clave</strong>
						</td>
						<td style="color:#052450;">
							<strong>Categoría</strong>
						</td>
						<td style="color:#052450;">
							<strong>Modalidad</strong>
						</td>
						<td style="color:#052450;">
							<strong>Título</strong>
						</td>
						<td style="color:#052450;">
							<strong>Trabajo Extenso</strong>
						</td>
						<td style="color:#052450;">
							<strong>Fecha</strong>
						</td>
						<td style="color:#052450;">
							<strong>Autor</strong>
						</td>
						<td style="color:#052450;">
							<strong>Evaluado por</strong>
						</td>
						<td style="color:#052450;">
							<strong>Año</strong>
						</td>
					</tr>
					<?php
                        include ("Conexion.php");
                        $consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
                        $row0=pg_fetch_row($consulta);
                        $consulta0=$row0[0];
                        $consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
                        $row=pg_fetch_row($consulta1);
                        $congresoActual=$row[0];//es el id congreso

							$sql="SELECT apo.id_ponencia_oral, ap.titulo, c.nombre_categoria, m.nombre_modalidad,apo.extenso, apo.fecha,u.nombres,u.primer_ap,u.segundo_ap, co.id_congreso, co.anio
							FROM
							actualizacion_resumen ap,
							ponencias p,
							categoria c,
							modalidad m,
							actualizacion_p_oral apo,
							usuario u,
							usuario_ponencias up,
							congreso co
							WHERE
							p.id_tipo_ponencia='PO'
							and ap.id_ponencia=p.id_ponencia
							and p.id_ponencia=apo.id_ponencia_oral
							and up.id_ponencias=p.id_ponencia
							and up.id_usuario=u.id_usuario
							and up.tipo_autor='Autor'
							and p.id_categoria=c.id_categoria
							and p.id_modalidad=m.id_modalidad
							and co.id_congreso=up.id_congreso
							and up.id_congreso=p.id_congreso
							and p.id_congreso=ap.id_congreso
							and ap.id_congreso=apo.id_congreso
							and co.id_congreso!='$congresoActual'
							and ap.estatus_actualizacion='Aceptado'
							and apo.estatus_actualizacion='Aceptado' ORDER BY co.anio DESC";
						

						$result =pg_query($conexion, $sql);
							
						while($mostrar=pg_fetch_array($result)){
                            $id_ponencia2=$mostrar['id_ponencia_oral'];
                            $consulta2="SELECT u.nombres, u.primer_ap, u.segundo_ap FROM usuario u, usuario_evalua_ponencias uep WHERE uep.id_ponencias='$id_ponencia2' and uep.id_usuario=u.id_usuario";
                            $consul2=pg_query($conexion,$consulta2);
                            $row2=pg_fetch_row($consul2);
					?>
					<tr>
						<td>
							<?php echo $mostrar['id_ponencia_oral']?>
						</td>
						<td>
							<?php echo $mostrar['nombre_categoria']?>
						</td>
						<td>
							<?php echo $mostrar['nombre_modalidad']?>
						</td>
                        <td>
							<?php echo $mostrar['titulo']?>
						</td>
                        <td>
							<a title="Descargar Archivo" href="<?php echo $mostrar['extenso'] ?>" download="<?php echo $mostrar['extenso'] ?>" style="color: blue; font-size:18px;"> Descargar</span> </a></td>
                        </td>
						<td>
							<?php echo $mostrar['fecha']?>
                        </td>
                        <td>
							<?php echo $mostrar['nombres']?>
							<?php echo $mostrar['primer_ap']?>
							<?php echo $mostrar['segundo_ap']?> 
						</td>
						<td>
							<?php echo @$row2[0]?>
							<?php echo @$row2[1]?>
							<?php echo @$row2[2]?> 
						</td>
						<td>
							<?php echo $mostrar['anio']?>
						</td>
					</tr>
					<?php
						}
                    ?>
				</table>
			</div>
			<!-----------------------------------------------------FIN DE LA TABLA DE EXTENSOS PONENCIAS ORALES------------------------------------------------------------------------------>
			
			<!-----------------------------------------------------TABLA DE VIDEOS PONENCIAS ORALES------------------------------------------------------------------------------>
			<div id="videoPO" style="display: none;">
			<table cellpadding="8px" border=1 class="table table-bordered" cellspacing="8px" align="center" style="text-align:left; margin-top:2%;">
					<tr>
						<td style="color:#052450;" colspan=8 align=center>
							<strong>VIDEO - PONENCIAS ORALES</strong>
						</td>
					</tr>
					<tr>
						<td style="color:#052450;">
							<strong>Clave</strong>
						</td>
						<td style="color:#052450;">
							<strong>Categoría</strong>
						</td>
						<td style="color:#052450;">
							<strong>Modalidad</strong>
						</td>
						<td style="color:#052450;">
							<strong>Título</strong>
						</td>
						<td style="color:#052450;">
							<strong>URL</strong>
						</td>
						<td style="color:#052450;">
							<strong>Fecha</strong>
						</td>
						<td style="color:#052450;">
							<strong>Autor</strong>
						</td>
						<td style="color:#052450;">
							<strong>Evaluado por</strong>
						</td>
						<td style="color:#052450;">
							<strong>Año</strong>
						</td>
					</tr>
					<?php
                        include ("Conexion.php");
                        $consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
                        $row0=pg_fetch_row($consulta);
                        $consulta0=$row0[0];
                        $consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
                        $row=pg_fetch_row($consulta1);
                        $congresoActual=$row[0];//es el id congreso

							$sql="SELECT p.id_ponencia,ca.nombre_categoria, mo.nombre_modalidad,p.titulo,po.url_video,p.referencias,p.fecha, u.nombres, u.primer_ap, u.segundo_ap, co.id_congreso, co.anio
                            FROM actualizacion_resumen p,
                             ponencias po,
                              usuario_ponencias up,
                              usuario u,
                              categoria ca,
                              modalidad mo,
							  congreso co
                                WHERE
                                po.id_tipo_ponencia='PO' 
                                and po.id_ponencia=p.id_ponencia
								and up.id_congreso=co.id_congreso
								and co.id_congreso=po.id_congreso
								and po.id_congreso=p.id_congreso
								and co.id_congreso!='$congresoActual'  
                                and up.id_ponencias=p.id_ponencia 
                                and up.tipo_autor='Autor'
                                and up.id_usuario=u.id_usuario
                                and po.id_categoria=ca.id_categoria
                                and po.id_modalidad=mo.id_modalidad
								and po.estatus_video='Aceptado'
                                and p.estatus_actualizacion='Aceptado' ORDER BY co.anio DESC";
						
						// se consultan los resumenes registrados exclusivamente ponencia oral en el ultimo congreso

						$result =pg_query($conexion, $sql);
							
						while($mostrar=pg_fetch_array($result)){
                            $id_ponencia2=$mostrar['id_ponencia'];
                            $consulta2="SELECT u.nombres, u.primer_ap, u.segundo_ap FROM usuario u, usuario_evalua_ponencias uep WHERE uep.id_ponencias='$id_ponencia2' and uep.id_usuario=u.id_usuario";
                            $consul2=pg_query($conexion,$consulta2);
                            $row2=pg_fetch_row($consul2);
					?>
					<tr>
						<td>
							<?php echo $mostrar['id_ponencia']?>
						</td>
						<td>
							<?php echo $mostrar['nombre_categoria']?>
						</td>
						<td>
							<?php echo $mostrar['nombre_modalidad']?>
						</td>
                        <td>
							<?php echo $mostrar['titulo']?>
						</td>
                        <td>
							<?php echo $mostrar['url_video']?>
                        </td>
                        <td>
							<?php echo $mostrar['fecha']?>
                        </td>
                        <td>
							<?php echo $mostrar['nombres']?>
							<?php echo $mostrar['primer_ap']?>
							<?php echo $mostrar['segundo_ap']?>  
						</td>
						<td>
							<?php echo @$row2[0]?>
							<?php echo @$row2[1]?>
							<?php echo @$row2[2]?> 
						</td>
						<td>
							<?php echo $mostrar['anio']?>
						</td>
					</tr>
					<?php
						}
                    ?>
				</table>
			</div>
			<!-----------------------------------------------------FIN TABLA DE VIDEOS PONENCIAS ORALES------------------------------------------------------------------------------>
			
			<!-----------------------------------------------------TABLA DE RESUMENES CARTELES------------------------------------------------------------------------------>
			<div id="resumenCA" style="display: none;">
			<table cellpadding="8px" border=1 class="table table-bordered" cellspacing="8px" align="center" style="text-align:left; margin-top:2%;">
					<tr>
						<td style="color:#052450;" colspan=8 align=center>
							<strong>RESUMEN - CARTELES</strong>
						</td>
					</tr>
					<tr>
						<td style="color:#052450;">
							<strong>Clave</strong>
						</td>
						<td style="color:#052450;">
							<strong>Categoría</strong>
						</td>
						<td style="color:#052450;">
							<strong>Modalidad</strong>
						</td>
						<td style="color:#052450;">
							<strong>Título</strong>
						</td>
						<td style="color:#052450;">
							<strong>Resumen</strong>
						</td>
						<td style="color:#052450;">
							<strong>Fecha</strong>
						</td>
						<td style="color:#052450;">
							<strong>Autor</strong>
						</td>
						<td style="color:#052450;">
							<strong>Evaluado por</strong>
						</td>
						<td style="color:#052450;">
							<strong>Año</strong>
						</td>
					</tr>
					<?php
                        include ("Conexion.php");
                        $consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
                        $row0=pg_fetch_row($consulta);
                        $consulta0=$row0[0];
                        $consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
                        $row=pg_fetch_row($consulta1);
                        $congresoActual=$row[0];//es el id congreso

							$sql="SELECT p.id_ponencia,ca.nombre_categoria, mo.nombre_modalidad,p.titulo,p.resumen,p.referencias,p.fecha, u.nombres, u.primer_ap, u.segundo_ap, co.id_congreso, co.anio
                            FROM actualizacion_resumen p,
                             ponencias po,
                              usuario_ponencias up,
                              usuario u,
                              categoria ca,
                              modalidad mo,
							  congreso co
                                 WHERE  
                                po.id_tipo_ponencia='CA' 
                                and po.id_ponencia=p.id_ponencia
								and co.id_congreso=p.id_congreso
								and co.id_congreso!='$congresoActual' 
                                and up.id_ponencias=p.id_ponencia 
                                and up.tipo_autor='Autor' 
                                and up.id_usuario=u.id_usuario
                                and po.id_categoria=ca.id_categoria
                                and po.id_modalidad=mo.id_modalidad
                                and p.estatus_actualizacion='Aceptado' ORDER BY co.anio DESC";
						
						// se consultan los resumenes registrados exclusivamente ponencia oral en el ultimo congreso

						$result =pg_query($conexion, $sql);
							
						while($mostrar=pg_fetch_array($result)){
                            $id_ponencia2=$mostrar['id_ponencia'];
                            $consulta2="SELECT u.nombres, u.primer_ap, u.segundo_ap FROM usuario u, usuario_evalua_ponencias uep WHERE uep.id_ponencias='$id_ponencia2' and uep.id_usuario=u.id_usuario";
                            $consul2=pg_query($conexion,$consulta2);
                            $row2=pg_fetch_row($consul2);
					?>
					<tr>
						<td>
							<?php echo $mostrar['id_ponencia']?>
						</td>
						<td>
							<?php echo $mostrar['nombre_categoria']?>
						</td>
						<td>
							<?php echo $mostrar['nombre_modalidad']?>
						</td>
                        <td>
							<?php echo $mostrar['titulo']?>
						</td>
                        <td>
							<?php echo $mostrar['resumen']?>
                        </td>
                        <td>
							<?php echo $mostrar['fecha']?>
                        </td>
                        <td>
							<?php echo $mostrar['nombres']?>
							<?php echo $mostrar['primer_ap']?>
							<?php echo $mostrar['segundo_ap']?>  
						</td>
						<td>
							<?php echo @$row2[0]?>
							<?php echo @$row2[1]?>
							<?php echo @$row2[2]?> 
						</td>
						<td>
							<?php echo $mostrar['anio']?>
						</td>
					</tr>
					<?php
						}
                    ?>
				</table>
 			</div>
			<!-----------------------------------------------------FIN DE LA TABLA DE RESUMENES CARTELES------------------------------------------------------------------------------>

			<!-----------------------------------------------------TABLA DE IMAGENES DE CARTELES------------------------------------------------------------------------------>
			<div id="trabajoCA" style="display: none;">
			<table cellpadding="8px" border=1 class="table table-bordered" cellspacing="8px" align="center" style="text-align:left; margin-top:2%;">
					<tr>
						<td style="color:#052450;" colspan=10 align=center>
							<strong>IMAGEN - CARTELES</strong>
						</td>
					</tr>
					<tr>
						<td style="color:#052450;">
							<strong>Clave</strong>
						</td>
						<td style="color:#052450;">
							<strong>Categoría</strong>
						</td>
						<td style="color:#052450;">
							<strong>Modalidad</strong>
						</td>
						<td style="color:#052450;">
							<strong>Título</strong>
						</td>
						<td style="color:#052450;">
							<strong>Imagen</strong>
						</td>
						<td style="color:#052450;">
							<strong>Calificación</strong>
						</td>
						<td style="color:#052450;">
							<strong>Rúbrica</strong>
						</td>
						<td style="color:#052450;">
							<strong>Fecha</strong>
						</td>
						<td style="color:#052450;">
							<strong>Autor</strong>
						</td>
						<td style="color:#052450;">
							<strong>Evaluado por</strong>
						</td>
						<td style="color:#052450;">
							<strong>Año</strong>
						</td>
					</tr>
					<?php
                        include ("Conexion.php");
                        $consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
                        $row0=pg_fetch_row($consulta);
                        $consulta0=$row0[0];
                        $consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
                        $row=pg_fetch_row($consulta1);
                        $congresoActual=$row[0];//es el id congreso
						
						$sql="SELECT 
						apo.id_ponencia_cartel, apo.fecha, apo.imagen,
						ap.titulo,
						c.nombre_categoria,
						m.nombre_modalidad,
						u.id_usuario, u.nombres, u.primer_ap, u.segundo_ap,
						co.id_congreso, co.anio

						FROM
							actualizacion_resumen ap, 
							ponencias p, 
							categoria c,
							modalidad m,
							actualizacion_cartel apo, 
							usuario u,
							usuario_ponencias up,
							congreso co
						WHERE
							ap.id_ponencia=p.id_ponencia
						and p.id_ponencia= apo.id_ponencia_cartel
						and up.id_ponencias=p.id_ponencia
						and up.id_usuario=u.id_usuario
						and up.tipo_autor='Autor'
						and p.id_categoria=c.id_categoria
						and p.id_modalidad=m.id_modalidad
						and co.id_congreso=up.id_congreso
						and up.id_congreso=p.id_congreso
						and p.id_congreso=ap.id_congreso
						and ap.id_congreso=apo.id_congreso
						and co.id_congreso!='$congresoActual'
						and apo.estatus_actualizacion='Aceptado'
						and ap.estatus_actualizacion='Aceptado'
						ORDER BY co.anio DESC";

						$result =pg_query($conexion, $sql);
							
						while($mostrar=pg_fetch_array($result)){
                            $id_ponencia2=$mostrar['id_ponencia_cartel'];
                            $consulta2="SELECT u.nombres, u.primer_ap, u.segundo_ap FROM usuario u, usuario_evalua_ponencias uep WHERE uep.id_ponencias='$id_ponencia2' and uep.id_usuario=u.id_usuario";
                            $consul2=pg_query($conexion,$consulta2);
							$row2=pg_fetch_row($consul2);

							$idc_congreso=$mostrar['id_congreso'];
							$consulta3="SELECT calificacion, rubrica FROM usuario_evalua_cartel WHERE id_congreso='$idc_congreso' and id_ponencia_cartel='$id_ponencia2'";
							$consul3=pg_query($conexion,$consulta3);
							$row3=pg_fetch_row($consul2);
					?>
					<tr>
						<td>
							<?php echo $mostrar['id_ponencia_cartel']?>
						</td>
						<td>
							<?php echo $mostrar['nombre_categoria']?>
						</td>
						<td>
							<?php echo $mostrar['nombre_modalidad']?>
						</td>
                        <td>
							<?php echo $mostrar['titulo']?>
						</td>
                        <td>
							<a title="Descargar Archivo" href="<?php echo $mostrar['imagen'] ?>" download="<?php echo $mostrar['imagen'] ?>" style="color: blue; font-size:18px;"> Imagen</span> </a></td>
                        </td>
                        <td>
							<?php echo @$row3[0]?>
						</td>
                        <td>
							<a title="Descargar Archivo" href="<?php echo @$row3[1]?>" download="<?php echo @$row3[1]?>" style="color: blue; font-size:18px;"> Rubrica</span> </a></td>
						</td>
						<td>
							<?php echo $mostrar['fecha']?>
                        </td>
                        <td>
							<?php echo $mostrar['nombres']?>
							<?php echo $mostrar['primer_ap']?>
							<?php echo $mostrar['segundo_ap']?> 
						</td>
						<td>
							<?php echo @$row2[0]?>
							<?php echo @$row2[1]?>
							<?php echo @$row2[2]?>  
						</td>
						<td>
							<?php echo $mostrar['anio']?>
						</td>
					</tr>
					<?php
						}
                    ?>
				</table>
 			</div>
			<!-----------------------------------------------------FIN DE LA TABLA DE IMAGENES DE CARTELES------------------------------------------------------------------------------>

			<!-----------------------------------------------------TABLA DE VIDEOS DE CARTELES------------------------------------------------------------------------------>
			<div id="videoCA" style="display: none;">
			<table cellpadding="8px" border=1 class="table table-bordered" cellspacing="8px" align="center" style="text-align:left; margin-top:2%;">
					<tr>
						<td style="color:#052450;" colspan=8 align=center>
							<strong>VIDEO - CARTELES</strong>
						</td>
					</tr>
					<tr>
						<td style="color:#052450;">
							<strong>Clave</strong>
						</td>
						<td style="color:#052450;">
							<strong>Categoría</strong>
						</td>
						<td style="color:#052450;">
							<strong>Modalidad</strong>
						</td>
						<td style="color:#052450;">
							<strong>Título</strong>
						</td>
						<td style="color:#052450;">
							<strong>URL</strong>
						</td>
						<td style="color:#052450;">
							<strong>Fecha</strong>
						</td>
						<td style="color:#052450;">
							<strong>Autor</strong>
						</td>
						<td style="color:#052450;">
							<strong>Evaluado por</strong>
						</td>
						<td style="color:#052450;">
							<strong>Año</strong>
						</td>
					</tr>
					<?php
                        include ("Conexion.php");
                        $consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
                        $row0=pg_fetch_row($consulta);
                        $consulta0=$row0[0];
                        $consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
                        $row=pg_fetch_row($consulta1);
                        $congresoActual=$row[0];//es el id congreso

							$sql="SELECT p.id_ponencia,ca.nombre_categoria, mo.nombre_modalidad,p.titulo,po.url_video,p.referencias,p.fecha, u.nombres, u.primer_ap, u.segundo_ap, co.id_congreso, co.anio
                            FROM actualizacion_resumen p,
                             ponencias po,
                              usuario_ponencias up,
                              usuario u,
                              categoria ca,
                              modalidad mo,
							  congreso co
                                WHERE
                                po.id_tipo_ponencia='CA' 
                                and po.id_ponencia=p.id_ponencia
								and co.id_congreso=p.id_congreso 
								and co.id_congreso!='$congresoActual'
                                and up.id_ponencias=p.id_ponencia
                                and up.tipo_autor='Autor' 
                                and up.id_usuario=u.id_usuario
                                and po.id_categoria=ca.id_categoria
                                and po.id_modalidad=mo.id_modalidad
								and po.estatus_video='Aceptado'
                                and p.estatus_actualizacion='Aceptado' ORDER BY co.anio DESC";
						
						// se consultan los resumenes registrados exclusivamente ponencia oral en el ultimo congreso

						$result =pg_query($conexion, $sql);
							
						while($mostrar=pg_fetch_array($result)){
                            $id_ponencia2=$mostrar['id_ponencia'];
                            $consulta2="SELECT u.nombres, u.primer_ap, u.segundo_ap FROM usuario u, usuario_evalua_ponencias uep WHERE uep.id_ponencias='$id_ponencia2' and uep.id_usuario=u.id_usuario";
                            $consul2=pg_query($conexion,$consulta2);
                            $row2=pg_fetch_row($consul2);
					?>
					<tr>
						<td>
							<?php echo $mostrar['id_ponencia']?>
						</td>
						<td>
							<?php echo $mostrar['nombre_categoria']?>
						</td>
						<td>
							<?php echo $mostrar['nombre_modalidad']?>
						</td>
                        <td>
							<?php echo $mostrar['titulo']?>
						</td>
                        <td>
							<?php echo $mostrar['url_video']?>
                        </td>
                        <td>
							<?php echo $mostrar['fecha']?>
                        </td>
                        <td>
							<?php echo $mostrar['nombres']?>
							<?php echo $mostrar['primer_ap']?>
							<?php echo $mostrar['segundo_ap']?> 
						</td>
						<td>
							<?php echo @$row2[0]?>
							<?php echo @$row2[1]?>
							<?php echo @$row2[2]?> 
						</td>
						<td>
							<?php echo $mostrar['anio']?>
						</td>
					</tr>
					<?php
						}
                    ?>
				</table>
			</div>
			<!-----------------------------------------------------FIN DE LA TABLA DE VIDEOS DE CARTELES------------------------------------------------------------------------------>

			<!-----------------------------------------------------TABLA DE RESUMENES DE TALLERES------------------------------------------------------------------------------>			
			<div id="resumenTA" style="display: none;">
			<table cellpadding="8px" border=1 class="table table-bordered" cellspacing="8px" align="center" style="text-align:left; margin-top:2%;">
					<tr>
						<td style="color:#052450;" colspan=8 align=center>
							<strong>RESUMEN - TALLERES</strong>
						</td>
					</tr>
					<tr>
						<td style="color:#052450;">
							<strong>Clave</strong>
						</td>
						<td style="color:#052450;">
							<strong>Título</strong>
						</td>
						<td style="color:#052450;">
							<strong>Resumen</strong>
						</td>
						<td style="color:#052450;">
							<strong>Fecha</strong>
						</td>
						<td style="color:#052450;">
							<strong>Autor</strong>
						</td>
						<td style="color:#052450;">
							<strong>Evaluado por</strong>
						</td>
						<td style="color:#052450;">
							<strong>Año</strong>
						</td>
					</tr>
					<?php
                        include ("Conexion.php");
                        $consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
                        $row0=pg_fetch_row($consulta);
                        $consulta0=$row0[0];
                        $consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
                        $row=pg_fetch_row($consulta1);
                        $congresoActual=$row[0];//es el id congreso

							$sql="SELECT p.id_ponencia,p.titulo,p.resumen,p.referencias,p.fecha, u.nombres, u.primer_ap, u.segundo_ap, co.id_congreso, co.anio
                            FROM actualizacion_resumen p,
                             ponencias po,
                              usuario_ponencias up,
                              usuario u,
							  congreso co
                              WHERE  
                                po.id_tipo_ponencia='TA' 
                                and po.id_ponencia=p.id_ponencia 
								and co.id_congreso!='$congresoActual'
                                and po.id_congreso=p.id_congreso
								and co.id_congreso=p.id_congreso
                                and up.id_ponencias=p.id_ponencia 
                                and up.tipo_autor='Autor' 
                                and up.id_usuario=u.id_usuario
                                and p.estatus_actualizacion='Aceptado' ORDER BY co.anio DESC";
						
						// se consultan los resumenes registrados exclusivamente ponencia oral en el ultimo congreso

						$result =pg_query($conexion, $sql);
							
						while($mostrar=pg_fetch_array($result)){
                            $id_ponencia2=$mostrar['id_ponencia'];
                            $consulta2="SELECT u.nombres, u.primer_ap, u.segundo_ap FROM usuario u, usuario_evalua_ponencias uep WHERE uep.id_ponencias='$id_ponencia2' and uep.id_usuario=u.id_usuario";
                            $consul2=pg_query($conexion,$consulta2);
                            $row2=pg_fetch_row($consul2);
					?>
					<tr>
						<td>
							<?php echo $mostrar['id_ponencia']?>
						</td>
                        <td>
							<?php echo $mostrar['titulo']?>
						</td>
                        <td>
							<?php echo $mostrar['resumen']?>
                        </td>
                        <td>
							<?php echo $mostrar['fecha']?>
                        </td>
                        <td>
							<?php echo $mostrar['nombres']?>
							<?php echo $mostrar['primer_ap']?>
							<?php echo $mostrar['segundo_ap']?>  
						</td>
						<td>
							<?php echo @$row2[0]?>
							<?php echo @$row2[1]?>
							<?php echo @$row2[2]?> 
						</td>
						<td>
							<?php echo $mostrar['anio']?>
						</td>
					</tr>
					<?php
						}
                    ?>
				</table>
			</div>
			<!-----------------------------------------------------FIN DE LA TABLA DE RESUMENES DE TALLERES------------------------------------------------------------------------------>

			<!-----------------------------------------------------TABLA DE VIDEOS DE TALLERES------------------------------------------------------------------------------>
			<div id="videoTA" style="display: none;">
    		<table cellpadding="8px" border=1 class="table table-bordered" cellspacing="8px" align="center" style="text-align:left; margin-top:2%;">
					<tr>
						<td style="color:#052450;" colspan=8 align=center>
							<strong>VIDEO - TALLERES</strong>
						</td>
					</tr>
					<tr>
						<td style="color:#052450;">
							<strong>Clave</strong>
						</td>
						<td style="color:#052450;">
							<strong>Título</strong>
						</td>
						<td style="color:#052450;">
							<strong>URL</strong>
						</td>
						<td style="color:#052450;">
							<strong>Fecha</strong>
						</td>
						<td style="color:#052450;">
							<strong>Autor</strong>
						</td>
						<td style="color:#052450;">
							<strong>Evaluado por</strong>
						</td>
						<td style="color:#052450;">
							<strong>Año</strong>
						</td>
					</tr>
					<?php
                        include ("Conexion.php");
                        $consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
                        $row0=pg_fetch_row($consulta);
                        $consulta0=$row0[0];
                        $consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
                        $row=pg_fetch_row($consulta1);
                        $congresoActual=$row[0];//es el id congreso

							$sql="SELECT p.id_ponencia,p.titulo,po.url_video,p.referencias,p.fecha, u.nombres, u.primer_ap, u.segundo_ap, co.id_congreso, co.anio
                            FROM actualizacion_resumen p,
                             ponencias po,
                              usuario_ponencias up,
                              usuario u,
							  congreso co
                                WHERE  
                                po.id_tipo_ponencia='TA'
                                and po.id_ponencia=p.id_ponencia
								and co.id_congreso=po.id_congreso
								and po.id_congreso=up.id_congreso
								and up.id_congreso=p.id_congreso
								and co.id_congreso!='$congresoActual'
                                and up.id_ponencias=p.id_ponencia 
                                and up.tipo_autor='Autor'
                                and up.id_usuario=u.id_usuario
								and po.estatus_video='Aceptado'
                                and p.estatus_actualizacion='Aceptado' ORDER BY co.anio DESC";
						// la consulta funciona solo si en el registro de la tabla ponencias agrego una categoria y modalidad, si estan vacios esos campos no funciona
						// se consultan los resumenes registrados exclusivamente ponencia oral en el ultimo congreso

						$result =pg_query($conexion, $sql);
							
						while($mostrar=pg_fetch_array($result)){
                            $id_ponencia2=$mostrar['id_ponencia'];
                            $consulta2="SELECT u.nombres, u.primer_ap, u.segundo_ap FROM usuario u, usuario_evalua_ponencias uep WHERE uep.id_ponencias='$id_ponencia2' and uep.id_usuario=u.id_usuario";
                            $consul2=pg_query($conexion,$consulta2);
                            $row2=pg_fetch_row($consul2);
					?>
					<tr>
						<td>
							<?php echo $mostrar['id_ponencia']?>
						</td>
                        <td>
							<?php echo $mostrar['titulo']?>
						</td>
                        <td>
							<?php echo $mostrar['url_video']?>
                        </td>
                        <td>
							<?php echo $mostrar['fecha']?>
                        </td>
                        <td>
							<?php echo $mostrar['nombres']?>
							<?php echo $mostrar['primer_ap']?>
							<?php echo $mostrar['segundo_ap']?>  
						</td>
						<td>
							<?php echo @$row2[0]?>
							<?php echo @$row2[1]?>
							<?php echo @$row2[2]?>  
						</td>
						<td>
							<?php echo $mostrar['anio']?>
						</td>
					</tr>
					<?php
						}
                    ?>	
				</table>
 			</div>
			<!-----------------------------------------------------FIN DE LA TABLA DE VIDEOS DE TALLERES------------------------------------------------------------------------------>
			
			<!-----------------------------------------------------TABLA DE RESUMENES DE PROTOTIPOS------------------------------------------------------------------------------>
			<div id="resumenPRO" style="display: none;">
			<table cellpadding="8px" border=1 class="table table-bordered" cellspacing="8px" align="center" style="text-align:left; margin-top:2%;">
					<tr>
						<td style="color:#052450;" colspan=8 align=center>
							<strong>RESUMEN - PROTOTIPOS</strong>
						</td>
					</tr>
					<tr>
						<td style="color:#052450;">
							<strong>Clave</strong>
						</td>
						<td style="color:#052450;">
							<strong>Título</strong>
						</td>
						<td style="color:#052450;">
							<strong>Resumen</strong>
						</td>
						<td style="color:#052450;">
							<strong>Fecha</strong>
						</td>
						<td style="color:#052450;">
							<strong>Autor</strong>
						</td>
						<td style="color:#052450;">
							<strong>Evaluado por</strong>
						</td>
						<td style="color:#052450;">
							<strong>Año</strong>
						</td>
					</tr>
					<?php
                        include ("Conexion.php");
                        $consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
                        $row0=pg_fetch_row($consulta);
                        $consulta0=$row0[0];
                        $consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
                        $row=pg_fetch_row($consulta1);
                        $congresoActual=$row[0];//es el id congreso

							$sql="SELECT 
							rpro.id_prototipo, rpro.titulo, rpro.resumen, rpro.fecha, 
							u.nombres, u.primer_ap, u.segundo_ap, co.id_congreso, co.anio
							FROM prototipos p,
							actualizacion_resumen_pro rpro,
							usuario u,
							prototipos_usuario pu,
							congreso co
							WHERE p.id_prototipo=rpro.id_prototipo
							and pu.id_prototipo=rpro.id_prototipo
							and pu.id_usuario = u.id_usuario 
							and pu.tipo_autor='Autor'
							and p.id_congreso=co.id_congreso
							and rpro.id_congreso = co.id_congreso
							and rpro.estatus_r_p='Aceptado'
							and co.id_congreso!='$congresoActual' ORDER BY co.anio DESC";
						
						// se consultan los resumenes registrados exclusivamente ponencia oral en el ultimo congreso

						$result =pg_query($conexion, $sql);
							
						while($mostrar=pg_fetch_array($result)){
                            $id_ponencia2=$mostrar['id_prototipo'];
                            $consulta2="SELECT u.nombres, u.primer_ap, u.segundo_ap FROM usuario u, usuario_evalua_ponencias uep WHERE uep.id_ponencias='$id_ponencia2' and uep.id_usuario=u.id_usuario";
                            $consul2=pg_query($conexion,$consulta2);
                            $row2=pg_fetch_row($consul2);
					?>
					<tr>
						<td>
							<?php echo $mostrar['id_prototipo']?>
						</td>
                        <td>
							<?php echo $mostrar['titulo']?>
						</td>
                        <td>
							<?php echo $mostrar['resumen']?>
                        </td>
                        <td>
							<?php echo $mostrar['fecha']?>
                        </td>
                        <td>
							<?php echo $mostrar['nombres']?>
							<?php echo $mostrar['primer_ap']?>
							<?php echo $mostrar['segundo_ap']?> 
						</td>
						<td>
							<?php echo @$row2[0]?>
							<?php echo @$row2[1]?>
							<?php echo @$row2[2]?> 
						</td>
						<td>
							<?php echo $mostrar['anio']?>
						</td>
					</tr>
					<?php
						}
                    ?>
				</table>
			</div>
			<!-----------------------------------------------------FIN DE LA TABLA DE RESUMENES DE PROTOTIPOS------------------------------------------------------------------------------>
		</div>
	</div>
</div>
<!-------------------------------------AQUI TERMINAN LAS TABLAS-------------------------------------------->

			<div class="btn" style="text-align:right; width:100%;  margin-bottom:5%;">
			<button type="B1" onclick="location.href='congresosp.php'" style="margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;">Regresar</button>
		</div>
	</div>
    <!-- End CONTENIDO -->
    
  <!-- Start Footer -->
        <footer class="footer-box">
        <div class="container">
            <div class="row">
               <div class="col-md-12 white_fonts" style="display: contents;">
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            
                            <div class="full">
							<h3>Dirección</h3>
                                <ul class="menu_footer">   
								     
								     <li><img src="images/i5.png"><span style="font-size: 85%;">Carr, Cuautitlán - Teoloyucan, San Sebastian Xhala, 54714 Cuautitlán Izcalli</span></li>

                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="full">
                                <div class="footer_blog full white_fonts">
                             <h3>Contacto</h3>
                             <ul class="full">
                               <li><img src="images/i6.png"><span style="font-size: 85%;"> <?php echo $correoC  ?></span></li>
                               <li><img src="images/i7.png"><span style="font-size: 85%;"><?php echo "$numeroC1    $numeroC2"; ?></span></li>
                             </ul>
                         </div>
                            </div>
                        </div>
                </div>
			 </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link active" href="derechosa.php" style="color:white">© Sitio desarrollado por CDD Solutions S.A. y administrado por el Departamento de Matemáticas. Hecho en México, (2020 - 2021)</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
	/* counter js */

(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
	</script>

<script type="text/javascript">
// Se despliega el principal menu de ponencias (Orales,carteles, talleres, prototipos)
    function showContent() {
        //element = document.getElementById("content");
		element2 = document.getElementById("cbox1");
		element3 = document.getElementById("cbox12");
		element4 = document.getElementById("cbox13");
        check = document.getElementById("boton1");
        if (check.checked) {
          //  element.style.display='block';
			element2.style.display='block';
			element3.style.display='block';
			element4.style.display='block';
        }
        else {
            //element.style.display='none';
			element2.style.display='none';
			element3.style.display='none';
			element4.style.display='none';
        }
    }

	function showContent2() {
        element = document.getElementById("cbox21");
		element2 = document.getElementById("cbox2");
		element3 = document.getElementById("cbox22");
		element4 = document.getElementById("cbox23");
        check = document.getElementById("boton2");
        if (check.checked) {
            element.style.display='block';
			element2.style.display='block';
			element3.style.display='block';
			element4.style.display='block';
        }
        else {
            element.style.display='none';
			element2.style.display='none';
			element3.style.display='none';
			element4.style.display='none';
        }
    }

	function showContent3() {
		element = document.getElementById("cbox3");
		element2 = document.getElementById("cbox31");
        check = document.getElementById("boton3");
        if (check.checked) {
			element.style.display='block';
			element2.style.display='block';
        }
        else {
            element.style.display='none';
			element2.style.display='none';
        }
    }

	function showContent4() {
        element = document.getElementById("cbox4");
        check = document.getElementById("boton4");
        if (check.checked) {
            element.style.display='block';
        }
        else {
            element.style.display='none';
        }
    }

	////////////Despliegue de la tabla de resumenes dependiendo el trabajo seleccionado //////
	function showContentRPO() {
        element = document.getElementById("resumenPO");
        check = document.getElementById("RPO");
        if (check.checked) {
        	element.style.display='block';	
        }
        else {
        	element.style.display='none';
		}
    }

	function showContentRCA() {
        element = document.getElementById("resumenCA");
		 check = document.getElementById("RCA");
        if (check.checked) {
           element.style.display='block';
        }
        else {
            element.style.display='none';	
        }
    }

	function showContentRTA() {
        element = document.getElementById("resumenTA");
		check = document.getElementById("RTA");
        if (check.checked) {
            element.style.display='block';
		}
        else {
            element.style.display='none';	
        }
    }

	function showContentRPRO() {
        element = document.getElementById("resumenPRO");
		check = document.getElementById("RPRO");
        if (check.checked) {
            element.style.display='block';
        }else {
            element.style.display='none';
		}
    }

	////////////Despliegue de la tabla de trabajos inscritos dependiendo el trabajo seleccionado/////////
	function showContentTPO() {
        element = document.getElementById("trabajoPO");
        check = document.getElementById("TPO");
        if (check.checked) {
        	element.style.display='block';	
        }
        else {
        	element.style.display='none';
		}
    }

	function showContentICA() {
        element = document.getElementById("trabajoCA");
		check = document.getElementById("ICA");
        if (check.checked) {
            element.style.display='block';
        }
        else {
            element.style.display='none';
        }
    }

	//////////// Despliegue de la tabla para ver videos dependiendo el trabajo seleccionado//////
	function showContentVPO() {
        element = document.getElementById("videoPO");
        check = document.getElementById("VPO");
        if (check.checked) {
        	element.style.display='block';
        }
        else {
        	element.style.display='none';
		}
    }

	function showContentVCA() {
        element = document.getElementById("videoCA");
		 check = document.getElementById("VCA");
        if (check.checked) {
            element.style.display='block';
        }
        else {
            element.style.display='none';
        }
    }

	function showContentVTA() {
        element = document.getElementById("videoTA");
		check = document.getElementById("VTA");
        if (check.checked) {
            element.style.display='block';
		}
        else {
            element.style.display='none';
        }
    }

</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>

</body>

</html>