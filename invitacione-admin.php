<?php
include ("Conexion.php");
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php'; 
require ('libreria/fpdf.php');
session_start();
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
$_SESSION['Usuario'];

if(isset($_POST['submit1'])){
	$id_usuario=$_POST['usuario'];
	$id_rol=$_POST['rol'];
	$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
	$row5=pg_fetch_row($consulta);
	$consulta0=$row5[0];
	$consulta1=pg_query($conexion,"SELECT nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
	$row6=pg_fetch_row($consulta1);
	$nombreCongreso=$row6[0];// se obtiene en id del congreso actual
	$datosrol=pg_query($conexion,"SELECT descripcion_rol FROM roles WHERE id_rol='$id_rol'");
	$rowRoles=pg_fetch_row($datosrol);
	$nombreRol=trim($rowRoles[0]);

	
// Creacion de PDF Autor
class PDF extends FPDF
{
// Cabecera de página
function Header()
{
	// Logo
	$this->Image('logo.jpg',0,0,220);
	// Arial bold 15
	$this->SetFont('Arial','B',15);
	// Movernos a la derecha
	$this->Cell(80);
	// Título
	$this->Cell(50,80,utf8_decode('Congreso de Matemáticas'),30,0,'C');
	// Salto de línea
	$this->Ln(50);
	
}

// Pie de página
function Footer()
{
	// Posición: a 1,5 cm del final
	$this->SetY(-15);
	// Arial italic 8
	$this->SetFont('Arial','I',8);
	// Número de página
	$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);

$pdf->Cell(40,10,utf8_decode('Invitación de participación en congreso'),0,1);
$pdf->Cell(40,10,utf8_decode(''),0,1);
$pdf->Cell(40,10,utf8_decode('Estimado usuario:'),0,1);
$pdf->Cell(40,10,utf8_decode(''),0,1);
$pdf->Cell(40,10,utf8_decode('A través de este medio, el '.$nombreCongreso.','),0,1);
$pdf->Cell(40,10,utf8_decode('le hace la cordial invitación para formar parte del comité como' ),0,1);
$pdf->Cell(40,10,utf8_decode($nombreRol) ,0,1);
$pdf->Cell(40,10,utf8_decode('De ser afirmativa su respuesta, le solicitamos responda a este'),0,1);
$pdf->Cell(40,10,utf8_decode('correo a la brevedad posible, confirmando su participación. '),0,1);
$pdf->Cell(40,10,utf8_decode(''),0,1);
$pdf->Cell(40,10,utf8_decode('Atentamente'),0,1);
$pdf->Cell(40,10,utf8_decode('Departamento de Matemáticas.'),0,1);
$archivoAdjunto = $pdf->Output("", "S");

//Envio de correo Autor
$mail = new PHPMailer(true);

try {
	//Server settings
	$mail->SMTPDebug = 0;                      // Enable verbose debug output
	$mail->isSMTP();                                            // Send using SMTP
	$mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
	$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
	$mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
	$mail->Password   = 'CongresoMate2020';                               // SMTP password
	$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
	$mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
  
	
	//Recipients
	$mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso de Matemáticas');
	$correosU=pg_query($conexion,"SELECT c.correo, u.nombres,u.primer_ap,u.segundo_ap FROM correos_usuario c, usuario u 
	WHERE c.id_usuario='$id_usuario'
	and c.id_usuario=u.id_usuario");
	
	while($correos=pg_fetch_array($correosU)){
		$n=trim($correos['nombres']);
		$p=trim($correos['primer_ap']);
		$a=trim($correos['segundo_ap']);
		$nombre="$n $p $a";
		$mail->addAddress("$correos[correo]", "$nombre");
		if(!empty($cor[1])){
			$mail->addAddress("$correos[correo]", "$nombre");
		}
		if(!empty($cor[2])){
			$mail->addAddress("$correos[correo]", "$nombre");
		}
	}		
	// Content
	$mail->isHTML(true);                                  // Set email format to HTML
	$mail->Subject = 'Invitación Congreso';
	$mail->Body    = 'En el siguiente documento se adjunta una invitación';
	$mail->addStringAttachment($archivoAdjunto, 'Invitacion.pdf');
	$mail->send();
	
} catch (Exception $e) {
	echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
}
	
	

	}

?>

<?php 
include ("Conexion.php");
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
					$row0=pg_fetch_row($consulta);
					$consulta0=$row0[0];
					$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
					$row=pg_fetch_row($consulta1);
					$congresoActual=$row[0];//es el id congreso
$contacto=pg_query($conexion,"SELECT cn.telefono,cn.telefono2,cn.email FROM contacto cn, congreso c
WHERE c.id_congreso='$congresoActual'
and  c.id_contacto =cn.id_contacto");
$contactos=pg_fetch_array($contacto);
$correoC=@$contactos[2];
$numeroC1=@$contactos[0];
$numeroC2=@$contactos[1];
?><!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title style="background-image: url('images/logos.jpg');">CISEMATH</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/cismath.png">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" /> 

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
      <?php
    ////Codigo para actualizar el banner
        include ('Conexion.php');
        $nmax=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
        $num=pg_fetch_row($nmax);
        $numcon=$num[0];
        $conact=pg_query($conexion,"SELECT id_congreso, banner FROM congreso WHERE numero_congreso='$numcon'");
        $ban=pg_fetch_row($conact);
        $banner=$ban[1];
        echo '<img src="'.$banner.'" style="width:100%; height:100%; background-size: cover; position: relative; display: flex;"/>';
    ?>

    <header class="top-header" >
        <div class="header_bottom">
          <div class="container">
		  <div class="row"> 
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 col-xl-12">
                <div class="menu_orange_section" style="background: #052450;">
                   <nav class="navbar header-nav navbar-expand-lg"> 
                     <div class="menu_section">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav" >
					    <li><a class="nav-link active" href="indexus.php"><img src="images/i4.png" alt="image"></a> </li>
                        <li><a class="nav-link active" href="acercadeus.php">Acerca de</a></li>
                        <li><a class="nav-link active" href="infus.php">Información</a></li>
                        <li><a class="nav-link active" href="instruccionesus.php">Instrucciones</a></li>
                        <li><a class="nav-link active" href="comitesus.php">Comités</a></li>
						<li><a class="nav-link active" href="memoriasus.php">Memorias</a></li>
						
						<li>
						<a style="position:relative; display:block;">
						<img src="images/user.png" alt="image"> 
						</a>
						</li>
						<li>
						<ul class="nav"><li class="dropdown langmenu" ><a href="#" style="display:block;"class="dropdown-toggle" data-toggle="dropdown" title="Idioma"><b class="caret"></b></a><ul class="dropdown-menu" style="color:black;"><li><a title="English ‎(en)‎" href="menu.php" style="color:black";>Menú‎‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="editarp.php"  style="color:black";>Perfil‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="index.php"  style="color:black";>Cerrar Sesión</a></li></ul></li></ul>
						</li>
						
                    </ul>
                </div>
                     </div>
                 </nav>
                 
                </div>
            </div>
          </div>
		  </div>
        </div>
        
    </header>
    <!-- End header -->
 
 <!-- Start CONTENIDO -->
 <div class="container">
		<div class="row">
			<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			 
				<div class="titulo" style="margin-top:8%">
					<h1 align="center" style="color:#052450;">INVITACIÓN A EVALUADORES</h1>
				</div>
			</div>
		</div>	 
	</div>	
 <div class="container">
		<div class="row" style="display:flex; justify-content:center;  margin-bottom:2%;">
			<div class="table-responsive">
				<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="post">
				<table cellpadding="6px" cellspacing="8px" align="center" style="text-align:left; margin-top:2%;">
					<tr>
						<td>
							<font style="font-size:18px; color:gray; text-align:left;">Nombre del usuario :</font>
						</td>
						<td>
						<select  class="form-control" id="sel1" name="usuario">
							<option value="" disable >Seleccione un usuario</option>
							<?php
							$usuarios=pg_query($conexion,"SELECT id_usuario,nombres,primer_ap,segundo_ap 
							FROM usuario
							WHERE id_cargo!='ALU001'
							AND id_escolaridad !='EST001'");
							while($row= pg_fetch_row($usuarios)){ // imprime la consulta
								$n=trim($row[1]);
								$p=trim($row[2]);
								$a=trim($row[3]);
								$nombre="$n $p $a";
								?> 
								<option value="<?php echo $row[0] ?>"><?php echo $nombre ?></option>
								<?php  
									
								}
							?>
						 </select>
						</td>
					</tr>
					
					<tr>
						<td>
							<font style="font-size:18px; color:gray; text-align:left;">Roles:</font>
						</td>
						<td>
						<select  class="form-control" id="sel1" name="rol">
							<option value="" disable >Seleccione un rol</option>
							<?php
							$roles=pg_query($conexion,"SELECT id_rol,descripcion_rol 
							FROM roles
							WHERE 
							id_rol='ECC013'
							OR id_rol='EPO010'
							OR id_rol='EVF014'
							OR id_rol='EVCA12'
							OR id_rol='COP024'");
							while($row= pg_fetch_row($roles)){ // imprime la consulta
								
								?> 
								<option value="<?php echo $row[0] ?>"><?php echo $row[1] ?></option>
								<?php  
									
								}
							?>
						 </select>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<button type="butmit" name="submit1" style="background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;">Invitar</button>
						</td>
					</tr>
				</table>
				</form>
			</div>
		</div>
			
	</div>
	
	<div class="btn" style="text-align:right; width:100%;  margin-bottom:5%;">
		<button type="B1" onclick="location.href='menu.php'" style="margin-right:5%; background: whitesmoke; color: black; padding: 6px 10px; border-color: whitesmoke;border-radius: 20px;">Regresar</button>
	</div>
	
    <!-- End CONTENIDO -->
    
  <!-- Start Footer -->
        <footer class="footer-box">
        <div class="container">
            <div class="row">
               <div class="col-md-12 white_fonts" style="display: contents;">
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            
                            <div class="full">
							<h3>Dirección</h3>
                                <ul class="menu_footer">   
								     
								     <li><img src="images/i5.png"><span style="font-size: 85%;">Carr, Cuautitlán - Teoloyucan, San Sebastian Xhala, 54714 Cuautitlán Izcalli</span></li>

                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="full">
                                <div class="footer_blog full white_fonts">
                             <h3>Contacto</h3>
                             <ul class="full">
                               <li><img src="images/i6.png"><span style="font-size: 85%;"> <?php echo $correoC  ?></span></li>
                               <li><img src="images/i7.png"><span style="font-size: 85%;"><?php echo "$numeroC1    $numeroC2"; ?></span></li>
                             </ul>
                         </div>
                            </div>
                        </div>
                </div>
			 </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link active" href="derechosa.php" style="color:white">© Sitio desarrollado por CDD Solutions S.A. y administrado por el Departamento de Matemáticas. Hecho en México, (2020 - 2021)</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
	/* counter js */

(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
	</script>
</body>

</html>