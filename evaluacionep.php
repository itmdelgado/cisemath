<?php 
include ("Conexion.php");
session_start();
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
$id_ponencia=$_POST['id_ponencia'];
//////////////////////////// Se obtinene el congreso actual////////////////////////////
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
$row5=pg_fetch_row($consulta);
$consulta0=$row5[0];
$consulta1=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$consulta0'");
$row6=pg_fetch_row($consulta1);
$numeroCongreso=$row6[0];
/////////// Se busca si existe una actualizacion del extenso/////////////////////////////////////////////////////////////
$consultaValidacion="SELECT id_ponencia_oral FROM actualizacion_p_oral WHERE id_ponencia_oral='$id_ponencia' and id_congreso='$numeroCongreso'";
$result=pg_query($conexion,$consultaValidacion);
$row=pg_fetch_row($result);
$validacion=@$row[0];
    if(!empty($validacion)){
        //////////////////Si existen actualizaciones, se busca la ultima de la ponencia a evaluar//////////////////////////////////////////////////////// 									 
        $consulta1=pg_query($conexion,"SELECT MAX(numeracion) FROM actualizacion_p_oral  where id_congreso='$numeroCongreso' and id_ponencia_oral ='$id_ponencia' ");
        $row7=pg_fetch_row($consulta1);
        $numeroid=$row7[0];
        
        $consulta1=pg_query($conexion,"SELECT MAX(numeracion) FROM actualizacion_resumen  where id_congreso='$numeroCongreso' and id_ponencia ='$id_ponencia' ");
        $row7=pg_fetch_row($consulta1);
        $numeroidR=$row7[0];

        //////////////////////////Se cambia el estado de la ponencia a en evaluacion para evitar que sea borrado o modificado///////////////////////////
        $actualizacion2="UPDATE actualizacion_p_oral SET estatus_actualizacion ='En Evaluacion' WHERE numeracion='$numeroid' AND id_ponencia_oral='$id_ponencia'";
        pg_query($conexion,$actualizacion2);
        //////////////////////////Se consultan los datos de la ultima actualizacion, para mostrarlos al evaluador///////////////////////////////////////
        $Datos=pg_query($conexion,"SELECT apo.extenso, ar.titulo, u.nombres,u.primer_ap, u.segundo_ap
        FROM actualizacion_p_oral apo, actualizacion_resumen ar, usuario u, usuario_ponencias up
        WHERE
        ar.id_ponencia='$id_ponencia'
        and ar.id_ponencia=apo.id_ponencia_oral
        and up.id_ponencias=ar.id_ponencia
        and up.id_usuario=u.id_usuario
        and up.tipo_autor='Autor'
        and up.id_congreso='$numeroCongreso'
        and ar.id_congreso=up.id_congreso
        AND apo.numeracion='$numeroid'
        and ar.numeracion='$numeroidR';");
        $row1=@pg_fetch_row($Datos);
        $extenso=@$row1[0];
        $titulo=@$row1[1];
        $n=@trim($row1[2]);
        $p=@trim($row1[3]);
        $m=@trim($row1[4]);
        $nombre=@"$n $p $m";
    
    }else{
        // se cambia el estado de la ponencia a en evaluacion para evitar que sea borrado o modificado//////////////////////////////////////////////////								 
        $consultaC=" SELECT id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso ;";//selecciona el id maximo del congreso 
        $NumeroC=pg_query($conexion,$consultaC);
        $filas=pg_fetch_row($NumeroC);
        $congresoActual=$filas[0];
        $actualizacion2="UPDATE ponencia_oral SET estatus_extenso ='En Evaluacion' WHERE id_ponencia_oral='$id_ponencia' and id_congreso='$congresoActual'";
        pg_query($conexion,$actualizacion2); 

        $consulta1=pg_query($conexion,"SELECT  po.extenso, ar.titulo, u.nombres,u.primer_ap, u.segundo_ap
        FROM ponencia_oral po, actualizacion_resumen ar, usuario u, usuario_ponencias up
        WHERE
        ar.id_ponencia='$id_ponencia'
        and ar.id_ponencia=po.id_ponencia_oral
        and up.id_ponencias=ar.id_ponencia
        and up.id_usuario=u.id_usuario
        and up.tipo_autor='Autor'
        and up.id_congreso='$congresoActual'
        and ar.id_congreso=up.id_congreso");
        
        $row1=@pg_fetch_row($consulta1);
        $extenso=@$row1[0];
        $titulo=@$row1[1];
        $n=@trim($row1[2]);
        $p=@trim($row1[3]);
        $m=@trim($row1[4]);
        $nombre=@"$n $p $m";
    
    }

?>
<?php 
include ("Conexion.php");
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
					$row0=pg_fetch_row($consulta);
					$consulta0=$row0[0];
					$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
					$row=pg_fetch_row($consulta1);
					$congresoActual=$row[0];//es el id congreso
$contacto=pg_query($conexion,"SELECT cn.telefono,cn.telefono2,cn.email FROM contacto cn, congreso c
WHERE c.id_congreso='$congresoActual'
and  c.id_contacto =cn.id_contacto");
$contactos=pg_fetch_array($contacto);
$correoC=@$contactos[2];
$numeroC1=@$contactos[0];
$numeroC2=@$contactos[1];
?><!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title style="background-image: url('images/logos.jpg');">CISEMATH</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/cismath.png">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" />
	<script
	src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>


    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
      <?php
    ////Codigo para actualizar el banner
        include ('Conexion.php');
        $nmax=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
        $num=pg_fetch_row($nmax);
        $numcon=$num[0];
        $conact=pg_query($conexion,"SELECT id_congreso, banner FROM congreso WHERE numero_congreso='$numcon'");
        $ban=pg_fetch_row($conact);
        $banner=$ban[1];
        echo '<img src="'.$banner.'" style="width:100%; height:100%; background-size: cover; position: relative; display: flex;"/>';
    ?>

    <header class="top-header" >
        <div class="header_bottom">
          <div class="container">
		   <div class="row">
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 col-xl-12">
                <div class="menu_orange_section" style="background: #052450;">
                   <nav class="navbar header-nav navbar-expand-lg"> 
                     <div class="menu_section">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav" >
					    <li><a class="nav-link active" href="indexus.php"><img src="images/i4.png" alt="image"></a> </li>
                        <li><a class="nav-link active" href="acercadeus.php">Acerca de</a></li>
                        <li><a class="nav-link active" href="infus.php">Información</a></li>
                        <li><a class="nav-link active" href="instruccionesus.php">Instrucciones</a></li>
                        <li><a class="nav-link active" href="comitesus.php">Comités</a></li>
						<li><a class="nav-link active" href="memoriasus.php">Memorias</a></li>
						
						<li>
						<a style="position:relative; display:block;">
						<img src="images/user.png" alt="image"> 
						</a>
						</li>
						<li>
						<ul class="nav"><li class="dropdown langmenu" ><a href="#" style="display:block;"class="dropdown-toggle" data-toggle="dropdown" title="Idioma"><b class="caret"></b></a><ul class="dropdown-menu" style="color:black;"><li><a title="English ‎(en)‎" href="menu.php" style="color:black";>Menú‎‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="editarp.php"  style="color:black";>Perfil‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="index.php"  style="color:black";>Cerrar Sesión</a></li></ul></li></ul>
						</li>
						
                    </ul>
                </div>
                     </div>
                 </nav>
                 
                </div>
            </div>
          </div>
		  </div>
        </div>
        
    </header>
    <!-- End header -->
 
 <!-- Start CONTENIDO -->
	
	 <div class="container">
	    <div class="row">
	        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
	     
				<div class="titulo" style="margin-top:8%">
				<font style="font-size: 17px; font-weight: 300;">
	            <h1 align="center" style="color: #052450;">EVALUACIÓN DE TRABAJOS EXTENSOS: PONENCIAS ORALES</h1>
	            </div>
	        </div>
		</div>	 
	</div>	
    <div class="container" style="margin-top:3%; margin-bottom:3%;">
		<div class="row" style="" >
			<div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6" style="" >
			<div class="table-responsive" style="" >
						<div class="field">
							Autor:<br>
							<input type="text" class="form-control" value="<?php echo $nombre ?>" style="width:80%;" disabled>
						</div>
						
						<div class="field">
							Título del trabajo:<br>
							<input type="" class="form-control" style="width:80%;"  disabled value="<?php echo $titulo ?>" id="titulot" maxlength="4" minlength="4" required=""  >
						</div>
						
						<div class="field">
							Clave:<br>
							<input type="text"  class="form-control" style="width:80%;" disabled value="<?php echo $id_ponencia ?>">
						</div>
			
						<div class="field">
							Archivo:<br>
							
							
							<td><a title="Descargar Archivo" href="<?php echo $extenso ?>" download="<?php echo $extenso ?>" style="color: blue; font-size:18px;"> Descargar Archivo</span> </a></td>
						</div>
				</div>	
			</div>
		
			 <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6" style=""> 
				 <form action="E_observaciones_extenso.php" method="post">
					<div class="field">
						<strong>
						Elija la opción a su criterio:<br>
						</strong>
						<input type="radio" id="male" name="gender" value="Aceptado">
						<label for="male">Aceptar</label><br>
						<input type="radio" id="male" name="gender" value="Con Modificaciones">
						<label for="male">Modificar</label><br>
						<input type="radio" id="male" name="gender" value="Rechazado">
						<label for="male">Rechazar</label><br>
					</div>
					<div class="field">
						<strong>
						Escriba sus comentarios u observaciones: <br>
						</strong>
						<textarea name="comentarios" id="inst" rows="5" cols="30"></textarea>								    
						<input type="hidden" class="form-control" name ="id_ponencia" value="<?php echo $id_ponencia ?>" size=50 >

					</div>	
				
			
			</div>
		</div>
		 <div class="btn" style="text-align:right; width:100%; margin-bottom: 5%; margin-top: 2%;">
				<input type="submit" value="Evaluar" style="margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;">
				<input type="reset" value="Cancelar" onclick="location.href='evaluartr.php'" style="margin-right:5%; background: whitesmoke; color: black; padding: 6px 10px; border-color: whitesmoke;border-radius: 20px;">
		</div>
		</form>
	</div>
	

    <!-- End CONTENIDO -->
    
 
    <!-- Start Footer -->
     <!-- Start Footer -->
        <footer class="footer-box">
        <div class="container">
            <div class="row">
               <div class="col-md-12 white_fonts" style="display: contents;">
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            
                            <div class="full">
							<h3>Dirección</h3>
                                <ul class="menu_footer">   
								     
								     <li><img src="images/i5.png"><span style="font-size: 85%;">Carr, Cuautitlán - Teoloyucan, San Sebastian Xhala, 54714 Cuautitlán Izcalli</span></li>

                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="full">
                                <div class="footer_blog full white_fonts">
                             <h3>Contacto</h3>
                             <ul class="full">
                               <li><img src="images/i6.png"><span style="font-size: 85%;"> <?php echo $correoC  ?></span></li>
                               <li><img src="images/i7.png"><span style="font-size: 85%;"><?php echo "$numeroC1    $numeroC2"; ?></span></li>
                             </ul>
                         </div>
                            </div>
                        </div>
                </div>
			 </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link active" href="derechosa.php" style="color:white">© Sitio desarrollado por CDD Solutions S.A. y administrado por el Departamento de Matemáticas. Hecho en México, (2020 - 2021)</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
	/* counter js */

(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
	</script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
</body>

</html>