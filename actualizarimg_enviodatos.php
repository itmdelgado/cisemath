<?php
    session_start();
    include ('Conexion.php');
    $usuario=$_SESSION['Usuario'];

    date_default_timezone_set("America/Mexico_City");
    
    $bandera=false;
    $error=[''];
    $success=[''];

    if(isset($_FILES["banner"]["name"]) && !empty($_FILES["banner"]["name"])){ //recibimos el archivo se verifica con el if
        $bandera=true;
        $nombre_base = basename($_FILES["banner"]["name"]);
        $nombre_final = date("y-m-d"). "_" . $nombre_base;
        $ruta = "imagenesCongreso/". $nombre_final;
        $subirarchivo = move_uploaded_file($_FILES["banner"]["tmp_name"], $ruta);
        if($subirarchivo){
            $consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
            $row0=pg_fetch_row($consulta);
            $consulta0=$row0[0];
            $consulta1=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$consulta0'");
            $row=pg_fetch_row($consulta1);
            $update = "UPDATE congreso SET banner = '$ruta' WHERE id_congreso= '$row[0]'";
            $actualizacion = pg_query($conexion, $update);
            if($actualizacion){
                array_push($success, "Banner actualizado");
            } else{
                array_push($error, "Error al subir el Banner");
            }
        }
    }

    if(isset($_FILES["convocatoria"]["name"]) && !empty($_FILES["convocatoria"]["name"])){ //recibimos el archivo se verifica con el if
        $bandera=true;
        $nombre_base = basename($_FILES["convocatoria"]["name"]);
        $nombre_final = date("y-m-d"). "_" . $nombre_base;
        $ruta = "imagenesCongreso/". $nombre_final;
        $subirarchivo = move_uploaded_file($_FILES["convocatoria"]["tmp_name"], $ruta);
        $tamano = $_FILES['convocatoria']['size'];
        if($tamano > 20000000){ //20,000,000 bytes APROX: 18MB
            echo"<script>alert('¡El archivo es demasiado grande (solo archivos de menos de 18MB)!');window.location='actualizarimg.php'</script>";
            return false;
        }
        if($subirarchivo){
            $consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
            $row0=pg_fetch_row($consulta);
            $consulta0=$row0[0];
            $consulta1=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$consulta0'");
            $row=pg_fetch_row($consulta1);
            $update = "UPDATE congreso SET convocatoria = '$ruta' WHERE id_congreso= '$row[0]'";
            $actualizacion = pg_query($conexion, $update);
            if($actualizacion){
                array_push($success, "Convocatoria actualizado");
            } else{
                array_push($error, "Error al subir la Convocatoria");
            }
        }
    }

    if(isset($_FILES["cartel"]["name"]) && !empty($_FILES["cartel"]["name"])){ //recibimos el archivo se verifica con el if
        $bandera=true;
        $nombre_base = basename($_FILES["cartel"]["name"]);
        $nombre_final = date("y-m-d"). "_" . $nombre_base;
        $ruta = "imagenesCongreso/". $nombre_final;
        $subirarchivo = move_uploaded_file($_FILES["cartel"]["tmp_name"], $ruta);
        if($subirarchivo){
            $consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
            $row0=pg_fetch_row($consulta);
            $consulta0=$row0[0];
            $consulta1=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$consulta0'");
            $row=pg_fetch_row($consulta1);
            $update = "UPDATE congreso SET cartel = '$ruta' WHERE id_congreso= '$row[0]'";
            $actualizacion = pg_query($conexion, $update);
            if($actualizacion){
                array_push($success, "Cartel actualizado");
            } else{
                array_push($error, "Error al subir el Cartel");
            }
        }
    }

    if(isset($_FILES["constancia"]["name"]) && !empty($_FILES["constancia"]["name"])){ //recibimos el archivo se verifica con el if
        $bandera=true;
        $nombre_base = basename($_FILES["constancia"]["name"]);
        $nombre_final = date("y-m-d"). "_" . $nombre_base;
        $ruta = "imagenesCongreso/". $nombre_final;
        $subirarchivo = move_uploaded_file($_FILES["constancia"]["tmp_name"], $ruta);
        if($subirarchivo){
            $consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
            $row0=pg_fetch_row($consulta);
            $consulta0=$row0[0];
            $consulta1=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$consulta0'");
            $row=pg_fetch_row($consulta1);
            $update = "UPDATE congreso SET constancia = '$ruta' WHERE id_congreso= '$row[0]'";
            $actualizacion = pg_query($conexion, $update);
            if($actualizacion){
                array_push($success, "Constancia actualizado");
            } else{
                array_push($error, "Error al subir la Constancia");
            }
        }
    }

    $mensajesuceso=implode('++ ', $success);
    $mensajeerror=implode('-- ', $error);

    if($bandera==true){
        echo"<script>alert('".$mensajesuceso." ".$mensajeerror."');window.location='actualizarimg.php'</script>";
    } else{
        echo"<script>alert('¡No has seleccionado ningun archivo!');window.location='actualizarimg.php'</script>";
    }
?>