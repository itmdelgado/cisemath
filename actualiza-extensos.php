<?php
session_start();
use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
   	require 'PHPMailer/Exception.php';
    require 'PHPMailer/PHPMailer.php';
    require 'PHPMailer/SMTP.php';
    require ('libreria/fpdf.php');

$usuario=$_SESSION['Usuario'];
$estatus_estenso="Enviado";
$observaciones= "";

date_default_timezone_set("America/Mexico_City");
$fecha=date("Y-m-d");
$hora=date("H:i:s");



if(isset($_POST['Clave']) && !empty($_POST['Clave'])){
	$id_trabajo = $_POST["Clave"];
	
}else{
		 echo"<script>alert('Error ID trabajo');window.location='trabr.php'</script>";
		
}
include ('Conexion.php');
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
$row5=pg_fetch_row($consulta);
$consulta0=$row5[0];



$consulta1=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$consulta0'");
$row6=pg_fetch_row($consulta1);
$numeroCongreso=$row6[0];// se obtiene en id del congreso actual

$consultaNumActualizaciones=pg_query($conexion, "SELECT MAX(numeracion) FROM actualizacion_p_oral where id_congreso='$numeroCongreso'");
$rowNumActualizaciones=pg_fetch_row($consultaNumActualizaciones);

if(empty($rowNumActualizaciones)){
	$numeroid=1;
	$id_actualizacion="ACT".$id_trabajo.$numeroid;// se crea id actualizacion en 1 si no existe ninguna actualizacion;
}else{
	 $numeroid=$rowNumActualizaciones[0]+1;
    $id_actualizacion="ACT".$id_trabajo.$numeroid;// se crea id actualizacion sumando el numero actualizacion anterior mas 1;
}
$informacionTrabajo=pg_query($conexion, "SELECT titulo FROM ponencias WHERE id_ponencia='$id_trabajo'");
  $rowinfo=pg_fetch_assoc($informacionTrabajo);
  //VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$id_trabajo' AND id_congreso='$numeroCongreso' AND estatus_actualizacion='Aceptado'");
$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);	

if(!empty($actualizacionTitulo)){
$titulo=$actualizacionTitulo['titulo'];

}else{
$titulo=$rowinfo['titulo'];
}

	if(isset($_POST['extenso'])){
	$nombreViejoArchivo= $_POST['extenso'];
	
		if(file_exists($nombreViejoArchivo)){//UBICACIÓN DEL VIEJO ARCHIVO EN LA CARPETA
	
					//VALIDACIÓN DE TERMINACIÓN DE ARCHIVOS
				    $directorio="extensos/";
				    $archivo=basename($_FILES["archivoNuevo"]["name"]);
				    $tipoArchivo=strtolower(pathinfo($archivo, PATHINFO_EXTENSION));
				     $maximob=10000000;
				    if ($tipoArchivo=="docx" || $tipoArchivo=="doc") {
				    	if($_FILES["archivoNuevo"]["size"]<=$maximob){
				    	    $nombre_final= "$id_actualizacion";
						    $ruta=$directorio.$nombre_final.$numeroCongreso;
						    $subirarchivos=move_uploaded_file($_FILES["archivoNuevo"]["tmp_name"],$ruta.".".$tipoArchivo);
						    $rutaF=$ruta.".".$tipoArchivo;
						    if($subirarchivos){//VALIDA QUE SE SUBA BIEN EL ARCHIVO

						    	
							$insertarActualizacionExtenso="INSERT INTO actualizacion_p_oral(id_actualizacion_p_oral,id_ponencia_oral, fecha, hora, id_congreso,extenso, estatus_actualizacion, numeracion) VALUES ('$id_actualizacion','$id_trabajo','$fecha','$hora','$numeroCongreso', '$rutaF', '$estatus_estenso', '$numeroid')";
							$resultado=pg_query($conexion,$insertarActualizacionExtenso);
								    	
								if($resultado){//Si se inserto en la tabla

								    	if(@unlink($nombreViejoArchivo)){//ELIMINACIÓN DEL VIEJO ARCHIVO EN LA CARPETA
											 //CORREO AUTOR
	                                    //**************************************************************************************
	                                    class PDF extends FPDF{
	                                    // Cabecera de página
	                                    function Header()
	                                    {
	                                        // Logo
	                                        $this->Image('logo.jpg',0,0,220);
	                                        // Arial bold 15
	                                        $this->SetFont('Arial','B',15);
	                                        // Movernos a la derecha
	                                        $this->Cell(80);
	                                        // Título
	                                        $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
	                                        // Salto de línea
	                                        $this->Ln(50);
	                                        
	                                    }
	                                    
	                                    // Pie de página
	                                    function Footer() {
	                                        // Posición: a 1,5 cm del final
	                                        $this->SetY(-15);
	                                        // Arial italic 8
	                                        $this->SetFont('Arial','I',8);
	                                        // Número de página
	                                        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	                                        }
	                                    }
	                                     // Creación del objeto de la clase heredada
	                                    $pdf = new PDF();
	                                    $pdf->AliasNbPages();
	                                    $pdf->AddPage();
	                                    $pdf->SetFont('Times','',12);
	                                    
	                                    $pdf->Cell(40,10,utf8_decode('Actualización del Extenso'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Usted ha Actualizado el extenso para la siguiente ponencia oral'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Clave de la ponencia: '.$id_trabajo),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Título de la ponencia: '.$titulo),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('El nombre del archivo registrado es: '.$archivo),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Fecha de registro del documento fue: '.$fecha),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Recuerde que si desea realizar alguna modificación o eliminar el documento actual,'),0,1); 
	                                    $pdf->Cell(40,10,utf8_decode('se debe realizar durante las fechas correspondientes en'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('la página de "Trabajos Registrados" en el apartado de "Extensos".'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('Le pedimos de la manera más atenta, estar al pendiente de su cuenta para conocer'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode('el estatus de su trabajo.'),0,1);
	                                    $pdf->Cell(40,10,utf8_decode(''),0,1);
	                                    
	                                    $archivoAdjunto = $pdf->Output("", "S");
	                                    $correoa=pg_query($conexion,"SELECT c.correo,us.nombres,us.primer_ap,us.segundo_ap FROM correos_usuario c, usuario us WHERE c.id_usuario='$usuario' and us.id_usuario ='$usuario'");
	                                    $i=0;
	                                    while($mostrarCR=pg_fetch_array($correoa)){
	                                        $cor[$i]=trim($mostrarCR['correo']);
	                                        $nombre=trim($mostrarCR['nombres']);
	                                        $apPA=trim($mostrarCR['primer_ap']);
	                                        $apMa=trim($mostrarCR['segundo_ap']);
	                                        
	                                        $i=$i+1;
	                                    }

	                                      $mail = new PHPMailer(true);
	                                    
	                                    try {
	                                        //Server settings
	                                        $mail->SMTPDebug = 0;                      // Enable verbose debug output
	                                        $mail->isSMTP();                                            // Send using SMTP
	                                        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
	                                        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
	                                        $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
	                                        $mail->Password   = 'CongresoMate2020';                               // SMTP password
	                                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
	                                        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
	                                        $mail->CharSet  = 'UTF-8';//PARA EL ACENTO
	                                    
	                                        //Recipients
	                                        $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
	                                        $mail->addAddress("$cor[0]", "$nombre $apPA");
	                                        if(!empty($cor[1])){
	                                            $mail->addAddress("$cor[1]", "$nombre $apPA");
	                                        }
	                                        if(!empty($cor[2])){
	                                            $mail->addAddress("$cor[2]", "$nombre $apPA");
	                                        }
	                                       // Content
	                                        $mail->isHTML(true);                                  // Set email format to HTML
	                                        $mail->Subject = 'Actualización del Extenso';
	                                        $mail->Body    = 'En el siguiente documento se adjuntan los datos del extenso actualizado.';
	                                        $mail->addStringAttachment($archivoAdjunto, 'Actualizacion_Extenso_Ponencia_Oral.pdf');
	                                        $mail->send();
	                                        
	                                        }catch (Exception $e) {
	                                            echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
	                                        }

	                                    //*****************************************************************************************
	                                           $numCoautoresTrabajo=pg_query($conexion, "SELECT COUNT (*) id_usuario FROM usuario_ponencias 
					                        WHERE id_ponencias ='$id_trabajo' and tipo_autor='Coautor' and id_congreso = '$numeroCongreso'");
					                        $numCoautores =pg_fetch_assoc($numCoautoresTrabajo);
					                        
					                        //VALIDA SI HAY COAUTORES
					                            if($numCoautores['id_usuario']!=0){
					                                  
					//********************************************************************************************************************
					                                class PDF2 extends FPDF
					                        {
					                        // Cabecera de página
					                        function Header()
					                        {
					                            // Logo
					                            $this->Image('logo.jpg',0,0,220);
					                            // Arial bold 15
					                            $this->SetFont('Arial','B',15);
					                            // Movernos a la derecha
					                            $this->Cell(80);
					                            // Título
					                            $this->Cell(50,80,utf8_decode('Congreso de Matemáticas '),30,0,'C');
					                            // Salto de línea
					                            $this->Ln(50);
					                            
					                        }
					                        
					                        // Pie de página
					                        function Footer()
					                        {
					                            // Posición: a 1,5 cm del final
					                            $this->SetY(-15);
					                            // Arial italic 8
					                            $this->SetFont('Arial','I',8);
					                            // Número de página
					                            $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
					                        }
					                        }
					                        
					                        // Creación del objeto de la clase heredada
					                         $pdf = new PDF();
					                        $pdf->AliasNbPages();
					                        $pdf->AddPage();
					                        $pdf->SetFont('Times','',12);

					                        $pdf->Cell(40,10,utf8_decode('Registro de Extenso'),0,1);
					                        $pdf->Cell(40,10,utf8_decode(''),0,1);
					                        $pdf->Cell(40,10,utf8_decode('El autor '.$nombre.' '.$apPA.' '.$apMa.' ha actualizado el extenso para la siguiente ponencia oral a la cual usted pertenece'),0,1);
					                        $pdf->Cell(40,10,utf8_decode('Clave de la ponencia: '.$id_trabajo),0,1);
					                        $pdf->Cell(40,10,utf8_decode('Título de la ponencia: '.$titulo),0,1);
					                        $pdf->Cell(40,10,utf8_decode('El nombre del archivo registrado es: '.$archivo),0,1);
					                        $pdf->Cell(40,10,utf8_decode('Fecha de registro del documento fue: '.$fecha),0,1);
					                        $pdf->Cell(40,10,utf8_decode('Si desea conocer el estatus de su trabajo, lo podrá visualizar'),0,1);
					                        $pdf->Cell(40,10,utf8_decode('en el apartado "trabajos registrados".'),0,1);
					                        $pdf->Cell(40,10,utf8_decode(''),0,1);

					                        $archivoAdjunto2 = $pdf->Output("", "S");
					    
					                        //Envio de correo coautores
					                        $mail = new PHPMailer(true);
					                        
					                        try {
					                            //Server settings
					                            $mail->SMTPDebug = 0;                      // Enable verbose debug output
					                            $mail->isSMTP();                                            // Send using SMTP
					                            $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
					                            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
					                            $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
					                            $mail->Password   = 'CongresoMate2020';                               // SMTP password
					                            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
					                            $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
					                            $mail->CharSet  = 'UTF-8';//PARA EL ACENTO
					                        
					                            //Recipients
					                            $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
					                            // obtener nombre,apellidos y correo de coautores del trabajo
					                          

					                            $infoCoau=pg_query($conexion,"select u.nombres,u.primer_ap ,u.segundo_ap,c.correo FROM correos_usuario c, usuario u, usuario_ponencias up WHERE up.id_ponencias='$id_trabajo'and up.id_usuario=u.id_usuario and u.id_usuario= c.id_usuario and up.tipo_autor!='Autor';");
					                            $j=0;
					                            while($infoCoa=pg_fetch_array($infoCoau)){
					                               
					                                $nombreCoa[$j]=trim($infoCoa['nombres']);
					                                $apellidoCoa[$j]=trim($infoCoa['primer_ap']);
					                                $apellidoCoa2[$j]=trim($infoCoa['segundo_ap']);
					                                $corC[$j]=trim($infoCoa['correo']);
					                               
					                            
					                                $j=$j+1;
					                               }
					                               if(!empty($corC[0])){
					                                $mail->addAddress("$corC[0]",utf8_decode("$nombreCoa[0] $apellidoCoa[0] $apellidoCoa2[0]"));
					    
					                               }
					                               if(!empty($corC[1])){
					                                $mail->addAddress("$corC[1]",utf8_decode("$nombreCoa[1] $apellidoCoa[1] $apellidoCoa2[1]"));
					    
					                               }
					                               if(!empty($corC[2])){
					                                $mail->addAddress("$corC[2]",utf8_decode("$nombreCoa[2] $apellidoCoa[2] $apellidoCoa2[2]"));
					    
					                               }
					                               if(!empty($corC[3])){
					                                $mail->addAddress("$corC[3]",utf8_decode("$nombreCoa[3] $apellidoCoa[3] $apellidoCoa2[3]"));
					    
					                               }   
					                        
					                            // Attachments
					                            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
					                            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
					                        
					                            // Content
					                            $mail->isHTML(true);                                  // Set email format to HTML
					                            $mail->Subject = 'Actualización de Extenso';
					                            $mail->Body    = 'En el siguiente documento se adjuntan los datos del extenso actualizado.';
					                            $mail->addStringAttachment($archivoAdjunto2, 'Actualizacion_Extenso_Ponencia_Oral.pdf');
					                            $mail->send();
					                            
					                        } catch (Exception $e) {
					                            echo "Error al enviar el mensaje para coautores: {$mail->ErrorInfo}";
					                        }

					//*********************************************************************************************************************
					                                       echo"<script>alert('¡El extenso se actualizado correctamente!');window.location='trabr.php'</script>"; 
					                                    }else{
					                                        //echo"NO HAY COAUTORES";
					                                        echo"<script>alert('¡El extenso se actualizado correctamente!');window.location='trabr.php'</script>";
					                        
					                                    }


										}else{
										 echo"<script>alert('No se pudo eliminar el archivo.');window.location='trabr.php'</script>";
										}
							 	}else{
						    		echo"<script>alert('No se pudo subir la actualizacion del extenso.');window.location='trabr.php'</script>";
						    	}

									
						    }else{//SI SE SUBIO BIEN EL ARCHIVO
						    	 echo"<script>alert('Error al subir archivo.');window.location='trabr.php'</script>";
						    }
						}else{
			                 echo"<script>alert('Error, Solo se aceptan archivos con un tamaño hasta de 10MB.');window.location='cartel.php'</script>"; 
			            }

						}else{
							 ?>
					    	<script>alert("Solo se aceptan archivos con terminación .doc o .docx, que corresponden a la 'Plantilla de Trabajos Extensos'. Por favor, seleccione el archivo  correspondiente.");
					    window.location='trabr.php'</script>;
					    <?php
						}							
	
		}else{
			 echo "<script>alert('No se encontro el archivo');window.location='trabr.php'</script>";//ERROR DE LOCALIZACIÓN DEL ARCHIVO
		}
	}else{
		echo "No se recibio el nombre del viejo archivo de manera correcta";//ERROR POR CARGA DEL VIEJO FORMULARIO
	}

?>