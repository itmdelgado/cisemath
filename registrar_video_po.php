<?php
// Extensiones para crear el PDF Y enviar correo
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';
require ('libreria/fpdf.php');
include ('Conexion.php');
// Recibe los datos por metodo post de video.php
$clave=$_POST['id_ponencia'];
$url=$_POST['url'];
            //Se consulta el numero del congreso actual
            $consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");
			$row5=pg_fetch_row($consulta);
			$consulta0=$row5[0];
			$consulta1=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$consulta0'");
            $row6=pg_fetch_row($consulta1);
            // se obtiene en id del congreso actual
            $congresoActual=$row6[0];
            // inserta las actualizaciones en la base de datos dependiendo la ponencia oral
            $inserturl="UPDATE ponencias SET url_video ='$url' WHERE id_ponencia='$clave'";
            $insertestatusvideo="UPDATE ponencias SET estatus_video ='Enviado' WHERE id_ponencia='$clave'";
            pg_query($conexion,$inserturl);
            pg_query($conexion,$insertestatusvideo);
            // se obtienen el nombre del Autor de la ponencia
            $infoAutor=pg_query($conexion,"SELECT u.nombres,u.primer_ap,u.segundo_ap FROM  usuario u, usuario_ponencias up WHERE up.id_ponencias ='$clave' and up.id_usuario=u.id_usuario  and up.tipo_autor='Autor';  ");
            $row8=pg_fetch_row($infoAutor);
            $nombre=trim($row8[0]);
            $apPA=trim($row8[1]);
            $apMa=trim($row8[2]);
            // se obtienen los correos del autor
            $correoa=pg_query($conexion,"SELECT c.correo FROM correos_usuario c,usuario u, usuario_ponencias up WHERE up.id_ponencias='$clave' and up.id_usuario=u.id_usuario and u.id_usuario=c.id_usuario and up.tipo_autor='Autor'");
            $i=0;
            while($mostrarCR=pg_fetch_array($correoa)){
                $cor[$i]=trim($mostrarCR['correo']);
                $i=$i+1;
            }
            
            
            
            // Creacion de PDF Autor
            class PDF extends FPDF
                                {
                                // Cabecera de página
                                function Header()
                                {
                                    // Logo
                                    $this->Image('logo.jpg',0,0,220);
                                    // Arial bold 15
                                    $this->SetFont('Arial','B',15);
                                    // Movernos a la derecha
                                    $this->Cell(80);
                                    // Título
                                    $this->Cell(50,80,utf8_decode('Congreso Matemáticas'),30,0,'C');
                                    // Salto de línea
                                    $this->Ln(50);
                                    
                                }
                                
                                // Pie de página
                                function Footer()
                                {
                                    // Posición: a 1,5 cm del final
                                    $this->SetY(-15);
                                    // Arial italic 8
                                    $this->SetFont('Arial','I',8);
                                    // Número de página
                                    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
                                }
                                }
                                
                                // Creación del objeto de la clase heredada
                                $pdf = new PDF();
                                $pdf->AliasNbPages();
                                $pdf->AddPage();
                                $pdf->SetFont('Times','',12);
                                
                                $pdf->Cell(40,10,utf8_decode('Registro Video Ponencia Oral'),0,1);
                                $pdf->Cell(40,10,utf8_decode(''),0,1);
                                $pdf->Cell(40,10,utf8_decode($nombre.' '.$apPA.' '.$apMa.' ha registrado el siguiente video para Ponencia Oral'),0,1);
                                $pdf->Cell(40,10,utf8_decode('Clave de la ponencia oral: '.$clave),0,1);
                                $pdf->Cell(40,10,utf8_decode('Url video: '.$url),0,1);
                                $pdf->Cell(40,10,utf8_decode('Le pedimos de la manera más atenta, estar al pendiente de su cuenta para conocer'),0,1);
                                $pdf->Cell(40,10,utf8_decode('el estatus de su trabajo.'),0,1);
                                $pdf->Cell(40,10,utf8_decode(''),0,1);
                                
                                $archivoAdjunto = $pdf->Output("", "S");
            
            //Envio de correo Autor
            $mail = new PHPMailer(true);
                                
                                try {
                                    //Server settings
                                    $mail->SMTPDebug = 0;                      // Enable verbose debug output
                                    $mail->isSMTP();                                            // Send using SMTP
                                    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                                    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                                    $mail->Username   = 'CongresoMatematicasFESC@gmail.com';                     // SMTP username
                                    $mail->Password   = 'CongresoMate2020';                               // SMTP password
                                    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                                    $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
                                
                                    //Recipients
                                    $mail->setFrom('CongresoMatematicasFESC@gmail.com', 'Congreso Matematicas');
                                    $mail->addAddress("$cor[0]", "$nombre $apPA");
                                    if(!empty($cor[1])){
                                        $mail->addAddress("$cor[1]", "$nombre $apPA");
                                    }
                                    if(!empty($cor[2])){
                                        $mail->addAddress("$cor[2]", "$nombre $apPA");
                                    }
                                                    
                                    // Content
                                    $mail->isHTML(true);                                  // Set email format to HTML
                                    $mail->Subject = 'Video Registrado ';
                                    $mail->Body    = 'En el siguiente documento se adjuntan los datos del video Registrado';
                                    $mail->addStringAttachment($archivoAdjunto, 'Registro_Video_Ponencia_oral.pdf');
                                    $mail->send();
                                    
                                } catch (Exception $e) {
                                    echo "Error al enviar el mensaje: {$mail->ErrorInfo}";
                                }

                                echo"<script>alert('¡VIDEO PONENCIA ORAL REGISTRADO!');window.location='ponencia.php'</script>";

 //header("location:ponencia.php");
?>