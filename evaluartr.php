<?php
session_start();

if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
$usuario=$_SESSION['Usuario'];

include ("Conexion.php");
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
					$row0=pg_fetch_row($consulta);
					$consulta0=$row0[0];
					$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
					$row=pg_fetch_row($consulta1);
					$congresoActual=$row[0];//es el id congreso
$contacto=pg_query($conexion,"SELECT cn.telefono,cn.telefono2,cn.email FROM contacto cn, congreso c
WHERE c.id_congreso='$congresoActual'
and  c.id_contacto =cn.id_contacto");
$contactos=pg_fetch_array($contacto);
$correoC=@$contactos[2];
$numeroC1=@$contactos[0];
$numeroC2=@$contactos[1];
?><!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title style="background-image: url('images/logos.jpg');">CISEMATH</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/cismath.png">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" />

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
      <?php
    ////Codigo para actualizar el banner
        include ('Conexion.php');
        $nmax=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
        $num=pg_fetch_row($nmax);
        $numcon=$num[0];
        $conact=pg_query($conexion,"SELECT id_congreso, banner FROM congreso WHERE numero_congreso='$numcon'");
        $ban=pg_fetch_row($conact);
        $banner=$ban[1];
        echo '<img src="'.$banner.'" style="width:100%; height:100%; background-size: cover; position: relative; display: flex;"/>';
    ?>

    <header class="top-header" >
        <div class="header_bottom">
          <div class="container">
		  <div class="row"> 
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 col-xl-12">
                <div class="menu_orange_section" style="background: #052450;">
                   <nav class="navbar header-nav navbar-expand-lg"> 
                     <div class="menu_section">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav" >
					    <li><a class="nav-link active" href="indexus.php"><img src="images/i4.png" alt="image"></a> </li>
                        <li><a class="nav-link active" href="acercadeus.php">Acerca de</a></li>
                        <li><a class="nav-link active" href="infus.php">Información</a></li>
                        <li><a class="nav-link active" href="instruccionesus.php">Instrucciones</a></li>
                        <li><a class="nav-link active" href="comitesus.php">Comités</a></li>
						<li><a class="nav-link active" href="memoriasus.php">Memorias</a></li>
						
						<li>
						<a style="position:relative; display:block;">
						<img src="images/user.png" alt="image"> 
						</a>
						</li>
						<li>
						<ul class="nav"><li class="dropdown langmenu" ><a href="#" style="display:block;"class="dropdown-toggle" data-toggle="dropdown" title="Idioma"><b class="caret"></b></a><ul class="dropdown-menu" style="color:black;"><li><a title="English ‎(en)‎" href="menu.php" style="color:black";>Menú‎‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="editarp.php"  style="color:black";>Perfil‎</a></li><li><li><a title="Español - Internacional ‎(es)‎" href="index.php"  style="color:black";>Cerrar Sesión</a></li></ul></li></ul>
						</li>
						
                    </ul>
                </div>
                     </div>
                 </nav>
                 
                </div>
            </div>
          </div>
		  </div>
        </div>
        
    </header>
    <!-- End header -->
 
 <!-- Start CONTENIDO -->
 
	 
	 <div class="container">
	    <div class="row">
	        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
	     
				<div class="titulo" style="margin-top:8%">
				<font style="font-size: 17px; font-weight: 300;">
	            <h1 align="center" style="margin-bottom:5%; color: #052450;">TRABAJOS REGISTRADOS PARA EVALUAR </h1>
	            </div>
	        </div>
		</div>	 
	</div>	
	
	<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

		<div class="container" style="margin-bottom:2%;">	 
			 
			<div class="row">
				<table cellpadding="10px" cellspacing="8px" align="center" style="text-align:left; margin-bottom:8%;" border="0">
				<font style="font-size: 17px; font-weight: 300;">
					
					<tr>
						<th style="color:#052450;">TIPO DE TRABAJO<br></th>
						<th style="color:#052450;">ESTATUS<br></th>
					</tr>
					<?php
					/////////////////////////////////////////////////////////////////////Se condificona a que el evaluador debe ser Evaluador de ponencias orales//////////////////////////////////////////7
					$RolEvaPo=@pg_query($conexion,"SELECT id_rol FROM usuario_roles WHERE id_usuario='$usuario'AND id_rol='EPO010' AND estatus_rol='TRUE'");
					$RowEPO=@pg_fetch_row($RolEvaPo);
					if(@$RowEPO[0]!= NULL){
					?>

					<tr>
						<td style="color:#052450;" ><br><br>PONENCIA(S) ORAL(ES)</td>
					</tr>
					<tr>
					<td style="color:#052450;">
						Resumen(es):
						</td></tr>

						<?php
						//////////////////////////////////////////////////////////Se consulta en congreso Actual//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							$consultaC=" SELECT id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso ;";//selecciona el id maximo del congreso 
							$NumeroC=pg_query($conexion,$consultaC);
							$filas=pg_fetch_row($NumeroC);
							$congresoActual=$filas[0];		
						///////////////////////////////////////////////////////////Se consultan todas las ponencias orales asignadas al evaluador//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							$sql="SELECT p.id_ponencia FROM ponencias p, usuario_evalua_ponencias uep WHERE p.id_ponencia=uep.id_ponencias and uep.id_usuario='$usuario' and p.id_congreso ='$congresoActual' and p.id_congreso=uep.id_congreso and p.id_tipo_ponencia='PO'";
						// se consultan los resumenes Asignados  exclusivamente a un evaluador y en el ultimo congreso
							$result =pg_query($conexion, $sql);
							$i=1;
							while($row=pg_fetch_array($result)){
							$mostrarid=$row['id_ponencia'];
								$validacion=pg_query($conexion,"SELECT * FROM actualizacion_resumen ar, ponencias p WHERE p.id_ponencia='$mostrarid' and p.id_ponencia=ar.id_ponencia");
								$rowV=pg_fetch_row($validacion);
								$validacion1=$rowV[0];
								if($validacion1==NULL){
									$consulta=pg_query($conexion,"SELECT p.id_ponencia, p.titulo,p.estatus_resumen FROM ponencias p, usuario_evalua_ponencias uep WHERE uep.id_ponencias ='$mostrarid' and p.id_ponencia = uep.id_ponencias and uep.id_usuario='$usuario'");
									$titulo=pg_fetch_row($consulta);
									$tituloP=$titulo[1];
									$estatusP=$titulo[2];
									$id_ponencia=$titulo[0];
									// "este es el titulo de la ponencia SIN modificaR $i".$tituloP.$estatusP.$id_ponencia."<br>";
								$tabla=" <tr>
												<td>
												$tituloP
												</td>
												<td> 
												$estatusP  
												</td>
												<td align='center'>
													<form action='evaluacionrpo.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
														<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
														<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
													</form>
												</td>	
											</tr>";
											$tabla2=" <tr>
												<td>
												$tituloP
												</td>
												<td> 
												$estatusP  
												</td>
												
											</tr>";
											$estatusPv=trim($estatusP);
											if($estatusPv!='Con Modificaciones' ){
												if( $estatusPv!='Aceptado'){
												echo $tabla."<br>";
												}else{
													echo $tabla2."<br>";
												}
											}else{
												echo $tabla2."<br>";
											}
								}else{
										$ultimaActualizacion=pg_query($conexion,"SELECT MAX(ar.numeracion) FROM actualizacion_resumen ar WHERE ar.id_congreso='$congresoActual' and id_ponencia='$mostrarid'");
										
										$row2=pg_fetch_row($ultimaActualizacion);
										$numeroid=$row2[0];
										$consulta=pg_query($conexion,"SELECT ar.id_ponencia, ar.titulo, ar.estatus_actualizacion FROM actualizacion_resumen ar,ponencias p, usuario_evalua_ponencias uep WHERE p.id_ponencia ='$mostrarid' and p.id_ponencia = uep.id_ponencias and p.id_ponencia = ar.id_ponencia and ar.numeracion='$numeroid'");
										$titulo=pg_fetch_row($consulta);
										$tituloP=$titulo[1];
										$estatusP=$titulo[2];
										$id_ponencia=$titulo[0];
										// "este es el titulo de la ponencia CON modificaR $i".$tituloP.$estatusP.$id_ponencia."<br>";
										$tabla=" <tr>
												<td>
												$tituloP
												</td>
												<td> 
												$estatusP  
												</td>
												<td align='center'>
													<form action='evaluacionrpo.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
														<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
														<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
													</form>
												</td>	
											</tr>";
											$tabla2=" <tr>
												<td>
												$tituloP
												</td>
												<td> 
												$estatusP  
												</td>
													
											</tr>";
											$estatusPv=trim($estatusP);
											if($estatusPv!='Con Modificaciones' ){
												if( $estatusPv!='Aceptado'){
												echo $tabla."<br>";
												}else{
													echo $tabla2."<br>";
												}
											}else{
												echo $tabla2."<br>";
											}

									
								}
								
							}
				?>
				
				

					<tr>
						<td style="color:#052450;">
						Extenso(s):
						</td>
						
						<td>
						
						</td>
						
						<td align="center">

						<?php
						///////////////////////////////////////////// SE CONSULTA EL CONGRESO ACTUAL/////////////////////////////////////////////77
						$consultaC=" SELECT id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso ;";//selecciona el id maximo del congreso 
						$NumeroC=pg_query($conexion,$consultaC);
						$filas=pg_fetch_row($NumeroC);
						$congresoActual=$filas[0];	
						//////////////////////////////////////////// SE BUSCAN TODAS LAS PONENCIAS ORALES REGISTRADAS PARA EVALUAR POR EL USUARIO/////////////	
						$sql="SELECT p.id_ponencia FROM ponencias p, usuario_evalua_ponencias uep WHERE p.id_ponencia=uep.id_ponencias and uep.id_usuario='$usuario' and p.id_congreso ='$congresoActual' and p.id_congreso=uep.id_congreso and p.id_tipo_ponencia='PO'";
						// se consultan los resumenes Asignados  exclusivamente a un evaluador y en el ultimo congreso
						$result =pg_query($conexion, $sql);
					
						while($row=pg_fetch_array($result)){
						$mostrarid= $row['id_ponencia'];

							$validacion=pg_query($conexion,"SELECT apo.* FROM actualizacion_p_oral apo WHERE apo.id_ponencia_oral='$mostrarid' and apo.id_congreso='$congresoActual'");
							$row=pg_fetch_row($validacion);
							$validacion1=$row;
							if($validacion1==NULL){
								// SE VALIDA EL NOMBRE DE LA PONENCIA ORAL
								$datosP=pg_query($conexion,"SELECT ar.id_ponencia FROM actualizacion_resumen ar WHERE ar.id_ponencia='$mostrarid' and ar.id_congreso='$congresoActual'");
								$row3=pg_Fetch_array($datosP);
								
								if($row3['id_ponencia']==NULL){
									// Se obtienen los datos de la ponencia de la tabla ponencia
									$datosPo=pg_query($conexion,"SELECT p.id_ponencia, p.titulo, po.estatus_extenso FROM ponencias p,ponencia_oral po
																WHERE p.id_ponencia='$mostrarid' AND p.id_ponencia=po.id_ponencia_oral AND 
																p.id_congreso=po.id_congreso AND p.id_congreso='$congresoActual'");
									$rowD=pg_fetch_array($datosPo);
									$tituloP=$rowD['titulo'];
									$estatusPE=$rowD['estatus_extenso'];
									$id_ponencia=$rowD['id_ponencia'];
								}else{
									// Se obtienen los datos de la ponencia de la tabla actualizacion_resumen
									// Se obtiene el numero de la ultima actualizacion del resumen de la ponencia
									$numeroM=pg_query($conexion,"SELECT MAX(numeracion) FROM actualizacion_resumen WHERE id_congreso='$congresoActual' and id_ponencia='$mostrarid'");
									$row4=pg_fetch_row($numeroM);
									$numeroAC=$row4[0];
									// se obtienen los demas datos de la ponencia 
									$datosPo=pg_query($conexion,"SELECT ar.id_ponencia,ar.titulo,po.estatus_extenso FROM actualizacion_resumen ar, ponencia_oral po
																WHERE ar.id_ponencia='$mostrarid' and ar.id_congreso='$congresoActual' and po.id_ponencia_oral=ar.id_ponencia
																and po.id_congreso=ar.id_congreso and ar.numeracion='$numeroAC'");
									$rowD=pg_fetch_array($datosPo);
									$tituloP=$rowD['titulo'];
									$estatusPE=$rowD['estatus_extenso'];
									$id_ponencia=$rowD['id_ponencia'];
								}
										if($tituloP!=NULL){
											$tabla=" <tr>
															<td>
															$tituloP
															</td>
															<td> 
															$estatusPE  
															</td>
															<td align='center'>
																<form action='evaluacionep.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
																	<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
																	<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
																</form>
															</td>	
														</tr>";
														
											$tabla2=" <tr>
															<td>
															$tituloP
															</td>
															<td> 
															$estatusPE	  
															</td>
														</tr>";
													$estatusPv=trim($estatusPE);
													if($estatusPv!='Con Modificaciones' ){
														if( $estatusPv!='Aceptado'){
														echo $tabla."<br>";
														}else{
															echo $tabla2."<br>";
														}
													}else{
														echo $tabla2."<br>";
													}
																
										}
												
							}else{
									// Si existe una actualizacion del extenso se busca el extenso
									// Se obtiene el numero de la ultima actualizacion del extenso de la ponencia
									$numeroM=pg_query($conexion,"SELECT MAX(numeracion) FROM actualizacion_p_oral WHERE id_congreso='$congresoActual' and id_ponencia_oral='$mostrarid'");
									$row4=pg_fetch_row($numeroM);
									$numeroACP=$row4[0];
									// Se obtiene los demas datos de la ponencia
									// Se valida si existe actualizacion del resumen de la ponencia Oral
									$datosP=pg_query($conexion,"SELECT ar.id_ponencia FROM actualizacion_resumen ar WHERE ar.id_ponencia='$mostrarid' and ar.id_congreso='$congresoActual' 
																and ar.numeracion in (SELECT MAX(numeracion) FROM actualizacion_resumen WHERE id_ponencia='$mostrarid' and id_congreso='$congresoActual');");
									$row3=pg_fetch_array($datosP);
									if($row3['id_ponencia']==NULL){
										// Se obtienen los datos de la ponencia de la tabla ponencia
										$datosPo=pg_query($conexion,"SELECT p.id_ponencia, p.titulo, apo.estatus_actualizacion FROM ponencias p,actualizacion_p_oral apo
																	WHERE p.id_ponencia='$mostrarid' AND p.id_ponencia=apo.id_ponencia_oral AND 
																	p.id_congreso=apo.id_congreso AND p.id_congreso='$congresoActual'
																	and apo.numeracion='$numeroACP'");
										$rowD=pg_fetch_array($datosPo);
										$tituloP=$rowD['titulo'];
										$estatusPE=$rowD['estatus_actualizacion'];
										$id_ponencia=$rowD['id_ponencia'];
										
									}else{
										// Se obtienen los datos de la ponencia de la tabla actualizacion_resumen
										// Se obtiene el numero de la ultima actualizacion del resumen de la ponencia
										$numeroM=pg_query($conexion,"SELECT MAX(numeracion) FROM actualizacion_resumen WHERE id_congreso='$congresoActual' and id_ponencia='$mostrarid'");
										$row4=pg_fetch_row($numeroM);
										$numeroAC=$row4[0];
										// se obtienen los demas datos de la ponencia 
										$datosPo=pg_query($conexion,"SELECT ar.id_ponencia,ar.titulo,apo.estatus_actualizacion FROM actualizacion_resumen ar, actualizacion_p_oral apo
																	WHERE ar.id_ponencia='$mostrarid' and ar.id_congreso='$congresoActual' and apo.id_ponencia_oral=ar.id_ponencia
																	and apo.id_congreso=ar.id_congreso and ar.numeracion='$numeroAC'
																	and apo.numeracion='$numeroACP'");
										$rowD=pg_fetch_array($datosPo);
										$tituloP=$rowD['titulo'];
										$estatusPE=$rowD['estatus_actualizacion'];
										$id_ponencia=$rowD['id_ponencia'];
									}

								$tabla=" <tr>
											<td>
												$tituloP
											</td>
											<td> 
												$estatusPE  
											</td>
											<td align='center'>
												<form action='evaluacionep.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
													<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
													<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
												</form>
											</td>	
										</tr>";
								$tabla2="<tr>
											<td>
												$tituloP
											</td>
											<td> 
												$estatusPE	  
											</td>
										</tr>";
								$estatusPv=trim($estatusPE);
								if($estatusPv!='Con Modificaciones' ){
									if( $estatusPv!='Aceptado'){
									echo $tabla."<br>";
									}else{
										echo $tabla2."<br>";
									}
								}else{
									echo $tabla2."<br>";
								}
											
							}
							
						}
								?>
						</td>
					</tr>

					<tr>
						<td style="color:#052450;">
						Vídeo(s):
						</td>
						
					</tr>
					<?php
					////////////////////////////////////SE CONSULTA EL CONGRESO ACTUAL///////////////////////////////////////////////7
							$consultaC=" SELECT id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso ;";//selecciona el id maximo del congreso 
							$NumeroC=pg_query($conexion,$consultaC);
							$filas=pg_fetch_row($NumeroC);
							$congresoActual=$filas[0];		
					///////////////////////////////////SE OBTIENE EL ID DE LAS PONENCIAS ORALES///////////////////////////////////////	
							
							$sql="SELECT p.id_ponencia 
							FROM ponencias p, usuario_evalua_ponencias up 
							WHERE p.id_ponencia=up.id_ponencias 
							and up.id_usuario='$usuario' 
							and p.id_congreso ='$congresoActual' 
							and p.id_congreso=up.id_congreso 
							and p.id_tipo_ponencia='PO'
							and url_video is not null";
							$result =pg_query($conexion, $sql);
							$i=1;
							while($row=pg_fetch_array($result)){
							$mostrarid= $row['id_ponencia'];
							
							////////////////////////////////// SE CONSULTA LA URL DEL VIDE DE LA PONENCIA SELECCIONADA///////////////////////////
								$consultavalidacionV=@pg_query($conexion,"SELECT p.url_video
								FROM ponencias p
								WHERE p.id_congreso ='$congresoActual'
								and  p.id_tipo_ponencia='PO'
								and p.id_ponencia='$mostrarid'");

								$video=pg_fetch_row($consultavalidacionV);
								if($video[0]!=NULL){
									$validacionV=pg_Query($conexion,"SELECT ar.id_ponencia,titulo FROM actualizacion_resumen ar WHERE ar.id_congreso='$congresoActual' and ar.id_ponencia='$mostrarid' and ar.numeracion in (SELECT MAX(numeracion) FROM actualizacion_resumen WHERE id_congreso='$congresoActual' and id_ponencia='$mostrarid')");
									$rowValidacion=pg_fetch_array($validacionV);
									if($rowValidacion['id_ponencia']!=NULL){
										$consulta=pg_query($conexion,"SELECT p.estatus_video FROM ponencias p WHERE p.id_ponencia='$mostrarid' and p.id_congreso='$congresoActual'");
										$row4=pg_fetch_row($consulta);
										$tituloP=$rowValidacion['titulo'];
										$estatusP=$row4[0];
										$id_ponencia=$rowValidacion['id_ponencia'];

										
										
									}else{
										$consulta=@pg_query($conexion,"SELECT p.id_ponencia, p.titulo,p.estatus_video FROM ponencias p WHERE p.id_ponencia ='$mostrarid'  and p.id_congreso='$congresoActual'");
										$titulo=@pg_fetch_row($consulta);
										$tituloP=$titulo[1];
										$estatusP=$titulo[2];
										$id_ponencia=$titulo[0];
										
									}
									// "este es el titulo de la ponencia SIN modificaR $i".$tituloP.$estatusP.$id_ponencia."<br>";
								$tabla=" <tr>
												<td>
												$tituloP
												</td>
												<td> 
												$estatusP  
												</td>
												<td align='center'>
													<form action='evaluacionvp.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
														<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
														<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
													</form>
												</td>	
											</tr>";
											$tabla2=" <tr>
											<td>
											$tituloP
											</td>
											<td> 
											$estatusP  
											</td>
											
										</tr>";
										$estatusPv=trim($estatusP);
										if($estatusPv!='Con Modificaciones' ){
											if( $estatusPv!='Aceptado'){
											echo $tabla."<br>";
											}else{
												echo $tabla2."<br>";
											}
										}else{
											echo $tabla2."<br>";
										}
							}
						}

				
					//////////////////////////////////////////////////////////////////////////////////Hasta aqui se condiciona en primer IF para ponencias orales//////////////////////////////////////////////////////
					}
					
					/////////////////////////////////////////////////////////////////////Se condificona a que el evaluador debe ser Evaluador de carteles //////////////////////////////////////////7
					$RolEvaCa=@pg_query($conexion,"SELECT id_rol FROM usuario_roles WHERE id_usuario='$usuario'AND id_rol='EVCA12' AND estatus_rol='TRUE'");
					$RowEPO=@pg_fetch_row($RolEvaCa);
					if(@$RowEPO[0]!= NULL){
					?>
					

					<tr>
						<td style="color:#052450;">CARTEL(ES)</td>
					</tr>
					<?php
							$consultaC=" SELECT id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso ;";//selecciona el id maximo del congreso 
							$NumeroC=pg_query($conexion,$consultaC);
							$filas=pg_fetch_row($NumeroC);
							$congresoActual=$filas[0];		
							$sql="SELECT p.id_ponencia FROM ponencias p, usuario_evalua_ponencias uep WHERE p.id_ponencia=uep.id_ponencias and uep.id_usuario='$usuario' and p.id_congreso ='$congresoActual' and p.id_congreso=uep.id_congreso and p.id_tipo_ponencia='CA'";
						// se consultan los resumenes Asignados  exclusivamente a un evaluador y en el ultimo congreso
							$result =pg_query($conexion, $sql);
							$i=1;
							while($row=pg_fetch_array($result)){
							$mostrarid= $row['id_ponencia'];
								$validacion=pg_query($conexion,"SELECT * FROM actualizacion_resumen ar, ponencias p WHERE p.id_ponencia='$mostrarid' and p.id_ponencia=ar.id_ponencia");
								$row=pg_fetch_row($validacion);
								$validacion1=$row[0];
								if($validacion1==NULL){
									$consulta=pg_query($conexion,"SELECT p.id_ponencia, p.titulo,p.estatus_resumen FROM ponencias p, usuario_evalua_ponencias uep WHERE p.id_ponencia ='$mostrarid' and p.id_ponencia = uep.id_ponencias");
									$titulo=pg_fetch_row($consulta);
									$tituloP=$titulo[1];
									$estatusP=$titulo[2];
									$id_ponencia=$titulo[0];
									// "este es el titulo de la ponencia SIN modificaR $i".$tituloP.$estatusP.$id_ponencia."<br>";
								$tabla=" <tr>
												<td>
												$tituloP
												</td>
												<td> 
												$estatusP  
												</td>
												<td align='center'>
													<form action='evaluacionrc.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
														<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
														<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
													</form>
												</td>	
											</tr>";
											$tabla2=" <tr>
												<td>
												$tituloP
												</td>
												<td> 
												$estatusP  
												</td>
												<td align='center'>
													
												</td>	
											</tr>";
											$estatusPv=trim($estatusP);
										if($estatusPv!='Con Modificaciones' ){
											if( $estatusPv!='Aceptado'){
											echo $tabla."<br>";
											}else{
												echo $tabla2."<br>";
											}
										}else{
											echo $tabla2."<br>";
										}
								}else{
										$ultimaActualizacion=pg_query($conexion,"SELECT MAX(ar.numeracion) FROM actualizacion_resumen ar WHERE ar.id_congreso='$congresoActual' and id_ponencia='$mostrarid'");
										
										$row2=pg_fetch_row($ultimaActualizacion);
										$numeroid=$row2[0];
										$consulta=pg_query($conexion,"SELECT ar.id_ponencia, ar.titulo, ar.estatus_actualizacion FROM actualizacion_resumen ar,ponencias p, usuario_evalua_ponencias uep WHERE p.id_ponencia ='$mostrarid' and p.id_ponencia = uep.id_ponencias and p.id_ponencia = ar.id_ponencia and ar.numeracion='$numeroid'");
										$titulo=pg_fetch_row($consulta);
										$tituloP=$titulo[1];
										$estatusP=$titulo[2];
										$id_ponencia=$titulo[0];
										// "este es el titulo de la ponencia CON modificaR $i".$tituloP.$estatusP.$id_ponencia."<br>";
										$tabla=" <tr>
												<td>
												$tituloP
												</td>
												<td> 
												$estatusP  
												</td>
												<td align='center'>
													<form action='evaluacionrc.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
														<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
														<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
													</form>
												</td>	
											</tr>";
											$tabla2=" <tr>
											<td>
											$tituloP
											</td>
											<td> 
											$estatusP  
											</td>
											<td align='center'>
												
											</td>	
										</tr>";
										$estatusPv=trim($estatusP);
										if($estatusPv!='Con Modificaciones' ){
											if( $estatusPv!='Aceptado'){
											echo $tabla."<br>";
											}else{
												echo $tabla2."<br>";
											}
										}else{
											echo $tabla2."<br>";
										}
								}
								$i=$i+1;
							}
				?>
					

					<tr>
						<td>
						Imagen
						</td>
						
						<td>
						
						</td>
						
						<td align="center">
							<?php
								//////////////////////////////////////////////////////////Se consulta en congreso Actual//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							$consultaC=" SELECT id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso ;";//selecciona el id maximo del congreso 
							$NumeroC=pg_query($conexion,$consultaC);
							$filas=pg_fetch_row($NumeroC);
							$congresoActual=$filas[0];		
							//*******************************CONSULTA PARA MOSTRAR IMAGENES A EVALUAR POR EL USUARIO*******************************
							$consultaP=pg_query($conexion,"SELECT p.id_ponencia FROM ponencias p, usuario_evalua_ponencias uep, ponencia_cartel pc
									WHERE p.id_ponencia=uep.id_ponencias and uep.id_usuario='$usuario' 
									and p.id_congreso ='$congresoActual' and p.id_congreso=uep.id_congreso and p.id_tipo_ponencia='CA' and pc.id_ponencia_cartel=p.id_ponencia and pc.imagen is not null ");
							$iI=1;
							while($resultado=pg_fetch_array($consultaP)){
								$id_cartel=$resultado['id_ponencia'];
								$consultaActualizaciones=pg_query($conexion, "SELECT * FROM actualizacion_cartel ac, ponencias p WHERE p.id_ponencia='$id_cartel' and p.id_ponencia=ac.id_ponencia_cartel AND ac.id_congreso='$congresoActual'");
								$actualiacion=pg_fetch_row($consultaActualizaciones);
								$validacion=$actualiacion;

								if($validacion==NULL){//SI NO HAY ACTUALIACIONES
									//***************************************Validacion de titulo actualizado***************************************
									$ultimaActualizacionResumenC=pg_query($conexion,"SELECT id_ponencia FROM actualizacion_resumen ar WHERE ar.id_congreso='$congresoActual' and id_ponencia='$id_cartel' AND estatus_actualizacion='Aceptado'");
										
										$row2RPC=pg_fetch_row($ultimaActualizacionResumenC);
										$id_actualizacion_resumenC=@$row2RPC[0];
										
										if(!empty(@$id_actualizacion_resumenC)){//DE ACTUALIZACION RESUMEN

													$consultaI=pg_query($conexion, "SELECT p.id_ponencia, ar.titulo,pc.estatus_imagen FROM ponencias p, usuario_evalua_ponencias uep, ponencia_cartel as pc, actualizacion_resumen AS ar
														WHERE p.id_ponencia ='$id_cartel' and p.id_ponencia = uep.id_ponencias AND p.id_ponencia = ar.id_ponencia and uep.id_usuario='$usuario'
														and pc.id_ponencia_cartel= uep.id_ponencias and p.id_congreso='$congresoActual'");
											$imagen=pg_fetch_row($consultaI);
											$tituloC=@$imagen[1];
											$estatusC=@$imagen[2];
											$id_ponencia=@$imagen[0];
										$tabla=" <tr>
														<td>
														$tituloC
														</td>
														<td> 
														$estatusC  
														</td>
														<td align='center'>
															<form action='evaluacionic.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
																<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
																<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
															</form>
														</td>	
													</tr>";
													
														echo $tabla."<br>";
										$iI=$iI+1;	

										}else{
											$consultaI=pg_query($conexion, "SELECT p.id_ponencia, p.titulo,pc.estatus_imagen FROM ponencias p, usuario_evalua_ponencias uep, ponencia_cartel as pc
														WHERE p.id_ponencia ='$id_cartel' and p.id_ponencia = uep.id_ponencias and uep.id_usuario='$usuario'
														and pc.id_ponencia_cartel= uep.id_ponencias and p.id_congreso='$congresoActual'");
											$imagen=pg_fetch_row($consultaI);
											$tituloC=@$imagen[1];
											$estatusC=@$imagen[2];
											$id_ponencia=@$imagen[0];
										$tabla=" <tr>
														<td>
														$tituloC
														</td>
														<td> 
														$estatusC  
														</td>
														<td align='center'>
															<form action='evaluacionic.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
																<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
																<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
															</form>
														</td>	
													</tr>";
													
														echo $tabla."<br>";
										$iI=$iI+1;	
										}		
								
									
								}else{//**********************************DE LA TABLA ACTUALIZACION PONENCIAS**************************
									$ultimaActMax=pg_query($conexion, "SELECT MAX(ac.id_actualizacion) FROM actualizacion_cartel ac, ponencias p WHERE p.id_ponencia='$id_cartel' and p.id_ponencia=ac.id_ponencia_cartel");

									$id_ultimo=pg_fetch_row($ultimaActMax);
									//****************************************VALIDACION DE TITULO ACTUALIZADO****************************
									$ultimaActualizacionResumenPC=pg_query($conexion,"SELECT ar.id_ponencia FROM actualizacion_resumen ar, actualizacion_cartel AS apc WHERE  apc.id_ponencia_cartel = '$id_cartel' and ar.id_ponencia = apc.id_ponencia_cartel and apc.id_actualizacion = '$id_ultimo[0]' and apc.id_congreso='$congresoActual'");
										$rowATC=pg_fetch_row($ultimaActualizacionResumenPC);
										$id_actualizacion_resumen=@$rowATC[0];
										
										if(!empty(@$id_actualizacion_resumen)){
										$consultaInfoActualizacion=pg_query($conexion, "SELECT ac.id_ponencia_cartel, ac.estatus_actualizacion, ar.titulo FROM actualizacion_cartel ac, actualizacion_resumen ar WHERE ac.id_actualizacion='$id_ultimo[0]' and ar.id_ponencia=ac.id_ponencia_cartel AND ac.id_congreso='$congresoActual'");
									while($actualizacionImagen=pg_fetch_row($consultaInfoActualizacion)){
										
									$tituloA=@$actualizacionImagen[2];
									$estatusA=@$actualizacionImagen[1];
									$id_ponencia=@$actualizacionImagen[0];
									
								$tabla=" <tr>
												<td>
												$tituloA
												</td>
												<td> 
												$estatusA  
												</td>
												<td align='center'>
													<form action='evaluacionic.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
														<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
														<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
													</form>
												</td>	
											</tr>";
											
											ECHO $tabla;
										
								$iI=$iI+1;	
										} //WHILE

										}else{
								
									$consultaInfoActualizacion=pg_query($conexion, "SELECT ac.id_ponencia_cartel, ac.estatus_actualizacion, p.titulo FROM actualizacion_cartel ac, ponencias p WHERE ac.id_actualizacion='$id_ultimo[0]' and p.id_ponencia=ac.id_ponencia_cartel AND ac.id_congreso='$congresoActual'");
									while($actualizacionImagen=pg_fetch_row($consultaInfoActualizacion)){
										
									$tituloA=@$actualizacionImagen[2];
									$estatusA=@$actualizacionImagen[1];
									$id_ponencia=@$actualizacionImagen[0];
									
								$tabla=" <tr>
												<td>
												$tituloA
												</td>
												<td> 
												$estatusA  
												</td>
												<td align='center'>
													<form action='evaluacionic.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
														<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
														<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
													</form>
												</td>	
											</tr>";
											
											ECHO $tabla;
										
								$iI=$iI+1;	
										} //WHILE
									}//TITULO ACTUALIZADO 
								

								}
							}
							?>
						</td>
					</tr>



					<tr>
						<td>
						Video 
						</td>
					</tr>
					<?php
					////////////////////////////////////SE CONSULTA EL CONGRESO ACTUAL///////////////////////////////////////////////7
							$consultaC=" SELECT id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso ;";//selecciona el id maximo del congreso 
							$NumeroC=pg_query($conexion,$consultaC);
							$filas=pg_fetch_row($NumeroC);
							$congresoActual=$filas[0];		
					///////////////////////////////////SE OBTIENE EL ID DE LAS PONENCIAS ORALES///////////////////////////////////////	
							
							$sql="SELECT p.id_ponencia 
							FROM ponencias p, usuario_evalua_ponencias up 
							WHERE p.id_ponencia=up.id_ponencias 
							and up.id_usuario='$usuario' 
							and p.id_congreso ='$congresoActual' 
							and p.id_congreso=up.id_congreso 
							and p.id_tipo_ponencia='CA'
							and url_video is not null";
							$result =pg_query($conexion, $sql);
							$i=1;
							while($row=pg_fetch_array($result)){
							$mostrarid= $row['id_ponencia'];
							
							////////////////////////////////// SE CONSULTA LA URL DEL VIDE DE LA PONENCIA SELECCIONADA///////////////////////////
								$consultavalidacionV=@pg_query($conexion,"SELECT p.url_video
								FROM ponencias p
								WHERE p.id_congreso ='$congresoActual'
								and  p.id_tipo_ponencia='CA'
								and p.id_ponencia='$mostrarid'");

								$video=pg_fetch_row($consultavalidacionV);
								if($video[0]!=NULL){
									$validacionV=pg_Query($conexion,"SELECT ar.id_ponencia,titulo FROM actualizacion_resumen ar WHERE ar.id_congreso='$congresoActual' and ar.id_ponencia='$mostrarid' and ar.numeracion in (SELECT MAX(numeracion) FROM actualizacion_resumen WHERE id_congreso='$congresoActual' and id_ponencia='$mostrarid')");
									$rowValidacion=pg_fetch_array($validacionV);
									if($rowValidacion['id_ponencia']!=NULL){
										$consulta=pg_query($conexion,"SELECT p.estatus_video FROM ponencias p WHERE p.id_ponencia='$mostrarid' and p.id_congreso='$congresoActual'");
										$row4=pg_fetch_row($consulta);
										$tituloP=$rowValidacion['titulo'];
										$estatusP=$row4[0];
										$id_ponencia=$rowValidacion['id_ponencia'];

										
										
									}else{
										$consulta=@pg_query($conexion,"SELECT p.id_ponencia, p.titulo,p.estatus_video FROM ponencias p WHERE p.id_ponencia ='$mostrarid'  and p.id_congreso='$congresoActual'");
										$titulo=@pg_fetch_row($consulta);
										$tituloP=$titulo[1];
										$estatusP=$titulo[2];
										$id_ponencia=$titulo[0];
										
									}
									// "este es el titulo de la ponencia SIN modificaR $i".$tituloP.$estatusP.$id_ponencia."<br>";
								$tabla=" <tr>
												<td>
												$tituloP
												</td>
												<td> 
												$estatusP  
												</td>
												<td align='center'>
													<form action='evaluacionvc.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
														<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
														<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
													</form>
												</td>	
											</tr>";
											$tabla2=" <tr>
											<td>
											$tituloP
											</td>
											<td> 
											$estatusP  
											</td>
											
										</tr>";
										$estatusPv=trim($estatusP);
										if($estatusPv!='Con Modificaciones' ){
											if( $estatusPv!='Aceptado'){
											echo $tabla."<br>";
											}else{
												echo $tabla2."<br>";
											}
										}else{
											echo $tabla2."<br>";
										}
							}
						}
?>
					
					<?php
					//////////////////////////////////////////////////////////////////////////////////Hasta aqui se condiciona en primer IF para carteles//////////////////////////////////////////////////////
					}
					
					/////////////////////////////////////////////////////////////////////Se condificona a que el evaluador debe ser Evaluador de talleres //////////////////////////////////////////7
					$RolEvata=@pg_query($conexion,"SELECT id_rol FROM usuario_roles WHERE id_usuario='$usuario'AND id_rol='ECC013' AND estatus_rol='TRUE'");
					$RowETA=@pg_fetch_row($RolEvata);
					if(@$RowETA[0]!= NULL){
					?>

					<tr>
						<td style="color:#052450;">TALLER(ES)</td>
					</tr>
					<?php
							$consultaC=" SELECT id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso ;";//selecciona el id maximo del congreso 
							$NumeroC=pg_query($conexion,$consultaC);
							$filas=pg_fetch_row($NumeroC);
							$congresoActual=$filas[0];		
							$sql="SELECT p.id_ponencia FROM ponencias p, usuario_evalua_ponencias uep WHERE p.id_ponencia=uep.id_ponencias and uep.id_usuario='$usuario' and p.id_congreso ='$congresoActual' and p.id_congreso=uep.id_congreso and p.id_tipo_ponencia='TA'";
						// se consultan los resumenes Asignados  exclusivamente a un evaluador y en el ultimo congreso
							$result =pg_query($conexion, $sql);
							while($row=pg_fetch_array($result)){
							$mostrarid= $row['id_ponencia'];
							
								$validacion=pg_query($conexion,"SELECT ar.id_ponencia FROM actualizacion_resumen ar WHERE ar.id_ponencia='$mostrarid'  and ar.id_congreso='$congresoActual' and ar.numeracion in (SELECT MAX(ar.numeracion) FROM actualizacion_resumen ar WHERE ar.id_ponencia='$mostrarid'  and ar.id_congreso='$congresoActual')  ");
								$row=pg_fetch_row($validacion);
								$validacion1=$row[0];
								if($validacion1==NULL){
									$consulta=@pg_query($conexion,"SELECT p.id_ponencia, p.titulo,p.estatus_resumen FROM ponencias p, usuario_evalua_ponencias uep WHERE p.id_ponencia ='$mostrarid' and p.id_ponencia = uep.id_ponencias");
									$titulo=@pg_fetch_row($consulta);
									$tituloP=@$titulo[1];
									$estatusP=@$titulo[2];
									$id_ponencia=@$titulo[0];
									// "este es el titulo de la ponencia SIN modificaR $i".$tituloP.$estatusP.$id_ponencia."<br>";
								$tabla=" <tr>
												<td>
												$tituloP
												</td>
												<td> 
												$estatusP  
												</td>
												<td align='center'>
													<form action='evaluacionrt.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
														<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
														<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
													</form>
												</td>	
											</tr>";
											$tabla2=" <tr>
												<td>
												$tituloP
												</td>
												<td> 
												$estatusP  
												</td>
												<td align='center'>
													
												</td>	
											</tr>";
											$estatusPv=trim($estatusP);
										if($estatusPv!='Con Modificaciones' ){
											if( $estatusPv!='Aceptado'){
											echo $tabla."<br>";
											}else{
												echo $tabla2."<br>";
											}
										}else{
											echo $tabla2."<br>";
										}
								}else{
										$ultimaActualizacion=pg_query($conexion,"SELECT MAX(ar.numeracion) FROM actualizacion_resumen ar WHERE ar.id_congreso='$congresoActual' and id_ponencia='$mostrarid'");
										
										$row2=pg_fetch_row($ultimaActualizacion);
										$numeroid=$row2[0];
										$consulta=@pg_query($conexion,"SELECT ar.id_ponencia, ar.titulo, ar.estatus_actualizacion FROM actualizacion_resumen ar,ponencias p, usuario_evalua_ponencias uep WHERE p.id_ponencia ='$mostrarid' and p.id_ponencia = uep.id_ponencias and p.id_ponencia = ar.id_ponencia and ar.numeracion='$numeroid'");
										$titulo=@pg_fetch_row($consulta);
										$tituloP=@$titulo[1];
										$estatusP=@$titulo[2];
										$id_ponencia=@$titulo[0];
										// "este es el titulo de la ponencia CON modificaR $i".$tituloP.$estatusP.$id_ponencia."<br>";
										$tabla=" <tr>
												<td>
												$tituloP
												</td>
												<td> 
												$estatusP  
												</td>
												<td align='center'>
													<form action='evaluacionrt.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
														<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
														<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
													</form>
												</td>	
											</tr>";$tabla2=" <tr>
											<td>
											$tituloP
											</td>
											<td> 
											$estatusP  
											</td>
											<td align='center'>
												
											</td>	
										</tr>";
										$estatusPv=trim($estatusP);
										if($estatusPv!='Con Modificaciones' ){
											if( $estatusPv!='Aceptado'){
											echo $tabla."<br>";
											}else{
												echo $tabla2."<br>";
											}
										}else{
											echo $tabla2."<br>";
										}
								}
								$i=$i+1;
							}
				?>
					
					<tr>
						<td>
						Video
						</td>
					</tr>
					<?php
							
							////////////////////////////////////SE CONSULTA EL CONGRESO ACTUAL///////////////////////////////////////////////7
									$consultaC=" SELECT id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso ;";//selecciona el id maximo del congreso 
									$NumeroC=pg_query($conexion,$consultaC);
									$filas=pg_fetch_row($NumeroC);
									$congresoActual=$filas[0];		
							///////////////////////////////////SE OBTIENE EL ID DE LAS PONENCIAS ORALES///////////////////////////////////////	
									
									$sql="SELECT p.id_ponencia 
									FROM ponencias p, usuario_evalua_ponencias up 
									WHERE p.id_ponencia=up.id_ponencias 
									and up.id_usuario='$usuario' 
									and p.id_congreso ='$congresoActual' 
									and p.id_congreso=up.id_congreso 
									and p.id_tipo_ponencia='TA'
									and url_video is not null";
									$result =pg_query($conexion, $sql);
									$i=1;
									while($row=pg_fetch_array($result)){
									$mostrarid= $row['id_ponencia'];
									
									////////////////////////////////// SE CONSULTA LA URL DEL VIDE DE LA PONENCIA SELECCIONADA///////////////////////////
										$consultavalidacionV=@pg_query($conexion,"SELECT p.url_video
										FROM ponencias p
										WHERE p.id_congreso ='$congresoActual'
										and  p.id_tipo_ponencia='TA'
										and p.id_ponencia='$mostrarid'");
		
										$video=pg_fetch_row($consultavalidacionV);
										if($video[0]!=NULL){
											$validacionV=pg_Query($conexion,"SELECT ar.id_ponencia,titulo FROM actualizacion_resumen ar WHERE ar.id_congreso='$congresoActual' and ar.id_ponencia='$mostrarid' and ar.numeracion in (SELECT MAX(numeracion) FROM actualizacion_resumen WHERE id_congreso='$congresoActual' and id_ponencia='$mostrarid')");
											$rowValidacion=pg_fetch_array($validacionV);
											if($rowValidacion['id_ponencia']!=NULL){
												$consulta=pg_query($conexion,"SELECT p.estatus_video FROM ponencias p WHERE p.id_ponencia='$mostrarid' and p.id_congreso='$congresoActual'");
												$row4=pg_fetch_row($consulta);
												$tituloP=$rowValidacion['titulo'];
												$estatusP=$row4[0];
												$id_ponencia=$rowValidacion['id_ponencia'];
		
												
												
											}else{
												$consulta=@pg_query($conexion,"SELECT p.id_ponencia, p.titulo,p.estatus_video FROM ponencias p WHERE p.id_ponencia ='$mostrarid'  and p.id_congreso='$congresoActual'");
												$titulo=@pg_fetch_row($consulta);
												$tituloP=$titulo[1];
												$estatusP=$titulo[2];
												$id_ponencia=$titulo[0];
												
											}
											// "este es el titulo de la ponencia SIN modificaR $i".$tituloP.$estatusP.$id_ponencia."<br>";
										$tabla=" <tr>
														<td>
														$tituloP
														</td>
														<td> 
														$estatusP  
														</td>
														<td align='center'>
															<form action='evaluacionvt.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
																<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
																<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
															</form>
														</td>	
													</tr>";
													$tabla2=" <tr>
													<td>
													$tituloP
													</td>
													<td> 
													$estatusP  
													</td>
													
												</tr>";
												$estatusPv=trim($estatusP);
												if($estatusPv!='Con Modificaciones' ){
													if( $estatusPv!='Aceptado'){
													echo $tabla."<br>";
													}else{
														echo $tabla2."<br>";
													}
												}else{
													echo $tabla2."<br>";
												}
									}
								}
		
				?>
					<?php 
					}





						$consulta=pg_query($conexion,"SELECT r.id_rol FROM usuario_roles r WHERE r.id_usuario= '$usuario' and r.id_rol = 'COP024'");
						$row=pg_fetch_row($consulta);
						
						$rol=@$row[0];
						
						if(!empty($rol)){
							
						
						?>
					<tr>
						<td style="color:#052450;">PROTOTIPO(S)</td>
					</tr>

					<?php
							$consultaC=" SELECT id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso ;";//selecciona el id maximo del congreso 
							$NumeroC=pg_query($conexion,$consultaC);
							$filas=pg_fetch_row($NumeroC);
							$congresoActual=$filas[0];		
							$sql="SELECT pr.id_prototipo FROM prototipos pr, usuario_evalua_prototipo uep WHERE pr.id_congreso='$congresoActual' and uep.id_usuario ='$usuario' and uep.id_prototipo=pr.id_prototipo and uep.id_congreso=pr.id_congreso";
						// se consultan los resumenes Asignados  exclusivamente a un evaluador y en el ultimo congreso
							$result =pg_query($conexion, $sql);
							$i=1;
							while($row=pg_fetch_array($result)){
							$mostrarid= $row['id_prototipo'];
								$validacion=pg_query($conexion,"SELECT * FROM actualizacion_resumen_pro ar, prototipos p WHERE p.id_prototipo='$mostrarid' and p.id_prototipo=ar.id_prototipo AND p.id_congreso='$congresoActual' and ar.id_congreso=p.id_congreso");
								$row=@pg_fetch_row($validacion);
								$validacion1=@$row[0];
								
								if($validacion1==NULL){
									$consulta=pg_query($conexion,"SELECT p.id_prototipo, p.nombre,p.estatus_prototipos FROM prototipos p, usuario_evalua_prototipo uep WHERE p.id_prototipo ='$mostrarid' and p.id_prototipo = uep.id_prototipo and uep.id_usuario='$usuario' and uep.id_congreso='$congresoActual' and uep.id_congreso=p.id_congreso");
									$titulo=pg_fetch_row($consulta);
									$tituloP=$titulo[1];
									$estatusP=$titulo[2];
									$id_ponencia=$titulo[0];
									// "este es el titulo de la ponencia SIN modificaR $i".$tituloP.$estatusP.$id_ponencia."<br>";
								$tabla=" <tr>
												<td>
												$tituloP
												</td>
												<td> 
												$estatusP  
												</td>
												<td align='center'>
													<form action='evaluacionrpr.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
														<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
														<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
													</form>
												</td>	
											</tr>";
											$tabla2=" <tr>
											<td>
											$tituloP
											</td>
											<td> 
											$estatusP  
											</td>
											<td align='center'>
												
											</td>	
										</tr>";
										$estatusPv=trim($estatusP);
										if($estatusPv!='Con Modificaciones' ){
											if( $estatusPv!='Aceptado'){
											echo $tabla."<br>";
											}else{
												echo $tabla2."<br>";
											}
										}else{
											echo $tabla2."<br>";
										}
								}else{
										$ultimaActualizacion=pg_query($conexion,"SELECT MAX(ar.numeracion) FROM actualizacion_resumen_pro ar WHERE ar.id_congreso='$congresoActual' and id_prototipo='$mostrarid'");
										
										$row2=pg_fetch_row($ultimaActualizacion);
										$numeroid=$row2[0];
										$consulta=pg_query($conexion,"SELECT ar.id_prototipo, ar.titulo, ar.estatus_r_p FROM actualizacion_resumen_pro ar,prototipos p, usuario_evalua_prototipo uep 
										WHERE p.id_prototipo ='$mostrarid' and p.id_prototipo = uep.id_prototipo and p.id_prototipo = ar.id_prototipo and ar.numeracion='$numeroid'");
										$titulo=pg_fetch_row($consulta);
										$tituloP=@$titulo[1];
										$estatusP=@$titulo[2];
										$id_ponencia=@$titulo[0];
										// "este es el titulo de la ponencia CON modificaR $i".$tituloP.$estatusP.$id_ponencia."<br>";
										$tabla=" <tr>
												<td>
												$tituloP
												</td>
												<td> 
												$estatusP  
												</td>
												<td align='center'>
													<form action='evaluacionrpr.php' method='POST'><!-- Se envia el titulo a la pagina editar, por metodo post-->
														<input  name='id_ponencia' type='hidden' value='$id_ponencia'>
														<input type='submit' value='Evaluar' style='margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;'>
													</form>
												</td>	
											</tr>";$tabla2=" <tr>
											<td>
											$tituloP
											</td>
											<td> 
											$estatusP  
											</td>
											<td align='center'>
												
											</td>	
										</tr>";
										$estatusPv=trim($estatusP);
										if($estatusPv!='Con Modificaciones' ){
											if( $estatusPv!='Aceptado'){
											echo $tabla."<br>";
											}else{
												echo $tabla2."<br>";
											}
										}else{
											echo $tabla2."<br>";
										}
								}
								$i=$i+1;
							}
						}
				?>
				</table>
				
			</div>
		</div>
	 </div>
	 
    <!-- End CONTENIDO -->
    
  <!-- Start Footer -->
        <footer class="footer-box">
        <div class="container">
            <div class="row">
               <div class="col-md-12 white_fonts" style="display: contents;">
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            
                            <div class="full">
							<h3>Dirección</h3>
                                <ul class="menu_footer">   
								     
								     <li><img src="images/i5.png"><span style="font-size: 85%;">Carr, Cuautitlán - Teoloyucan, San Sebastian Xhala, 54714 Cuautitlán Izcalli</span></li>

                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="full">
                                <div class="footer_blog full white_fonts">
                             <h3>Contacto</h3>
                             <ul class="full">
                               <li><img src="images/i6.png"><span style="font-size: 85%;"> <?php echo $correoC  ?></span></li>
                               <li><img src="images/i7.png"><span style="font-size: 85%;"><?php echo "$numeroC1    $numeroC2"; ?></span></li>
                             </ul>
                         </div>
                            </div>
                        </div>
                </div>
			 </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link active" href="derechosa.php" style="color:white">© Sitio desarrollado por CDD Solutions S.A. y administrado por el Departamento de Matemáticas. Hecho en México, (2020 - 2021)</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
	/* counter js */

(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it fVerst
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for eVery time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
	</script>
	
	
</body>

</html>