<?php 
session_start();

if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
$usuario=$_SESSION['Usuario'];
include ('Conexion.php');
//VALIDAR FECHA PARA EL CONCURSO A CARTELES	
	date_default_timezone_set("America/Mexico_City");
	$date_nueva=date("d-m-Y");	
	// FUNCION PARA VALIDAR FECHA CONCURSO DE CARTELES
function verifica_rango_C($date_inicio, $date_fin, $date_nueva) {
    $date_inicio = strtotime($date_inicio);
    $date_fin = strtotime($date_fin);
    $date_nueva = strtotime($date_nueva);
    if (($date_nueva >= $date_inicio) && ($date_nueva <= $date_fin)){
        return true;//SI SE ENCUENTRA DENTRO DEL RANGO
    }
        
    return false;//SI NO SE ENCUENTRA DENTRO DEL RANGo
 }

//************************
if(!isset($_POST["id_ponencia"])){
	echo"<script>alert('Selecciona el trabajo a evaluar');window.location='evaluartr.php'</script>";
}else{
$clave=$_POST["id_ponencia"];// se recibe el id de la ponencia a evaluar 	
}		 

$consultaValidacion="SELECT * FROM actualizacion_cartel WHERE id_ponencia_cartel='$clave'";
$result=pg_query($conexion,$consultaValidacion);
$row=pg_fetch_row($result);
$validacion=@$row[0];
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
$row5=pg_fetch_row($consulta);
$consulta0=$row5[0];
$consulta1=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$consulta0'");
$row6=pg_fetch_row($consulta1);
$numeroCongreso=$row6[0];// se obtiene en id del congreso actual

$FechasI=pg_query($conexion,"SELECT f.fecha_inicio, f.fecha_fin FROM (SELECT (id_congreso) as id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso) C, fechas_importantes f
			WHERE f.id_congreso= C.id_congreso and f.descripcion= 'Recepción de Carteles' or f.descripcion= 'Recepción de Imagen para Carteles' or f.descripcion= 'Recepción de Imágenes'");// trae las fechas de la tabla Fechas importantes
			$row=pg_fetch_row($FechasI);
			$date_inicio=$row[0];
			$date_fin=$row[1];
$FechasConcurso=pg_query($conexion,"SELECT f.fecha_inicio, f.fecha_fin FROM (SELECT (id_congreso) as id_congreso  FROM congreso c, (SELECT MAX(numero_congreso)as id FROM congreso) t WHERE t.id = c.numero_congreso) C, fechas_importantes f
			WHERE f.id_congreso= C.id_congreso and f.descripcion= 'Concurso de Carteles' or f.descripcion= 'Evaluación del Concurso de Carteles'");// trae las fechas de la tabla Fechas importantes
			$row=pg_fetch_row($FechasConcurso);
			$date_inicioConcurso=$row[0];
			$date_finConcurso=$row[1];
//**********************************************************************************INFO USUARIO ************************************************************************
$consultaUsuario=pg_query($conexion, "SELECT  nombres||' '|| primer_ap||' '|| segundo_ap as evaluador
FROM usuario WHERE id_usuario='$usuario'");
$nombreEva=pg_fetch_row($consultaUsuario);
if(!empty($validacion)){//SI TRAE ALGO DE ACTUALIZAR
      $consulta1=pg_query($conexion,"SELECT MAX(id_actualizacion) FROM actualizacion_cartel  where id_congreso='$numeroCongreso' and id_ponencia_cartel ='$clave'");
    $row7=pg_fetch_row($consulta1);
    $numeroid=$row7[0];
	$Qtitulo="SELECT  u.nombres||' '|| u.primer_ap||' '|| u.segundo_ap as autor, p.titulo, apc.estatus_actualizacion, apc.imagen, apc.observaciones  
	FROM ponencias p, usuario as u, actualizacion_cartel as apc, usuario_ponencias as up
	WHERE p.id_ponencia='$clave'and p.id_ponencia=apc.id_ponencia_cartel and apc.id_actualizacion='$numeroid'
	AND up.id_usuario=u.id_usuario and apc.id_congreso='$numeroCongreso' AND up.tipo_autor='Autor' and up.id_ponencias=p.id_ponencia";
									$consulta2=pg_query($conexion,$Qtitulo);
									$row2=pg_fetch_row($consulta2);
									$autor=trim($row2[0]);
									
									$estatus=$row2[2];
									$imagen=$row2[3];
									$observacion=$row2[4];
									$idActualizacion=$numeroid;
									//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
						$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$clave' AND id_congreso='$numeroCongreso' AND estatus_actualizacion='Aceptado'");
						$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);	

						if(!empty($actualizacionTitulo)){
						$titulo=$actualizacionTitulo['titulo'];

						}else{
						$titulo=trim($row2[1]);
						}


if(verifica_rango_C($date_inicio, $date_fin, $date_nueva)==false){
			echo"<script>alert('Para evaluar el cartel recuerde realizarlo dentro de las fechas indicadas en la Convocatoria.');window.location='menu.php'</script>";
	}else{

		if($estatus==='Enviado'){		
		  $actualizacion2=pg_query($conexion,"UPDATE actualizacion_cartel SET estatus_actualizacion ='En Evaluacion' WHERE id_actualizacion='$numeroid'");
		}else if($estatus!= 'Enviado' && $estatus!='Aceptado' && $estatus!='En Evaluacion'){
				echo"<script>alert('Ya evaluaste este cartel. Para el concurso de carteles recuerda que es dentro de las fechas que se encuentran en la convocatoria.');window.location='menu.php'</script>";
		}
	}



if($estatus==='Aceptado'){
	if(verifica_rango_C($date_inicio, $date_fin, $date_nueva)==false){
		echo"<script>alert('Ya evaluaste este cartel, para el concurso de carteles recuerda que es dentro de las fechas que se encuentran en la convocatoria.');window.location='menu.php'</script>";
	}if(verifica_rango_C($date_inicioConcurso, $date_finConcurso, $date_nueva)==false){
			echo"<script>alert('Este cartel ya se encuentra evaluado. Para el concurso de carteles recuerde verificar las fechas indicadas en la Convocatoria.');window.location='menu.php'</script>";
	}else if(verifica_rango_C($date_inicioConcurso, $date_finConcurso, $date_nueva)==true){ 
			$banderaFecha="1";//SI SE ENCUENTRA DENTRO DE LAS FECHAS
	}
}							
									

	
}else{
	$Qtitulo="SELECT  u.nombres||' '|| u.primer_ap||' '|| u.segundo_ap as autor, p.titulo, pc.estatus_imagen, pc.imagen, pc.observaciones   
						FROM ponencias p, usuario as u, ponencia_cartel as pc, usuario_ponencias as up
						WHERE p.id_ponencia='$clave'and p.id_ponencia=pc.id_ponencia_cartel
						AND up.id_usuario=u.id_usuario and pc.id_congreso='$numeroCongreso' AND up.tipo_autor='Autor' and up.id_ponencias=p.id_ponencia";
									$consulta2=pg_query($conexion,$Qtitulo);
									$row2=pg_fetch_row($consulta2);
									$autor=trim($row2[0]);
									
									$estatus=$row2[2];
									$imagen=$row2[3];
									$observacion=$row2[4];
									$idActualizacion="";

									//VALIDACION DE NOMBRE ACTUALIZACION RESUMEN
						$consultaTitulo=pg_query($conexion, "SELECT titulo FROM actualizacion_resumen WHERE id_ponencia= '$clave' AND id_congreso='$numeroCongreso' AND estatus_actualizacion='Aceptado'");
						$actualizacionTitulo=pg_fetch_assoc($consultaTitulo);	

						if(!empty($actualizacionTitulo)){
						$titulo=$actualizacionTitulo['titulo'];

						}else{
						$titulo=trim($row2[1]);
						}


	if(verifica_rango_C($date_inicio, $date_fin, $date_nueva)==false){
			echo"<script>alert('Para evaluar el cartel recuerde realizarlo dentro de las fechas indicadas en la Convocatoria.');window.location='menu.php'</script>";
	}else{									
			if($estatus === 'Enviado'){
			$actualizacion2="UPDATE ponencia_cartel SET estatus_imagen ='En Evaluacion' WHERE id_ponencia_cartel='$clave'";
			pg_query($conexion,$actualizacion2); 
			}if($estatus !='Enviado' && $estatus!='Aceptado' && $estatus!='En Evaluacion'){
				echo"<script>alert('Ya evaluaste este cartel. Para el concurso de carteles recuerda que es dentro de las fechas que se encuentran en la convocatoria.');window.location='menu.php'</script>";
			}
	}
	if($estatus==='Aceptado'){
		if(verifica_rango_C($date_inicio, $date_fin, $date_nueva)==false){
			echo"<script>alert('Ya evaluaste este cartel. Para el concurso de carteles recuerda que es dentro de las fechas que se encuentran en la convocatoria.');window.location='menu.php'</script>";
		}
		if(verifica_rango_C($date_inicioConcurso, $date_finConcurso, $date_nueva)==false){
			echo"<script>alert('Este cartel ya se encuentra evaluado. Para el concurso de carteles recuerde verificar las fechas indicadas en la Convocatoria.');window.location='menu.php'</script>";
		}else if(verifica_rango_C($date_inicioConcurso, $date_finConcurso, $date_nueva)==true){ 
			$banderaFecha="1";//SI SE ENCUENTRA DENTRO DE LAS FECHAS
		}
	}	
}
 
 	
 	
 
?>
<?php 
include ("Conexion.php");
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
					$row0=pg_fetch_row($consulta);
					$consulta0=$row0[0];
					$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
					$row=pg_fetch_row($consulta1);
					$congresoActual=$row[0];//es el id congreso
$contacto=pg_query($conexion,"SELECT cn.telefono,cn.telefono2,cn.email FROM contacto cn, congreso c
WHERE c.id_congreso='$congresoActual'
and  c.id_contacto =cn.id_contacto");
$contactos=pg_fetch_array($contacto);
$correoC=@$contactos[2];
$numeroC1=@$contactos[0];
$numeroC2=@$contactos[1];
?><!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title style="background-image: url('images/logos.jpg');">CISEMATH</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/cismath.png">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" />
	<script
	src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>


    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98" onload="tipoEvaluacion();">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->

      <?php
    ////Codigo para actualizar el banner
        include ('Conexion.php');
        $nmax=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
        $num=pg_fetch_row($nmax);
        $numcon=$num[0];
        $conact=pg_query($conexion,"SELECT id_congreso, banner FROM congreso WHERE numero_congreso='$numcon'");
        $ban=pg_fetch_row($conact);
        $banner=$ban[1];
        echo '<img src="'.$banner.'" style="width:100%; height:100%; background-size: cover; position: relative; display: flex;"/>';
    ?>

    <header class="top-header" >
        <div class="header_bottom">
          <div class="container">
		   <div class="row">
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 col-xl-12">
                <div class="menu_orange_section" style="background: #052450;">
                   <nav class="navbar header-nav navbar-expand-lg"> 
                     <div class="menu_section">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav" >
					    <li><a class="nav-link active" href="indexus.php"><img src="images/i4.png" alt="image"></a> </li>
                        <li><a class="nav-link active" href="acercadeus.php">Acerca de</a></li>
                        <li><a class="nav-link active" href="infus.php">Información</a></li>
                        <li><a class="nav-link active" href="instruccionesus.php">Instrucciones</a></li>
                        <li><a class="nav-link active" href="comitesus.php">Comités</a></li>
						<li><a class="nav-link active" href="memoriasus.php">Memorias</a></li>
						
						<li>
						<a style="position:relative; display:block;">
						<img src="images/user.png" alt="image"> 
						</a>
						</li>
						<li>
						<ul class="nav"><li class="dropdown langmenu" ><a href="#" style="display:block;"class="dropdown-toggle" data-toggle="dropdown" title="Idioma"><b class="caret"></b></a><ul class="dropdown-menu" style="color:black;"><li><a title="English ‎(en)‎" href="menu.php" style="color:black";>Menú‎‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="editarp.php"  style="color:black";>Perfil‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="index.php"  style="color:black";>Cerrar Sesión</a></li></ul></li></ul>
						</li>
						
                    </ul>
                </div>
                     </div>
                 </nav>
                 
                </div>
            </div>
          </div>
		  </div>
        </div>
        
    </header>
    <!-- End header -->
 
 <!-- Start CONTENIDO -->
	<?php 
 		$consultaCC=pg_query($conexion,"SELECT * FROM usuario_evalua_cartel
	WHERE id_usuario = '$usuario' and id_ponencia_cartel = '$clave' and id_congreso='$numeroCongreso'");
	$resultadoCC=pg_fetch_row($consultaCC);
	
	
	if (!empty($consultaCC) && $resultadoCC!== false && !empty($banderaFecha)) { ?>
		<input id="banderaCC" name="banderaCC" type="hidden" value="1">
	<?php 
		
	}else{
		?>
		<input id="banderaCC" name="banderaCC" type="hidden" value="">
	<?php 
	}
	
	if($estatus==='En Evaluacion' && $observacion==null){//SI NO HAY COMENTARIO?>
		<input id="banderae" name="banderae" type="hidden" value="1">
	<?php 
	 	
	 }else{?>
		<input id="banderae" name="banderae" type="hidden" value="">
	<?php 
	 		
	 }
	?>
		
	
	<form method="post" action="evaluar-imgc.php" id="evaluacionForm" enctype="multipart/form-data">
	 <div class="container">
	    <div class="row">
	        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
	     
				<div class="titulo" style="margin-top:8%">
				<font style="font-size: 17px; font-weight: 300;">
	            <h1 align="center" style="color: #052450;">EVALUACIÓN: IMAGEN CARTEL</h1>
	            </div>
	        </div>
		</div>	 
	</div>	
	<div class="container">
	    <div class="row" id="rubrica">
	        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
	     		<div class="widget" style="margin-top:4%">
					<a class="nav-link active"  href="images/Formato_Evaluacion_Carteles.xls" style="color:#052450; size:18px;"><strong>Rúbrica de Evaluación</strong></a>
	            </div>
	        </div>
		</div>	 
	</div>
    <div class="container" style="margin-top:3%; margin-bottom:3%;">	 
		<div class="row" style="" >
			<div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6" style="" >
					<div class="field">
						<input  name='estatus' id='estatus' type='hidden' value='<?php echo $estatus?>'>
						<strong>
						Información de la imagen<br></strong>
						Autor:<br>
						
						<input type="text" class="form-control" name="autor" id="autor" style="width:80%"; value="<?php echo $autor ?>" readonly="readonly">
						
					</div>
					
					<div class="field">
						Título del trabajo:<br>
						
						
						<input type="" class="form-control" style="width:80%";  name="titulo" id="titulo" value="<?php echo $titulo ?>"  readonly="readonly">
					</div>
					
					<div class="field">
						Clave:<br>
						<input type="text"  class="form-control" style="width:80%;"  name="clave" id="clave" value="<?php echo $clave ?>"  readonly="readonly">
					</div>
		
					<div class="field">
						Imagen:<br>
						<div class="col-12 col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8"> 
							
							<a class="nav-link active" href="imagen-Cartel.php?imagen=<?php echo $imagen ?>" target="_blank" style="color:#052450; size:18px;"><img src="<?php echo $imagen ?>" style="width:100%; height:100%;"/></a>
						</div>
					</div>
			</div>	
			<div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6" style=""> 
				 
					<div class="field" id="label_rubrica">
						<strong>
							Rúbrica:
						</strong>
					</div>
					<div class="field">
						<input type="file" id="archivo" name="archivo" accept=".xlsx, .xls">
					</div>
					<div class="field" id="label_calificacion">
						<strong>
						Calificación Total:
						</strong>
						<input type="text" class="form-control" size="13" name="calificacion" id="calificacion" maxlength="15" minlength="10" placeholder="" onkeypress='return validaNumericos(event)'> 
						<script type="text/javascript">
						document.getElementById("calificacion").addEventListener("input", (e) => {
						let value = e.target.value;
						e.target.value = value.replace(/[^A-Z\d-]/g, "");
						});
						</script>
					</div>
					<div class="field" id="criterios">
						<strong>
						Elija la opción a su criterio:<br>
						</strong>
						<input type="radio" id="rcriterio" name="rcriterio" value="Aceptado">
						<label for="male">Aceptar</label><br>
						<input type="radio" id="rcriterio" name="rcriterio" value="Con Modificaciones">
						<label for="male">Modificar</label><br>
						<input type="radio" id="rcriterio" name="rcriterio" value="Rechazado">
						<label for="male">Rechazar</label><br>
					</div>
					<div class="field" id="comentariodiv">
						<strong>
						Escriba sus comentarios u observaciones: <br>
						</strong>
						<textarea  id="comentario" name="comentario" rows="5" cols="30" required=""></textarea>	
						<div class="field">
						Evaluador:<br>
						<input type="text" name="evaluador" value="<?php echo $nombreEva[0]?>" readonly="readonly">		
						</div>					    
						<input type="hidden" name ="id_ponencia" id="id_ponencia" value="<?php echo $clave?>">
						<input type="hidden" name="idActualizacion" id="idActualizacion" value="<?php echo $idActualizacion?>">
						<input type="hidden" name="estatusIMG" id="estatusIMG"  value="<?php echo $estatus?>">
					</div>	
			</div>
		</div>
		<div class="btn" style="text-align:right; width:100%; margin-bottom: 5%; margin-top: 2%;">

				<input type="submit" value="Evaluar"  id="submit" style=" margin-right:5%; background: #052450; color: white; padding: 6px 10px; border-color: #052450;border-radius: 20px;">
				
				<input type="reset" id="cancelar" value="Cancelar" style="margin-right:5%; background: whitesmoke; color: black; padding: 6px 10px; border-color: whitesmoke;border-radius: 20px;">
				<button type="return" id="Regresar"  onclick="history.go(-1)" style="margin-right:5%; background: whitesmoke; color: black; padding: 6px 10px; border-color: whitesmoke;border-radius: 20px;">Regresar</button>
		</div>
	</form>
	</div>
	
		 
		
	
	

    <!-- End CONTENIDO -->
    
 
    <!-- Start Footer -->
     <!-- Start Footer -->
        <footer class="footer-box">
        <div class="container">
            <div class="row">
               <div class="col-md-12 white_fonts" style="display: contents;">
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            
                            <div class="full">
							<h3>Dirección</h3>
                                <ul class="menu_footer">   
								     
								     <li><img src="images/i5.png"><span style="font-size: 85%;">Carr, Cuautitlán - Teoloyucan, San Sebastian Xhala, 54714 Cuautitlán Izcalli</span></li>

                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="full">
                                <div class="footer_blog full white_fonts">
                             <h3>Contacto</h3>
                             <ul class="full">
                               <li><img src="images/i6.png"><span style="font-size: 85%;"> <?php echo $correoC  ?></span></li>
                               <li><img src="images/i7.png"><span style="font-size: 85%;"><?php echo "$numeroC1    $numeroC2"; ?></span></li>
                             </ul>
                         </div>
                            </div>
                        </div>
                </div>
			 </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link active" href="derechosa.php" style="color:white">© Sitio desarrollado por CDD Solutions S.A. y administrado por el Departamento de Matemáticas. Hecho en México, (2020 - 2021)</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
	/* counter js */

function tipoEvaluacion(){
	var estatus_imagen=document.getElementById('estatus').value;
	var rubrica = document.getElementById("rubrica");
	var label_rubrica=document.getElementById('label_rubrica');
	var archivo=document.getElementById('archivo');
	var label_calificacion=document.getElementById('label_calificacion');
	var calificacion=document.getElementById('calificacion');
	var criterios=document.getElementById('criterios');
	var botonEvaluar=document.getElementById('submit');
	var botonCancelar=document.getElementById('cancelar');
	var botonRegresar=document.getElementById('Regresar');
	var comentariodiv=document.getElementById('comentariodiv');
	var banderaCC=document.getElementById('banderaCC').value;
	var banderae=document.getElementById('banderae').value;
	
	if(estatus_imagen=="Enviado" && banderaCC==="" && banderae===""){
		rubrica.style.display='none';
		label_rubrica.style.display='none';
		archivo.style.display='none';
		label_calificacion.style.display='none';
		calificacion.style.display='none';
		criterios.style.display='inline';
		botonRegresar.style.display='none';

	}else if(estatus_imagen=="Aceptado" && banderaCC!==null && banderae===""){
		if(banderaCC==""){
		rubrica.style.display='inline';
		label_rubrica.style.display='inline';
		archivo.style.display='inline';
		label_calificacion.style.display='inline';
		calificacion.style.display='inline';
		criterios.style.display='none';
		botonRegresar.style.display='none';
	}else if( banderaCC=="1"){	
		alert("Ya evaluó este cartel para el concurso, para mayor información, favor de verificar su correo electrónico.");
		window.location='evaluartr.php';
	}

	}else if(estatus_imagen=="En Evaluacion" && banderaCC===""||estatus_imagen=="Con Modificaciones" && banderaCC===""||estatus_imagen=="Rechazado" && banderaCC==="" && banderae!== null){
		if(banderae==""){
		rubrica.style.display='none';
		label_rubrica.style.display='none';
		archivo.style.display='none';
		label_calificacion.style.display='none';
		calificacion.style.display='none';
		criterios.style.display='none';	
		botonEvaluar.style.display='none';
		botonCancelar.style.display='none';
		botonRegresar.style.display='inline';
		comentariodiv.style.display='none';
		}else if(banderae=="1"){
		rubrica.style.display='none';
		label_rubrica.style.display='none';
		archivo.style.display='none';
		label_calificacion.style.display='none';
		calificacion.style.display='none';
		criterios.style.display='inline';
		botonRegresar.style.display='none';	
		}
	}
}
//*********************FUNCION PARA VALIDAR QUE INGRESE LOS DATOS DEPENDIENDO EL TIPO DE EVALUACION************************
// bindamos al evento submit del elemento formulario la función de validación
document.getElementById("evaluacionForm").addEventListener("submit", function(event){
    let hasError = false;
    var estatus_imagen=document.getElementById('estatus').value;
    var calificacion=document.getElementById('calificacion').value;
    var archivo=document.getElementById('archivo').value;
    // obtenemos todos los input radio del grupo rcriterio que esten chequeados
    // si no hay ninguno lanzamos alerta
    if(estatus_imagen=="Enviado"){
	    if(!document.querySelector('input[name="rcriterio"]:checked')) {
	      alert('Error, rellena el criterio a evaluar');
	      hasError = true;
	      }
	}else if(estatus_imagen=="Aceptado"){
		if( calificacion == null || calificacion.length == 0) {
		      alert('Error, rellena el campo de la calificacion');
		      hasError = true;
		    }
		 if(archivo == null || archivo.length==0){
		 	 alert('Error, selecciona el archivo .xlsx o .xls en el apartado de Rúbrica');
		      hasError = true;
		 }
	}
    
    // si hay algún error no efectuamos la acción submit del form
    if(hasError) event.preventDefault();
});

(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
	</script>
</body>

</html>