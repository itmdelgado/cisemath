<?php 
session_start();
if(empty($_SESSION['Usuario'])){
	header("location:avisologin.php");
	
}
include ("Conexion.php");
$consulta=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
					$row0=pg_fetch_row($consulta);
					$consulta0=$row0[0];
					$consulta1=pg_query($conexion,"SELECT id_congreso,nombre_congreso FROM congreso WHERE numero_congreso='$consulta0'");
					$row=pg_fetch_row($consulta1);
					$congresoActual=$row[0];//es el id congreso
$contacto=pg_query($conexion,"SELECT cn.telefono,cn.telefono2,cn.email FROM contacto cn, congreso c
WHERE c.id_congreso='$congresoActual'
and  c.id_contacto =cn.id_contacto");
$contactos=pg_fetch_array($contacto);
$correoC=@$contactos[2];
$numeroC1=@$contactos[0];
$numeroC2=@$contactos[1];
?><!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title style="background-image: url('images/logos.jpg');">CISEMATH</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/cismath.png">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css" /> 

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
	<?php
    ////Codigo para actualizar el banner
        include ('Conexion.php');
        $nmax=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
        $num=pg_fetch_row($nmax);
        $numcon=$num[0];
        $conact=pg_query($conexion,"SELECT id_congreso, banner FROM congreso WHERE numero_congreso='$numcon'");
        $ban=pg_fetch_row($conact);
        $banner=$ban[1];
        echo '<img src="'.$banner.'" style="width:100%; height:100%; background-size: cover; position: relative; display: flex;"/>';
    ?>

    <header class="top-header" >
        <div class="header_bottom">
          <div class="container">
		  <div class="row"> 
            <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 col-xl-12">
                <div class="menu_orange_section" style="background: #052450;">
                   <nav class="navbar header-nav navbar-expand-lg"> 
                     <div class="menu_section">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav" >
					    <li><a class="nav-link active" href="indexus.php"><img src="images/i4.png" alt="image"></a> </li>
                        <li><a class="nav-link active" href="acercadeus.php">Acerca de</a></li>
                        <li><a class="nav-link active" href="infus.php">Información</a></li>
                        <li><a class="nav-link active" href="instruccionesus.php">Instrucciones</a></li>
                        <li><a class="nav-link active" href="comitesus.php">Comités</a></li>
						<li><a class="nav-link active" href="memoriasus.php">Memorias</a></li>
						
						<li>
						<a style="position:relative; display:block;">
						<img src="images/user.png" alt="image"> 
						</a>
						</li>
						<li>
						<ul class="nav"><li class="dropdown langmenu" ><a href="#" style="display:block;"class="dropdown-toggle" data-toggle="dropdown" title="Idioma"><b class="caret"></b></a><ul class="dropdown-menu" style="color:black;"><li><a title="English ‎(en)‎" href="menu.php" style="color:black";>Menú‎‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="editarp.php"  style="color:black";>Perfil‎</a></li><li><a title="Español - Internacional ‎(es)‎" href="index.php"  style="color:black";>Cerrar Sesión</a></li></ul></li></ul>
						</li>
						
                    </ul>
                </div>
                     </div>
                 </nav>
                 
                </div>
            </div>
          </div>
		  </div>
        </div>
        
    </header>
    <!-- End header -->
 
 <!-- Start CONTENIDO -->
 <div class="container">
		<div class="row">
			<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			 
				<div class="titulo" style="margin-top:8%; margin-bottom:4%;">
					<h1 align="center" style="color:#052450;">TRABAJOS ACEPTADOS</h1>
				</div>
			</div>
		</div>	 
	</div>	
 <div class="container">
		<div class="row" style="display:flex; justify-content:center;   margin-bottom:2%;">
			<div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="table-responsive">
					<!---------------------------------TABLA PONENCIAS ORALES---------------------------------------------->
					<table  border=1 class="table table-bordered" align="center" style="text-align:center; ">
						<tr>
							<font style="font-size:14px; color:#052450;; text-align:left;">PONENCIAS ORALES:</font>
						</tr>
						<tr>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">CLAVE</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">TÍTULO</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">AUTOR</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">TIPO AUTOR</font>
							</td>
						</tr>

						<?php
						include ('Conexion.php');
						    $numerocongreso=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
							$row0=pg_fetch_row($numerocongreso);
							$numeracion=$row0[0];
							$id_congreso=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$numeracion'"); //id del ultimo congreso
							$row=pg_fetch_row($id_congreso);
							$id_congreso_actual=$row[0];

							$consulta_po=pg_query($conexion, "SELECT
							p.id_ponencia, p.id_congreso, p.id_tipo_ponencia, p.estatus_video,
							ar.id_ponencia, ar.id_congreso, ar.titulo, ar.estatus_actualizacion,
							up.id_ponencias, up.id_congreso, up.id_usuario, up.tipo_autor,
							u.nombres, u.primer_ap, u.segundo_ap, u.id_usuario
							FROM ponencias p,
							 usuario u, 
							 actualizacion_resumen ar,
							 usuario_ponencias up
							WHERE p.id_ponencia=ar.id_ponencia
							and ar.id_ponencia=up.id_ponencias
							and p.id_congreso=ar.id_congreso
							and ar.id_congreso=up.id_congreso
							and up.id_congreso='$id_congreso_actual'
							and up.id_usuario=u.id_usuario
							and p.id_tipo_ponencia='PO'
							and p.estatus_video='Aceptado'
							and ar.estatus_actualizacion='Aceptado'
							ORDER BY p.id_ponencia");

							while($mostrar_po=pg_fetch_array($consulta_po)){
						?>

						<tr>
							<td>
								<?php echo $mostrar_po['id_ponencia']; ?>
							</td>
							<td>
								<?php echo $mostrar_po['titulo']; ?>
							</td>
							<td>
								<?php echo $mostrar_po['nombres'];?>
								<?php echo $mostrar_po['primer_ap'];?>
								<?php echo $mostrar_po['segundo_ap'];?> 
							</td>
							<td>
								<?php echo $mostrar_po['tipo_autor']; ?>
							</td>
						</tr>

						<?php
							}
						?>

					</table>

					<!---------------------------------TABLA CARTELES---------------------------------------------->
					<table  border=1 class="table table-bordered" align="center" style="text-align:center; ">
						<tr>
							<font style="font-size:14px; color:#052450;; text-align:left;">CARTELES:</font>
						</tr>
						<tr>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">CLAVE</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">TÍTULO</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">AUTOR</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">TIPO AUTOR</font>
							</td>
						</tr>

						<?php
						include ('Conexion.php');
						    $numerocongreso=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
							$row0=pg_fetch_row($numerocongreso);
							$numeracion=$row0[0];
							$id_congreso=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$numeracion'"); //id del ultimo congreso
							$row=pg_fetch_row($id_congreso);
							$id_congreso_actual=$row[0];

							$consulta_po=pg_query($conexion, "SELECT
							p.id_ponencia, p.id_congreso, p.id_tipo_ponencia, p.estatus_video,
							ar.id_ponencia, ar.id_congreso, ar.titulo, ar.estatus_actualizacion,
							up.id_ponencias, up.id_congreso, up.id_usuario, up.tipo_autor,
							u.nombres, u.primer_ap, u.segundo_ap, u.id_usuario
							FROM ponencias p,
							 usuario u, 
							 actualizacion_resumen ar,
							 usuario_ponencias up
							WHERE p.id_ponencia=ar.id_ponencia
							and ar.id_ponencia=up.id_ponencias
							and p.id_congreso=ar.id_congreso
							and ar.id_congreso=up.id_congreso
							and up.id_congreso='$id_congreso_actual'
							and up.id_usuario=u.id_usuario
							and p.id_tipo_ponencia='CA'
							and p.estatus_video='Aceptado'
							and ar.estatus_actualizacion='Aceptado'
							ORDER BY p.id_ponencia");

							while($mostrar_po=pg_fetch_array($consulta_po)){
						?>

						<tr>
							<td>
								<?php echo $mostrar_po['id_ponencia']; ?>
							</td>
							<td>
								<?php echo $mostrar_po['titulo']; ?>
							</td>
							<td>
								<?php echo $mostrar_po['nombres']?>
								<?php echo $mostrar_po['primer_ap']?>
								<?php echo $mostrar_po['segundo_ap']?> 
							</td>
							<td>
								<?php echo $mostrar_po['tipo_autor']; ?>
							</td>
						</tr>

						<?php
							}
						?>

					</table>

					<!---------------------------------TABLA TALLERES---------------------------------------------->
					<table  border=1 class="table table-bordered" align="center" style="text-align:center; ">
						<tr>
							<font style="font-size:14px; color:#052450;; text-align:left;">TALLERES:</font>
						</tr>
						<tr>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">CLAVE</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">TÍTULO</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">AUTOR</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">TIPO AUTOR</font>
							</td>
						</tr>

						<?php
						include ('Conexion.php');
						    $numerocongreso=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
							$row0=pg_fetch_row($numerocongreso);
							$numeracion=$row0[0];
							$id_congreso=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$numeracion'"); //id del ultimo congreso
							$row=pg_fetch_row($id_congreso);
							$id_congreso_actual=$row[0];

							$consulta_po=pg_query($conexion, "SELECT
							p.id_ponencia, p.id_congreso, p.id_tipo_ponencia, p.estatus_video,
							ar.id_ponencia, ar.id_congreso, ar.titulo, ar.estatus_actualizacion,
							up.id_ponencias, up.id_congreso, up.id_usuario, up.tipo_autor,
							u.nombres, u.primer_ap, u.segundo_ap, u.id_usuario
							FROM ponencias p,
							 usuario u, 
							 actualizacion_resumen ar,
							 usuario_ponencias up
							WHERE p.id_ponencia=ar.id_ponencia
							and ar.id_ponencia=up.id_ponencias
							and p.id_congreso=ar.id_congreso
							and ar.id_congreso=up.id_congreso
							and up.id_congreso='$id_congreso_actual'
							and up.id_usuario=u.id_usuario
							and p.id_tipo_ponencia='TA'
							and p.estatus_video='Aceptado'
							and ar.estatus_actualizacion='Aceptado'
							ORDER BY p.id_ponencia");

							while($mostrar_po=pg_fetch_array($consulta_po)){
						?>

						<tr>
							<td>
								<?php echo $mostrar_po['id_ponencia']; ?>
							</td>
							<td>
								<?php echo $mostrar_po['titulo']; ?>
							</td>
							<td>
								<?php echo $mostrar_po['nombres']?>
								<?php echo $mostrar_po['primer_ap']?>
								<?php echo $mostrar_po['segundo_ap']?> 
							</td>
							<td>
								<?php echo $mostrar_po['tipo_autor']; ?>
							</td>
						</tr>

						<?php
							}
						?>

					</table>

					<!---------------------------------TABLA PROTOTIPOS---------------------------------------------->
					<table  border=1 class="table table-bordered" align="center" style="text-align:center; ">
						<tr>
							<font style="font-size:14px; color:#052450;; text-align:left;">PROTOTIPOS:</font>
						</tr>
						<tr>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">CLAVE</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">TÍTULO</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">AUTOR</font>
							</td>
							<td>
								<font style="font-size:14px; color:#052450;; text-align:left;">TIPO AUTOR</font>
							</td>
						</tr>

						<?php
							include ('Conexion.php');

							$numerocongreso=pg_query($conexion,"SELECT MAX(numero_congreso) as id FROM congreso");//numero actual de congreso
							$row0=pg_fetch_row($numerocongreso);
							$numero=$row0[0];
							$id_congreso_actual=pg_query($conexion,"SELECT id_congreso FROM congreso WHERE numero_congreso='$numero'"); //id del ultimo congreso
							$row=pg_fetch_row($id_congreso_actual);
							$id_congreso=$row[0];

							$consulta_pro_r=pg_query($conexion, "SELECT
							pr.id_prototipo, pr.nombre, pr.id_congreso, pr.estatus_prototipos,
							pru.id_prototipo, pru.id_usuario, pru.hora_presentacion, pru.fecha_presentacion, pru.id_sala, pru.id_congreso, pru.tipo_autor,
							arp.id_actualiza_r_p, arp.id_prototipo, arp.estatus_r_p, arp.id_congreso, arp.titulo,
							u.id_usuario, u.nombres, u.primer_ap, u.segundo_ap
							FROM prototipos pr, prototipos_usuario pru, actualizacion_resumen_pro arp, usuario u
							WHERE pr.id_prototipo=pru.id_prototipo
							and pru.id_prototipo=arp.id_prototipo
							and pr.id_congreso=pru.id_congreso
							and pru.id_congreso=arp.id_congreso
							and arp.id_congreso='$id_congreso'
							and pru.id_usuario=u.id_usuario
							and arp.estatus_r_p='Aceptado'
							ORDER BY arp.id_prototipo asc");

							while($mostrar_pro=pg_fetch_array($consulta_pro_r)){
						?>

						<tr>
							<td>
								<?php echo $mostrar_pro['id_prototipo']; ?>
							</td>
							<td>
								<?php echo $mostrar_pro['titulo']; ?>
							</td>
							<td>
								<?php echo $mostrar_pro['nombres']?>
								<?php echo $mostrar_pro['primer_ap']?>
								<?php echo $mostrar_pro['segundo_ap']?> 
							</td>
							<td>
								<?php echo $mostrar_pro['tipo_autor']; ?>
							</td>
						</tr>

						<?php
							}
						?>

					</table>
				</div>
			</div>
			
		</div>
			
	</div>
	
	<div class="btn" style="text-align:right; width:100%;  margin-top:2%; margin-bottom:6%;">
		<button type="B1" onclick="location.href='menucomite.php'"style="margin-right:5%; background: whitesmoke; color: black; padding: 6px 10px; border-color: whitesmoke;border-radius: 20px;">Regresar</button>
	</div>
	
    <!-- End CONTENIDO -->
    
  <!-- Start Footer -->
        <footer class="footer-box">
        <div class="container">
            <div class="row">
               <div class="col-md-12 white_fonts" style="display: contents;">
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            
                            <div class="full">
							<h3>Dirección</h3>
                                <ul class="menu_footer">   
								     
								     <li><img src="images/i5.png"><span style="font-size: 85%;">Carr, Cuautitlán - Teoloyucan, San Sebastian Xhala, 54714 Cuautitlán Izcalli</span></li>

                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="full">
                                <div class="footer_blog full white_fonts">
                             <h3>Contacto</h3>
                             <ul class="full">
                               <li><img src="images/i6.png"><span style="font-size: 85%;"> <?php echo $correoC  ?></span></li>
                               <li><img src="images/i7.png"><span style="font-size: 85%;"><?php echo "$numeroC1    $numeroC2"; ?></span></li>
                             </ul>
                         </div>
                            </div>
                        </div>
                </div>
			 </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a class="nav-link active" href="derechosa.php" style="color:white">© Sitio desarrollado por CDD Solutions S.A. y administrado por el Departamento de Matemáticas. Hecho en México, (2020 - 2021)</a>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
	<script>
	/* counter js */

(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});
	</script>
</body>

</html>